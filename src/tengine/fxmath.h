/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#ifndef __FXMATH_H__
#define __FXMATH_H__

#include "platform.h"

#ifdef __cplusplus
extern "C" {
#endif

//---------------------------------------------------------------------------

#define FX_PI FX32(3.141592653)

#define FXMAX 0x7fffffff

#ifndef NITRO_SDK
 #define FX_CORDIC_LOW_PRECISION 7
 #define FX_CORDIC_MIDDLE_PRECISION 8
 #define FX_CORDIC_HIGH_PRECISION 10
#endif

//---------------------------------------------------------------------------

struct fxVec2
{
	fx32 x;
	fx32 y;
};

#ifndef NITRO_SDK

typedef union
{
 struct
 {
		struct fxVec2 col1;
		struct fxVec2 col2;
	}c;
 struct
 {
	fx32    _00, _01;
	fx32    _10, _11;
 }mt;
 fx32    m[2][2];
 fx32    a[4];
}
fxMat22;

#endif

//---------------------------------------------------------------------------

void mthInitRandom(void);

u32 mthGetRandom(u32 iMax);

//---------------------------------------------------------------------------

fx32 fxSin(fx32 angle_rad);

fx32 fxCos(fx32 angle_rad);

fx32 fxAsin(fx32 val_rad);

fx32 fxAcos(fx32 val_rad);

fx32 fxAtan(fx32 val_rad);

void fxCordic(fx32 theta_rad, fx32 *osin, fx32 *ocos, s32 num_iterations);

//---------------------------------------------------------------------------

u32 mthSqrtI(u32 val);

fx32 fxSqrt(fx32 val);

//---------------------------------------------------------------------------

#ifndef NITRO_SDK

#define FX_Mul(x, y) ((fx32)(((s64)(x) * (y)) >> FX32_SHIFT))

#define FX_Div(x, y) ((fx32)(((s64)(x) * FX32_ONE) / (y)))

s32 MATH_ABS(s32 val);

#endif

static inline fx32 int2fx(s32 fcx)
{
	return (fcx << FX32_SHIFT);
}

static inline s32 fx2int(fx32 fcx)
{
	return (fcx >> FX32_SHIFT);
}

static inline fx32 fxMul(fx32 x, fx32 y)
{
	return (fx32)(((s64)(x) * (y)) >> FX32_SHIFT);
}

static inline fx32 fxDiv(fx32 x, fx32 y)
{
	return (fx32)(((s64)(x) * FX32_ONE) / (y));
}

static inline fx32 fxAbs(fx32 fcx)
{ 
	return (fcx + (fcx >> 31)) ^ (fcx >> 31);
}

static inline fx32 fxSign(fx32 x)
{
	return (1 | (x >> 31)) << FX32_SHIFT;  
}

static inline fx32 fxMod(fx32 fcx)
{
	return (fxAbs(fcx) & 0x0000ffff) * (1 | (fcx >> 31));
}

static inline fx32 fxFloor(fx32 fcx)
{
	return (fxAbs(fcx) & 0xffff0000) * (1 | (fcx >> 31));
}

static inline fx32 fxFloorHalf(fx32 fcx)
{
	if((fcx & 0xffff) >= 0x7fff)
	{
		return fxFloor(fcx) + 0x00010000;
	}
	else
	{
		return fxFloor(fcx);
	}	
}

static inline fx32 fxMin(fx32 v1, fx32 v2)
{
    return v2 ^ ((v1 ^ v2) & -(v1 < v2));
}

static inline fx32 fxMax(fx32 v1, fx32 v2)
{
    return v1 ^ ((v1 ^ v2) & -(v1 < v2));
}

static inline fx32 fxClamp(fx32 a, fx32 low, fx32 high)
{
	return fxMax(low, fxMin(a, high));
}

fx32 fxAtan2(fx32 inY, fx32 inX);

static inline struct fxVec2 fxVec2Create(fx32 x, fx32 y)
{
    struct fxVec2 ret;
    ret.x = x;
    ret.y = y;
    return ret;
}

//---------------------------------------------------------------------------

fxMat22 fxMat22FromAngle(fx32 angle);

fxMat22 fxMat22FromVecs(struct fxVec2* col1, struct fxVec2* col2);

fxMat22 fxMat22Transpose(fxMat22* mat22);

fxMat22 fxMat22Invert(fxMat22* mat22);

struct fxVec2 fxMat22MulVec(fxMat22* mat, struct fxVec2* vec);

fxMat22 fxMat22Abs(fxMat22 * mat);

//---------------------------------------------------------------------------

struct fxVec2 fxVec2Abs(struct fxVec2 vec);

struct fxVec2 fxVec2Add(struct fxVec2 const v1, struct fxVec2 const v2);

struct fxVec2 fxVec2Sub(struct fxVec2 const v1, struct fxVec2 const v2);

struct fxVec2 fxVec2Mul(struct fxVec2 const v1, struct fxVec2 const v2);

struct fxVec2 fxVec2Div(struct fxVec2 const v1, struct fxVec2 const v2);

fx32 fxVec2Length(struct fxVec2 const v1);

struct fxVec2 fxVec2Normalize(struct fxVec2 const v1);

struct fxVec2 fxVec2MulFx(struct fxVec2 const v1, fx32 const val);

struct fxVec2 fxVec2DivFx(struct fxVec2 const v1, fx32 const val);

fx32 fxVec2Dot(struct fxVec2 const v1, struct fxVec2 const v2);

fx32 fxVec2Cross(struct fxVec2 const v1, struct fxVec2 const v2);

struct fxVec2 fxVec2CrossFx(struct fxVec2 const v1, fx32 const s);

struct fxVec2 fxVec2CrossFxInv(fx32 const s, struct fxVec2 const v1);

static inline BOOL fxVecInRect(const struct fxVec2* point, const struct fxVec2* rectPos, const struct fxVec2* rectSize)
{
	if( (point->x > rectPos->x) &&
			(point->y > rectPos->y) &&
			((point->x - rectPos->x) < rectSize->x ) &&
			((point->y - rectPos->y) < rectSize->y ) )
    {
        return TRUE;
    }
	return FALSE;
}

static inline struct fxVec2 fxVec2Inv(struct fxVec2 vec)
{
	return fxVec2Sub( fxVec2Create(FX32(0.0f), FX32(0.0f)), vec);
}

//---------------------------------------------------------------------------

fxMat22 fxMat22Add(fxMat22* mat1, fxMat22* mat2);
fxMat22 fxMat22Mul(fxMat22* mat1, fxMat22* mat2);


//---------------------------------------------------------------------------

BOOL mthIntersectPointAndRectangle(struct fxVec2 point, struct fxVec2 rect[4]);

//---- Check intersecting Polygon with Polygon by Projection alg. ( with returning Penetration vector )
BOOL mthIntersect(struct fxVec2* a, s32 aSize, struct fxVec2* b, s32 bSize, struct fxVec2 const offset, struct fxVec2 * shift);

//---- Check intersecting Rect with Rect by Projection alg. ( with returning Penetration vector )
static inline BOOL mthIntersectShiftRR(struct fxVec2 a[4], struct fxVec2 b[4], struct fxVec2 const offset, struct fxVec2 * shift)
{
    return mthIntersect(a, 4, b, 4, offset, shift);
}

//---- Check intersecting Rect with Line by Projection alg. ( with returning Penetration vector )
static inline BOOL mthIntersectShiftRL(struct fxVec2 a[4], struct fxVec2 b[2], struct fxVec2 const offset, struct fxVec2 * shift)
{
    return mthIntersect(a, 4, b, 2, offset, shift);
}

//---- Check intersecting Line with Line by Projection alg. ( with returning Penetration vector )
static inline BOOL mthIntersectShiftLL(struct fxVec2 a[2], struct fxVec2 b[2], struct fxVec2 const offset, struct fxVec2 * shift)
{
    return mthIntersect(a, 2, b, 2, offset, shift);
}

//---- Check intersecting Rect with Rect by Projection alg.
static inline BOOL mthIntersectRR(struct fxVec2 a[4], struct fxVec2 b[4])
{
    return mthIntersect(a, 4, b, 4, fxVec2Create(0, 0), NULL);
}

//---- Check intersecting Rect with Line by Projection alg.
static inline BOOL mthIntersectRL(struct fxVec2 a[4], struct fxVec2 b[2])
{
    return mthIntersect(a, 4, b, 2, fxVec2Create(0, 0), NULL);
}

//---- Check intersecting Line with Line by Projection alg.
static inline BOOL mthIntersectLL(struct fxVec2 a[2], struct fxVec2 b[2])
{
    return mthIntersect(a, 2, b, 2, fxVec2Create(0, 0), NULL);
}


//---- Check intersecting Line with Line by HalfPlane alg.
BOOL mthIntersectLLv2(struct fxVec2 a[2], struct fxVec2 b[2]);

//---- Check intersecting Point with Rect by Projection alg.
BOOL mthIntersectPR(struct fxVec2 a[4], s32 size, struct fxVec2 b);

//---- Check intersecting Point with Rect by Ray alg.
BOOL mthIntersectPRv2(struct fxVec2 a[4], struct fxVec2 b);

//---- Check intersecting Point with Rect by Projection alg. v2
BOOL mthIntersectPRv3(struct fxVec2 a[4], struct fxVec2 b);

//---------------------------------------------------------------------------

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
