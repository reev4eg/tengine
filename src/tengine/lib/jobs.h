#ifndef JOBS_H
#define JOBS_H

#include "platform.h"

#ifdef __cplusplus
extern "C" {
#endif

struct JOBAnimStreamTask;
struct JOBFileStreamTask;

void jobInit(void);
void jobRelease(void);
void jobResetTasks(void);
void jobSetActive(BOOL val);
BOOL jobIsActive(void);

void jobAddStreamVideoTask(struct JOBAnimStreamTask *data);
void jobRemoveStreamVideoTask(const struct JOBAnimStreamTask *data);

void jobAddStreamFileTask(struct JOBFileStreamTask *data);
void jobRemoveStreamFileTask(const struct JOBFileStreamTask *data);

#ifdef SDK_DEBUG
void jobResetTextureCounter(void);
s32 jobGetTextureCounter(void);
#endif

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif

