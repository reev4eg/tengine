//-------------------------------------------------------------------------------------------
/*

	This example is based on code
	https://github.com/subprotocol/verlet-js
	demo:
	https://github.com/subprotocol/verlet-js/blob/master/examples/spiderweb.html
	http://subprotocol.com/verlet-js/examples/spiderweb.html

*/
//-------------------------------------------------------------------------------------------



#include "tengine.h"
#include "texts.h"
#include "touchpad.h"
#include "gamepad.h"
#include "gamefield.h"
#include "level_id.h"
#include "sound.h"

#include "constants.h"

#include "verlet.h"

//#define DEMO_SOUND

//-------------------------------------------------------------------------------------------

enum KeyCodes
{
    UP = 1,
    DOWN,
    RIGHT,
    LEFT,
    FIRE,
    SELECT,
    START,
    ANYKEY
};

enum GameStates
{
	gstNONE = 0,
	gstDISPLAYINIT,
	gstLOADING,
	gstGAME
};

 static s32 gameState = gstNONE;
 static fx32 hero_x;
 static fx32 hero_y;
 static s32 key_action;
 static u32 c_c;
 static struct fxVec2 camera_pos;

 static fx32 direction_power[4];

 static s32 fps_timer = 0;
 static s32 fps_tick = 0;

 static s32 resizeW = 0;
 static s32 resizeH = 0;

 static TouchPadData tp_data;

 static s32 spiderweb_id = -1;

 static struct Verlet gsVerlet = {0};

 enum ControlDirections
 {
	CD_UP = 0,
	CD_DOWN,
	CD_LEFT,
	CD_RIGHT
 };

 enum LoadMarkers
 {
	LM_INIT = 0,
	LM_LOAD
 };

 enum GameLayers
 {
	GAME_LAYER = 0,
	HUD_LAYER = 1,
	LAYER_COUNT
 };
 
//-------------------------------------------------------------------------------------------

static void onEngineEvent(const struct EventCallbackData *pData);

static void keyProcessing(void);

static void onUpdateObject(u32 layer, s32 iId, BOOL* const oDraw);
static void onUpdateBackgroundTiles(u32 layer);
static void onVrltRenderCallback(const struct VTCallbackData *iData);

static void releaseTengineData(void);

static void load(void);

static void game_step(s32 ms);

static void doResize(void);

static void onLevelMapLoad(void);
static void onLevelHudLoad(void);

static s32 spiderweb(struct fxVec2 origin, fx32 radius, s32 segments, s32 depth);

//-------------------------------------------------------------------------------------------

void tfgInitMemory()
{
	InitMemoryAllocator();
}
//-------------------------------------------------------------------------------------------

void tfgInit()
{
    InitGamePad();
	InitTouchPad();
#ifdef DEMO_SOUND
	sndInitSoundSystem();
#endif
	gameState = gstNONE;
}
//-------------------------------------------------------------------------------------------

void tfgRelease()
{
	releaseTengineData();
	sndReleaseSoundSystem();
}
//-------------------------------------------------------------------------------------------

void releaseTengineData()
{
	// to avoid memory fragmentation, releaseMapData in opposite side
	releaseResources();
	vrltRelease(&gsVerlet);
	releaseMapData(HUD_LAYER);
	releaseMapData(GAME_LAYER);
	releaseTextData();
	releaseCommonData();
	releaseRenderPlane(BGSELECT_SUB3);
	releaseEngine();
}
//-------------------------------------------------------------------------------------------

void tfgResize(s32 w, s32 h)
{
	gameState = gstDISPLAYINIT;
	c_c = 0;
	resizeW = w;
	resizeH = h;
}
//-------------------------------------------------------------------------------------------

void doResize()
{
	struct RenderPlaneInitParams mp1;
	mp1.mBGType = BGSELECT_SUB3;
	mp1.mSizes.mViewWidth = resizeW; 
	mp1.mSizes.mViewHeight = resizeH;
	mp1.mMaxRenderObjectsOnPlane = 512;
	mp1.mX = 0;
	mp1.mY = 0;

	if(!isRenderPlaneInit(GAME_LAYER)) // any active plane, whatever
	{
		struct InitEngineParameters initparams;
		releaseTengineData();
		
		initparams.layersCount = LAYER_COUNT;
		initparams.particlesPoolSize = 0;

		initEngine(&initparams);
		initRenderPlane(&mp1);
		
		assignLayerWithRenderPlane(GAME_LAYER, BGSELECT_SUB3, TRUE);
		assignLayerWithRenderPlane(HUD_LAYER, BGSELECT_SUB3, FALSE);

        //register callbacks
		setOnEvent(onEngineEvent);
		setOnUpdateGameObject(onUpdateObject);
		setOnUpdateBackgroundTiles(onUpdateBackgroundTiles);

		//begin load task
		//parameter will return with iData.initiatorId in onEngineEvent() function ( EVENT_TYPE_ON_END_ASYNH_LOAD_DATA)
		beginLoadListAsynh(LM_INIT);
		{
			//load common data
			addToLoadListCommonData();
			//load text data for certain language
			// to avoid memory fragmentation load it once after common data
			addToLoadListLanguageData(LANGUAGE_ENG);
		}
		//end load task
		endLoadListAsynh(FALSE);
	
		camera_pos.x = hero_x = 0;
		camera_pos.y = hero_y = 0;
		key_action = 0;
		c_c = 0;

		//wait for  EVENT_TYPE_ON_END_ASYNH_LOAD_DATA in onEngineEvent() function
		gameState = gstNONE;
	}
	else
	{
		resizeRenderPlane(GAME_LAYER, &mp1.mSizes);
		gameState = gstGAME;
	}
}
//-------------------------------------------------------------------------------------------

void load()
{
#ifdef NITRO_SDK
    GX_SetVisiblePlane(GX_PLANEMASK_NONE);
#endif
	
	// to avoid memory fragmentation, releaseMapData in opposite side

	releaseResources();

	vrltRelease(&gsVerlet);

	releaseMapData(HUD_LAYER);
	releaseMapData(GAME_LAYER);

	OS_Printf("mem on newGame = %u\n", GetFreeMemorySize());

	beginLoadListAsynh(LM_LOAD);
	{
		//layer LAYER_GAME, load current level
		addToLoadListMap(GAME_LAYER, 0);
		//layer LAYER_HUD, load or reload hud
		addToLoadListMap(HUD_LAYER, 1);
	}
	endLoadListAsynh(TRUE);
	
	//wait for  EVENT_TYPE_ON_END_ASYNH_LOAD_DATA and EVENT_TYPE_ON_END_ASYNH_LOAD_RESOURSES in onEngineEvent() function
	gameState = gstNONE;
}
//-------------------------------------------------------------------------------------------

// file index 1, layer GAME_LAYER
void onLevelMapLoad()
{
	struct fxVec2 pos;
	
	c_c = 0;

	resetParticles();

	// only on onLevelMapLoad (called from onEngineEvent function)
	// you can work with M0_* objects, because current active layer is GAME_LAYER 
	pos = prGetPosition(M0_SPIDER_BODY_4); // constants.h (gererated by MapEditor3)
	hero_x = camera_pos.x = 0;//pos.x;
	hero_y = camera_pos.y = 0;//pos.y;
	direction_power[CD_UP] = 0;
	direction_power[CD_DOWN]= 0;
	direction_power[CD_LEFT]= 0;
	direction_power[CD_RIGHT]= 0;

	{
		struct VerletInitParams vp;
		struct VerletObjectInitParams carr[2];
		
		//spiderweb
		carr[0].mConstraintsMax = 284;
		carr[0].mParticlesMax = 140;
		carr[0].mFriction = FX32(0.99f);
		carr[0].mGravity = fxVec2Create(FX32(0), FX32(0.2f));
		carr[0].mGroundFriction = FX32(0.8f);

		carr[1].mConstraintsMax = 7 * 3;
		carr[1].mParticlesMax = 7 + 1;
		carr[1].mFriction = FX32(1.0f);
		carr[1].mGravity = fxVec2Create(FX32(0), FX32(0.2f));
		carr[1].mGroundFriction = FX32(0.8f);

		vp.mObjectsCount = 2;
		vp.mConstraintsPoolMax = 1428;
		vp.mParticlesPoolMax = 1448;
		vp.mHeight = (u32)resizeH;
		vp.mWidth = (u32)resizeW;
		vp.mppObjectInitArr = carr;

		vrltInit(&gsVerlet, &vp);

		{
			s32 size = (resizeW > resizeH ? resizeH : resizeW) / 2;
			spiderweb_id = spiderweb(fxVec2Create(FX32(resizeW / 2), FX32(resizeH / 2)), FX32(size), 20, 7);
		}
	}
}
//-------------------------------------------------------------------------------------------

s32 spiderweb(struct fxVec2 origin, fx32 radius, s32 segments, s32 depth)
{
	s32 i, obj_id;
	fx32 distance;
	const struct fxVec2 *pos1;
	const struct fxVec2 *pos2;
	const fx32 stiffness = FX32(0.6f);
	const fx32 tensor = FX32(0.3f);
	const fx32 stride = (2 * FX_PI) / segments;
	const s32 n = segments * depth;
	const fx32 radiusStride = radius / n;

	obj_id = vrltBeginInitObject(&gsVerlet);
	SDK_ASSERT(obj_id >= 0);

	for (i = 0; i < n ; i++)
	{
		struct fxVec2 pos;
		fx32 theta, shrinkingRadius, offy;
		theta = i * stride + fxMul(fxCos(i * FX32(0.4f)), FX32(0.05f)) + fxMul(fxCos(i * FX32(0.05f)), FX32(0.2f));
		shrinkingRadius = radius - radiusStride * i + fxCos(i * FX32(0.1f)) * 20;
		offy = fxMul(fxCos(fxMul(theta, FX32(2.1f))), fxMul((radius / depth), FX32(0.2f)));
		pos.x = origin.x + fxMul(fxCos(theta), shrinkingRadius);
		pos.y = origin.y + fxMul(fxSin(theta), shrinkingRadius) + offy;
		vrltInitObject_AddParticle(&gsVerlet, obj_id, pos);
	}

	for (i = 0; i < segments; i += 4)
	{
		vrltInitObject_AddPinConstraint(&gsVerlet, obj_id, i, NULL);
	}

	for (i = 0; i< n - 1; i++)
	{
		s32 off;
		// neighbor
		pos1 = vrltGetObjectParticlePosition(&gsVerlet, obj_id, i);
		pos2 = vrltGetObjectParticlePosition(&gsVerlet, obj_id, i + 1);
		distance = fxMul(fxVec2Length(fxVec2Sub(*pos1, *pos2)), tensor);
		vrltInitObject_AddDistanceConstraint(&gsVerlet, obj_id, i, i + 1, stiffness, &distance);
		// span rings
		off = i + segments;
		if (off < n - 1)
		{
			pos1 = vrltGetObjectParticlePosition(&gsVerlet, obj_id, i);
			pos2 = vrltGetObjectParticlePosition(&gsVerlet, obj_id, off);
			distance = fxMul(fxVec2Length(fxVec2Sub(*pos1, *pos2)), tensor);
			vrltInitObject_AddDistanceConstraint(&gsVerlet, obj_id, i, off, stiffness, &distance);
		}
		else
		{
			pos1 = vrltGetObjectParticlePosition(&gsVerlet, obj_id, i);
			pos2 = vrltGetObjectParticlePosition(&gsVerlet, obj_id, n - 1);
			distance = fxMul(fxVec2Length(fxVec2Sub(*pos1, *pos2)), tensor);
			vrltInitObject_AddDistanceConstraint(&gsVerlet, obj_id, i, n - 1, stiffness, &distance);
		}
	}

	pos1 = vrltGetObjectParticlePosition(&gsVerlet, obj_id, 0);
	pos2 = vrltGetObjectParticlePosition(&gsVerlet, obj_id, segments - 1);
	distance = fxMul(fxVec2Length(fxVec2Sub(*pos1, *pos2)), tensor);
	vrltInitObject_AddDistanceConstraint(&gsVerlet, obj_id, 0, segments - 1, stiffness, &distance);
	
	vrltEndInitObject(&gsVerlet, obj_id);
	return obj_id;
}
//-------------------------------------------------------------------------------------------

void onLevelHudLoad()
{
}
//------------------------------------------------------------------------------------

void tfgLostRenderDevice()
{
}
//-------------------------------------------------------------------------------------------

void tfgRestoreRenderDevice()
{
}
//------------------------------------------------------------------------------------

void tfgLowMemory()
{
}
//------------------------------------------------------------------------------------

void tfgTick(s32 ms)
{
	UpdateGamePad();
	keyProcessing();
	
	updateEngine(ms);

	switch(gameState)
	{
		case gstNONE:
		break;

		case gstDISPLAYINIT:
			doResize();
		break;

		case gstLOADING:
			load();
		break;

		case gstGAME:
			game_step(ms);
	}
}
//-------------------------------------------------------------------------------------------

void keyProcessing()
{
    key_action = 0;

	ReadTouchPadData(&tp_data);

    if(IsButtonDown(TYPE_START))
    {
        key_action = START;
		gameState = gstLOADING;
    }
}
//----------------------------------------------------------------------------------

void game_step(s32 ms)
{	
	const static fx32 MAX_SPEED = FX32(60);

	fps_timer += ms;

	if(direction_power[CD_UP] > MAX_SPEED)
	{
		direction_power[CD_UP] = MAX_SPEED;
	}
	if(direction_power[CD_DOWN] > MAX_SPEED)
	{
		direction_power[CD_DOWN] = MAX_SPEED;
	}
	if(direction_power[CD_LEFT] > MAX_SPEED)
	{
		direction_power[CD_LEFT] = MAX_SPEED;
	}
	if(direction_power[CD_RIGHT] > MAX_SPEED)
	{
		direction_power[CD_RIGHT] = MAX_SPEED;
	}

	direction_power[CD_UP] -= FX32(0.5f);
	direction_power[CD_DOWN] -= FX32(0.5f);
	direction_power[CD_LEFT] -= FX32(0.5f);
	direction_power[CD_RIGHT] -= FX32(0.5f);
	if(direction_power[CD_UP] < 0)
	{
		direction_power[CD_UP] = 0;
	}
	if(direction_power[CD_DOWN] < 0)
	{
		direction_power[CD_DOWN] = 0;
	}
	if(direction_power[CD_LEFT] < 0)
	{
		direction_power[CD_LEFT] = 0;
	}
	if(direction_power[CD_RIGHT] < 0)
	{
		direction_power[CD_RIGHT] = 0;
	}
	
	vrltUpdate(&gsVerlet, /*ms*/16);
}
//----------------------------------------------------------------------------------

void onUpdateBackgroundTiles(u32 layer)
{
	//set camera here!
	
	switch(layer)
	{
		case GAME_LAYER:
			setCamera(camera_pos);
		break;
	}
}
//----------------------------------------------------------------------------------

void onVrltRenderCallback(const struct VTCallbackData *iData)
{
	switch(iData->mType)
	{
		case VTRT_PARTICLE:
		{
			s32 x, y;
			setColor(GX_RGBA(31,0,0,1));
			x = iData->mpPoint[0]->x; 
			y = iData->mpPoint[0]->y;
			fillRect(x - FX32(2), y - FX32(2), FX32(4), FX32(4));
		}
		break;
		case VTRT_CONSTRAINT:
		{
			if(iData->mPointCount == 2)
			{
				s32 x1, y1, x2, y2;
				setColor(GX_RGBA(0,0,31,1));
				x1 = iData->mpPoint[0]->x; 
				y1 = iData->mpPoint[0]->y;
				x2 = iData->mpPoint[1]->x; 
				y2 = iData->mpPoint[1]->y;
				drawLine(x1, y1, x2, y2);
			}
		}
	}
}
//----------------------------------------------------------------------------------

void onUpdateObject(u32 layer, s32 iId, BOOL* const oDraw)
{
	(void)oDraw;
	switch(layer)
	{
		case GAME_LAYER:
			switch(iId)
			{
				case M0_SPIDER_BODY_4:				
					
					vrltRender(&gsVerlet, 0, onVrltRenderCallback);

				break;
			
				case M0_TXTBOX_6:
				{
					wchar aaa[16];
					wchar bbb[16];
					bbb[0] = L'x';
					bbb[1] = L'=';
					bbb[2] = L'%';
					bbb[3] = L'd';
					bbb[4] = L'\0';
					STD_WSprintf(aaa, bbb, tp_data.point[0].mX);
					txbSetDynamicTextLine(M0_TXTBOX_6, 0, aaa);
					bbb[0] = L'y';
					STD_WSprintf(aaa, bbb, tp_data.point[0].mY);
					txbSetDynamicTextLine(M0_TXTBOX_6, 1, aaa);
					txbSetDynamicTextLine(M0_TXTBOX_6, 2, getStaticText(TXT_STRING1, 0));
					txbSetDynamicTextLine(M0_TXTBOX_6, 3, getStaticText(TXT_STRING2, 0));
				}
			}
		break;
		
		case HUD_LAYER:
		{
			switch(iId)
			{
				case M1_TXTBOX_FPS:
				{
					fps_tick++;
					if(fps_timer >= 1000)
					{
						wchar aaa[16];
						wchar bbb[16];
						bbb[0] = L'f';
						bbb[1] = L'p';
						bbb[2] = L's';
						bbb[3] = L'=';
						bbb[4] = L'%';
						bbb[5] = L'd';
						bbb[6] = L'\0';
						STD_WSprintf(aaa, bbb, fps_tick);
						txbSetDynamicTextLine(M1_TXTBOX_FPS, 0, aaa);
						fps_tick = fps_timer = 0;
					}		
				}
			}
		}
	}
}
//----------------------------------------------------------------------------------

void onEngineEvent(const struct EventCallbackData *pData)
{
	switch(pData->eventType)
	{
		case  EVENT_TYPE_ON_END_ASYNH_LOAD_DATA:
			switch(pData->eventId)
			{
				case LOAD_TYPE_ENDLOADDATATASK:
					if(pData->initiatorId == LM_INIT)
					{
						gameState = gstLOADING;
					}
				break;
				case LOAD_TYPE_MAPDATA:
					if(pData->initiatorId == LM_LOAD)
					{
						switch(pData->layer)
						{
							case HUD_LAYER:
								onLevelHudLoad();
							break;
							case GAME_LAYER:
								onLevelMapLoad();
						}
					}
			}
		break;
		
		case EVENT_TYPE_ON_END_ASYNH_LOAD_RESOURSES:
			setVisible(HUD_LAYER, TRUE);
			setVisible(GAME_LAYER, TRUE);
			gameState = gstGAME;

#ifdef DEMO_SOUND
			{
				struct SoundHandle *sh;
				struct SoundData sd;
				sd.Id = M0_SND_BG_MUSIC;
				sd.Looped = TRUE;
				sd.Type = SOUND_TYPE_BGM2;
				sd.Volume = FX32(0.5f);
				sh = sndPlay(&sd);
			}
#endif
	}
}
//----------------------------------------------------------------------------------
