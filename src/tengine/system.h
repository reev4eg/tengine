/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#ifndef SYSTEM_H_
#define SYSTEM_H_

#include "platform.h"

#ifndef NITRO_SDK
#ifdef ANDROID_NDK
 #include "stdlib.h"
#endif

#ifdef IOS_APP
#include "stdlib.h"
#endif

#ifdef WINDOWS_APP
 #include "time.h"
 #include "stdlib.h"
#endif

#ifdef NIX_APP
 #include "time.h"
 #include "stdlib.h"
#endif
#endif

#if defined USE_OPENGL_RENDER || defined USE_CUSTOM_RENDER
#else
	#define USE_OPENGL_RENDER
#endif

#if defined USE_NO_SOUND || defined USE_SLES_SOUND || defined USE_OPENAL_SOUND 
#else
 #ifdef ANDROID_NDK
	#undef USE_SLES_SOUND
 #else
	#define USE_OPENAL_SOUND
 #endif
#endif

#ifdef JOBS_IN_SINGLE_THREAD 
	#undef JOBS_IN_SEPARATE_THREAD
#else
	#define JOBS_IN_SEPARATE_THREAD
#endif

#define CUSTOMAFFINETRANSFORM_SUPPORT

#ifdef NITRO_SDK
	#undef JOBS_IN_SEPARATE_THREAD
	#undef USE_OPENAL_SOUND
	#define USE_NNS_SOUND
#endif

#ifdef __cplusplus
extern "C" {
#endif

#ifdef NITRO_SDK
static const s32 SCREEN_WIDTH = 256;
static const s32 SCREEN_HEIGHT = 192;
#undef USE_OPENGL_RENDER 
#define USE_CUSTOM_RENDER
#endif

//---------------------------------------------------------------------------

enum BGSelect
{
    BGSELECT_MAIN2,
    BGSELECT_MAIN3,
    BGSELECT_SUB2,
    BGSELECT_SUB3,
    BGSELECT_NUM
};

//---------------------------------------------------------------------------

enum BMPType
{
    BMP_TYPE_256 = 0,
	BMP_TYPE_DC16,
#ifndef NITRO_SDK
#ifdef USE_OPENGL_RENDER
	BMP_TYPE_DC32,
#endif
#endif
	BMP_TYPE_NUM
};

//---------------------------------------------------------------------------

enum DrawImageType
{
	DIT_TileBG1 = 0,
	DIT_TileBG2,
	DIT_TileBG3,
	DIT_TileBG4,
	DIT_TileBG5,
	DIT_Obj
};

//---------------------------------------------------------------------------

enum RectPoints
{
	RECT_LEFT_TOP_X = 0,
	RECT_LEFT_TOP_Y = 1,
	RECT_RIGHT_TOP_X = 2,
	RECT_RIGHT_TOP_Y = 3,
	RECT_RIGHT_BOTTOM_X = 4,
	RECT_RIGHT_BOTTOM_Y = 5,
	RECT_LEFT_BOTTOM_X = 6,
	RECT_LEFT_BOTTOM_Y = 7,
	RECT_SIZE
};

//---------------------------------------------------------------------------

struct BMPImage
{
	u16  mType;
	u16  mOpaqType;
	u16  mWidth;
	u16  mHeight;
	u16  mWidth2n;
	u16  mHeight2n;
	s32  mRef;
	u32  mA5DataSize;
	u32  mACMDataSize;
	u32  mDataSize;
	u16	*mpA5Data;
	u8	*mpACMData;
	union
    {
#ifndef NITRO_SDK
#ifdef USE_OPENGL_RENDER
		GXRgba32	*mpDataDC32;
#endif
#endif
		GXRgba		*mpDataDC16;
		u8			*mpData256;
	}data;
};

typedef struct BMPPalette
{
    u16	mType;
    u16	mSize;          
	GXRgb	*mpData;
}BMPPalette;

//---------------------------------------------------------------------------

struct RenderPlaneSizeParams
{
	s32				mViewWidth;
	s32				mViewHeight;
#if defined USE_CUSTOM_RENDER || defined NITRO_SDK
#ifdef NITRO_SDK
	u16				mFrameBufferWidth8;	// buffer must be multiple of 8
	u16				mFrameBufferHeight8;
#else
	u16				mFrameBufferWidth2n;  // buffer must be multiple 2^n
	u16				mFrameBufferHeight2n;
#endif
#endif
};

//---------------------------------------------------------------------------

struct RenderPlaneInitParams
{
	struct RenderPlaneSizeParams mSizes;
	enum BGSelect mBGType;
#ifdef USE_OPENGL_RENDER
	u32 mMaxRenderObjectsOnPlane;
#endif
#if defined USE_CUSTOM_RENDER || defined NITRO_SDK
	BMPType mColorType;
#ifdef NITRO_SDK
	s32 mBGPriority;
	GXBGBmpScrBase mScreenBase;
#endif
#endif
	s32 mX;
	s32 mY;
};

//---------------------------------------------------------------------------

enum SrcDataEnum
{
	SRC_DATA_X = 0,
	SRC_DATA_Y,
	SRC_DATA_W,
	SRC_DATA_H,
	SRC_DATA_SIZE
};

//---------------------------------------------------------------------------

struct DrawImageFunctionData
{
	const struct BMPImage* mpSrcData;
	const s32* mpClipRect;
	fx32 mX;
	fx32 mY;
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
	fx32 mAffineOriginX;
	fx32 mAffineOriginY;
	fx32 mSin;
	fx32 mCos;
	fx32 mScaleX;
	fx32 mScaleY;
#endif
	enum DrawImageType mType;
	GXRgba mFillWithColor;
	u16 mSrcSizeData[SRC_DATA_SIZE];
	u8 mAlpha;
	BOOL text;
};

//---------------------------------------------------------------------------

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif //SYSTEM_H_
