/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#ifndef FILESYSTEM_H
#define FILESYSTEM_H

#include "platform.h"

#ifdef __cplusplus
extern "C" {
#endif

//----------------------------------------------------------------------------------------------
#define DMA_NO 3

#define	MAX_OPENFILES 3

#ifdef NITRO_SDK
#define	MAX_FILENAME 64
#define	MAX_FILEPATH 128
#endif
#ifdef ANDROID_NDK
#define	MAX_FILENAME 64
#define	MAX_FILEPATH 128
#ifdef NDK_NATIVE_API10
struct AAssetManager;
#endif
#endif
#if defined WINDOWS_APP || defined  NIX_APP
#define	MAX_FILENAME 64
#define	MAX_FILEPATH 1024
#endif
#ifdef IOS_APP
#define	MAX_FILENAME 64
#define	MAX_FILEPATH 1024
#endif

#define	FILERES_EXTENTION ".res"
#define	FONT_EXTENTION ".fnt"
#define	SOUND_EXTENTION ".wav"

struct InitFileSystemData
{
#ifndef NDK_NATIVE_API10
	 const char* mpPath;
#else
	 struct AAssetManager* mpAssetManager;
#endif
};

void InitFileSystem(struct InitFileSystemData* data);

void ReleaseFileSystem(void);

//----------------------------------------------------------------------------------------------

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
