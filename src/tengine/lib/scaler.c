#include "scaler.h"

#include "tengine.h"
#include "math.h"

void s_scaler_recalc_obj(struct fxVec2 scale, scaler_P scaler, scaler_obj_P sobj)
{
	sobj->realScale = sobj->scale;
	if(sobj->scaleType == ScalerScaleBoth)
	{
		sobj->realScale = fxVec2Mul(sobj->realScale, scale);
	}else if(sobj->scaleType == ScalerScaleHorizontal)
	{
		sobj->realScale.x = fxMul(sobj->realScale.x, scale.x);
		sobj->realScale.y = fxMul(sobj->realScale.y, scale.x);
	}else if(sobj->scaleType == ScalerScaleVertical)
	{
		sobj->realScale.x = fxMul(sobj->realScale.x, scale.y);
		sobj->realScale.y = fxMul(sobj->realScale.y, scale.y);
	}

	sobj->realSize = fxVec2Mul(sobj->size, sobj->realScale);

	sobj->realPos = fxVec2Mul(sobj->pos, sobj->realScale);

	if(sobj->horAnchor == ScalerAnchorLeft)
	{

	}else if(sobj->horAnchor == ScalerAnchorHCenter)
	{
		sobj->realPos.x = sobj->realPos.x - fxDiv(sobj->realSize.x, FX32(2.0f));
	}else if(sobj->horAnchor == ScalerAnchorRight)
	{
		sobj->realPos.x = sobj->realPos.x - sobj->realSize.x;
	}

	if(sobj->verAnchor == ScalerAnchorTop)
	{

	}else if(sobj->verAnchor == ScalerAnchorVCenter)
	{
		sobj->realPos.y = sobj->realPos.y - fxDiv(sobj->realSize.y, FX32(2.0f));
	}else if(sobj->verAnchor == ScalerAnchorBottom)
	{
		sobj->realPos.y = sobj->realPos.y - sobj->realSize.y;
	}


	if(sobj->horAlign == ScalerAlignLeft)
	{

	}else if(sobj->horAlign == ScalerAlignHCenter)
	{
		sobj->realPos.x = sobj->realPos.x + fxDiv(scaler->realSize.x, FX32(2.0f));
	}else if(sobj->horAlign == ScalerAlignRight)
	{
		sobj->realPos.x = sobj->realPos.x + scaler->realSize.x;
	}

	if(sobj->verAlign == ScalerAlignLeft)
	{

	}else if(sobj->verAlign == ScalerAlignVCenter)
	{
		sobj->realPos.y = sobj->realPos.y + fxDiv(scaler->realSize.y, FX32(2.0f));
	}else if(sobj->verAlign == ScalerAlignRight)
	{
		sobj->realPos.y = sobj->realPos.y + scaler->realSize.y;
	}

}

void scaler_init(scaler_P scaler, s32 objMax)
{
	SDK_ASSERT(scaler->isInited==FALSE);
	scaler->isNeedRecalc = FALSE;
	scaler->isNeedRecalcAll = FALSE;
	scaler->realSize = fxVec2Create(FX32(1.0f), FX32(1.0f));
	scaler->absSize = fxVec2Create(FX32(1.0f), FX32(1.0f));
	StaticAllocator_Init(&scaler->allocator, scaler->allocBuffer, SCALER_ALLOC_SIZE);
	AllocatorList_Init(&scaler->items, sizeof(struct scaler_obj_s), &scaler->allocator);
	scaler->isInited = TRUE;
}

void scaler_set_abs_size(scaler_P scaler, struct fxVec2 absSize)
{
	SDK_ASSERT(scaler->isInited==TRUE);
	scaler->absSize = absSize;
}

void scaler_resize(scaler_P scaler, struct fxVec2 size)
{
	SDK_ASSERT(scaler->isInited==TRUE);
	scaler->realSize = size;
}

scaler_obj_P scaler_add(scaler_P scaler, s32 objId)
{
	struct scaler_obj_s nScaler;
	SDK_ASSERT(scaler->isInited==TRUE);
	nScaler.horAlign = ScalerAlignHCenter;
	nScaler.verAlign = ScalerAlignVCenter;
	nScaler.horAnchor = ScalerAnchorHCenter;
	nScaler.verAnchor = ScalerAnchorVCenter;

	nScaler.isNeedRecalc = TRUE;
	nScaler.objId = objId;
	nScaler.pos = fxVec2Create(0, 0);
	nScaler.scale = fxVec2Create(FX32(1.0f), FX32(1.0f));
	nScaler.size = fxVec2Create(FX32(1.0f), FX32(1.0f));

	nScaler.realPos = fxVec2Create(0, 0);
	nScaler.realScale = fxVec2Create(FX32(1.0f), FX32(1.0f));
	nScaler.realSize = fxVec2Create(FX32(1.0f), FX32(1.0f));

	nScaler.scaleType = ScalerScaleNone;

	nScaler.isInited = TRUE;

	AllocatorList_PushBack(&scaler->items, &nScaler);
	return AllocatorList_Val(&scaler->items, AllocatorList_Tail(&scaler->items));
}

s32 scaler_remove(scaler_P scaler, scaler_obj_P sobj)
{
	struct ListItem* iter;
	struct ListItem* eter;
	SDK_ASSERT(scaler->isInited==TRUE);
	SDK_ASSERT(sobj->isInited==TRUE);
	s32 ret = -1;
	for(iter = AllocatorList_Begin(&scaler->items), eter = AllocatorList_End(&scaler->items);
		iter!=eter;
		iter=AllocatorList_Next(&scaler->items, iter))
	{
		if(AllocatorList_Val(&scaler->items, iter)==sobj)
		{
			ret = ((scaler_obj_P)AllocatorList_Val(&scaler->items, iter))->objId;
			break;
		}
	};
	AllocatorList_Erase(&scaler->items, iter);
	SDK_ASSERT(ret>=0);
	return ret;
}

void scaler_set_pos(scaler_P scaler, scaler_obj_P sobj, struct fxVec2 pos)
{
	SDK_ASSERT(scaler->isInited==TRUE);
	SDK_ASSERT(sobj->isInited==TRUE);
	sobj->pos = pos;
	sobj->isNeedRecalc = TRUE;
	scaler->isNeedRecalc = TRUE;
}

void scaler_set_size(scaler_P scaler, scaler_obj_P sobj, struct fxVec2 size)
{
	SDK_ASSERT(scaler->isInited==TRUE);
	SDK_ASSERT(sobj->isInited==TRUE);
	sobj->size = size;
	sobj->isNeedRecalc = TRUE;
	scaler->isNeedRecalc = TRUE;
}

void scaler_set_scale(scaler_P scaler, scaler_obj_P sobj, struct fxVec2 scale)
{
	SDK_ASSERT(scaler->isInited==TRUE);
	SDK_ASSERT(sobj->isInited==TRUE);
	sobj->scale = scale;
	sobj->isNeedRecalc = TRUE;
	scaler->isNeedRecalc = TRUE;
}

void scaler_set_anchor(scaler_P scaler, scaler_obj_P sobj, enum ScalerAnchorHorizontal hAnchor, enum ScalerAnchorVertical vAnchor)
{
	SDK_ASSERT(scaler->isInited==TRUE);
	SDK_ASSERT(sobj->isInited==TRUE);
	sobj->horAnchor = hAnchor;
	sobj->verAnchor = vAnchor;
	sobj->isNeedRecalc = TRUE;
	scaler->isNeedRecalc = TRUE;
}

void scaler_set_align(scaler_P scaler, scaler_obj_P sobj, enum ScalerAlignHorisontal hAlign, enum ScalerAlignVertical vAlign)
{
	SDK_ASSERT(scaler->isInited==TRUE);
	SDK_ASSERT(sobj->isInited==TRUE);
	sobj->horAlign = hAlign;
	sobj->verAlign = vAlign;
	sobj->isNeedRecalc = TRUE;
	scaler->isNeedRecalc = TRUE;
}

void scaler_set_scale_type(scaler_P scaler, scaler_obj_P sobj, enum ScalerScaleType scaleType)
{
	SDK_ASSERT(scaler->isInited==TRUE);
	SDK_ASSERT(sobj->isInited==TRUE);
	sobj->scaleType = scaleType;
	sobj->isNeedRecalc = TRUE;
	scaler->isNeedRecalc = TRUE;
}

void scaler_update(scaler_P scaler)
{
	SDK_ASSERT(scaler->isInited==TRUE);
	if( scaler->isNeedRecalc==TRUE || scaler->isNeedRecalcAll==TRUE )
	{
		struct ListItem* iter;
		struct ListItem* eter;
		struct fxVec2 scale = fxVec2Div( scaler->realSize, scaler->absSize );
		for(iter = AllocatorList_Begin(&scaler->items), eter = AllocatorList_End(&scaler->items);
			iter!=eter;
			iter=AllocatorList_Next(&scaler->items, iter))
		{
			scaler_obj_P item = AllocatorList_Val(&scaler->items, iter);
			if( scaler->isNeedRecalcAll==TRUE || item->isNeedRecalc==TRUE )
			{
				s_scaler_recalc_obj( scale, scaler, item );
				prSetPosition(item->objId, item->realPos);
				prSetCustomScale(item->objId, item->realScale);
				item->isNeedRecalc = TRUE;
			}
		}
		scaler->isNeedRecalcAll = FALSE;
		scaler->isNeedRecalc = FALSE;
	}
}



s32 scaler_get_objid(scaler_P scaler, scaler_obj_P sobj)
{
	SDK_ASSERT(scaler->isInited==TRUE);
	SDK_ASSERT(sobj->isInited==TRUE);
	return sobj->objId;
}
