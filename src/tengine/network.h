/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#ifndef NETWORK_H
#define NETWORK_H

#include "platform.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifndef INADDR_LOOPBACK
#define INADDR_LOOPBACK		0x7f000001
#endif

#if defined ANDROID_NDK
    #include <sys/endian.h>
#endif

//---------------------------------------------------------------------------

#ifndef MAX_NETWORK_CLIENTS
#define MAX_NETWORK_CLIENTS 20
#endif

#ifndef MAX_DATABUF_SIZE
#define MAX_DATABUF_SIZE 512
#endif

struct broadcastCB_
{
    u8* data;
    s32 maxSize;
    s32 length;
};

typedef u32 UDPconn;

struct NWsenderUDP;


BOOL NWInitNetwork(void);

BOOL NWstartUDPreciever(u16 port, BOOL (*CB_)(u32 addr, struct broadcastCB_* datain, struct broadcastCB_* dataout));

BOOL NWinitUDPsender(u32 addr, u16 port, UDPconn* conn);

BOOL NWsendUDP(UDPconn conn, struct broadcastCB_* outData);

BOOL NWcloseUDPsender(UDPconn conn);

#ifdef __cplusplus
} /* extern "C" */
#endif

//---------------------------------------------------------------------------

#endif //NETWORK_H
