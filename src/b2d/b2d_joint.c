
#include "b2d_joint.h"

#include "b2d_world.h"

void b2d_joint_init(struct b2d_joint* joint)
{
	joint->body1 = NULL;
	joint->body2 = NULL;
	joint->P = fxVec2Create(FX32(0.0f), FX32(0.0f));
	joint->biasFactor = FX32(0.2f);
	joint->softness = FX32(0.0f);
}

void b2d_joint_set(	struct b2d_joint* joint
				   , struct b2d_body* body1, struct b2d_body* body2
				   , const struct fxVec2* anchor)
{
	fxMat22 rot1 = fxMat22FromAngle(body1->rotation);
	fxMat22 rot2 = fxMat22FromAngle(body2->rotation);
	fxMat22 rot1t = fxMat22Transpose(&rot1);
	fxMat22 rot2t = fxMat22Transpose(&rot2);
	struct fxVec2 tvec;

	joint->body1 = body1;
	joint->body2 = body2;

	tvec = fxVec2Sub(*anchor, body1->position);
	joint->localAnchor1 = fxMat22MulVec(&rot1t, &tvec);

	tvec = fxVec2Sub(*anchor, body2->position);
	joint->localAnchor2 = fxMat22MulVec(&rot2t, &tvec);

	joint->P = fxVec2Create(FX32(0.0f), FX32(0.0f));
	joint->biasFactor = FX32(0.2f);
	joint->softness = FX32(0.0f);
}

void b2d_joint_prestep(struct b2d_joint* joint, fx32 inv_dt)
{
	fxMat22 Rot1 = fxMat22FromAngle(joint->body1->rotation);
	fxMat22 Rot2 = fxMat22FromAngle(joint->body2->rotation);
	fxMat22 tmpK;
	fxMat22 K;
	fxMat22 K1;
	fxMat22 K2;
	fxMat22 K3;
	struct fxVec2 p1;
	struct fxVec2 p2;
	struct fxVec2 dp;

	joint->r1 = fxMat22MulVec(&Rot1, &joint->localAnchor1);
	joint->r2 = fxMat22MulVec(&Rot2, &joint->localAnchor2);

	K1.c.col1.x = joint->body1->invMass + joint->body2->invMass;
	K1.c.col2.x = FX32(0.0f);
	K1.c.col1.y = FX32(0.0f);
	K1.c.col2.y = joint->body1->invMass + joint->body2->invMass;

	K2.c.col1.x = fxMul(joint->body1->invI, fxMul(joint->r1.y, joint->r1.y)); 
	K2.c.col2.x = fxMul(-joint->body1->invI, fxMul(joint->r1.x, joint->r1.y)); 
	K2.c.col1.y = fxMul(-joint->body1->invI, fxMul(joint->r1.x, joint->r1.y));
	K2.c.col2.y = fxMul(joint->body1->invI, fxMul(joint->r1.x, joint->r1.x));

	K3.c.col1.x = fxMul(joint->body2->invI, fxMul(joint->r2.y, joint->r2.y)); 
	K3.c.col2.x = fxMul(-joint->body2->invI, fxMul(joint->r2.x, joint->r2.y)); 
	K3.c.col1.y = fxMul(-joint->body2->invI, fxMul(joint->r2.x, joint->r2.y));
	K3.c.col2.y = fxMul(joint->body2->invI, fxMul(joint->r2.x, joint->r2.x));

	tmpK = fxMat22Add(&K1, &K2);
	K = fxMat22Add( &tmpK, &K3);
	K.c.col1.x += joint->softness;
	K.c.col2.y += joint->softness;

	joint->M = fxMat22Invert(&K);

	p1 = fxVec2Add(joint->body1->position, joint->r1); 
	p2 = fxVec2Add(joint->body2->position, joint->r2); 
	dp = fxVec2Sub(p2, p1);

	if(b2d_world_position_correction==TRUE)
	{
		joint->bias = fxVec2MulFx(dp, fxMul( (FX32(0.0f)- joint->biasFactor), inv_dt) );
	}else
	{
		joint->bias = fxVec2Create(FX32(0.0f), FX32(0.0f));
	}

	if(b2d_world_warm_starting==TRUE)
	{
		joint->body1->velocity = fxVec2Sub(joint->body1->velocity, fxVec2MulFx(joint->P, joint->body1->invMass));
		joint->body1->angularVelocity -= fxMul( joint->body1->invI, fxVec2Cross( joint->r1, joint->P ) );

		joint->body2->velocity = fxVec2Add(joint->body2->velocity, fxVec2MulFx(joint->P, joint->body2->invMass));
		joint->body2->angularVelocity += fxMul( joint->body2->invI, fxVec2Cross( joint->r2, joint->P ) );
	}else
	{
		joint->P = fxVec2Create(FX32(0.0f), FX32(0.0f));
	}
}

void b2d_joint_apply_impulse(struct b2d_joint* joint)
{
	struct fxVec2 dv = fxVec2Sub(
		fxVec2Sub(
			fxVec2Add(joint->body2->velocity, fxVec2CrossFxInv(joint->body2->angularVelocity, joint->r2) ),
			joint->body1->velocity),
		fxVec2CrossFxInv(joint->body1->angularVelocity, joint->r1) );
	struct fxVec2 tvec = fxVec2Sub(joint->bias, fxVec2Sub( dv, fxVec2MulFx( joint->P, joint->softness) ) );
	struct fxVec2 impulse = fxMat22MulVec(&joint->M, &tvec);
		
	joint->body1->velocity = fxVec2Sub(joint->body1->velocity, fxVec2MulFx( impulse, joint->body1->invMass) );
	joint->body1->angularVelocity -= fxMul(joint->body1->invI, fxVec2Cross(joint->r1, impulse) );

	joint->body2->velocity = fxVec2Add(joint->body2->velocity, fxVec2MulFx( impulse, joint->body2->invMass) );
	joint->body2->angularVelocity += fxMul(joint->body2->invI, fxVec2Cross(joint->r2, impulse) );
}


