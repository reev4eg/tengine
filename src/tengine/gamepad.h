/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#ifndef GAME_PAD_H
#define GAME_PAD_H

#include "platform.h"

#ifdef __cplusplus
extern "C" {
#endif

//----------------------------------------------------------------------------------------------

#ifndef NITRO_SDK
 #define PAD_BUTTON_A            0x0001 // A
 #define PAD_BUTTON_B            0x0002 // B
 #define PAD_BUTTON_SELECT       0x0004 // SELECT
 #define PAD_BUTTON_START        0x0008 // START
 #define PAD_KEY_RIGHT           0x0010 // RIGHT	00010000
 #define PAD_KEY_LEFT            0x0020 // LEFT		00100000
 #define PAD_KEY_UP              0x0040 // UP		01000000
 #define PAD_KEY_DOWN            0x0080 // DOWN		10000000
 #define PAD_BUTTON_R            0x0100 // R
 #define PAD_BUTTON_L            0x0200 // L
 #define PAD_BUTTON_X            0x0400 // X
 #define PAD_BUTTON_Y            0x0800 // Y
 #define PAD_BUTTON_DEBUG        0x2000 // Debug button
#endif

typedef enum KeyType
{
    TYPE_KEY_NONE = 0,                 	               	
    TYPE_SELECT,            	
    TYPE_START,   
    TYPE_LEFT,             	
    TYPE_RIGHT,             	
    TYPE_DOWN,              	
    TYPE_UP,                	           	               	
    TYPE_B,                 	
    TYPE_A,                 	           	
    TYPE_L,				    
    TYPE_R,				    
    TYPE_X,				    
    TYPE_Y,		
    TYPE_D,		    			    
    TYPE_ANY_KEY,
    KEY_TYPE_COUNT
}KeyType;				    

//----------------------------------------------------------------------------------------------

void InitGamePad(void);

void UpdateGamePad(void);

void ResetGamePad(void);

BOOL IsButtonDown(KeyType iButton);

BOOL IsButtonUp(KeyType iButton);

BOOL IsButtonPress(KeyType iButton);

//----------------------------------------------------------------------------------------------

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
