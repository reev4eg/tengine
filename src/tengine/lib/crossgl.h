#ifndef _CROSSGL_H_
#define _CROSSGL_H_

#ifdef __cplusplus
extern "C" {
#endif

#ifndef NITRO_SDK

/*
#if defined WINDOWS_APP || defined ANDROID_NDK || defined IOS_APP
 #define USE_GL_GLEXT
#endif
*/

#ifdef ANDROID_NDK
 
 #include <GLES/gl.h>
 #ifdef USE_GL_GLEXT
  #include <GLES/glext.h>
 #endif

 typedef GLfixed GLfx32;
 typedef GLclampx GLclampfx32;
 #define glOrtho_x glOrthox
 #define glClearColor_x glClearColorx
 #define glColor4_x glColor4x
 #define glFrustum_x glFrustumx
 #define glLight_xv glLightxv
 #define glMaterial_xv glMaterialxv
 #define glMaterial_x glMaterialx
 #define glScale_x glScalex
 #define glTranslate_x glTranslatex
 #define glRotate_x glRotatex
 #define glMultMatrix_x glMultMatrixx
 #define glPointSize_x glPointSizex
 #define glLineWidth_x glLineWidthx

#else

 #ifdef IOS_APP
  
  #include "OpenGLES/ES1/gl.h"
  #ifdef USE_GL_GLEXT
   #include "OpenGLES/ES1/glext.h"
  #endif

  #undef GL_FIXED
  #define GL_FIXED GL_FLOAT
  typedef float GLfx32;
  typedef float GLclampfx32;
  #define glOrtho_x glOrthof
  #define glClearColor_x glClearColor
  #define glColor4_x glColor4f
  #define glFrustum_x glFrustum
  #define glLight_xv glLightfv
  #define glMaterial_xv glMaterialfv
  #define glMaterial_x glMaterialf
  #define glScale_x glScalef
  #define glTranslate_x glTranslatef
  #define glRotate_x glRotatef
  #define glMultMatrix_x glMultMatrixf
  #define glPointSize_x glPointSize
  #define glLineWidth_x glLineWidth

 #else

  #include "platform.h"
  #include <GL/gl.h> 
  #ifdef USE_GL_GLEXT
   #include "gl/glext.h"
  #endif
 
  #if defined WINDOWS_APP || defined NIX_APP
   #undef GL_FIXED
   #define GL_FIXED GL_FLOAT
   typedef BOOL EGLboolean;
   typedef float GLfx32;
   typedef float GLclampfx32;
   void APIENTRY glOrtho_x(GLfx32 left, GLfx32 right, GLfx32 bottom, GLfx32 top, GLfx32 zNear, GLfx32 zFar);
   void APIENTRY glClearColor_x(GLclampfx32 red, GLclampfx32 green, GLclampfx32 blue, GLclampfx32 alpha);
   void APIENTRY glColor4_x(GLfx32 red, GLfx32 green, GLfx32 blue, GLfx32 alpha);
   void APIENTRY glFrustum_x(GLfx32 left, GLfx32 right, GLfx32 bottom, GLfx32 top, GLfx32 zNear, GLfx32 zFar);
   void APIENTRY glLight_xv(GLenum light, GLenum pname, const GLfx32 *params);
   void APIENTRY glMaterial_xv(GLenum face, GLenum pname, const GLfx32 *params);
   void APIENTRY glMaterial_x(GLenum face, GLenum pname, GLfx32 param);
   void APIENTRY glScale_x(GLfx32 x, GLfx32 y, GLfx32 z);
   void APIENTRY glTranslate_x(GLfx32 x, GLfx32 y, GLfx32 z);
   void APIENTRY glRotate_x(GLfx32 angle, GLfx32 x, GLfx32 y, GLfx32 z);
   void APIENTRY glMultMatrix_x(const GLfx32 *m);
   void APIENTRY glPointSize_x(GLfx32 size);
   void APIENTRY glLineWidth_x (GLfx32 width);
  #endif

 #endif

#endif

#endif //NITRO_SDK

#ifdef __cplusplus
}
#endif

#endif
