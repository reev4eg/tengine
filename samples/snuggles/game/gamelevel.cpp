
#include "gamelevel.h"
#include "tengine.h"

//-------------------------------------------------------------------------------------------

GameFieldLevel::GameFieldLevel()
	: mLevelFileIdx(-1)
	, mLayerIdx(NONE_MAP_IDX)
{
}
//-------------------------------------------------------------------------------------------

GameFieldLevel::~GameFieldLevel()
{
	release();
}
//-------------------------------------------------------------------------------------------

void GameFieldLevel::init(u32 layerIdx)
{
	mLayerIdx = layerIdx;
}
//-------------------------------------------------------------------------------------------

void GameFieldLevel::release()
{
	SDK_ASSERT(mLayerIdx < NONE_MAP_IDX);
	::setVisible(mLayerIdx, FALSE);
	releaseMapData(mLayerIdx);
}
//-------------------------------------------------------------------------------------------

void GameFieldLevel::setVisible(BOOL val)
{
	SDK_ASSERT(mLayerIdx < NONE_MAP_IDX);
	::setVisible(mLayerIdx, val);
}
//-------------------------------------------------------------------------------------------

const s32 GameFieldLevel::getLevelFileIdx()
{
	SDK_ASSERT(mLevelFileIdx >= 0);
	return mLevelFileIdx;
}
//-------------------------------------------------------------------------------------------

void GameFieldLevel::afterDataLoad(s32 level)
{
	(void)level;
}
//-------------------------------------------------------------------------------------------

void GameFieldLevel::gameStep(s32 ms)
{
	(void)ms;
}
//-------------------------------------------------------------------------------------------

void GameFieldLevel::onEngineEvent(const struct EventCallbackData *pData)
{
	(void)pData;
}
//-------------------------------------------------------------------------------------------

void GameFieldLevel::OnGUIEvent(const GUIEvent* event)
{
	(void)event;
}
//-------------------------------------------------------------------------------------------

void GameFieldLevel::onUpdateObject(s32 iId, BOOL* const oDraw)
{
	(void)iId;
	(void)oDraw;
}
//-------------------------------------------------------------------------------------------
