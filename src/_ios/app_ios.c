/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include <sys/time.h>
#include <time.h>
#include <stdint.h>

#include "gamefield.h"
#include "filesystem.h"
#include "lib/tengine_low.h"
#include "lib/render2dgl.h"
#include "lib/a_touchpad.h"
#include "lib/jobs_low.h"
#include "texts.h"
#ifdef JOBS_IN_SEPARATE_THREAD
#include "pthread.h"
#endif
#include <errno.h>
#include "profiler/profile.h"
#ifdef PROFILER_ENABLE
#include "static_allocator.h"
#endif
#include "app_ios.h"

#ifdef JOBS_IN_SEPARATE_THREAD
static void* ThreadTasks(void *t);
static pthread_t sgThread;
#endif

static long sPrevTime = 0;

struct ApplicationData
{
	BOOL tengineInit;
    BOOL appActive;
	BOOL renderDevice;
};

#ifdef PROFILER_ENABLE
#define PROFILER_STATIC_ALLOCATOR_HEAP_SIZE (1024 * 1024 * 4)
static struct StaticAllocator sProfilerStaticAllocator;
static u8 sStaticAllocatorHeap[PROFILER_STATIC_ALLOCATOR_HEAP_SIZE];
#endif

static struct ApplicationData gsAppData;
static char gSysBundkePath[MAX_FILEPATH] = {0};
static BOOL sApplication = TRUE;

static void _release_tengine();
static void _init_tengine(s32 w, s32 h);

#define RES_PATH "data/"

//-----------------------------------------------------------------------

static long _getTime(void)
{
    struct timeval now;
    gettimeofday(&now, NULL);
    return (long)(now.tv_sec * 1000 + now.tv_usec / 1000);
}
//-----------------------------------------------------------------------

void IOSApp_nativeInit(const char* bundle_path)
{
#ifdef PROFILER_ENABLE
	StaticAllocator_Init(&sProfilerStaticAllocator, sStaticAllocatorHeap, PROFILER_STATIC_ALLOCATOR_HEAP_SIZE);
	PROFILE_INIT(sProfilerStaticAllocator);
#endif
    SDK_NULL_ASSERT(bundle_path);
    SDK_ASSERT(STD_StrLen(bundle_path) < MAX_FILEPATH);
    STD_StrCpy(gSysBundkePath, bundle_path);
    STD_StrCat(gSysBundkePath, RES_PATH);
}
//-----------------------------------------------------------------------

void IOSApp_nativeCreateRenderDevice()
{
	if(gsAppData.tengineInit)
	{
		OS_Printf("app restore res data\n");
		restoreRenderDevice();
		tfgRestoreRenderDevice();
		gsAppData.renderDevice = TRUE;
	}
}
//-----------------------------------------------------------------------

void IOSApp_nativeStop()
{
	gsAppData.renderDevice = FALSE;
	OS_Printf("app lost render device\n");
	if(gsAppData.tengineInit)
	{
		tfgLostRenderDevice();
		lostRenderDevice();
	}
}
//-----------------------------------------------------------------------

void IOSApp_nativeRelease()
{
	sApplication = FALSE;
	gsAppData.appActive = FALSE;
	_release_tengine();
}
//-----------------------------------------------------------------------

void IOSApp_nativePause()
{
	IOSApp_nativeStop();
}
//-----------------------------------------------------------------------

void IOSApp_nativeResume()
{
    IOSApp_nativeCreateRenderDevice();
}
//-----------------------------------------------------------------------

void IOSApp_nativeLowMemory()
{
	OS_Printf("app low memory\n");
	if(gsAppData.tengineInit)
	{
		tfgLowMemory();
	}
}
//-----------------------------------------------------------------------

void IOSApp_nativeResize(int w, int h)
{
	if(gsAppData.tengineInit)
	{
		tfgResize(w, h);
		glRender_Resize(w, h);
	}
	else
	{
		if(sApplication)
		{
			gsAppData.appActive = TRUE;
			_init_tengine(w, h);
			gsAppData.renderDevice = TRUE;
		}
	}
}
//-----------------------------------------------------------------------

void IOSApp_nativeTouchPadDown(int ptid, float x, float y)
{
	onTouchPadDown((u8)ptid, (u16)x, (u16)y);
}
//-----------------------------------------------------------------------

void IOSApp_nativeTouchPadUp(int ptid, float x, float y)
{
	onTouchPadUp((u8)ptid, (u16)x, (u16)y);
}
//-----------------------------------------------------------------------

void IOSApp_nativeTouchPadMove(int ptid, float x, float y)
{
	onTouchPadMove((u8)ptid, (u16)x, (u16)y);
}
//-----------------------------------------------------------------------

void IOSApp_nativeKeyDown(int keyCode)
{
	(void)keyCode;
}
//-----------------------------------------------------------------------

void IOSApp_nativeKeyUp(int keyCode)
{
	(void)keyCode;
}
//-----------------------------------------------------------------------

void IOSApp_nativeTick()
{
	long curTime, ms;
	PROFILE_START;
	curTime = _getTime();
	ms = curTime - sPrevTime;
	sPrevTime = curTime;
	if (gsAppData.renderDevice && gsAppData.tengineInit)
	{
		tfgTick((s32)ms);
	}
	PROFILE_FINISH;
}
//-----------------------------------------------------------------------

int IOSApp_nativeRender()
{
	PROFILE_START;
	if (gsAppData.renderDevice)
	{
		transferToVRAM();
		return glRender_DrawFrame();
	}
	PROFILE_FINISH;
    return 0;
}
//-----------------------------------------------------------------------

#ifdef JOBS_IN_SEPARATE_THREAD
void* ThreadTasks(void *t)
{
	(void)t;
	while(gsAppData.appActive)
	{
		if(gsAppData.renderDevice 
#ifdef NDK_NATIVE_ACTIVITY
			&& gsAppData.focus
#endif
			)
		{
			jobSeparateThreadUpdate();
		}
		sched_yield();
	}
	pthread_exit(NULL);
	return NULL;
}
#endif
//-------------------------------------------------------------------------------------------

void _init_tengine(s32 w, s32 h)
{
	if(!gsAppData.tengineInit && sApplication)
	{
		struct InitFileSystemData fileSystemData;
		
	    tfgInitMemory();

	    glRender_Init();
        
        SDK_ASSERT(gSysBundkePath[0] != '\0');
		fileSystemData.mpPath = gSysBundkePath;
		InitFileSystem(&fileSystemData);

		tfgInit();
		tfgResize(w, h);
		OS_Printf("init tengine display with size: %d %d\n", w, h);
		glRender_Resize(w, h);

#ifdef JOBS_IN_SEPARATE_THREAD
		{
			s32 rc;
			pthread_attr_t threadAttr;
			pthread_attr_init(&threadAttr);
			pthread_attr_setdetachstate(&threadAttr, PTHREAD_CREATE_JOINABLE);
			rc = pthread_create(&sgThread, &threadAttr, ThreadTasks, NULL);
			if(rc)
			{
				OS_Warning("error: return code from pthread_create() is %d\n", rc);
				SDK_ASSERT(0);
			}
			pthread_attr_destroy(&threadAttr);
		}
#endif
#ifdef NDK_NATIVE_ACTIVITY
		if (app->savedState != NULL)
		{
			// We are starting with a previous saved state; restore from it
		}
#endif
		gsAppData.tengineInit = TRUE;
	}
}
//-------------------------------------------------------------------------------------------

void _release_tengine()
{
	if(gsAppData.tengineInit)
	{
#ifdef JOBS_IN_SEPARATE_THREAD
		{
			s32 rc = pthread_join(sgThread, NULL);
			if(rc)
			{
				OS_Warning("error: return code from pthread_join() is %d\n", rc);
				SDK_ASSERT(0);
			}
		}
#endif
		tfgRelease();
		ReleaseFileSystem();
		glRender_Release();
	}
	gsAppData.tengineInit = FALSE;
}
//-----------------------------------------------------------------------
