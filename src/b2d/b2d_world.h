#ifndef B2D_WORLD_H
#define B2D_WORLD_H

#include "tengine.h"
#include "array.h"

#include "b2d_body.h"
#include "b2d_joint.h"

#ifdef __cplusplus
extern "C" {
#endif

	static BOOL b2d_world_accumulate_impulses;
	static BOOL b2d_world_warm_starting;
	static BOOL b2d_world_position_correction;

	struct b2d_world
	{

		//std::vector<Body*> bodies;
		struct t_array bodies;
		//std::vector<Joint*> joints;
		struct t_array joints;

		struct fxVec2 gravity;
		s32 iterations;
		BOOL inited_;
	};

	void b2d_world_init(struct b2d_world*, struct fxVec2 gravity, s32 iterations);
	void b2d_world_add_body(struct b2d_world*, struct b2d_body* body);
	void b2d_world_add_joint(struct b2d_world*, struct b2d_joint* joint);
	void b2d_world_clear(struct b2d_world*);

	void b2d_world_step(struct b2d_world*, fx32 dt);

	void b2d_world_broad_phase(struct b2d_world*);


#ifdef __cplusplus
} /* extern "C" */
#endif

#endif //ARRAY_H
