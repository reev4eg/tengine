#ifndef TENGINE_LOW_H
#define TENGINE_LOW_H
//---------------------------------------------------------------------------

#include "tengine.h"

#ifdef __cplusplus
extern "C" {
#endif

struct TERFont;
struct TERSound;
struct JOBAnimStreamTask;

#define ALPHA_OPAQ 0x1f

// do not renumerate any index!
enum
{
	NULL_IMG_IDX = 65535,
	MAX_RES_IDX = 65535,
	MAX_EVEND_IDX = 255,

	DISPLAY_VIEW_W_MAX = 65535,
	DISPLAY_VIEW_H_MAX = 65535,

	LOGIC_ZONES_MAX = 64,

	UEOF_EMPTY = 0,
	UEOF_READY = 1,
	UEOF_SKIP_FRAME_1,
	UEOF_SKIP_FRAME_2,
	UEOF_COUNT,

	IPARENT_IDX = 2,
	IZONE = 3,
	ISTATE = 4,
	ITEMPSTATE = 5,
	ILEFT = 6,
	ITOP = 7,
	ICURRWIDTH = 8,
	ICURRHEIGHT = 9,
	IANIFRIDX = 10,
	IANIENDFLAG = 11,
	ITIMER = 12,
	prpCOUNT = 13,

	ILINKTOOBJ = 0,
	IJOINNODE1,
	IJOINNODE2,
	IJOINNODE3,
	INODE,
	IPREVNODE,
	ICBLEFT0,
	ICBTOP0,
	ICBRIGHT0,
	ICBBOTTOM0,
	IVBLEFT0,
	IVBTOP0,
	IVBRIGHT0,
	IVBBOTTOM0,
	IX0_NODE,
	IY0_NODE,
	IDISTLENGHT,
	ICUSTOMALPHA,
	ICUSTOMBLENDCOLOR,
	ICUSTOMFLAGS,
	ICLIPRECTOBJ,
	ICLIPRECTTEMPXY,
	ICLIPRECTTEMPWH,
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
	ICUSTOMROTATIONVALUE,
	ICUSTOMSCALEXVALUE,
	ICUSTOMSCALEYVALUE,
	IROTSIN,
	IROTCOS,
	ICBLTX,
	ICBLTY,
	ICBRTX,
	ICBRTY,
	ICBRBX,
	ICBRBY,
	ICBLBX,
	ICBLBY,
	IVBLTX,
	IVBLTY,
	IVBRTX,
	IVBRTY,
	IVBRBX,
	IVBRBY,
	IVBLBX,
	IVBLBY,
#endif
	prpgCOUNT,

	ICUSTOMROTATIONFLAG = 0x1,
	ICUSTOMALPHAFLAG = 0x2,
	ICUSTOMSCALEFLAG = 0x4,
	IRECALCULATELOGICRECTSFLAG = 0x8,

	RES_IMG_IDX = 0,
	RES_X = 1,
	RES_Y = 2,
	RES_W = 3,
	RES_H = 4,
	resobjprpCount = 5,
	resfrprpCount = 3,
	TILE_PRP = 0,
	TILE_LAYERS = 1,
	tileprpCount = 2,

	ANIVROFFXLT = 0,
	ANIVROFFYLT = 1,
	ANIVROFFXRB = 2,
	ANIVROFFYRB = 3,
	ANILOOP = 4,
	ANIFRCOUNT = 5,
	ppranidataCOUNT = 6,

	ANIFRCROFFXLT = 0,
	ANIFRCROFFYLT = 1,
	ANIFRCROFFXRB = 2,
	ANIFRCROFFYRB = 3,
	ANIFRN1OFFX = 4,
	ANIFRN1OFFY = 5,
	ANIFRN2OFFX = 6,
	ANIFRN2OFFY = 7,
	ANIFRN3OFFX = 8,
	ANIFRN3OFFY = 9,
	ANIFRANGLE = 10,
	ANIFROFFL = 11,
	ANIFROFFT = 12,
	ANIFRW = 13,
	ANIFRH = 14,
	ANIFRTIME = 15,
	ANIEVENTID = 16,
	ANIFRFRCOUNT = 17,
	ppranifrdataCOUNT = 18,

	ANIFRFRID = 0,
	ANIFRFROFFX = 1,
	ANIFRFROFFY = 2,
	ANIFRFRALPHA = 3,
	ANIFRFRDATA = 4,
	ppranifrfrdataCOUNT = 5,

	TXTSTRCOUNT = 0,
	TXTCHRCOUNT = 1,
	TXTWIDTH = 2,
	TXTHEIGHT = 3,
	TXTMARGIN = 4,
	TXTCOLOR = 5,
	TXTFONTID = 6,
	pprtxtdataCOUNT = 7
};

enum
{
	TRIG_MAP_TYPE = 0,
	TRIG_PROPERTIES_TYPE = 1
};

enum
{
	ITRIG_TYPE = 2,
	ITRIG_TARGET = 3,
	ITRIG_MDIR_PTILE = 4,
	ITRIG_MIDX_POPER = 5,
	ITRIG_MCOUNT_POBJ = 6,
	trprpCOUNT = 7
};

enum
{
	TRIG_UP_DIR = 2,
	TRIG_DOWN_DIR = 8,
	TRIG_LEFT_DIR = 4,
	TRIG_RIGHT_DIR = 6
};

enum
{
	TRIG_CHANGE_PRP_TRANSMIT = 0,
	TRIG_CHANGE_PRP_DECREASE = 1,
	TRIG_CHANGE_PRP_INCREASE = 2
};

enum
{
	TRIG_CHANGE_PRP_INITIATOR = -1,
	TRIG_CHANGE_PRP_SECOND = -2,
	TRIG_CHANGE_PRP_OPPOSITESTATE = 127,
	TRIG_CHANGE_PRP_BLANKACTION = 127,

	LR_COLLIDEW = 0,
	LR_COLLIDEH = 1,
	LR_VIEWW = 2,
	LR_VIEWH = 3,
	LR_COUNT = 4,

	PP_TRIGGER = 4,
	PP_CHANGEPOSFLAF_EVENTID = 5,
	PP_COUNT = 6,

	SPTTYPE_NONE = 0,
	SPTTYPE_COLLIDE = 1,
	SPTTYPE_DIRECTOR = 2,
	SPTTYPE_PROPERTY = 3,

	IMULTI = 0,
	ISWITCHTO = 1,
	IGOTOZONE = 2,
	IENDLEVEL = 3,
	IMESSAGE = 4,
	IINITIATORPRP = 5,
	IINITIATORPVAL = 6,
	IINITIATORPOPER = 7,
	IINITIATORSTATE = 8,
	IINITIATORSOPER = 9,
	ISECONDPRP = 10,
	ISECONDPVAL = 11,
	ISECONDPOPER = 12,
	ISECONDSTATE = 13,
	ISECONDSOPER = 14,
	ISPTTYPE = 15,
	sptprpCOUNT = 16
};

enum
{
	SMT_LESSEQ = 0, // <=
	SMT_EQUAL = 1, // ==
	SMT_MOREEQ = 2, // >=
	SMT_NOT = 3 // !=
};

enum
{
	SAVE = 1,
	OPT = 2,

	OBJ_GAME = 0,
	OBJ_BACK = 1,
	OBJ_FORE = 2,
	OBJ_COUNT = 3,

	CHECK_OFFSET = 1,

	stIDANIMATION = 0,
	stDIRECTION = 1,
	stCOUNT = 2,

	PARENT_TEXT_OBJ = 2
};

enum
{
	RES_TYPE_NONE = 0,
	RES_TYPE_IMAGE = 1,
	RES_TYPE_FONT = 2,
	RES_TYPE_SOUND = 3
};

enum
{
	PCS_OFFX = 2,
	PCS_OFFY = 3,
	PCS_W = 4,
	PCS_H = 5,
	PCS_SPEED = 6,
	PCS_TGTX = 7,
	PCS_TGTY = 8,
	PCS_LIFE = 9,
	PCS_RESID = 10,
	PCS_TGTX0 = 11,
	PCS_TGTY0 = 12,
	PCS_EXPLODE = 13,
	PCS_SINCOS = 14,
	PCS_DISTLENGHT = 15,
	PCS_ALPHA = 16,
	PCS_ADDITIONAL = 17,
	PCS_ROTATIONORIGX = 18,
	PCS_ROTATIONORIGY = 19,
	PCS_COUNT = 20,
	PCS_DEF_POOL_SIZE = 1024
};
//---------------------------------------------------------------------------

struct AnimStreamData
{
	const char *mpFilename;
	struct BMPImage *mpImage;
	BOOL mBufferStatus;
	BOOL mBindStatus;
	u16 mFrpcdata[resobjprpCount];
};
//---------------------------------------------------------------------------

struct FileStreamData
{
	const char *mpFilename;
	struct BMPImage *mpImage;
	u8 *mpFile;
	BOOL mStatus;
	u16 mIdx;
	u8 mValue;
	u8 mMode;
};
//---------------------------------------------------------------------------

struct TEngineInstance
{
	u8 *data_file;

	u16 *map;
	u16 *t_map;
	const s16 **tile_data;
	const u16 ***fr_img;
	const u16 *map_img;
	const u16 *map_fnt;
	const u16 *map_snd_sfx;
	const u16 *map_snd_bgm;

	const u8* sclmap;
	const s32 *scllines;
	u8 scl_ct;

	s32 ds_plane_fr_array_beg[BGSELECT_NUM];
	s32 ds_plane_fr_array_end[BGSELECT_NUM];

	u32 pr_arr_off[OBJ_COUNT];
	s32 **trig_idata;

	u16
	**pr_spt,
	**nodes,
	**spt_cldobjlist,
	**spt_bdata,
	**spt_triglist,
	**trig_spt;
	u16 **pr_textdata;

	u8 **trig_st;

	fx32
	**pr_prp,
	**trig_prp;

	char **spt_cldgrouplist;
	wchar ***pr_textdatatext;

	u32
	*spt_cldgrouplist_ct,
	*spt_cldobjlist_ct,
	*spt_triglist_ct,
	*trig_st_ct,
	*trig_prp_ct,
	*pr_textdataalign;

	s32
	**pr_idata,
	**prg_idata,
	**nodes_idata,
	**pr_znlist,
	**rndr_znlist,
	*pr_count;

	s16
	**df_znlist,
	**db_znlist,
	*db_count,
	*df_count;

	enum BGSelect bgType;
	s32 visible_mask;
	s32 ueof_state;

    u16 map_img_ct;
    u16 map_fnt_ct;
    u16 map_snd_sfx_ct;
    u16 map_snd_bgm_ct;
	s32 pr_idata_ct;
	s32 all_pr_count;
	s32 zones_ct;
	s32 nodes_ct;
	s32 trig_common_count;
	s32 script_common_count;
	s32 pnt_count;
    u16 ld_img_res_ct;
    u16 ld_fnt_res_ct;
    u16 ld_snd_sfx_res_ct;
    u16 ld_snd_bgm_res_ct;
	u16 maxbgres;

	s32 map_h;
	s32 map_w;
	s32 d_w;
	s32 d_h;
	s32 logicWidth;
	s32 logicHeight;
	s32 sz_map;
	s32 sz_mapH;
	s32 sz_mapW;
	fx32 dpx_fx;
	fx32 dpy_fx;
	fx32 sdx_fx;
	fx32 sdy_fx;
	fx32 camerax;
	fx32 cameray;
	s32 current_zone;
	s32 t_current_zone;
	s32 shiftTilesMax;
	fx32 tsdy8_fx[BGSELECT_NUM];
	fx32 tsdx8_fx[BGSELECT_NUM];
	fx32 tsdx0_fx[BGSELECT_NUM];
	fx32 tsdy0_fx[BGSELECT_NUM];
	fx32 tcx_fx[BGSELECT_NUM];
	fx32 tcy_fx[BGSELECT_NUM];
	s32 tc_dx8_fx[BGSELECT_NUM];
	s32 tc_dy8_fx[BGSELECT_NUM]; 
	BOOL primary;

	s32 **pcs_data;
	s32 particlesCount;

	// callbacks
	OnScriptCallback _onScriptCB;
	OnChangeHeroCallback _onChangeHeroCB;
};
//---------------------------------------------------------------------------

enum ALState
{
	ALS_STATE_READY = 0,
	ALS_STATE_LIST,
	ALS_STATE_LOAD_DATA
};
// ----------------------------------------------------------------------------------

struct TEngineCommonData
{
	s32 states_ct;
	s32 ppr_count;
	s32 lang_id;
	s32 global_delay_ct;
	u16 p_w;
	u16 p_h;
	u16 delay;
	u16 tl_layers_ct;
	u16 res_img_ct;
	u16 res_fnt_ct;
	u16 res_snd_ct;
	u16 anim_ct;
	BOOL draw;
	BOOL rl_available;

	u16 spd_prp_id;
	u16 enable_prp_id;

	struct InitEngineParameters initParams;
	struct TEngineInstance **instances;
	u32 a_layer;
	struct TEngineInstance *ainst;

	const char **res_img_fnames;
	const char **res_fnt_fnames;
	const char **res_snd_fnames;
	const u8 **states;
	const u8 *states_map;
	s16 *ppr_decortype;
	u8 *common_data_file;
	s16 ***ppr_anim_data;
	const s32 ****ppr_fr_data;
	const s16 *****ppr_frfr_data;
	const u16 **fr_data;
	const u8 *common_data_tr_pt;
	const u8 *common_data_sp_pt;
	u8 *text_data_file;
	const wchar ***textdatatext;
	u16 *textdatatext_sct;

	struct JOBAnimStreamTask ***strm_obj_data;
	const char **res_strm_names;
	struct TEngineInstance *strm_instance;
	const struct TEngineInstance *snd_instance;
	s32 strm_type_ct;
	s32 strm_obj_ct;

	u32 *ld_p1;
	u16 *ld_p2;
	s16 *ld_task;
	s16 ld_ct;
	s16 ld_ct_t;
	enum ALState ld_st;
	BOOL ag_st;
	s32 ld_param;
	s32 agrl_s;
	s32 agrl_i;
	u16 glstrmidx;
	s32 agrl_strmprid;
	s32 agrl_strmaid;
	s32 agrl_strmbfi;

	struct TERFont **res_fnt;
	struct BMPImage **res_img;
	struct TERSound **res_snd;

	OnUpdateObject _onUpdateGameObject;
	OnUpdateObject _onUpdateDecorationObject;
	OnUpdateBackgroundTiles _onDrawBackgroundTiles;
	OnFinishUpdate _onFinishUpdate;
	OnEventCallback _onEventCB;
};
//---------------------------------------------------------------------------

void transferToVRAM(void);

void lostRenderDevice(void);
void restoreRenderDevice(void);

//---------------------------------------------------------------------------

s32 *low_getZoneGameObjDataArray(u32 layer, s32 zone);

s32 low_getZoneGameObjCount(u32 layer, s32 zone);

void low_addXYToPosition(s32 pr_id, fx32 xv, fx32 yv, u32 layer);

void low_getCollideRect(s32 pr_id, fx32 rect[8], u32 layer);

void low_getViewRect(s32 pr_id, fx32 rect[8], u32 layer);

BOOL low_getCurrentAnimationFrameInfo(s32 pr_id, struct GameObjectAnimationFrameInfo *const goafi, u32 layer);

void low_getAnimationFrameInfo(s32 gr_id, s32 anim_id, s32 frame_id, struct GameObjectAnimationFrameInfo *const goafi);

void low_setClipRect(s32 pr_id, s32 clip_pr_id, u32 layer);

//---------------------------------------------------------------------------

u16 low_getPrpSpeedIdx(void);

u16 low_getPrpEnabledIdx(void);

//---------------------------------------------------------------------------

void _loadDataInit(void);
BOOL _nextPointAtLine(fx32 x0, fx32 y0, fx32 tx, fx32 ty, s32 *l, fx32 speed, fx32 pos[2]);
const s16 *_getFramePieceData(s32 ppr_id, s32 anim_id, s32 frame_idx, s32 pc_idx);
void _prGetFramePiecesInfo(struct TEngineInstance *ei, s32 id, s32 *pc_count, s32 *anim_id);
void _updateAnimation(s32 pr_id, s32 ms);
void _updateLoadingProcess(void);
void _setActiveLayer(u32 layer);
void _setState(struct TEngineInstance *ei, s32 pr_id, s32 statedata, BOOL updateNodes);
void _processMovement(s32 pr);

struct TEngineInstance* _getInstance(u32 idx);
BOOL _isStreamFrame(s16 data);
s32 _calcLenght(fx32 x0, fx32 y0, fx32 tx, fx32 ty);
void _calculateCXY8(struct TEngineInstance * i, fx32 *cx, fx32 *cy, fx32 *c_dx, fx32 *c_dy);
void _calculateLogicSizes(struct TEngineInstance *i, struct TEngineCommonData *cd);
//---------------------------------------------------------------------------

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /*TENGINE_LOW_H*/

