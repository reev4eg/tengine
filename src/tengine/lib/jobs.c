/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include "lib/jobs_low.h"
#include "lib/sound_low.h"
#include "sound.h"
#include "filesystem.h"
#include "jobs.h"
#include "texts.h"
#include "loadhelpers.h"
#ifdef USE_CUSTOM_RENDER
	#include "easygraphics.h"
#endif 
#ifdef USE_OPENGL_RENDER
	#include "render2dgl.h"
#endif 
#ifdef JOBS_IN_SEPARATE_THREAD
#include "pthread.h"
#endif

//-------------------------------------------------------------------------------------------

enum
{
	JOB_TASK_TYPE_VIDEOSTREAM = 0,
	JOB_TASK_TYPE_AUDIOSTREAM = 1,
	JOB_TASK_TYPE_FILESTREAM = 2,
	JOB_TASK_TYPE_COUNT,

	JOB_TASK_VIDEOSTREAM_MAX = 64,
	JOB_TASK_FILESTREAM_MAX = 1,
	JOB_TASK_AUDIOSTREAM_MAX = 2
};

struct JOBTask
{
#ifdef SDK_DEBUG
	s32 mType;
#endif
	union TaskData
	{
		void *mpPointer;
		struct JOBAnimStreamTask *mpAnimStreamData;
		struct JOBFileStreamTask *mpFileStreamData;
	}mTaskData;
};

static const s32 LOAD_TEXTURES_PER_TICK_MAX 
#ifdef JOBS_IN_SEPARATE_THREAD
												= 1;
#else
												= 3;
#endif

static struct JOBTask *gspJobTask[JOB_TASK_TYPE_COUNT] = {NULL};
static u32 gsTaskCount[JOB_TASK_TYPE_COUNT] = {0};
static BOOL gsProcessDelay = TRUE;
static BOOL gsInit = FALSE;
#ifdef SDK_DEBUG
static s32 sgTextureLoad = 0;
#endif

#ifdef JOBS_IN_SEPARATE_THREAD
static pthread_mutex_t gsMutex = PTHREAD_MUTEX_INITIALIZER;
#endif

static s32 _findTask(const void *data, s32 type);

// AnimStream

typedef void (*processASTaskFn)(struct AnimStreamData *asd, s32 *var, BOOL *exit_process);
typedef BOOL (*conditionASFn)(struct AnimStreamData *asd, s32 *var, BOOL *exit_process);

static BOOL _conditionASFn_Default(struct AnimStreamData *asd, s32 *var, BOOL *exit_process);
static BOOL _conditionASFn_LoadTexture(struct AnimStreamData *asd, s32 *var, BOOL *exit_process);
#ifdef USE_OPENGL_RENDER
static BOOL _conditionASFn_BindTexture(struct AnimStreamData *asd, s32 *var, BOOL *exit_process);
#endif
static void _processTaskASFn_LoadTexture(struct AnimStreamData *asd, s32 *var, BOOL *exit_process);
#ifdef USE_OPENGL_RENDER
static void _processTaskASFn_BindTexture(struct AnimStreamData *asd, s32 *var, BOOL *exit_process);
#endif
#ifdef JOBS_IN_SEPARATE_THREAD
static void _processTaskASFn_Lock(struct AnimStreamData *asd, s32 *var, BOOL *exit_process);
static BOOL _jobTrySetMutexForAnimStreamTasks(void);
#endif
static void _processASTask(s32 depth, processASTaskFn wfn, conditionASFn cfn, s32* var);

// FileStream

typedef void (*processFSTaskFn)(struct FileStreamData *fsd, s32 *var);
typedef BOOL (*conditionFSFn)(struct FileStreamData *fsd);

static BOOL _conditionFSFn_Default(struct FileStreamData *fsd);
static void _processTaskFSFn_LoadFile(struct FileStreamData *fsd, s32 *var);
#ifdef JOBS_IN_SEPARATE_THREAD
static void _processTaskFSFn_Lock(struct FileStreamData *fsd, s32 *var);
static BOOL _jobTrySetMutexForFileSystemTasks(void);
#endif
static void _processFSTask(processFSTaskFn wfn, conditionFSFn cfn, s32* var);

// AudioStream

#ifdef JOBS_IN_SEPARATE_THREAD
BOOL _jobTrySetMutexForAudioStreamTasks(void);
#endif

//-------------------------------------------------------------------------------------------

void jobInit(void)
{
	if(!gsInit)
	{
#ifdef SDK_DEBUG
		sgTextureLoad = 0;
#endif
		gsTaskCount[JOB_TASK_TYPE_VIDEOSTREAM] = 0;
		gsTaskCount[JOB_TASK_TYPE_AUDIOSTREAM] = 0;
		gsTaskCount[JOB_TASK_TYPE_FILESTREAM] = 0;
		gsProcessDelay = TRUE;
#ifdef JOBS_IN_SEPARATE_THREAD
		pthread_mutex_init(&gsMutex, NULL);
#endif
		gspJobTask[JOB_TASK_TYPE_VIDEOSTREAM] = (struct JOBTask*)MALLOC(JOB_TASK_VIDEOSTREAM_MAX * sizeof(struct JOBTask), "jobInit:gspJobTask[0]");
		gspJobTask[JOB_TASK_TYPE_AUDIOSTREAM] = NULL; //1
		gspJobTask[JOB_TASK_TYPE_FILESTREAM] = (struct JOBTask*)MALLOC(JOB_TASK_FILESTREAM_MAX * sizeof(struct JOBTask), "jobInit:gspJobTask[2]");
		gsInit = TRUE;
	}
}
//-------------------------------------------------------------------------------------------

void jobRelease(void)
{
	gsProcessDelay = TRUE;
	gsTaskCount[JOB_TASK_TYPE_VIDEOSTREAM] = 0;
	gsTaskCount[JOB_TASK_TYPE_AUDIOSTREAM] = 0;
	gsTaskCount[JOB_TASK_TYPE_FILESTREAM] = 0;
	if(gspJobTask[JOB_TASK_TYPE_FILESTREAM])
	{
		FREE(gspJobTask[JOB_TASK_TYPE_FILESTREAM]);
	}
	if(gspJobTask[JOB_TASK_TYPE_AUDIOSTREAM])
	{
		FREE(gspJobTask[JOB_TASK_TYPE_AUDIOSTREAM]);
	}
	if(gspJobTask[JOB_TASK_TYPE_VIDEOSTREAM])
	{
		FREE(gspJobTask[JOB_TASK_TYPE_VIDEOSTREAM]);	
	}
	gspJobTask[JOB_TASK_TYPE_VIDEOSTREAM] =
	gspJobTask[JOB_TASK_TYPE_AUDIOSTREAM] = 
	gspJobTask[JOB_TASK_TYPE_FILESTREAM] = NULL;
#ifdef JOBS_IN_SEPARATE_THREAD
	if(gsInit)	
	{	
		pthread_mutex_destroy(&gsMutex);
	}
#endif
	gsInit = FALSE;
}
//-------------------------------------------------------------------------------------------

#ifdef JOBS_IN_SEPARATE_THREAD
BOOL jobCriticalSectionBegin(void)
{
	if(gsInit)
	{
		pthread_mutex_lock(&gsMutex);
		return TRUE;
	}
	return FALSE;
}
//-------------------------------------------------------------------------------------------

BOOL jobCriticalSectionEnd(void)
{
	if(gsInit)
	{
		pthread_mutex_unlock(&gsMutex);
		return TRUE;
	}
	return FALSE;
}
//-------------------------------------------------------------------------------------------
#endif

s32 _findTask(const void *data, s32 type)
{
	u32 i;
	for(i = 0; i < gsTaskCount[type]; i++)
	{
		if(gspJobTask[type][i].mTaskData.mpPointer == data)
		{
			return (s32)i;
		}
	}
	return -1;
}
//-------------------------------------------------------------------------------------------

void jobResetTasks(void)
{
	u32 i, j, k;
#ifdef SDK_DEBUG
	sgTextureLoad = 0;
#endif
	for(k = 0; k < JOB_TASK_TYPE_COUNT; k++)
	{
		for(i = 0; i < gsTaskCount[k]; i++)
		{
			switch(k)
			{
				case JOB_TASK_TYPE_VIDEOSTREAM:
				{
					for(j = 0; j < JOB_ANIM_TASK_BUFFER_MAX; j++)
					{
						gspJobTask[k][i].mTaskData.mpAnimStreamData->mData[j].mBufferStatus = FALSE;
						gspJobTask[k][i].mTaskData.mpAnimStreamData->mData[j].mBindStatus = FALSE;
						gspJobTask[k][i].mTaskData.mpAnimStreamData->mData[j].mpImage = NULL;
					}
				}
				break;

				case JOB_TASK_TYPE_FILESTREAM:
					gspJobTask[k][i].mTaskData.mpFileStreamData->mData.mStatus = FALSE;
			}
		}
	}
}
//-------------------------------------------------------------------------------------------

void jobSetActive(BOOL val)
{
	gsProcessDelay = !val;
}
//-------------------------------------------------------------------------------------------

BOOL jobIsActive()
{
	return !gsProcessDelay;
}
//-------------------------------------------------------------------------------------------


void jobAddStreamVideoTask(struct JOBAnimStreamTask *data)
{
	SDK_ASSERT(gsInit);
	if(_findTask(data, JOB_TASK_TYPE_VIDEOSTREAM) < 0)
	{
		gspJobTask[JOB_TASK_TYPE_VIDEOSTREAM][gsTaskCount[JOB_TASK_TYPE_VIDEOSTREAM]].mTaskData.mpAnimStreamData = data;
#ifdef SDK_DEBUG
		gspJobTask[JOB_TASK_TYPE_VIDEOSTREAM][gsTaskCount[JOB_TASK_TYPE_VIDEOSTREAM]].mType = JOB_TASK_TYPE_VIDEOSTREAM;
#endif
		gsTaskCount[JOB_TASK_TYPE_VIDEOSTREAM]++;
		SDK_ASSERT(JOB_TASK_VIDEOSTREAM_MAX >= gsTaskCount[JOB_TASK_TYPE_VIDEOSTREAM]);
	}
}
//-------------------------------------------------------------------------------------------

void jobRemoveStreamVideoTask(const struct JOBAnimStreamTask *data)
{
	s32 i;
	if(gsInit)
	{
		i = _findTask(data, JOB_TASK_TYPE_VIDEOSTREAM);
		if(i >= 0)
		{
			gsTaskCount[JOB_TASK_TYPE_VIDEOSTREAM]--;
			gspJobTask[JOB_TASK_TYPE_VIDEOSTREAM][i] = gspJobTask[JOB_TASK_TYPE_VIDEOSTREAM][gsTaskCount[JOB_TASK_TYPE_VIDEOSTREAM]];
		}
	}
}
//-------------------------------------------------------------------------------------------

void jobAddStreamFileTask(struct JOBFileStreamTask *data)
{
	SDK_ASSERT(gsInit);
	if(_findTask(data, JOB_TASK_TYPE_FILESTREAM) < 0)
	{
		gspJobTask[JOB_TASK_TYPE_FILESTREAM][gsTaskCount[JOB_TASK_TYPE_FILESTREAM]].mTaskData.mpFileStreamData = data;
#ifdef SDK_DEBUG
		gspJobTask[JOB_TASK_TYPE_FILESTREAM][gsTaskCount[JOB_TASK_TYPE_FILESTREAM]].mType = JOB_TASK_TYPE_FILESTREAM;
#endif
		gsTaskCount[JOB_TASK_TYPE_FILESTREAM]++;
		SDK_ASSERT(JOB_TASK_FILESTREAM_MAX >= gsTaskCount[JOB_TASK_TYPE_FILESTREAM]);
	}
}
//-------------------------------------------------------------------------------------------

void jobRemoveStreamFileTask(const struct JOBFileStreamTask *data)
{
	s32 i;
	if(gsInit)
	{
		i = _findTask(data, JOB_TASK_TYPE_FILESTREAM);
		if(i >= 0)
		{
			gsTaskCount[JOB_TASK_TYPE_FILESTREAM]--;
			gspJobTask[JOB_TASK_TYPE_FILESTREAM][i] = gspJobTask[JOB_TASK_TYPE_FILESTREAM][gsTaskCount[JOB_TASK_TYPE_FILESTREAM]];
		}
	}
}
//-------------------------------------------------------------------------------------------

#ifdef JOBS_IN_SEPARATE_THREAD
void jobSeparateThreadUpdate()
{
	//load files
	if(_jobTrySetMutexForFileSystemTasks()) // <-- jobCriticalSectionBegin()
	{
		jobUpdateFileStreamTasks();
		jobCriticalSectionEnd();
	}

	// steaming video
	if(_jobTrySetMutexForAnimStreamTasks()) // <-- jobCriticalSectionBegin()
	{
		jobUpdateAnimStreamTasksAndLoadTextures();
		jobCriticalSectionEnd();
	}
	
	// process audio
	if(_jobTrySetMutexForAudioStreamTasks()) // <-- jobCriticalSectionBegin()
	{
		jobUpdateAudioStreamTasks();
		jobCriticalSectionEnd();
	}
}
#endif
//-------------------------------------------------------------------------------------------

// video stream

//-------------------------------------------------------------------------------------------
#ifdef JOBS_IN_SEPARATE_THREAD
BOOL _jobTrySetMutexForAnimStreamTasks(void)
{
	s32 var = 0;
	_processASTask(JOB_ANIM_TASK_BUFFER_MAX, _processTaskASFn_Lock, _conditionASFn_Default, &var);
	return var == 1;
}
//-------------------------------------------------------------------------------------------

void _processTaskASFn_Lock(struct AnimStreamData *asd, s32 *var, BOOL *exit_process)
{
	(void)asd;
	*exit_process = TRUE;
	*var = jobCriticalSectionBegin();
}
//-------------------------------------------------------------------------------------------

#ifdef USE_OPENGL_RENDER
void _processTaskASFn_BindTexture(struct AnimStreamData *asd, s32 *var, BOOL *exit_process)
{
	(void)var;
	(void)exit_process;
	glRender_LoadTextureToVRAM(asd->mpImage, asd->mpImage->mOpaqType);
	asd->mBindStatus = TRUE;
}
#endif
//-------------------------------------------------------------------------------------------

BOOL jobBindTexturesToVRAM(void)
{
#ifdef USE_OPENGL_RENDER
	s32 var = 1;
	_processASTask(1, _processTaskASFn_BindTexture, _conditionASFn_BindTexture, &var);
	return var;
#endif
}
#endif
//-------------------------------------------------------------------------------------------

void jobUpdateAnimStreamTasksAndLoadTextures(void)
{
	s32 var = 0;
	_processASTask(
#ifdef JOBS_IN_SEPARATE_THREAD
		JOB_ANIM_TASK_BUFFER_MAX,
#else
		JOB_ANIM_TASK_BUFFER_MAX - 1,
#endif
		_processTaskASFn_LoadTexture, _conditionASFn_LoadTexture, &var);
#ifdef SDK_DEBUG
	sgTextureLoad += var;
#endif
}
//-------------------------------------------------------------------------------------------

#ifdef SDK_DEBUG
void jobResetTextureCounter(void){ sgTextureLoad = 0; }
s32 jobGetTextureCounter(void){ return sgTextureLoad; }
#endif
//-------------------------------------------------------------------------------------------

void _processTaskASFn_LoadTexture(struct AnimStreamData *asd, s32 *var, BOOL *exit_process)
{
#ifdef USE_OPENGL_RENDER
	u16 glidx;
	char fullPath[MAX_FILEPATH];
	STD_StrCpy(fullPath, asd->mpFilename);
	STD_StrCat(fullPath, FILERES_EXTENTION);
	glidx = asd->mpImage->mOpaqType;
	LoadBMPImage(fullPath, &asd->mpImage, TRUE);
	asd->mpImage->mOpaqType = glidx;
#ifndef JOBS_IN_SEPARATE_THREAD
	glRender_LoadTextureToVRAM(asd->mpImage, glidx);
#endif
#endif
#ifdef USE_CUSTOM_RENDER
	LoadBMPImage(asd->mpFilename, &asd->mpImage, TRUE);
#endif
	asd->mBufferStatus = TRUE;
	asd->mBindStatus = FALSE;
	++(*var);
	(void)exit_process;
}
//-------------------------------------------------------------------------------------------

BOOL _conditionASFn_Default(struct AnimStreamData *asd, s32 *var, BOOL *exit_process)
{
	(void)var;
	*exit_process = FALSE;
	return asd->mBufferStatus == FALSE && asd->mpImage != NULL;
}
//-------------------------------------------------------------------------------------------

BOOL _conditionASFn_LoadTexture(struct AnimStreamData *asd, s32 *var, BOOL *exit_process)
{
	*exit_process = *var >= LOAD_TEXTURES_PER_TICK_MAX;
	return asd->mBufferStatus == FALSE && asd->mpImage != NULL && !*exit_process;
}
//-------------------------------------------------------------------------------------------

#ifdef USE_OPENGL_RENDER
BOOL _conditionASFn_BindTexture(struct AnimStreamData *asd, s32 *var, BOOL *exit_process)
{
	(void)exit_process;
	if(asd->mBufferStatus == FALSE)
	{
		*var = 0;
	}
	return asd->mBufferStatus == TRUE && asd->mBindStatus == FALSE && asd->mpImage != NULL; 
}
#endif
//-------------------------------------------------------------------------------------------

void _processASTask(s32 depth, processASTaskFn wfn, conditionASFn cfn, s32* var)
{
	if(!gsProcessDelay && gsTaskCount[JOB_TASK_TYPE_VIDEOSTREAM])
	{
		s32 i;
		u32 j;		
		for(i = 0; i < depth; i++)
		{
			for(j = 0; j < gsTaskCount[JOB_TASK_TYPE_VIDEOSTREAM]; j++)
			{
				struct AnimStreamData *asd;
				BOOL exit_process;
#ifdef SDK_DEBUG
				SDK_ASSERT(gspJobTask[JOB_TASK_TYPE_VIDEOSTREAM][j].mType == JOB_TASK_TYPE_VIDEOSTREAM);
#endif
				asd = &gspJobTask[JOB_TASK_TYPE_VIDEOSTREAM][j].mTaskData.mpAnimStreamData->mData[i]; 
				exit_process = FALSE;
				if(cfn(asd, var, &exit_process))
				{
					wfn(asd, var, &exit_process);
				}
				if(exit_process)
				{
					return;
				}
			}
		}
	}
}
//-------------------------------------------------------------------------------------------

// file stream

//-------------------------------------------------------------------------------------------

#ifdef JOBS_IN_SEPARATE_THREAD
BOOL _jobTrySetMutexForFileSystemTasks()
{
	s32 var = 0;
	_processFSTask(_processTaskFSFn_Lock, _conditionFSFn_Default, &var);
	return var == 1;
}
//-------------------------------------------------------------------------------------------

void _processTaskFSFn_Lock(struct FileStreamData *fsd, s32 *var)
{
	(void)fsd;
	*var = jobCriticalSectionBegin();
}
//-------------------------------------------------------------------------------------------
#endif

void jobUpdateFileStreamTasks()
{
	s32 var = 0;
	_processFSTask(_processTaskFSFn_LoadFile, _conditionFSFn_Default, &var);
}
//-------------------------------------------------------------------------------------------

static BOOL _conditionFSFn_Default(struct FileStreamData *fsd)
{
	return fsd->mStatus == FALSE && fsd->mMode != 0;
}
//-------------------------------------------------------------------------------------------

static void _processTaskFSFn_LoadFile(struct FileStreamData *fsd, s32 *var)
{
	switch(fsd->mMode)
	{
		case JFSDM_MODE_IMG:
			LoadBMPImage(fsd->mpFilename, &fsd->mpImage, FALSE);
			fsd->mStatus = TRUE;
		break;
		case JFSDM_MODE_FILE:
			fsd->mpFile = LoadFile(fsd->mpFilename, FALSE);
			fsd->mStatus = TRUE;
		break;
		case JFSDM_MODE_SND:
			fsd->mpFile = (u8*)sndLoadWavData(fsd->mpFilename, fsd->mIdx, fsd->mValue == 1); //sfx
			fsd->mStatus = TRUE;
	}
	++(*var);
}
//-------------------------------------------------------------------------------------------

void _processFSTask(processFSTaskFn wfn, conditionFSFn cfn, s32* var)
{
	if(!gsProcessDelay && gsTaskCount[JOB_TASK_TYPE_FILESTREAM])
	{
		u32 j;		
		for(j = 0; j < gsTaskCount[JOB_TASK_TYPE_FILESTREAM]; j++)
		{
			struct FileStreamData *fsd;
#ifdef SDK_DEBUG
			SDK_ASSERT(gspJobTask[JOB_TASK_TYPE_FILESTREAM][j].mType == JOB_TASK_TYPE_FILESTREAM);
#endif
			fsd = &gspJobTask[JOB_TASK_TYPE_FILESTREAM][j].mTaskData.mpFileStreamData->mData; 
			if(cfn(fsd))
			{
				wfn(fsd, var);
			}
		}
	}
}
//-------------------------------------------------------------------------------------------

// audio stream

//-------------------------------------------------------------------------------------------
#ifdef JOBS_IN_SEPARATE_THREAD
BOOL _jobTrySetMutexForAudioStreamTasks()
{
	if(!gsProcessDelay && sndIsBGMPlaying())
	{
		return jobCriticalSectionBegin();
	}
	return FALSE;
}
//-------------------------------------------------------------------------------------------
#endif

void jobUpdateAudioStreamTasks()
{
	if(!gsProcessDelay)
	{
		sndUpdateSoundStream();
	}
}
//-------------------------------------------------------------------------------------------
