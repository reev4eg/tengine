
#include "verlet.h"
#include "verlet_low.h"

//---------------------------------------------------------------------------

static struct VTConstraint* _vrltConstraintPushToObj(struct Verlet *v, struct VTObject *c);
static void _vrltConstraintPush(struct VTObject *c, struct VTConstraint* p);

static fx32 _angle2(struct fxVec2 *v1, struct fxVec2 *v2, struct fxVec2 *v3);
static void _rotate(struct fxVec2 *res, struct fxVec2 *v, struct fxVec2 *o, fx32 t);

//---------------------------------------------------------------------------

struct VTConstraint* _vrltConstraintPushToObj(struct Verlet *v, struct VTObject *c)
{
	SDK_ASSERT(v->mConstraintsPoolMax > 0);
	if(v->mConstraintsPoolCount < v->mConstraintsPoolMax)
	{
		struct VTConstraint* p;
		p = v->mppConstraintsPool[v->mConstraintsPoolCount];
		v->mConstraintsPoolCount++;
		_vrltConstraintPush(c, p);
		return p;
	}
	SDK_ASSERT(v->mConstraintsPoolCount < v->mConstraintsPoolMax);
	return NULL;
}
//---------------------------------------------------------------------------

void _vrltConstraintPush(struct VTObject *c, struct VTConstraint* p)
{
	SDK_ASSERT(c->mConstraintsMax > 0);
	if(c->mConstraintsCount < c->mConstraintsMax)
	{
		c->mppConstraints[c->mConstraintsCount] = p;
		c->mConstraintsCount++;
		return;
	}
	SDK_ASSERT(c->mConstraintsCount < c->mConstraintsMax);
}
//---------------------------------------------------------------------------

void _vrltPinConstraintPushToObj(struct Verlet *v, struct VTObject *c, s32 index, struct fxVec2 *pos)
{
	struct fxVec2 *ppos;
	struct VTConstraint* p;
	SDK_NULL_ASSERT(c);
	p = _vrltConstraintPushToObj(v, c);
	SDK_NULL_ASSERT(p);
	ppos = pos == NULL ? &c->mppParticles[index]->pos : pos;
	SDK_ASSERT(index < c->mParticlesCount);
	p->mpParticle1 = c->mppParticles[index];
	p->mpParticle2 = NULL;
	p->mpParticle3 = NULL;
	p->mPos = *ppos;
	p->mType = VTCT_PIN;
}
//---------------------------------------------------------------------------

void _vrltDistanceConstraintPushToObj(struct Verlet *v, struct VTObject *c, s32 index1, s32 index2, fx32 stiffness, fx32 *distance)
{
	struct VTConstraint* p;
	SDK_NULL_ASSERT(c);
	p = _vrltConstraintPushToObj(v, c);
	SDK_NULL_ASSERT(p);
	SDK_ASSERT(index1 < c->mParticlesCount);
	SDK_ASSERT(index2 < c->mParticlesCount);
	p->mpParticle1 = c->mppParticles[index1];
	p->mpParticle2 = c->mppParticles[index2];
	p->mpParticle3 = NULL;
	p->mPos.x = stiffness;
	if(distance == NULL)
	{
		struct fxVec2 val = fxVec2Sub(c->mppParticles[index1]->pos, c->mppParticles[index2]->pos);
		p->mPos.y = fxVec2Length(val);
	}
	else
	{
		p->mPos.y = *distance;
	}
	p->mType = VTCT_DISTANCE;
}
//---------------------------------------------------------------------------

void _vrltAngleConstraintPushToObj(struct Verlet *v, struct VTObject *c, s32 index1, s32 index2, s32 index3, fx32 stiffness)
{
	struct VTConstraint* p;
	SDK_NULL_ASSERT(c);
	p = _vrltConstraintPushToObj(v, c);
	SDK_NULL_ASSERT(p);
	SDK_ASSERT(index1 < c->mParticlesCount);
	SDK_ASSERT(index2 < c->mParticlesCount);
	SDK_ASSERT(index3 < c->mParticlesCount);
	p->mpParticle1 = c->mppParticles[index1];
	p->mpParticle2 = c->mppParticles[index2];
	p->mpParticle3 = c->mppParticles[index3];
	p->mPos.x = stiffness;
	p->mPos.y = _angle2(&p->mpParticle2->pos, &p->mpParticle1->pos, &p->mpParticle3->pos);
	p->mType = VTCT_ANGLE;
}
//---------------------------------------------------------------------------

fx32 _angle2(struct fxVec2 *v1, struct fxVec2 *v2, struct fxVec2 *v3)
{
	struct fxVec2 vl, vr;
	vl = fxVec2Sub(*v2, *v1);
	vr = fxVec2Sub(*v3, *v1);
	return fxAtan2(fxMul(vl.x, vr.y) - fxMul(vl.y, vr.x), fxMul(vl.x, vr.x) + fxMul(vl.y, vr.y));
}
//---------------------------------------------------------------------------

void _rotate(struct fxVec2 *res, struct fxVec2 *v, struct fxVec2 *o, fx32 t)
{
	fx32 fsin, fcos;
	fx32 x = v->x - o->x;
	fx32 y = v->y - o->y;
	fxCordic(t, &fsin, &fcos, FX_CORDIC_LOW_PRECISION);
	res->x = fxMul(x, fcos) - fxMul(y, fsin) + o->x;
	res->y = fxMul(x, fsin) + fxMul(y, fcos) + o->y;
}
//---------------------------------------------------------------------------

void _vrltConstraintRelax(struct VTConstraint *c, fx32 k)
{
	SDK_NULL_ASSERT(c);
	switch(c->mType)
	{
		case VTCT_PIN:
			c->mpParticle1->pos = c->mPos;
		break;
		case VTCT_DISTANCE:
		{
			s64 m, val1, val2;
			struct fxVec2 norm = fxVec2Sub(c->mpParticle1->pos, c->mpParticle2->pos);
			val1 = ((s64)(norm.x) * norm.x) >> FX32_SHIFT;
			SDK_ASSERT(val1 >= 0); // oveflow!
			val2 = ((s64)(norm.y) * norm.y) >> FX32_SHIFT;
			SDK_ASSERT(val2 >= 0); // oveflow!
			m = val1 + val2;
			SDK_ASSERT(m >= 0); // oveflow!
			val1 = ((s64)(c->mPos.y) * c->mPos.y) >> FX32_SHIFT; 
			SDK_ASSERT(val1 >= 0); // oveflow!
			val1 -= m;
			val2 = (val1 << FX32_SHIFT) / m;
			val2 = (val2 * c->mPos.x) >> FX32_SHIFT;
			val2 = (val2 * k) >> FX32_SHIFT;
			norm = fxVec2MulFx(norm, (fx32)val2);
			c->mpParticle1->pos = fxVec2Add(c->mpParticle1->pos, norm);
			c->mpParticle2->pos = fxVec2Sub(c->mpParticle2->pos, norm);
		}
		break;
		case VTCT_ANGLE:
		{
			fx32 angle = _angle2(&c->mpParticle2->pos, &c->mpParticle1->pos, &c->mpParticle3->pos);
			fx32 diff = angle - c->mPos.y;
			if(diff <= -FX_PI)
			{
				diff += 2 * FX_PI;
			}
			else
			if(diff >= FX_PI)
			{
				diff -= 2 * FX_PI;
			}
			diff = fxMul(diff, fxMul(k, c->mPos.x));
			_rotate(&c->mpParticle1->pos, &c->mpParticle1->pos, &c->mpParticle2->pos, diff);
			_rotate(&c->mpParticle3->pos, &c->mpParticle3->pos, &c->mpParticle2->pos, -diff);
			_rotate(&c->mpParticle2->pos, &c->mpParticle2->pos, &c->mpParticle1->pos, diff);
			_rotate(&c->mpParticle2->pos, &c->mpParticle2->pos, &c->mpParticle3->pos, -diff);
		}
		break;
		default:
			SDK_ASSERT(0); // something wrong
	}
}
//---------------------------------------------------------------------------
