/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#ifndef ALLOCATOR_ARRAY_H
#define ALLOCATOR_ARRAY_H

#include "tengine.h"

#ifdef __cplusplus
extern "C" {
#endif

#define ALLOCATOR_ARRAY_RESERVE_COUNT 4

struct StaticAllocator;

struct AllocatorArray
{
	u32 item_size;
	u32 item_count;
	u32 allocated_count;
	u8* ptr;
	struct StaticAllocator *allocator;
};

u32 AllocatorArray_CalculateHeapSize(u32 item_size, u32 max_items);
void AllocatorArray_Init(struct AllocatorArray* parray, u32 item_size, struct StaticAllocator *allocator);
void AllocatorArray_InitWithReserve(struct AllocatorArray* parray, u32 reserve_count, u32 item_size, struct StaticAllocator *allocator);
void AllocatorArray_Release(struct AllocatorArray* parray);
void AllocatorArray_Clear(struct AllocatorArray* parray);
void AllocatorArray_ClearFast(struct AllocatorArray* parray);
void AllocatorArray_PushBack(struct AllocatorArray* parray, void* data);
void AllocatorArray_Insert(struct AllocatorArray* parray, u32 pos, void* data);
void AllocatorArray_Erase(struct AllocatorArray* parray, u32 pos);
void* AllocatorArray_At(struct AllocatorArray* parray, u32 pos);
u32 AllocatorArray_Size(struct AllocatorArray* parray);

#define A_ARRAY_INIT(TYPE, ARRAY_STRUCT, ALLOCATOR) AllocatorArray_Init(&ARRAY_STRUCT, sizeof(TYPE), &(ALLOCATOR))
#define A_ARRAY_INIT_RESERVE(TYPE, RESERV, ARRAY_STRUCT, ALLOCATOR) AllocatorArray_InitWithReserve(&ARRAY_STRUCT, RESERV, sizeof(TYPE), &(ALLOCATOR))
#define A_ARRAY_RELEASE(TYPE, ARRAY_STRUCT) AllocatorArray_Release(&ARRAY_STRUCT)
#define A_ARRAY_PUSH_BACK(TYPE, ARRAY_STRUCT, ITEM) AllocatorArray_PushBack(&ARRAY_STRUCT, (void*)(&(ITEM)))
#define A_ARRAY_INSERT(TYPE, ARRAY_STRUCT, POS, ITEM) AllocatorArray_Insert(&ARRAY_STRUCT, POS, (void*)(&(ITEM)))
#define A_ARRAY_AT(TYPE, ARRAY_STRUCT, POS) *((TYPE*)AllocatorArray_At(&ARRAY_STRUCT, POS))
#define A_ARRAY_CLEAR(TYPE, ARRAY_STRUCT) AllocatorArray_Clear(&ARRAY_STRUCT)
#define A_ARRAY_CLEAR_FAST(TYPE, ARRAY_STRUCT) AllocatorArray_ClearFast(&ARRAY_STRUCT)
#define A_ARRAY_ERASE(TYPE, ARRAY_STRUCT, POS) AllocatorArray_Erase(&ARRAY_STRUCT, POS)
#define A_ARRAY_SIZE(ARRAY_STRUCT) AllocatorArray_Size(&ARRAY_STRUCT)

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif //ARRAY_H
