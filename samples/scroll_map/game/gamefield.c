// demo:
// - tengine init (optional with USE_STATIC_MEMORY pool)
// - renderplane init
// - 2 different levels created with mapeditor uses to render on one renderplane (BGSELECT_SUB3)
//     layer 1 - GAME_LAYER based on level map l0 (mapeditor)
//     layer 2 - HUD_LAYER based on level map l1 (mapeditor)
//     (also layer 2 uses as "low memory" message layer (level map l2))
// - gui init and using
// - FRAME_ALLOCATOR principle functionality memory using
// - "low memory" signal
// - sounds and sfx
// - wchar
// - touchpad and keypad

#include "tengine.h"
#include "texts.h"
#include "touchpad.h"
#include "gamepad.h"
#include "gamefield.h"
#include "level_id.h"
#include "sound.h"
#ifdef USE_STATIC_MEMORY
#include "static_allocator.h"
#endif

#include "gui.h"

#include "constants.h"

#define DEMO_SOUND

#ifdef NITRO_SDK
#include "sound.sadl"
#endif

//-------------------------------------------------------------------------------------------

enum KeyCodes
{
    UP = 1,
    DOWN,
    RIGHT,
    LEFT,
    FIRE,
    SELECT,
    START,
    ANYKEY
};

enum GameStates
{
	gstNONE = 0,
	gstFORCEDISPLAYINIT,
	gstDISPLAYINIT,
	gstLOADING,
	gstGAME
};

enum LoadingStates
{
	lstGAME = 0,
	lstLOWMEMORYMESSAGE,
};

 static s32 gameState = gstNONE;
 static s32 loadingState = lstGAME;
 static fx32 hero_x;
 static fx32 hero_y;
 static s32 key_action;
 static fx32 button_angle = 0;
 static fx32 face_angle = 0;
 static u8 button_alpha = 31;
 static u32 c_c;
 static BOOL need_rearrangeSpeedButtons = FALSE;
 static struct fxVec2 camera_pos;

 static fx32 direction_power[4];

 static s32 fps_timer = 0;
 static s32 fps_tick = 0;

 static s32 resizeW = 0;
 static s32 resizeH = 0;

 static TouchPadData tp_data;

 static struct GUIContainer gGUIContainer = {0};

 enum ControlDirections
 {
	CD_UP = 0,
	CD_DOWN,
	CD_LEFT,
	CD_RIGHT
 };

 enum LoadMarkers
 {
	LM_INIT = 0,
	LM_LOAD
 };

 enum GameLayers
 {
	GAME_LAYER = 0,
	HUD_LAYER = 1,
	LAYER_COUNT
 };
 
 #ifdef NITRO_SDK
 static const u32 SOUND_HEAD_SIZE = 233000;
 static const char* SOUND_BANK_NAME = "data/sound.sdat";
 #endif
 
#ifdef USE_STATIC_MEMORY
 // MAIN_STATIC_ALLOCATOR_SIZE = 50Mb 
#define MAIN_STATIC_ALLOCATOR_SIZE (1024 * 1024 * 50)
static u8 sgMainStaticHeap[MAIN_STATIC_ALLOCATOR_SIZE];
static struct StaticAllocator sgMainStaticAllocator;
#endif

//-------------------------------------------------------------------------------------------

static void onEngineEvent(const struct EventCallbackData *pData);

static void keyProcessing(void);

static void onUpdateObject(u32 layer, s32 iId, BOOL* const oDraw);
static void onUpdateBackgroundTiles(u32 layer);

static void releaseTengineData(void);
static void releaseGameResources(void);

static void load(void);

static void game_step(s32 ms);

static void doResize(void);

static void onLevelMapLoad(void);
static void onLevelHudLoad(void);
static void rearrangeSpeedButtons(void);

static void onGUIEvent(const struct GUIEvent* event);

//-------------------------------------------------------------------------------------------

void tfgInitMemory()
{
#ifdef USE_STATIC_MEMORY
	StaticAllocator_Init(&sgMainStaticAllocator, sgMainStaticHeap, MAIN_STATIC_ALLOCATOR_SIZE);
	InitMemoryAllocator(&sgMainStaticAllocator);
#else
	InitMemoryAllocator();
#endif
}
//-------------------------------------------------------------------------------------------

void tfgInit()
{
    InitGamePad();
	InitTouchPad();
#ifdef DEMO_SOUND
#ifndef NITRO_SDK
	sndInitSoundSystem();
#else
	sndInitSoundSystem(SOUND_HEAD_SIZE, SOUND_BANK_NAME);
#endif
#endif
	gameState = gstNONE;
}
//-------------------------------------------------------------------------------------------

void tfgRelease()
{
	releaseTengineData();
	sndReleaseSoundSystem();
}
//-------------------------------------------------------------------------------------------

void tfgResize(s32 w, s32 h)
{
	gameState = gstDISPLAYINIT;
	c_c = 0;
	resizeW = w;
	resizeH = h;
}
//-------------------------------------------------------------------------------------------

static void doResize()
{
	struct RenderPlaneInitParams mp1;

	initRenderPlaneParametersWithDefaultValues(&mp1);
	mp1.mBGType = BGSELECT_SUB3;
	mp1.mSizes.mViewWidth = resizeW; 
	mp1.mSizes.mViewHeight = resizeH;
	if(loadingState == lstGAME)
	{
		mp1.mMaxRenderObjectsOnPlane = 2000; // max objs estimation on scene
	}
	else //lstLOWMEMORYMESSAGE
	{
		mp1.mMaxRenderObjectsOnPlane = 128; // "low memory" level uses only characters and one button (mapeditor)
	}

#ifndef NITRO_SDK
#ifdef USE_CUSTOM_RENDER
	mp1.mColorType = BMP_TYPE_DC16;
	{
		s32 w2n;
		s32 h2n;
		w2n = h2n = 2;
		while(w2n < resizeW)
		{
			w2n <<= 1;
		}
		while(h2n < resizeH)
		{
			h2n <<= 1;
		}
#ifdef FRAME_ALLOCATOR
		// when the screen orientation changes, it is necessary to re-create a buffer (resize)
		// if defined FRAME_ALLOCATOR, we cannot recreate new buffer for easygraphics module because of assertion)
		// so the buffer must be the same for the larger size of the screen
		w2n = (w2n > h2n) ? w2n : h2n; 
		mp1.mSizes.mFrameBufferWidth2n = w2n;
		mp1.mSizes.mFrameBufferHeight2n = h2n;
#else
		mp1.mSizes.mFrameBufferWidth2n = w2n;
		mp1.mSizes.mFrameBufferHeight2n = h2n;
#endif
	}
#endif
#else
	mp1.mColorType = BMP_TYPE_DC16;
	mp1.mBGPriority = 0;
	mp1.mScreenBase = GX_BG_BMPSCRBASE_0x00000;
	mp1.mSizes.mFrameBufferWidth8 = (u16)resizeW;
	mp1.mSizes.mFrameBufferHeight8 = (u16)resizeH; 
#endif
	mp1.mX = 0;
	mp1.mY = 0;

#ifdef NITRO_SDK
	//GX_SetBankForBG(GX_VRAM_BG_128_A);
    //GX_SetGraphicsMode(GX_DISPMODE_GRAPHICS, GX_BGMODE_5, GX_BG0_AS_2D);
	GX_SetBankForSubBG(GX_VRAM_SUB_BG_128_C);
    GXS_SetGraphicsMode(GX_BGMODE_5); 
    GXS_DispOn();
#endif

	if(!isRenderPlaneInit(GAME_LAYER)) // any active plane, whatever
	{
		struct InitEngineParameters params;

		releaseTengineData();

		initEngineParametersWithDefaultValues(&params);
		params.layersCount = LAYER_COUNT;
		if(loadingState == lstGAME)
		{
			params.particlesPoolSize = 1024;
		}
		else
		{
			params.particlesPoolSize = 0;
		}

		initEngine(&params);
		initRenderPlane(&mp1);
		
		assignLayerWithRenderPlane(GAME_LAYER, BGSELECT_SUB3, TRUE);
		assignLayerWithRenderPlane(HUD_LAYER, BGSELECT_SUB3, FALSE);

		//setVisible(HUD_LAYER, FALSE);
		//setVisible(GAME_LAYER, FALSE);

        //register callbacks
		setOnEvent(onEngineEvent);
		setOnUpdateGameObject(onUpdateObject);
		setOnUpdateBackgroundTiles(onUpdateBackgroundTiles);

		//begin load task
		//parameter will return with iData.initiatorId in onEngineEvent() function ( EVENT_TYPE_ON_END_ASYNH_LOAD_DATA)
		beginLoadListAsynh(LM_INIT);
		{
			//load common data
			addToLoadListCommonData();
			//load text data for certain language
			// to avoid memory fragmentation load it once after common data
			addToLoadListLanguageData(LANGUAGE_ENG);
		}
		//end load task
		endLoadListAsynh(FALSE);
	
		camera_pos.x = hero_x = 0;
		camera_pos.y = hero_y = 0;
		key_action = 0;
		c_c = 0;

		//wait for EVENT_TYPE_ON_END_ASYNH_LOAD_DATA in onEngineEvent() function
		gameState = gstNONE;
	}
	else
	{
		resizeRenderPlane(GAME_LAYER, &mp1.mSizes);
		need_rearrangeSpeedButtons = TRUE;
		gameState = gstGAME;
	}
}
//-------------------------------------------------------------------------------------------

void releaseTengineData()
{
	// to avoid memory fragmentation, releaseMapData in opposite side
	releaseGameResources();
	releaseTextData();
	releaseCommonData();
	releaseRenderPlane(BGSELECT_SUB3);
	releaseEngine();
}
//-------------------------------------------------------------------------------------------

void releaseGameResources()
{
	releaseResources();

	GUIFactory_Reset();
	GUIContainer_FreeAll(&gGUIContainer);	

	releaseMapData(HUD_LAYER);
	releaseMapData(GAME_LAYER);

	ResetTouchPad();
}
//-------------------------------------------------------------------------------------------

void load()
{
#ifdef NITRO_SDK
    GX_SetVisiblePlane(GX_PLANEMASK_NONE);
#endif
	
	// to avoid memory fragmentation, releaseMapData in opposite side

	//setVisible(HUD_LAYER, FALSE);
	//setVisible(GAME_LAYER, FALSE);

	releaseGameResources();
	OS_Printf("mem on newGame = %u\n", GetFreeMemorySize());

	beginLoadListAsynh(LM_LOAD);
	{
		if(loadingState == lstGAME)
		{
			//layer LAYER_GAME, load current level
			addToLoadListMap(GAME_LAYER, 0); // i.e. l0 map file in assets/data folder
			//layer LAYER_HUD, load or reload hud
			addToLoadListMap(HUD_LAYER, 1); // i.e. l1 map file in assets/data folder
		}
		else //lstLOWMEMORYMESSAGE
		{
			//layer LAYER_HUD, load or reload memory warning message
			addToLoadListMap(HUD_LAYER, 2); // i.e. l2 map file in assets/data folder
		}
	}
	endLoadListAsynh(TRUE);
	
	//wait for  EVENT_TYPE_ON_END_ASYNH_LOAD_DATA and EVENT_TYPE_ON_END_ASYNH_LOAD_RESOURSES in onEngineEvent() function
	gameState = gstNONE;
}
//-------------------------------------------------------------------------------------------

// file index 1, layer GAME_LAYER
void onLevelMapLoad()
{
	struct fxVec2 pos;
	
	c_c = 0;

	resetParticles();

	// only on onLevelMapLoad (called from onEngineEvent function)
	// you can work with M0_* objects, because current active layer is GAME_LAYER 
	pos = prGetPosition(M0_CAMERA_1); // constants.h (gererated by MapEditor3)
	hero_x = camera_pos.x = pos.x;
	hero_y = camera_pos.y = pos.y;
	direction_power[CD_UP] = 0;
	direction_power[CD_DOWN]= 0;
	direction_power[CD_LEFT]= 0;
	direction_power[CD_RIGHT]= 0;
}
//-------------------------------------------------------------------------------------------

void onLevelHudLoad()
{
	// only on onLevelHudLoad (called from onEngineEvent function)
	// you can work with M1_* objects, because current active layer is HUD_LAYER

	s32 i;
	struct GUISpeedButtonInitParameters bp;
	bp.baseInitParameters.enabledPrpId = PRP_ENABLE;
	// id's from constants.h, "Game object types" section
	bp.baseInitParameters.mapObjTypeID[BUTTON_MAPOBJ_ID_IDLE] = BUTTON_IDLE;
	bp.baseInitParameters.mapObjTypeID[BUTTON_MAPOBJ_ID_PRESSED] = BUTTON_PRESSED;
	bp.baseInitParameters.mapObjTypeID[BUTTON_MAPOBJ_ID_FOCUSED] = -1;
	bp.baseInitParameters.mapObjTypeID[BUTTON_MAPOBJ_ID_TEXT] = TEXTBOX;

	GUIContainer_Init(&gGUIContainer, HUD_LAYER, 4); // 4 - maximum gui objects
	GUIContainer_SetGUIEventHandler(&gGUIContainer, onGUIEvent); // OnGuiEvent() registration

	GUIFactory_Init(&gGUIContainer);
	GUIFactory_AddGUISpeedButtonType(&bp); // register button type

	// parsing array with id's and create gui objects
	// based on registred types
	GUIFactory_ParseMapObjects();

	//create gui from HUD_LAYER data 
	if(loadingState == lstGAME)
	{
		for(i = 0; i < GUIContainer_GetObjectsCount(&gGUIContainer); i++)
		{
			const s32 BUTTON_SIZE = FX32(64);
			struct GUIObject2D *o; 
			wchar txt[10];
			txt[0] = L'\0';
			o = GUIContainer_GetObject(&gGUIContainer, i);
			if(GUIObject2D_GetType(o) == GUI_TYPE_SPEEDBUTTON)
			{
				struct GUISpeedButton* b = (struct GUISpeedButton*)o;
				switch(GUISpeedButton_GetMapObjId(b))
				{
					case M1_BUTTON_1:
						txt[0] = L'U';
						txt[1] = L'p';
						txt[3] = L'\0';
						GUISpeedButton_SetPosition(b, FX32(getViewWidth(HUD_LAYER)) / 2, BUTTON_SIZE / 2);
						GUISpeedButton_SetHotkey(b, TYPE_UP);
                    break;
					case M1_BUTTON_2:
						txt[0] = L'D';
						txt[1] = L'o';
						txt[2] = L'w';
						txt[3] = L'n';
						txt[4] = L'\0';
						GUISpeedButton_SetPosition(b,  FX32(getViewWidth(HUD_LAYER)) / 2, FX32(getViewHeight(HUD_LAYER)) - BUTTON_SIZE / 2);
						GUISpeedButton_SetHotkey(b, TYPE_DOWN);
					break;
					case M1_BUTTON_3:
						txt[0] = L'L';
						txt[1] = L'e';
						txt[2] = L'f';
						txt[3] = L't';
						txt[4] = L'\0';
						GUISpeedButton_SetPosition(b, BUTTON_SIZE / 2,  FX32(getViewHeight(HUD_LAYER)) / 2);
						GUISpeedButton_SetHotkey(b, TYPE_LEFT);
					break;
					case M1_BUTTON_4:
						txt[0] = L'R';
						txt[1] = L'i';
						txt[2] = L'g';
						txt[3] = L'h';
						txt[4] = L't';
						txt[5] = L'\0';
						GUISpeedButton_SetPosition(b,  FX32(getViewWidth(HUD_LAYER)) - BUTTON_SIZE / 2,  FX32(getViewHeight(HUD_LAYER)) / 2);
						GUISpeedButton_SetHotkey(b, TYPE_RIGHT);
				}
				GUISpeedButton_SetText(b, txt);
			}
		}

		prSetCustomRotation(M1_BUTTON_3, FX32(45));
	}
	else //lstLOWMEMORYMESSAGE
	{
		// nope
	}

#ifdef NITRO_SDK
    GXS_SetVisiblePlane(GX_PLANEMASK_BG3);
#endif

    gameState = gstGAME;
}
//------------------------------------------------------------------------------------

void rearrangeSpeedButtons()
{
	s32 i;
	for(i = 0; i < GUIContainer_GetObjectsCount(&gGUIContainer); i++)
	{
		const s32 BUTTON_SIZE = FX32(64);
		struct GUIObject2D *o; 
		o = GUIContainer_GetObject(&gGUIContainer, i);
		if(GUIObject2D_GetType(o) == GUI_TYPE_SPEEDBUTTON)
		{
			struct GUISpeedButton* b = (struct GUISpeedButton*)o;
			switch(GUISpeedButton_GetMapObjId(b))
			{
				case M1_BUTTON_1:
					GUISpeedButton_SetPosition(b, FX32(getViewWidth(HUD_LAYER)) / 2, BUTTON_SIZE / 2);
                break;
				case M1_BUTTON_2:
					GUISpeedButton_SetPosition(b, FX32(getViewWidth(HUD_LAYER)) / 2, FX32(getViewHeight(HUD_LAYER)) - BUTTON_SIZE / 2);
				break;
				case M1_BUTTON_3:
					GUISpeedButton_SetPosition(b, BUTTON_SIZE / 2, FX32(getViewHeight(HUD_LAYER)) / 2);
				break;
				case M1_BUTTON_4:
					GUISpeedButton_SetPosition(b, FX32(getViewWidth(HUD_LAYER)) - BUTTON_SIZE / 2, FX32(getViewHeight(HUD_LAYER)) / 2);
			}
		}
	}
}
//-------------------------------------------------------------------------------------------

void tfgLostRenderDevice()
{
}
//-------------------------------------------------------------------------------------------

void tfgRestoreRenderDevice()
{
}
//------------------------------------------------------------------------------------

void tfgLowMemory()
{	
	// now load "level" with warning message
	// note: this level must be as small as possible, without BG elements and large textures
	loadingState = lstLOWMEMORYMESSAGE;

	// todo: save current gamestate

	// delete all data and tengine instance
	releaseTengineData();

	// reinit engine with minimal memory consumption
	gameState = gstDISPLAYINIT;
}
//-------------------------------------------------------------------------------------------

void tfgTick(s32 ms)
{
	UpdateGamePad();
	keyProcessing();
	
	updateEngine(ms);

	switch(gameState)
	{
		case gstNONE:
		break;

		case gstFORCEDISPLAYINIT:
			releaseTengineData();
		case gstDISPLAYINIT:
			doResize();
		break;

		case gstLOADING:
			load();
		break;

		case gstGAME:
			game_step(ms);
	}
}
//-------------------------------------------------------------------------------------------

void keyProcessing()
{
    key_action = 0;

    //lotcheck
    if(IsButtonPress(TYPE_L) && IsButtonPress(TYPE_R) && IsButtonPress(TYPE_SELECT) && IsButtonPress(TYPE_START))
    {
        OS_ResetSystem(0);
        return;
	}

	ReadTouchPadData(&tp_data);
	GUIContainer_ProcessInput(&gGUIContainer, &tp_data);

    if(IsButtonDown(TYPE_START))
    {
        key_action = START;
		gameState = gstLOADING;
    }
}
//----------------------------------------------------------------------------------

void onGUIEvent(const struct GUIEvent* event)
{
	if(GUIObject2D_GetType(event->mpSender) == GUI_TYPE_SPEEDBUTTON)
	{
		if(event->mState == ST_PRESSED)
		{
			const struct GUISpeedButton* b = (struct GUISpeedButton*)event->mpSender;
			
			// in case of low memory button message button
			if(lstLOWMEMORYMESSAGE == loadingState)
			{
				loadingState = lstGAME;
				gameState = gstFORCEDISPLAYINIT;
				return;
			}
			
			switch(GUISpeedButton_GetHotkey(b))
			{
				case TYPE_LEFT:
					key_action = TYPE_LEFT;
					direction_power[CD_LEFT] += FX32(2.5);
					prSetCustomRotation(M1_CAMERA_18, face_angle);
					face_angle += FX32(0.5f);
				break;
				case TYPE_RIGHT:
					key_action = TYPE_RIGHT;
					direction_power[CD_RIGHT] += FX32(2.5);

					prSetCustomRotation(M1_BUTTON_1, button_angle);
					face_angle = prGetCustomRotation(M1_CAMERA_18);
					button_angle += FX32(0.5f);
				break;
				case TYPE_UP:
					key_action = TYPE_UP;
					direction_power[CD_UP] += FX32(2.5);
				break;
				case TYPE_DOWN:
					key_action = TYPE_DOWN;
					direction_power[CD_DOWN] += FX32(2.5);
                break;
				case TYPE_KEY_NONE:
				case TYPE_SELECT:
				case TYPE_START:
				case TYPE_B:
				case TYPE_A:
				case TYPE_L:
				case TYPE_R:
				case TYPE_X:
				case TYPE_Y:
				case TYPE_D:
				case TYPE_ANY_KEY:
				case KEY_TYPE_COUNT:
                break;
			}
		}
	}
}
//----------------------------------------------------------------------------------

void game_step(s32 ms)
{	
	const static fx32 MAX_SPEED = FX32(60);

	fps_timer += ms;

	if(direction_power[CD_UP] > MAX_SPEED)
	{
		direction_power[CD_UP] = MAX_SPEED;
	}
	if(direction_power[CD_DOWN] > MAX_SPEED)
	{
		direction_power[CD_DOWN] = MAX_SPEED;
	}
	if(direction_power[CD_LEFT] > MAX_SPEED)
	{
		direction_power[CD_LEFT] = MAX_SPEED;
	}
	if(direction_power[CD_RIGHT] > MAX_SPEED)
	{
		direction_power[CD_RIGHT] = MAX_SPEED;
	}

	direction_power[CD_UP] -= FX32(0.5f);
	direction_power[CD_DOWN] -= FX32(0.5f);
	direction_power[CD_LEFT] -= FX32(0.5f);
	direction_power[CD_RIGHT] -= FX32(0.5f);
	if(direction_power[CD_UP] < 0)
	{
		direction_power[CD_UP] = 0;
	}
	if(direction_power[CD_DOWN] < 0)
	{
		direction_power[CD_DOWN] = 0;
	}
	if(direction_power[CD_LEFT] < 0)
	{
		direction_power[CD_LEFT] = 0;
	}
	if(direction_power[CD_RIGHT] < 0)
	{
		direction_power[CD_RIGHT] = 0;
	}
}
//----------------------------------------------------------------------------------

static void onUpdateBackgroundTiles(u32 layer)
{
	//set camera here!
	
	switch(layer)
	{
		case GAME_LAYER:
			setCamera(camera_pos);
		break;
		case HUD_LAYER:
			if(need_rearrangeSpeedButtons)
			{
				need_rearrangeSpeedButtons = FALSE;
				rearrangeSpeedButtons();
			}
	}
}
//----------------------------------------------------------------------------------

void onUpdateObject(u32 layer, s32 iId, BOOL* const oDraw)
{
	(void)oDraw;
	switch(layer)
	{
		case GAME_LAYER:
			switch(iId)
			{
				case M0_CAMERA_1: // constants.h (gererated by MapEditor3)
				{
					struct fxVec2 pos;
					fx32 forceX, forceY, xxx, ival;

					pos = prGetPosition(M0_CAMERA_1);
					hero_x = pos.x;
					hero_y = pos.y;

					if(c_c % 90 == 0)
					{
						createParticles(M0_CAMERA_1, 3, 3, FX32(0.5), FX32(0.2), 3000, 1500, 300, 0, 0);
						createSimpleParticles(5, GX_RGBA(31,0,0,1), hero_x, hero_y, 10, 200, FX32(1.1f), FX32(0), 3000, 1500, 300, NULL, NULL);
						createExplode(hero_x, hero_y, 20, 10, 1000, M0_CAMERA_1, STATE_RIGHT);
#ifdef DEMO_SOUND
						{
							struct SoundHandle *sh;
							struct SoundData sd;
							sd.Id = M0_SND_DING;
							sd.Looped = FALSE;
							sd.Type = SOUND_TYPE_SFX;
							sd.Volume = FX32(0.0f);
							sh = sndPlay(&sd);
							if(sh)
							{
								sndSetVolume(sh, FX32(0.8f));
							}
						}
#endif
					}

					forceX = FX_Div((direction_power[CD_RIGHT] - direction_power[CD_LEFT]), 5 << FX32_SHIFT);
					forceY = FX_Div((direction_power[CD_DOWN] - direction_power[CD_UP]), 5 << FX32_SHIFT);
					camera_pos.y += forceY;
					camera_pos.x += forceX;
					xxx = getViewHeight(HUD_LAYER) / 2;
					ival = camera_pos.y >> FX32_SHIFT;
					if(ival < xxx)
					{
						camera_pos.y = FX32(xxx);
						direction_power[CD_UP] = 0;
						direction_power[CD_DOWN]= 0;
						direction_power[CD_LEFT]= 0;
						direction_power[CD_RIGHT]= 0;
					}
					xxx = getViewWidth(HUD_LAYER) / 2;
					ival = camera_pos.x >> FX32_SHIFT;
					if(ival < xxx)
					{
						camera_pos.x = FX32(xxx);
						direction_power[CD_UP] = 0;
						direction_power[CD_DOWN]= 0;
						direction_power[CD_LEFT]= 0;
						direction_power[CD_RIGHT]= 0;
					}
					xxx = getMapHeight() - getViewHeight(HUD_LAYER) / 2;
					ival = camera_pos.y >> FX32_SHIFT;
					if(ival > xxx)
					{
						camera_pos.y = FX32(xxx);
						direction_power[CD_UP] = 0;
						direction_power[CD_DOWN]= 0;
						direction_power[CD_LEFT]= 0;
						direction_power[CD_RIGHT]= 0;
					}
					xxx = getMapWidth() - getViewWidth(HUD_LAYER) / 2;
					ival = camera_pos.x >> FX32_SHIFT;
					if(ival > xxx)
					{
						camera_pos.x = FX32(xxx);
						direction_power[CD_UP] = 0;
						direction_power[CD_DOWN]= 0;
						direction_power[CD_LEFT]= 0;
						direction_power[CD_RIGHT]= 0;
					}

					c_c++;
				}
				break;	

				case M0_TXTBOX_6:
				{
					wchar aaa[16];
					wchar bbb[16];
					bbb[0] = L'x';
					bbb[1] = L'=';
					bbb[2] = L'%';
					bbb[3] = L'd';
					bbb[4] = L'\0';
					STD_WSprintf(aaa, bbb, tp_data.point[0].mX);
					txbSetDynamicTextLine(M0_TXTBOX_6, 0, aaa);
					bbb[0] = L'y';
					STD_WSprintf(aaa, bbb, tp_data.point[0].mY);
					txbSetDynamicTextLine(M0_TXTBOX_6, 1, aaa);
					txbSetDynamicTextLine(M0_TXTBOX_6, 2, getStaticText(TXT_STRING1, 0));
					txbSetDynamicTextLine(M0_TXTBOX_6, 3, getStaticText(TXT_STRING2, 0));
				}
			}
		break;
		
		case HUD_LAYER:
		{
			switch(iId)
			{
				case M1_BUTTON_2:
					{
						struct fxVec2 scale;
						prSetCustomAlpha(iId, button_alpha);
						button_alpha -= 1;
						if(button_alpha < 15)
						{
							button_alpha = 31;
						}
						scale.x = FX32_ONE + FX_Div(FX32(31 - button_alpha), FX32(31));
						scale.y = scale.x;
						prSetCustomScale(iId, scale);
					}
				break;

				case M1_TXTBOX_FPS: // (hack) or text field on low memory warning button, same ID
				{
					fps_tick++;
					if(fps_timer >= 1000)
					{
						wchar aaa[16];
						wchar bbb[16];
						bbb[0] = L'f';
						bbb[1] = L'p';
						bbb[2] = L's';
						bbb[3] = L'=';
						bbb[4] = L'%';
						bbb[5] = L'd';
						bbb[6] = L'\0';
						STD_WSprintf(aaa, bbb, fps_tick);
						txbSetDynamicTextLine(M1_TXTBOX_FPS, 0, aaa);
						fps_tick = fps_timer = 0;
					}		
				}
			}
		}
	}
}
//----------------------------------------------------------------------------------

//CALLBACK FUNCTIONS

//----------------------------------------------------------------------------------

static void onEngineEvent(const struct EventCallbackData *pData)
{
	switch(pData->eventType)
	{
		case EVENT_TYPE_ON_ANIMATION:
		case EVENT_TYPE_ON_DIRECTOR:
        break;
    
		case  EVENT_TYPE_ON_END_ASYNH_LOAD_DATA:
			switch(pData->eventId)
			{
				case LOAD_TYPE_ENDLOADDATATASK:
					if(pData->initiatorId == LM_INIT)
					{
						gameState = gstLOADING;
					}
				break;
				case LOAD_TYPE_MAPDATA:
					if(pData->initiatorId == LM_LOAD)
					{
						switch(pData->layer)
						{
							case HUD_LAYER:
								onLevelHudLoad();
							break;
							case GAME_LAYER:
								onLevelMapLoad();
						}
					}
			}
		break;
		
		case EVENT_TYPE_ON_END_ASYNH_LOAD_RESOURSES:
			//setVisible(HUD_LAYER, TRUE);
			//setVisible(GAME_LAYER, TRUE);
			gameState = gstGAME;

#ifdef DEMO_SOUND
			{
				struct SoundHandle *sh;
				struct SoundData sd;
				sd.Id = M0_SND_BG_MUSIC;
				sd.Looped = TRUE;
				sd.Type = SOUND_TYPE_BGM2;
				sd.Volume = FX32(0.5f);
				sh = sndPlay(&sd);
			}
#endif
	}
}
//----------------------------------------------------------------------------------
