/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include "lib/tengine_low.h"
#include "touchpad.h"
#include "guispeedbutton.h"
#include "guicontainer.h"
#include "guifactory.h"
#include "gui_low.h"

//---------------------------------------------------------------------------

void _GUISpeedButton_GUIObject2D_SetFocusFlag(struct GUIObject2D *obj, BOOL state);
void _GUISpeedButton_GUIObject2D_ProcessInput(struct GUIObject2D *obj, const TouchPadData *tpData);
s32 _GUISpeedButton_GUIObject2D_GetTabOrder(struct GUIObject2D *obj);
void _GUISpeedButton_GUIObject2D_SetEnable(struct GUIObject2D *obj, BOOL state);
void _GUISpeedButton_GUIBaseButton_ButtonPress(struct GUIBaseButton *obj);
void _GUISpeedButton_GUIBaseButton_ButtonRelease(struct GUIBaseButton *obj);
void _GUISpeedButton_GUIBaseButton_SetEnableInput(struct GUIBaseButton *obj, BOOL val);

//---------------------------------------------------------------------------

void GUISpeedButton_protected_Init(struct GUISpeedButton *self, const struct GUISpeedButtonInitParameters* params, s32 mapObjID, struct GUIContainer* parent)
{
	SDK_ASSERT(mapObjID >= 0);

	GUIBaseButton_protected_Init(&self->baseButton);
	
	self->baseButton.guiObject.mType = GUI_TYPE_SPEEDBUTTON;
	self->baseButton.guiObject.mpObject = self;

	self->mpParams = params;
	SDK_NULL_ASSERT(self->mpParams);
	SDK_ASSERT(self->mpParams->baseInitParameters.mapObjTypeID[BUTTON_MAPOBJ_ID_IDLE] != -1);
	self->baseButton.mpBaseParams = &self->mpParams->baseInitParameters;

	self->baseButton.mAction.mType = AT_DO_NOTHING;
	self->baseButton.guiObject.mMapObjID = mapObjID;
	self->mTxtMapPbjId = -1;
	self->mHotkeyDown = FALSE;
	self->baseButton.guiObject.mpParent = parent;
	self->mHotKey = TYPE_KEY_NONE;

	self->baseButton.guiObject.mpProcessInput = _GUISpeedButton_GUIObject2D_ProcessInput;
	self->baseButton.guiObject.mpGetTabOrder = _GUISpeedButton_GUIObject2D_GetTabOrder;
	self->baseButton.guiObject.mpSetEnable = _GUISpeedButton_GUIObject2D_SetEnable;
	self->baseButton.guiObject.mpSetFocusFlag = _GUISpeedButton_GUIObject2D_SetFocusFlag;
	self->baseButton.guiObject.mpUpdate = NULL;
	self->baseButton.mTouchPointerId = -1;

	self->baseButton.mpButtonPress = _GUISpeedButton_GUIBaseButton_ButtonPress;
	self->baseButton.mpButtonRelease = _GUISpeedButton_GUIBaseButton_ButtonRelease;
	self->baseButton.mpSetEnableInput = _GUISpeedButton_GUIBaseButton_SetEnableInput;

	prChangeTypeLayer(self->baseButton.guiObject.mMapObjID, self->mpParams->baseInitParameters.mapObjTypeID[BUTTON_MAPOBJ_ID_IDLE], GUIContainer_GetLayer(parent));
}
//---------------------------------------------------------------------------

void GUISpeedButton_protected_Release(struct GUISpeedButton *self)
{
	GUIBaseButton_protected_Release(&self->baseButton);
	MI_CpuClear8(self, sizeof(struct GUISpeedButton));
}
//---------------------------------------------------------------------------

s32 GUISpeedButton_GetMapObjId(const struct GUISpeedButton *self)
{
	return GUIObject2D_GetMapObjId(&self->baseButton.guiObject);
}
//---------------------------------------------------------------------------

s32 GUISpeedButton_GetTabOrder(const struct GUISpeedButton *self)
{
	return GUIObject2D_GetTabOrder(&self->baseButton.guiObject);
}
//---------------------------------------------------------------------------

void GUISpeedButton_SetEnable(struct GUISpeedButton *self, BOOL value)
{
	GUIObject2D_SetEnable(&self->baseButton.guiObject, value);
}
//---------------------------------------------------------------------------

void GUISpeedButton_Click(struct GUISpeedButton *self)
{
	GUIBaseButton_Click(&self->baseButton);
}
//---------------------------------------------------------------------------

BOOL GUISpeedButton_IsEnable(const struct GUISpeedButton *self)
{
	return GUIObject2D_IsEnable(&self->baseButton.guiObject);
}
//---------------------------------------------------------------------------

void GUISpeedButton_SetAction(struct GUISpeedButton *self, const struct GUIAction *action)
{
	GUIBaseButton_SetAction(&self->baseButton, action);
}
//---------------------------------------------------------------------------

struct GUIAction GUISpeedButton_GetAction(const struct GUISpeedButton *self)
{
	return GUIBaseButton_GetAction(&self->baseButton);
}
//---------------------------------------------------------------------------

void GUISpeedButton_SetPosition(struct GUISpeedButton *self, fx32 x, fx32 y)
{
	GUIObject2D_SetPosition(&self->baseButton.guiObject, x, y);
}
//---------------------------------------------------------------------------

void GUISpeedButton_GetPosition(const struct GUISpeedButton *self, fx32 *x, fx32 *y)
{
	GUIObject2D_GetPosition(&self->baseButton.guiObject, x, y);
}
//---------------------------------------------------------------------------

void GUISpeedButton_GetCollideRect(const struct GUISpeedButton *self, fx32 rect[RECT_SIZE])
{
	GUIBaseButton_GetCollideRect(&self->baseButton, rect);
}
//---------------------------------------------------------------------------

void GUISpeedButton_protected_SetTextMapObjId(struct GUISpeedButton *self, s32 id)
{
	self->mTxtMapPbjId = id;
}
//---------------------------------------------------------------------------

s32 _GUISpeedButton_GUIObject2D_GetTabOrder(struct GUIObject2D *obj)
{
    (void)obj;
	return -1;
}  
//---------------------------------------------------------------------------

void _GUISpeedButton_GUIObject2D_SetFocusFlag(struct GUIObject2D *obj, BOOL state)
{
    (void)obj;
    (void)state;
}
//---------------------------------------------------------------------------

void _GUISpeedButton_GUIBaseButton_ButtonPress(struct GUIBaseButton *obj)
{
	struct GUISpeedButton *self = (struct GUISpeedButton *)obj;
	if(GUIObject2D_IsEnable(&self->baseButton.guiObject))
	{
		SDK_NULL_ASSERT(self->mpParams);
		if(self->mpParams->baseInitParameters.mapObjTypeID[BUTTON_MAPOBJ_ID_PRESSED] != -1)
		{
			prChangeTypeLayer(self->baseButton.guiObject.mMapObjID, self->mpParams->baseInitParameters.mapObjTypeID[BUTTON_MAPOBJ_ID_PRESSED], GUIContainer_GetLayer(self->baseButton.guiObject.mpParent));
		}
	}
}
//---------------------------------------------------------------------------

void _GUISpeedButton_GUIBaseButton_ButtonRelease(struct GUIBaseButton *obj)
{
	struct GUISpeedButton *self = (struct GUISpeedButton *)obj;
	if(self->baseButton.mState == ST_PRESSED)
	{
		SDK_NULL_ASSERT(self->mpParams);
		SDK_ASSERT(self->mpParams->baseInitParameters.mapObjTypeID[BUTTON_MAPOBJ_ID_IDLE] != -1);
		prChangeTypeLayer(self->baseButton.guiObject.mMapObjID, self->mpParams->baseInitParameters.mapObjTypeID[BUTTON_MAPOBJ_ID_IDLE], GUIContainer_GetLayer(self->baseButton.guiObject.mpParent));
	}
}
//---------------------------------------------------------------------------

void _GUISpeedButton_GUIObject2D_SetEnable(struct GUIObject2D *obj, BOOL state)
{
	u8 i;
	struct GUISpeedButton *sb = (struct GUISpeedButton*)obj;
	SDK_NULL_ASSERT(sb->mpParams);
	SDK_ASSERT(sb->mpParams->baseInitParameters.enabledPrpId != -1);
	prSetPropertyLayer(GUIObject2D_GetMapObjId(&sb->baseButton.guiObject),
						sb->mpParams->baseInitParameters.enabledPrpId,
							FX32((char)state),
								GUIContainer_GetLayer(sb->baseButton.guiObject.mpParent));
	for(i = 0; i < 3; i++)
	{
		s32 ch_idx = prGetJoinedChildLayer(GUIObject2D_GetMapObjId(&sb->baseButton.guiObject), i, 
										GUIContainer_GetLayer(sb->baseButton.guiObject.mpParent));
		if(NONE_MAP_IDX != ch_idx)
		{
			prSetPropertyLayer(ch_idx,
					sb->mpParams->baseInitParameters.enabledPrpId,
					FX32((char)state),
					GUIContainer_GetLayer(sb->baseButton.guiObject.mpParent));
		}
	}
	sb->baseButton.mPointerState = PST_UP;
	if(sb->baseButton.mState != ST_NONE)
    {
		sb->baseButton.mState = ST_NONE;
		SDK_ASSERT(sb->mpParams->baseInitParameters.mapObjTypeID[BUTTON_MAPOBJ_ID_IDLE] != -1);
		prChangeTypeLayer(GUIObject2D_GetMapObjId(&sb->baseButton.guiObject), sb->mpParams->baseInitParameters.mapObjTypeID[BUTTON_MAPOBJ_ID_IDLE], GUIContainer_GetLayer(sb->baseButton.guiObject.mpParent));
	}
}
//---------------------------------------------------------------------------

void _GUISpeedButton_GUIObject2D_ProcessInput(struct GUIObject2D *obj, const TouchPadData *tpData)
{
	s32 stp, tp;
	struct fxVec2 rect[4];
	fx32 r[RECT_SIZE];
	BOOL tpfound;
	struct GUISpeedButton *sb = (struct GUISpeedButton*)obj;
	
	if(!GUIObject2D_IsEnable(&sb->baseButton.guiObject) || !sb->baseButton.mEnableInput)
    {
        return;
    }
    
    SDK_NULL_ASSERT(sb->mpParams);
	if(IsButtonDown(sb->mHotKey) || IsButtonPress(sb->mHotKey))
    {
		sb->baseButton.mPointerState = IsButtonDown(sb->mHotKey) ? PST_DOWN : PST_ENTER;
		GUIBaseButton_protected_ButtonPress(&sb->baseButton, TRUE);
        if(IsButtonDown(sb->mHotKey))
        {
			GUIContainer_protected_SetTrigged(sb->baseButton.guiObject.mpParent, &sb->baseButton.guiObject, 0);
			sb->baseButton.mTouchPointerId = 0;
			sb->mHotkeyDown = TRUE;
		}
		return;
	}
	else if(IsButtonUp(sb->mHotKey))
	{
		GUIBaseButton_protected_ButtonRelease(&sb->baseButton, TRUE);
		if(sb->mHotkeyDown)
		{
			GUIContainer_protected_SetTrigged(sb->baseButton.guiObject.mpParent, NULL, 0);
			GUIBaseButton_Click(&sb->baseButton);
		}
		sb->mHotkeyDown = FALSE;
		sb->baseButton.mTouchPointerId = -1;
		return;
	}

	GUISpeedButton_GetCollideRect(sb, r);
	rect[0].x = r[RECT_LEFT_TOP_X];
	rect[0].y = r[RECT_LEFT_TOP_Y];
	rect[1].x = r[RECT_RIGHT_TOP_X];
	rect[1].y = r[RECT_RIGHT_TOP_Y];
	rect[2].x = r[RECT_RIGHT_BOTTOM_X];
	rect[2].y = r[RECT_RIGHT_BOTTOM_Y];
	rect[3].x = r[RECT_LEFT_BOTTOM_X];
	rect[3].y = r[RECT_LEFT_BOTTOM_Y];
	stp = tp = sb->baseButton.mTouchPointerId;
	tpfound = FALSE;
	if(tp < 0)
	{
		stp = tp = 0;
	}
	do
	{
		if(tpData->point[tp].mTouch == TRUE)
		{
			s32 vt, vl, vw, vh;
			s32 layer = GUIContainer_GetLayer(sb->baseButton.guiObject.mpParent);
			vt = getViewTop(layer);
			vl = getViewLeft(layer);
			vw = getViewWidth(layer);
			vh = getViewHeight(layer);
			if(tpData->point[tp].mX > vl && tpData->point[tp].mY > vt &&
					tpData->point[tp].mX < vl + vw && tpData->point[tp].mY < vt + vh)
			{
				fx32 scale;
				struct fxVec2 point;
				scale = getRenderPlaneScale(layer);
				point.x = FX_Div(FX32(tpData->point[tp].mX - vl), scale) + getViewOffsetX(layer);
				point.y = FX_Div(FX32(tpData->point[tp].mY - vt), scale) + getViewOffsetY(layer);
				if(mthIntersectPointAndRectangle(point, rect))
				{
					tpfound = TRUE;
					sb->baseButton.mPointerState = tpData->point[tp].mTrg ? PST_ENTER : PST_DOWN;
					GUIContainer_protected_SwapTriggedPointers(sb->baseButton.guiObject.mpParent, sb->baseButton.mTouchPointerId, tp);
					sb->baseButton.mTouchPointerId = tp;
					break;
				}
			}
		}
		if(++tp == TOUCH_POINTS_MAX)
		{
			tp = 0;
		}
	}
	while(stp != tp);

	if(sb->baseButton.mTouchPointerId >= 0)
	{
		if(tpData->point[sb->baseButton.mTouchPointerId].mTouch == TRUE)
		{
            if(tpfound)
			{
				const struct GUIObject2D* obj = GUIContainer_protected_GetTrigged(sb->baseButton.guiObject.mpParent, sb->baseButton.mTouchPointerId);
				if(obj == NULL || obj == &sb->baseButton.guiObject)
				{
					GUIBaseButton_protected_ButtonPress(&sb->baseButton, TRUE);
					GUIContainer_protected_SetTrigged(sb->baseButton.guiObject.mpParent, &sb->baseButton.guiObject, sb->baseButton.mTouchPointerId);
				}
			}
			else
			{
				if(sb->baseButton.mState == ST_PRESSED)
				{
					SDK_NULL_ASSERT(sb->mpParams);
					sb->baseButton.mState = ST_NONE;
					SDK_ASSERT(sb->mpParams->baseInitParameters.mapObjTypeID[BUTTON_MAPOBJ_ID_IDLE] != -1);
					prChangeTypeLayer(GUIObject2D_GetMapObjId(&sb->baseButton.guiObject), sb->mpParams->baseInitParameters.mapObjTypeID[BUTTON_MAPOBJ_ID_IDLE], GUIContainer_GetLayer(sb->baseButton.guiObject.mpParent));
					GUIContainer_protected_SetTrigged(sb->baseButton.guiObject.mpParent, NULL, sb->baseButton.mTouchPointerId);
					sb->baseButton.mTouchPointerId = -1;

					if(sb->baseButton.guiObject.mpParent != NULL && sb->baseButton.guiObject.mpParent->mpEventHandler != NULL)
					{
						u32 layer;
						struct GUIEvent e;
						e.mpSender = &sb->baseButton.guiObject;
						e.mpAction = &sb->baseButton.mAction;
						e.mState = sb->baseButton.mState;
						e.mPointerState = PST_LEAVE;
						e.mPointerId = sb->baseButton.mTouchPointerId;
						layer = getActiveLayer();
						_setActiveLayer(GUIContainer_GetLayer(sb->baseButton.guiObject.mpParent));
						sb->baseButton.guiObject.mpParent->mpEventHandler(&e);
						_setActiveLayer(layer);
					}
				}
			}
		}
		else
		{
			BOOL isPressed = sb->baseButton.mState == ST_PRESSED;
			GUIBaseButton_protected_ButtonRelease(&sb->baseButton, TRUE);
			if(GUIContainer_protected_GetTrigged(sb->baseButton.guiObject.mpParent, sb->baseButton.mTouchPointerId) == &sb->baseButton.guiObject)
			{
				GUIContainer_protected_SetTrigged(sb->baseButton.guiObject.mpParent, NULL, sb->baseButton.mTouchPointerId);
				if(isPressed)
				{
					GUISpeedButton_Click(sb);
				}
			}
			sb->baseButton.mTouchPointerId = -1;
		}
	}
}
//---------------------------------------------------------------------------

void GUISpeedButton_SetText(struct GUISpeedButton *self, const wchar *text)
{
	if(self->mTxtMapPbjId >= 0)
	{
		txbSetDynamicTextLayer(self->mTxtMapPbjId, text, GUIContainer_GetLayer(self->baseButton.guiObject.mpParent));
	}
}
//---------------------------------------------------------------------------

void GUISpeedButton_SetHotkey(struct GUISpeedButton *self, enum KeyType key)
{
	self->mHotKey = key;
}
//---------------------------------------------------------------------------

enum KeyType GUISpeedButton_GetHotkey(const struct GUISpeedButton *self)
{
    return self->mHotKey;
}
//---------------------------------------------------------------------------

void GUISpeedButton_SetEnableInput(struct GUISpeedButton *self, BOOL val)
{
	GUIBaseButton_EnableInput(&self->baseButton, val);
}
//---------------------------------------------------------------------------

void _GUISpeedButton_GUIBaseButton_SetEnableInput(struct GUIBaseButton *obj, BOOL val)
{
	(void)obj;
	(void)val;
}
//---------------------------------------------------------------------------

BOOL GUISpeedButton_IsEnableInput(const struct GUISpeedButton *self)
{
	return GUIBaseButton_IsEnableInput(&self->baseButton);
}
//---------------------------------------------------------------------------