/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include "system.h"
#include "filesystem.h"
#include "loadhelpers.h"
#include "memory.h"
#include "texts.h"
#ifdef ANDROID_NDK
 #include "lib/a_filesystem.h"
#endif
#if defined WINDOWS_APP || defined NIX_APP || defined IOS_APP
 #include "lib/stdio_filesystem.h"
#endif

BOOL gsFilesystemError = FALSE;

static u8* LoadFileToMainMem_(const char* pFname, const u32* iDataSize, u32* oFileSize, u8* iMemory);

//---------------------------------------------------------------------------

void InitFileSystem(struct InitFileSystemData* data)
{
	FILESYSTEMFN(InitFileSystem)(data);
}
//-------------------------------------------------------------------------------------------

void ReleaseFileSystem(void)
{
	FILESYSTEMFN(ReleaseFileSystem)();
}
//-------------------------------------------------------------------------------------------

BOOL IsFileSystemError(void)
{
	return gsFilesystemError;
}
//-------------------------------------------------------------------------------------------

#ifdef SDK_DEBUG
    #define LoadXXXX(ppData, pFname, unpack, print) \
        u8*   pFile = NULL;                                 \
        SDK_NULL_ASSERT( ppData );                          \
        SDK_NULL_ASSERT( pFname );                          \
        pFile = LoadFileToMainMem_(pFname, NULL, NULL, NULL);\
        if( pFile != NULL )                                 \
        {                                                   \
            if( unpack( pFile, ppData ) )                   \
            {                                               \
                print( *ppData );                           \
                return pFile;                               \
            }                                               \
            FREE( pFile );                                  \
        }                                                   \
        return NULL;                                        
#else

    #define LoadXXXX(ppData, pFname, unpack, print) \
        u8*   pFile = NULL;                                 \
        SDK_NULL_ASSERT( ppData );                          \
        SDK_NULL_ASSERT( pFname );                          \
        pFile = LoadFileToMainMem_(pFname, NULL, NULL, NULL);\
        if( pFile != NULL )                                 \
        {                                                   \
            if( unpack( pFile, ppData ) )                   \
            {                                               \
                return pFile;                               \
            }                                               \
            FREE( pFile );                                  \
        }                                                   \
        return NULL;   
#endif

//-------------------------------------------------------------------------------------------

u32 FileSize(const char* pFname)
{
	s16 fHandle;
	u32 sz;
	SDK_NULL_ASSERT(pFname);
	SDK_ASSERT(FILESYSTEMFN(IsFileSystemInit)()); //InitFileSystem
	if(gsFilesystemError)
	{
		return 0;
	}
	fHandle = FILESYSTEMFN(fopen)(pFname);
	if(fHandle >= 0)
    {
		sz = FILESYSTEMFN(fsize)(fHandle);
		FILESYSTEMFN(fclose(fHandle));
    }
    else
    {
        sz = 0;
		gsFilesystemError = TRUE;
		OS_Warning("Can't find the file : %s\n", pFname);
		SDK_ASSERT(0);
    }
	return sz;
}
//-------------------------------------------------------------------------------------------

static u8* LoadFileToMainMem_(const char* pFname, const u32* iDataSize, u32* oFileSize, u8* iMemory)
{
	s16 fHandle;
	u8* pFile = NULL;
	BOOL is_m_alloc = FALSE;

	SDK_NULL_ASSERT(pFname);
	SDK_ASSERT(FILESYSTEMFN(IsFileSystemInit)());

	if(gsFilesystemError)
	{
		return NULL;
	}

	fHandle = FILESYSTEMFN(fopen)(pFname);
	if(fHandle >= 0)
    {
		s32 szFile = 0;
#ifndef SDK_DEBUG
		if(iDataSize == NULL)
#endif
		{
			szFile = FILESYSTEMFN(fsize)(fHandle);	
		}
		if(oFileSize != NULL)
        {
			*oFileSize = szFile;
        }
		if(iDataSize != NULL)
		{
#ifdef SDK_DEBUG
			SDK_ASSERT(*iDataSize <= (u32)szFile);
#endif
			szFile = *iDataSize; 
		}
		SDK_ASSERT(szFile != 0);
		{
			if(iMemory != NULL)
			{
				SDK_ASSERT(iMemory != NULL && iDataSize != NULL);
				pFile = iMemory;
			}
			else
			{
				char tbdstr[PTCOLLECTOR_MARK_SIZE];
#ifdef FRAME_ALLOCATOR
				s32 tbdstr_pos, tbdfname_pos;
				STD_StrCpy(tbdstr, "LoadFileToMainMem:");
				tbdstr_pos = STD_StrLen(tbdstr);
				tbdfname_pos = 0;
				while(pFname[tbdfname_pos] != 0 && tbdstr_pos < PTCOLLECTOR_MARK_SIZE)
				{
					tbdstr[tbdstr_pos] = pFname[tbdfname_pos];
					tbdfname_pos++;
					tbdstr_pos++;
				}
				if(tbdstr_pos == PTCOLLECTOR_MARK_SIZE)
				{
					tbdstr_pos = PTCOLLECTOR_MARK_SIZE - 1;
				}
				tbdstr[tbdstr_pos] = 0;
#endif
				pFile = (u8*)MALLOC(szFile, tbdstr);
				is_m_alloc = TRUE;
			}
		}
		SDK_NULL_ASSERT(pFile);
		if(szFile != FILESYSTEMFN(fread)(pFile, szFile, fHandle)) 
		{
			gsFilesystemError = TRUE;
			OS_Warning("Error while reading file : %s\n", pFname);
			SDK_ASSERT(0);
			if(is_m_alloc)
			{
				FREE(pFile);
				pFile = NULL;
			}
		}
		FILESYSTEMFN(fclose(fHandle));
    }
    else
    {
        gsFilesystemError = TRUE;
		OS_Warning("Can't find the file : %s\n", pFname);
		SDK_ASSERT(0);
    }
    return pFile;
}
//-------------------------------------------------------------------------------------------

u8* LoadFile(const char* pFname, u32* oFileSize)
{
	return LoadFileToMainMem_(pFname, NULL, oFileSize, NULL);
}
//-------------------------------------------------------------------------------------------

void LoadFileToSpecificMemory(const char* pFname, u8* pPointer, u32 iSize)
{
	LoadFileToMainMem_(pFname, &iSize, NULL, pPointer);
}
//-------------------------------------------------------------------------------------------

void InitBMPImage(struct BMPImage *img)
{
	MI_CpuFill8(img, 0, sizeof(struct BMPImage)); 
}
//-------------------------------------------------------------------------------------------

void DeleteBMPImage(struct BMPImage *img)
{
	SDK_NULL_ASSERT(img);
	if(img->mpA5Data != NULL)
	{
		FREE(img->mpA5Data);
		img->mpA5Data = NULL;
	}
	if(img->data.mpData256 != NULL)
	{
		FREE(img->data.mpData256);
		img->data.mpData256 = NULL;
	}
	if(img->mpACMData != NULL)
	{
		FREE(img->mpACMData);
		img->mpACMData = NULL;
	}
	FREE(img);
}
//-------------------------------------------------------------------------------------------

void LoadBMPImage(const char* pFname, struct BMPImage** img, BOOL updateImage)
{
	s16 fHandle;

	SDK_NULL_ASSERT(pFname);
	SDK_ASSERT(FILESYSTEMFN(IsFileSystemInit)());

	if(gsFilesystemError)
	{
		return;
	}
	
	fHandle = FILESYSTEMFN(fopen)(pFname);
	if(fHandle >= 0)
    {
		struct BMPImage th;
		if(sizeof(struct BMPImage) != (u32)FILESYSTEMFN(fread)((u8*)&th, sizeof(struct BMPImage), fHandle))
		{
			gsFilesystemError = TRUE;
			OS_Warning("Error while reading BMPImage file : %s\n", pFname);
			SDK_ASSERT(0);
			return;
		}
		if(updateImage)
		{
			SDK_NULL_ASSERT(*img);
			SDK_ASSERT((*img)->mDataSize == th.mDataSize);
			SDK_ASSERT((*img)->mACMDataSize == th.mACMDataSize);
			SDK_ASSERT((*img)->mA5DataSize == th.mA5DataSize);
		}
		else
		{
			char tbdstr[PTCOLLECTOR_MARK_SIZE];
#ifdef FRAME_ALLOCATOR
			s32 tbdstr_pos, tbdfname_pos;
			STD_StrCpy(tbdstr, "LoadBMP:img:");
			tbdstr_pos = STD_StrLen(tbdstr);
			tbdfname_pos = 0;
			while(pFname[tbdfname_pos] != 0 && tbdstr_pos < PTCOLLECTOR_MARK_SIZE)
			{
				tbdstr[tbdstr_pos] = pFname[tbdfname_pos];
				tbdfname_pos++;
				tbdstr_pos++;
			}
			if(tbdstr_pos == PTCOLLECTOR_MARK_SIZE)
			{
				tbdstr_pos = PTCOLLECTOR_MARK_SIZE - 1;
			}
			tbdstr[tbdstr_pos] = 0;
#endif
			*img = (struct BMPImage*)MALLOC(sizeof(struct BMPImage), tbdstr);
			SDK_NULL_ASSERT(*img);
			MI_CpuCopy8(&th, *img, sizeof(struct BMPImage));  
		}
		if((*img)->mACMDataSize != 0)
		{
			if(!updateImage)
			{
				char tbdstr[PTCOLLECTOR_MARK_SIZE];
#ifdef FRAME_ALLOCATOR
				s32 tbdstr_pos, tbdfname_pos;
				STD_StrCpy(tbdstr, "LoadBMP:acm:");
				tbdstr_pos = STD_StrLen(tbdstr);
				tbdfname_pos = 0;
				while(pFname[tbdfname_pos] != 0 && tbdstr_pos < PTCOLLECTOR_MARK_SIZE)
				{
					tbdstr[tbdstr_pos] = pFname[tbdfname_pos];
					tbdfname_pos++;
					tbdstr_pos++;
				}
				if(tbdstr_pos == PTCOLLECTOR_MARK_SIZE)
				{
					tbdstr_pos = PTCOLLECTOR_MARK_SIZE - 1;
				}
				tbdstr[tbdstr_pos] = 0;
#endif
				(*img)->mpACMData = (u8*)MALLOC((*img)->mACMDataSize, tbdstr);
			}
			SDK_NULL_ASSERT((*img)->mpACMData);
			if((*img)->mACMDataSize != (u32)FILESYSTEMFN(fread)((*img)->mpACMData, (*img)->mACMDataSize, fHandle)) 
			{
				gsFilesystemError = TRUE;
				OS_Warning("Error while reading BMPImage file : %s\n", pFname);
				SDK_ASSERT(0);
				if(!updateImage)
				{
					FREE((*img)->mpACMData);
					(*img)->mpACMData = NULL;
					FREE(*img);
					*img = NULL;
				}
				return;
			}
		}
		SDK_ASSERT((*img)->mDataSize);
		if(!updateImage)
		{
			char tbdstr[PTCOLLECTOR_MARK_SIZE];
#ifdef FRAME_ALLOCATOR
			s32 tbdstr_pos, tbdfname_pos;
			STD_StrCpy(tbdstr, "LoadBMP:data:");
			tbdstr_pos = STD_StrLen(tbdstr);
			tbdfname_pos = 0;
			while(pFname[tbdfname_pos] != 0 && tbdstr_pos < PTCOLLECTOR_MARK_SIZE)
			{
				tbdstr[tbdstr_pos] = pFname[tbdfname_pos];
				tbdfname_pos++;
				tbdstr_pos++;
			}
			if(tbdstr_pos == PTCOLLECTOR_MARK_SIZE)
			{
				tbdstr_pos = PTCOLLECTOR_MARK_SIZE - 1;
			}
			tbdstr[tbdstr_pos] = 0;
#endif
			(*img)->data.mpData256 = (u8*)MALLOC((*img)->mDataSize, tbdstr);
		}
		SDK_NULL_ASSERT((*img)->data.mpData256);
		if((*img)->mDataSize != (u32)FILESYSTEMFN(fread)((u8*)(*img)->data.mpData256, (*img)->mDataSize, fHandle)) 
		{
			gsFilesystemError = TRUE;
			OS_Warning("Error while reading BMPImage file : %s\n", pFname);
			SDK_ASSERT(0);
			if(!updateImage)
			{
				FREE((*img)->data.mpData256);
				(*img)->data.mpData256 = NULL;
				FREE((*img)->mpACMData);
				(*img)->mpACMData = NULL;
				FREE(*img);
				*img = NULL;
			}
			return;
		}
		if((*img)->mA5DataSize != 0)
		{
			if(!updateImage)
			{
				char tbdstr[PTCOLLECTOR_MARK_SIZE];
#ifdef FRAME_ALLOCATOR
				s32 tbdstr_pos, tbdfname_pos;
				STD_StrCpy(tbdstr, "LoadBMP:a5:");
				tbdstr_pos = STD_StrLen(tbdstr);
				tbdfname_pos = 0;
				while(pFname[tbdfname_pos] != 0 && tbdstr_pos < PTCOLLECTOR_MARK_SIZE)
				{
					tbdstr[tbdstr_pos] = pFname[tbdfname_pos];
					tbdfname_pos++;
					tbdstr_pos++;
				}
				if(tbdstr_pos == PTCOLLECTOR_MARK_SIZE)
				{
					tbdstr_pos = PTCOLLECTOR_MARK_SIZE - 1;
				}
				tbdstr[tbdstr_pos] = 0;
#endif
				(*img)->mpA5Data = (u16*)MALLOC((*img)->mA5DataSize, tbdstr);
			}
			SDK_NULL_ASSERT((*img)->mpA5Data);
			if((*img)->mA5DataSize != (u32)FILESYSTEMFN(fread)((u8*)(*img)->mpA5Data, (*img)->mA5DataSize, fHandle)) 
			{
				gsFilesystemError = TRUE;
				OS_Warning("Error while reading BMPImage file : %s\n", pFname);
				SDK_ASSERT(0);
				if(!updateImage)
				{
					FREE((*img)->mpA5Data);
					(*img)->mpA5Data = NULL;
					FREE((*img)->data.mpData256);
					(*img)->data.mpData256 = NULL;
					FREE((*img)->mpACMData);
					(*img)->mpACMData = NULL;
					FREE(*img);
					*img = NULL;
				}
				return;
			}
		}
		FILESYSTEMFN(fclose(fHandle));
    }
    else
    {
        gsFilesystemError = TRUE;
		OS_Warning("Can't find BMPImage file : %s\n", pFname);
		SDK_ASSERT(0);
    }
}
//-------------------------------------------------------------------------------------------

u8* LoadBMPPalette(BMPPalette** ppPalette, const char* pFname, u32* oFileSize)
{
    u8* res = LoadFile(pFname, oFileSize);
    *ppPalette = (struct BMPPalette*)res;
	SDK_ASSERT((*ppPalette)->mType == 1);
    (*ppPalette)->mpData = (GXRgb*)(res + 8);
    return res;
}
//-------------------------------------------------------------------------------------------

#ifdef NITRO_SDK
u8* LoadNCER(struct NNSG2dCellDataBank** ppData, const char* pFname)
{
    LoadXXXX(ppData, pFname, NNS_G2dGetUnpackedCellBank, NNS_G2dPrintCellBank);
}
//-------------------------------------------------------------------------------------------

u8* LoadNCGR(struct NNSG2dCharacterData** ppData, const char* pFname)
{
    LoadXXXX(ppData, pFname, NNS_G2dGetUnpackedCharacterData, NNS_G2dPrintCharacterData);
}
//-------------------------------------------------------------------------------------------

u8* LoadNCLR(struct NNSG2dPaletteData** ppData, const char* pFname)
{
    LoadXXXX(ppData, pFname, NNS_G2dGetUnpackedPaletteData, NNS_G2dPrintPaletteData);
}
//-------------------------------------------------------------------------------------------

u8* LoadNCBR(struct NNSG2dCharacterData** ppData, const char* pFname)
{
    LoadXXXX(ppData, pFname, NNS_G2dGetUnpackedCharacterData, NNS_G2dPrintCharacterData);
}
//-------------------------------------------------------------------------------------------

u8* LoadNCGRforBG(struct NNSG2dCharacterData** ppCharData, const char* pFname)
{
    LoadXXXX(ppCharData, pFname, NNS_G2dGetUnpackedBGCharacterData, NNS_G2dPrintCharacterData);
}
//-------------------------------------------------------------------------------------------
#endif

s32 _NBytesToInt(const u8* m, s32 N, s32 off)
{
    s32 in = 0, x = 0;
    while(in < 4 && N > 0)
    {
       x|=((m[off+in]&0xFF)<<(in<<3));
       in++;N--;
    }
    return x;
}
//----------------------------------------------------------------------------------

#ifdef NITRO_SDK
void TransferPaletteToMainVRAM(const struct BMPPalette* pPalette)
{
   SDK_NULL_ASSERT(pPalette);
   SDK_ASSERT(pPalette->mType == 1);
   DC_FlushRange(pPalette->mpData, pPalette->mSize);
   MI_DmaCopy32(DMA_NO, pPalette->mpData, (u8*)HW_BG_PLTT, (u32)(pPalette->mSize << 1)); 
}
//---------------------------------------------------------------------------

void TransferPaletteToSubVRAM(const struct BMPPalette* pPalette)
{
   SDK_NULL_ASSERT(pPalette);
   SDK_ASSERT(pPalette->mType == 1);
   DC_FlushRange(pPalette->mpData, pPalette->mSize);
   MI_DmaCopy32(DMA_NO, pPalette->mpData, (u8*)HW_DB_BG_PLTT, (u32)(pPalette->mSize << 1));
}
//---------------------------------------------------------------------------

void ChangePaletteColor(BMPPalette* pPalette, u8 iIndex, GXRgb iColor)
{
    SDK_NULL_ASSERT(pPalette);
    SDK_NULL_ASSERT(pPalette->mpData);
	SDK_ASSERT(pPalette->mType == 1);
    ((u16*)pPalette->mpData)[iIndex] = iColor;
}
//---------------------------------------------------------------------------

void ChangePaletteColorSubVRAM(u8 iIndex, GXRgb iColor)
{
    ((u16*)(HW_DB_BG_PLTT))[iIndex] = iColor;
}
//---------------------------------------------------------------------------

void ChangePaletteColorMainVRAM(u8 iIndex, GXRgb iColor)
{
    ((u16*)(HW_BG_PLTT))[iIndex] = iColor;
}
//---------------------------------------------------------------------------

void GetPalette256MainVRAM(BMPPalette *opPalette)
{
    SDK_NULL_ASSERT(opPalette);
    SDK_NULL_ASSERT(opPalette->mpData);
	opPalette->mType = 1;
	opPalette->mSize = 256;
    MI_DmaCopy32(DMA_NO, ((GXRgb*)(HW_BG_PLTT)), opPalette->mpData, 512);
}
//---------------------------------------------------------------------------

void GetPalette256SubVRAM(BMPPalette *opPalette)
{
    SDK_NULL_ASSERT(opPalette);
    SDK_NULL_ASSERT(opPalette->mpData);
	opPalette->mType = 1;
	opPalette->mSize = 256;
    MI_DmaCopy32(DMA_NO, ((GXRgb*)(HW_DB_BG_PLTT)), opPalette->mpData, 512);
}
//---------------------------------------------------------------------------
#endif