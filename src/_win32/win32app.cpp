/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include "platform.h"
#include <windowsx.h>

#ifndef _MSC_VER
	#pragma hdrstop
	#pragma argsused
#endif

#include <fcntl.h>
#include <io.h>
#include <iostream>
#include <fstream>

#include "lib/tengine_low.h"
#include "lib/a_touchpad.h"
#include "lib/a_gamepad.h"
#include "lib/render2dgl.h"
#include "filesystem.h"
#include "gamefield.h"
#include "lib/jobs_low.h"
#include "filesystem.h"
#include "gamepad.h"
#ifdef JOBS_IN_SEPARATE_THREAD
#include "pthread.h"
#endif
#include "profiler/profile.h"
#ifdef PROFILER_ENABLE
#include "static_allocator.h"
#endif

#ifndef _USE_OLD_IOSTREAMS
using namespace std;
#endif

static BOOL gAppAlive = TRUE;
static BOOL gWindowActive = FALSE;
static BOOL gsTengineInit = FALSE;
static BOOL gsRenderDevice = FALSE;
static BOOL lbuttonState = FALSE;
static _TCHAR gsStrPath[MAX_PATH * 2];

#define RES_PATH "assets/data/"
#define OUTPUT_CONSOLE 1

static const _TCHAR sAppName[] = _T("TEmulator");

//do not change values here, use your IDE debug command line instead
// for example -w1024 -h728 
#define DEFAULT_CLIENT_SIZE_W 512
#define DEFAULT_CLIENT_SIZE_H 512
static BOOL sLandscape = TRUE;
static s32 sWindowWidth0 = DEFAULT_CLIENT_SIZE_W;
static s32 sWindowHeight0 = DEFAULT_CLIENT_SIZE_H;
static s32 sWindowWidth = DEFAULT_CLIENT_SIZE_W;
static s32 sWindowHeight = DEFAULT_CLIENT_SIZE_H;
static s32 sClientWidth = DEFAULT_CLIENT_SIZE_W;
static s32 sClientHeight = DEFAULT_CLIENT_SIZE_H;

static HWND  shWnd = NULL;
static HDC   sghDC = NULL;
static HGLRC sghRC = NULL;
#ifdef JOBS_IN_SEPARATE_THREAD
static pthread_t sgThread;
#endif
//---------------------------------------------------------------------------

static BOOL SetupPixelFormat(HDC hdc);
static BOOL checkGLErrors(void);
static void _init_tengine();
static void _release_tengine();
static int _init_display();
static void _term_display();
static void OpenStdConsole();
static void CloseStdConsole();
#ifdef JOBS_IN_SEPARATE_THREAD
static void* ThreadTasks(void *t);
#endif

//---------------------------------------------------------------------------

#ifdef PROFILER_ENABLE
#define PROFILER_STATIC_ALLOCATOR_HEAP_SIZE (1024 * 1024 * 4)
static struct StaticAllocator sProfilerStaticAllocator;
static u8 sStaticAllocatorHeap[PROFILER_STATIC_ALLOCATOR_HEAP_SIZE];
#endif

//---------------------------------------------------------------------------

static BOOL checkGLErrors()
{
	GLenum error = glGetError();
	if (error != GL_NO_ERROR)
	{
		_TCHAR errorString[32];
		_stprintf(errorString, _T("0x%04x"), error);
		MessageBox(NULL, errorString, _T("GL Error"), MB_OK);
	}
	return error;
}
//---------------------------------------------------------------------------

static BOOL SetupPixelFormat(HDC hdc)
{
    int pixelformat;

	PIXELFORMATDESCRIPTOR pfd = {
        sizeof(PIXELFORMATDESCRIPTOR),  //  size of this pfd
        1,                     // version number
        PFD_DRAW_TO_WINDOW |   // support window
        PFD_SUPPORT_OPENGL |   // support OpenGL
        PFD_DOUBLEBUFFER,      // double buffered
        PFD_TYPE_RGBA,         // RGBA type
        32,                    // 32-bit color depth
        0, 0, 0, 0, 0, 0,      // color bits ignored
        0,                     // no alpha buffer
        0,                     // shift bit ignored
        0,                     // no accumulation buffer
        0, 0, 0, 0,            // accum bits ignored
        32,                    // 32-bit z-buffer
        0,                     // no stencil buffer
        0,                     // no auxiliary buffer
        PFD_MAIN_PLANE,        // main layer
        0,                     // reserved
        0, 0, 0                // layer masks ignored
	};
	if((pixelformat = ChoosePixelFormat(hdc, &pfd)) == 0 )
	{
		return FALSE;
	}
	if(SetPixelFormat(hdc, pixelformat, &pfd) == FALSE)
	{
		return FALSE;
	}
    return TRUE;
}
//---------------------------------------------------------------------------

static LRESULT CALLBACK wndProc(HWND wnd, UINT message,
								WPARAM wParam, LPARAM lParam)
{
    RECT rc;
    switch (message)
    {
		case WM_MOUSEMOVE:
			if((wParam & MK_LBUTTON) == MK_LBUTTON)
			{
				lbuttonState = 1;
				u16 xPos = GET_X_LPARAM(lParam);
				u16 yPos = GET_Y_LPARAM(lParam);
				onTouchPadMove(0, xPos, yPos);
			}
			return 0;

		case WM_LBUTTONDOWN:
			if((wParam & MK_LBUTTON) == MK_LBUTTON)
			{
				lbuttonState = 1;
				u16 xPos = GET_X_LPARAM(lParam);
				u16 yPos = GET_Y_LPARAM(lParam);
				onTouchPadDown(0, xPos, yPos);
			}
			return 0;

		case WM_LBUTTONUP:
			if(lbuttonState)
			{
				lbuttonState = 0;
				u16 xPos = GET_X_LPARAM(lParam);
				u16 yPos = GET_Y_LPARAM(lParam);
				onTouchPadUp(0, xPos, yPos);
			}
			return 0;

		case WM_KEYUP:
			switch(wParam)
			{
				case VK_F11:
					if(gsRenderDevice)
					{
						OS_Printf("lost render device signal\n");
						gsRenderDevice = FALSE;
						tfgLostRenderDevice();
						lostRenderDevice();
						_term_display();
					}
					else
					{
						_init_display();
						restoreRenderDevice();
						tfgRestoreRenderDevice();
						gsRenderDevice = TRUE;
					}
				break;
				case VK_F9:
					if(shWnd)
					{
						sLandscape = !sLandscape;
						if(sLandscape)
						{
							sWindowWidth = sWindowWidth0;
							sWindowHeight = sWindowHeight0;
						}
						else
						{
							sWindowWidth = sWindowHeight0;
							sWindowHeight = sWindowWidth0;
						}
						SetWindowPos(shWnd, 0, 0, 0, sWindowWidth, sWindowHeight, SWP_NOMOVE|SWP_NOZORDER|SWP_NOACTIVATE);
					}
				break;
				case VK_F7:
					tfgLowMemory();
			}
		case WM_KEYDOWN:
			if (wParam == VK_ESCAPE)
			{
				DestroyWindow(wnd);
				gAppAlive = FALSE;
			}
			else
			{
				s32 key = -1;
				switch(wParam)
				{
					case VK_CONTROL:
						key = PAD_BUTTON_A;
						// test resize GameField::GetInstance().resize(sWindowWidth, sWindowHeight);
					break;
					case VK_SHIFT:
						key = PAD_BUTTON_B;
					break;
					//case PAD_BUTTON_X;
					//break;
					//case PAD_BUTTON_Y;
					//break;
					//case PAD_BUTTON_SELECT;
					//break;
					case VK_RETURN:
						key = PAD_BUTTON_START;
					break;
					case VK_RIGHT: 
						key = PAD_KEY_RIGHT;
					break;
					case VK_LEFT:
						key = PAD_KEY_LEFT;
					break;
					case VK_UP:
						key = PAD_KEY_UP;
					break;
					case VK_DOWN:
						key = PAD_KEY_DOWN;
					break;
					//case PAD_BUTTON_R
					//case PAD_BUTTON_L
				}
				if(key >= 0)
				{
					switch (message)
					{
						case WM_KEYDOWN:
							onKeyDown(key);
						break;
						case WM_KEYUP:
							onKeyUp(key);
					}
				}
			}
			break;
			
		case WM_ACTIVATE:
		    if(!gsRenderDevice && !gWindowActive)
			{
				if(_init_display() != 0)
				{
		    		gAppAlive = FALSE;
					gsRenderDevice = FALSE;
				}
				else
				{
					_init_tengine();
					gsRenderDevice = TRUE;
				}
				gWindowActive = TRUE;
			}
			break;

		case WM_CLOSE:
			DestroyWindow(wnd);
			gAppAlive = FALSE;
			break;

		case WM_DESTROY:
			PostQuitMessage(0);
			gAppAlive = FALSE;
			if(gsRenderDevice)
			{
				gsRenderDevice = FALSE;
			}
			break;

		case WM_SIZE:
			GetClientRect(shWnd, &rc);
			if(sWindowWidth != rc.right || sWindowHeight != rc.bottom)
			{
				sWindowWidth = rc.right;
				sWindowHeight = rc.bottom;
				tfgResize(sWindowWidth, sWindowHeight);
				glRender_Resize(sWindowWidth, sWindowHeight);
			}
			return 0;
	}
	return DefWindowProc(wnd, message, wParam, lParam);
}
//---------------------------------------------------------------------------
 
int _init_display()
{
	if(sghRC == NULL)
	{
		sghDC = GetDC(shWnd);
		if(!SetupPixelFormat(sghDC))
		{
			_term_display();
			return -1;
		}
		sghRC = wglCreateContext(sghDC);
#ifdef JOBS_IN_SEPARATE_THREAD
		/*
		sghRCThread = wglCreateContext(sghDC);
		if(!wglShareLists(sghRC, sghRCThread))
		{
			_term_display();
			return -1;
		}
		*/
#endif
		wglMakeCurrent(sghDC, sghRC);

		checkGLErrors();

#ifdef USE_GLEW
		GLenum err = glewInit();
		if (GLEW_OK != err)
		{
			Release();
			return -1;
		}
#endif
	}
    return 0;
}
//---------------------------------------------------------------------------
 
void _term_display()
{
	wglMakeCurrent(NULL, NULL);
	if(sghRC)
	{
		wglDeleteContext(sghRC);
#ifdef JOBS_IN_SEPARATE_THREAD
		//wglDeleteContext(sghRCThread);
#endif
	}
	if(sghDC)
	{
		ReleaseDC(shWnd, sghDC);
	}
	sghRC = NULL;
	sghDC = NULL;
}
//---------------------------------------------------------------------------
 
void _init_tengine()
{
	if(!gsTengineInit)
	{
		s32 w, h;
		struct InitFileSystemData fileSystemData;

		tfgInitMemory();

	    glRender_Init();

		h = sClientHeight;
		w = sClientWidth;

		if(_tcslen(gsStrPath) > 0)
		{
			fileSystemData.mpPath = gsStrPath;
			InitFileSystem(&fileSystemData);
		}
		else
		{
			fileSystemData.mpPath = RES_PATH;
			InitFileSystem(&fileSystemData);
		}

		tfgInit();
		tfgResize(w, h);
		OS_Printf("init tengine display with size: %d %d\n", w, h);
		glRender_Resize(w, h);

		#ifdef JOBS_IN_SEPARATE_THREAD
		{
			s32 rc;
			pthread_attr_t threadAttr;
			pthread_attr_init(&threadAttr);
			pthread_attr_setdetachstate(&threadAttr, PTHREAD_CREATE_JOINABLE);
			rc = pthread_create(&sgThread, &threadAttr, ThreadTasks, NULL);
			if(rc)
			{
				OS_Warning("error: return code from pthread_create() is %d\n", rc);
				SDK_ASSERT(0);
			}
			pthread_attr_destroy(&threadAttr);
		}
		#endif
		gsTengineInit = TRUE;
	}
}
//---------------------------------------------------------------------------

void _release_tengine()
{
	if(gsTengineInit)
	{
#ifdef JOBS_IN_SEPARATE_THREAD
		{
			s32 rc = pthread_join(sgThread, NULL);
			if(rc)
			{
				OS_Warning("error: return code from pthread_join() is %d\n", rc);
				SDK_ASSERT(0);
			}
		}
#endif

		tfgRelease();

		ReleaseFileSystem();

		glRender_Release();
	}
	gsTengineInit = FALSE;
}
//-----------------------------------------------------------------------

int WINAPI _tWinMain(HINSTANCE instance, HINSTANCE prevInstance, LPTSTR cmdLine, int cmdShow)
{
    MSG msg;
	WNDCLASSEX wc;
    DWORD windowStyle;
	RECT clientRect;
	DWORD tickPreviousTime;
	s32 windowX, windowY;

	_TCHAR strw[MAX_PATH];
	_TCHAR strh[MAX_PATH];
	const _TCHAR *str = cmdLine;
	strw[0] = strh[0] = gsStrPath[0] = 0;  

#ifdef PROFILER_ENABLE
	StaticAllocator_Init(&sProfilerStaticAllocator, sStaticAllocatorHeap, PROFILER_STATIC_ALLOCATOR_HEAP_SIZE);
	PROFILE_INIT(sProfilerStaticAllocator);
#endif

	if(_tcslen(str) > 0)
	{
		size_t len, pos_t;
		const _TCHAR *pos_pt;
		const _TCHAR *chw = _T("-w");
		const _TCHAR *chh = _T("-h");
		const _TCHAR *chp = _T("-p");
		const _TCHAR *ch_ = _T(" ");
		const _TCHAR *che = _T("\0");
		len = _tcslen(str); 
		pos_pt	= _tcsstr(str, chw);
		if(pos_pt != NULL)
		{
			pos_t = (pos_pt - str) / sizeof(_TCHAR) + 2;
			while(len > pos_t && str[pos_t] != ch_[0] && str[pos_t] != che[0])
			{
				_tcsncat(strw, &str[pos_t], 1);
				pos_t++;
			}
		}
		pos_pt = _tcsstr(str, chh);
		if(pos_pt != NULL)
		{
			pos_t = (pos_pt - str) / sizeof(_TCHAR) + 2;
			while(len > pos_t && str[pos_t] != ch_[0] && str[pos_t] != che[0])
			{
				_tcsncat(strh, &str[pos_t], 1);
				pos_t++;
			}
		}
		pos_pt = _tcsstr(str, chp);
		if(pos_pt != NULL)
		{
			pos_t = (pos_pt - str) / sizeof(_TCHAR) + 2;
			while(len > pos_t && str[pos_t] != ch_[0] && str[pos_t] != che[0])
			{
				_tcsncat(gsStrPath, &str[pos_t], 1);
				pos_t++;
			}
		}
	}

	clientRect.left = 0;
	clientRect.top = 0;
	clientRect.right = sWindowWidth;
	clientRect.bottom = sWindowHeight;
	if(_tcslen(strw) > 0)
	{
		clientRect.right = _ttoi(strw);
	}
	if(_tcslen(strh) > 0)
	{
		clientRect.bottom = _ttoi(strh);
	}
	sClientWidth = clientRect.bottom;
	sClientHeight = clientRect.right;

	wc.cbSize = sizeof(WNDCLASSEX);
	wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
    wc.lpfnWndProc = (WNDPROC)wndProc;
    wc.cbClsExtra = 0;
    wc.cbWndExtra = 0;
	wc.hInstance = instance;
    wc.hIcon = LoadIcon(NULL, IDI_WINLOGO);
	wc.hCursor = LoadCursor(prevInstance, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
    wc.lpszMenuName = NULL;
    wc.lpszClassName = sAppName;
	wc.hIconSm = NULL;
	if (!RegisterClassEx(&wc))
	{
		return FALSE;
	}

	windowStyle = WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_VISIBLE;
    windowX = CW_USEDEFAULT;
	windowY = 0;

	if(AdjustWindowRect(&clientRect, windowStyle, FALSE) == FALSE)
	{
		//old style
		sWindowWidth += 2 * GetSystemMetrics(SM_CXFIXEDFRAME) - 2 * GetSystemMetrics(SM_CXBORDER);
		sWindowHeight += 2 * GetSystemMetrics(SM_CYFIXEDFRAME) + GetSystemMetrics(SM_CYCAPTION) - 2 * GetSystemMetrics(SM_CYBORDER);		
	}
	else
	{
		sWindowWidth = clientRect.right;
		sWindowHeight = clientRect.bottom;
	}
	sWindowWidth0 = sWindowWidth;
	sWindowHeight0 = sWindowHeight;

	shWnd = CreateWindow(sAppName, sAppName, windowStyle,
                        windowX, windowY,
						sWindowWidth, sWindowHeight,
                        NULL, NULL, instance, NULL);
	if (!shWnd)
	{
		return FALSE;
	}

	OpenStdConsole();

	if(_tcslen(strw) > 0 || _tcslen(strh) > 0 || _tcslen(gsStrPath) > 0)
	{
		_tprintf("parameters: ");
		if(_tcslen(strw) > 0)
		{
			_tprintf("w:%s ", strw);
		}
		if(_tcslen(strh) > 0)
		{
			_tprintf("h:%s ", strh);
		}
		if(_tcslen(gsStrPath) > 0)
		{
			_tprintf("assets: %s ", gsStrPath);
		}
		_tprintf("\n");
    }

	ShowWindow(shWnd, cmdShow);

#ifdef UNDER_CE
	SHFullScreen(sWnd,
				 SHFS_HIDETASKBAR | SHFS_HIDESIPBUTTON | SHFS_HIDESTARTICON);
	MoveWindow(sWnd, 0, 0, sWindowWidth, sWindowHeight, TRUE);
#endif

	UpdateWindow(shWnd);

	tickPreviousTime = 0;
	while (gAppAlive)
	{
		PROFILE_START;
		while (PeekMessage(&msg, shWnd, 0, 0, PM_NOREMOVE))
        {
			if (GetMessage(&msg, shWnd, 0, 0))
            {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }
			else
			{
				gAppAlive = 0;
				continue;
        	}
		}
		DWORD currentTime = OS_GetTime();
		DWORD ms = currentTime - tickPreviousTime;
		tickPreviousTime = currentTime;
		if(gsRenderDevice && ms)
		{	
			tfgTick(ms);
			transferToVRAM();
			glRender_DrawFrame();
			SwapBuffers(sghDC);
			if(checkGLErrors() != 0)
			{
				gAppAlive = 0;
			}
		}
		PROFILE_FINISH;
    }
	_release_tengine();
	_term_display();
	CloseStdConsole();
    return 0;
}
//---------------------------------------------------------------------------

#ifdef JOBS_IN_SEPARATE_THREAD
void* ThreadTasks(void *t)
{
	(void)t;
	while(gAppAlive)
	{
		if(gsRenderDevice)
		{
			jobSeparateThreadUpdate();
		}
		sched_yield();
	}
	pthread_exit(NULL);
	return NULL;
}
#endif
//-------------------------------------------------------------------------------------------

void OpenStdConsole()
{
#ifdef OUTPUT_CONSOLE
	AllocConsole();
	SetConsoleTitle("TEngine output window");

	LONG hStdHandle;
	int hConHandle;
	FILE *file;
	CONSOLE_SCREEN_BUFFER_INFO coninfo;

	GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &coninfo);
	coninfo.dwSize.Y = 500;
	SetConsoleScreenBufferSize(GetStdHandle(STD_OUTPUT_HANDLE), coninfo.dwSize);

	// redirect unbuffered STDOUT to the console
	hStdHandle = (LONG)GetStdHandle(STD_OUTPUT_HANDLE);
	hConHandle = _open_osfhandle(hStdHandle, _O_TEXT);
	file = _fdopen(hConHandle, "w");
	setvbuf(file, NULL, _IONBF, 0);
	*stdout = *file;

	// redirect unbuffered STDIN to the console
	hStdHandle = (LONG)GetStdHandle(STD_INPUT_HANDLE);
	hConHandle = _open_osfhandle(hStdHandle, _O_TEXT);
	file = _fdopen(hConHandle, "r");
	setvbuf(stdin, NULL, _IONBF, 0);
	*stdin = *file;

	// redirect unbuffered STDERR to the console
	hStdHandle = (LONG)GetStdHandle(STD_ERROR_HANDLE);
	hConHandle = _open_osfhandle(hStdHandle, _O_TEXT);
	file = _fdopen(hConHandle, "w");
	setvbuf(stderr, NULL, _IONBF, 0);
	*stderr = *file;

	ios::sync_with_stdio();
#endif
}
//---------------------------------------------------------------------------

void CloseStdConsole()
{
#ifdef OUTPUT_CONSOLE
	FreeConsole();
#endif
}
//---------------------------------------------------------------------------
