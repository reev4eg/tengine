/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include "platform.h"
#include "texts.h"
#include <ctype.h>
#include <string.h>
#include <stdarg.h>

#define ZEROPAD	1		/* pad with zero */
#define SIGN	2		/* unsigned/signed long */
#define PLUS	4		/* show plus */
#define SPACE	8		/* space if plus */
#define LEFT	16		/* left justified */
#define SPECIAL	32		/* 0x */
#define LARGE	64		/* use 'ABCDEF' instead of 'abcdef' */

static s32 _vsnwprintf_c(wchar *buf, const wchar *fmt, va_list args);
static s32 _iswdigit (wchar wc);
static s32 _skip_atoi(const wchar **s);
static wchar *_number (wchar *str, long long num, s32 base, s32 size, s32 precision, s32 type);

#ifdef ANDROID_NDK
	#include <time.h>
#endif

#ifndef NITRO_SDK

//---------------------------------------------------------------------------

s32 STD_StrLen(const char *str)
{
	return (s32)strlen(str);
}
//---------------------------------------------------------------------------

char* STD_StrCpy(char *dest, const char *src)
{
	return strcpy(dest, src);
}
//---------------------------------------------------------------------------

s32 STD_StrCmp(const char *a, const char *b)
{
	return strcmp(a, b);
}
//---------------------------------------------------------------------------

char *STD_StrCat(char *str1, const char *str2)
{
	return strcat(str1, str2);
}
//---------------------------------------------------------------------------

#endif

void STD_NumToString(u16 num, char *buf, u16 bufSize)
{
	s32 idx;
	s32 max = 1;
	if(num > 0)
	{
		idx = 0;
		while(num >= max)
		{
			max *= 10;
			++idx;
		}
		SDK_ASSERT(bufSize > idx);
		max = idx;
		while(num)
		{
			--idx;
			buf[idx] = (char)('0' + (num % 10));
			num /= 10;
		}
		buf[max] = 0;
	}
	else
	{
		SDK_ASSERT(bufSize >= 2);
		buf[0] = '0';
		buf[1] = 0;
	}
	(void)bufSize;
}
//----------------------------------------------------------------------------------

s32 STD_WStrLen(const wchar *str)
{
	s32 ct = 0;
	while(str[ct] != L'\0')ct++;
	return ct;
}
//---------------------------------------------------------------------------

s32 _iswdigit (wchar wc)
{
	if (wc < 0x100)
	  return isdigit((unsigned char) wc);
	else
	  return 0;
}
//---------------------------------------------------------------------------

s32 _skip_atoi(const wchar **s)
{
	s32 i=0;
	while (_iswdigit(**s))
		i = i*10 + *((*s)++) - L'0';
	return i;
}
//---------------------------------------------------------------------------

wchar *_number (wchar *str, long long num, s32 base, s32 size, s32 precision, s32 type)
{
   s32 i;
   wchar c, sign, tmp[66];
   wchar digits[37];

   digits[0] = L'0';
   digits[1] = L'1';
   digits[2] = L'2';
   digits[3] = L'3';
   digits[4] = L'4';
   digits[5] = L'5';
   digits[6] = L'6';
   digits[7] = L'7';
   digits[8] = L'8';
   digits[9] = L'9';
   digits[36] = L'\0';

   if (type & LARGE)
   {
	   digits[10] = L'A';
	   digits[11] = L'B';
	   digits[12] = L'C';
	   digits[13] = L'D';
	   digits[14] = L'E';
	   digits[15] = L'F';
	   digits[16] = L'G';
	   digits[17] = L'H';
	   digits[18] = L'I';
	   digits[19] = L'J';
	   digits[20] = L'K';
	   digits[21] = L'L';
	   digits[22] = L'M';
	   digits[23] = L'N';
	   digits[24] = L'O';
	   digits[25] = L'P';
	   digits[26] = L'Q';
	   digits[27] = L'R';
	   digits[28] = L'S';
	   digits[29] = L'T';
	   digits[30] = L'U';
	   digits[31] = L'V';
	   digits[32] = L'W';
	   digits[33] = L'X';
	   digits[34] = L'Y';
	   digits[35] = L'Z';
   }
   else
   {
	   digits[10] = L'a';
	   digits[11] = L'b';
	   digits[12] = L'c';
	   digits[13] = L'd';
	   digits[14] = L'e';
	   digits[15] = L'f';
	   digits[16] = L'g';
	   digits[17] = L'h';
	   digits[18] = L'i';
	   digits[19] = L'j';
	   digits[20] = L'k';
	   digits[21] = L'l';
	   digits[22] = L'm';
	   digits[23] = L'n';
	   digits[24] = L'o';
	   digits[25] = L'p';
	   digits[26] = L'q';
	   digits[27] = L'r';
	   digits[28] = L's';
	   digits[29] = L't';
	   digits[30] = L'u';
	   digits[31] = L'v';
	   digits[32] = L'w';
	   digits[33] = L'x';
	   digits[34] = L'y';
	   digits[35] = L'z';
   }

   if (type & LEFT)
	 type &= ~ZEROPAD;
   if (base < 2 || base > 36)
	 return 0;

   c = (wchar)((type & ZEROPAD) ? L'0' : L' ');
   sign = 0;

   if (type & SIGN)
     {
	if (num < 0)
	  {
	     sign = L'-';
	     num = -num;
	     size--;
	  }
	else if (type & PLUS)
	  {
	     sign = L'+';
	     size--;
	  }
	else if (type & SPACE)
	  {
	     sign = L' ';
	     size--;
	  }
     }

   if (type & SPECIAL)
     {
	if (base == 16)
	  size -= 2;
	else if (base == 8)
	  size--;
     }

   i = 0;
   if (num == 0)
     tmp[i++]='0';
   else while (num != 0)
   {
   /*
#define do_div(n, base) ({ \
s32 __res; \
__res = ((unsigned long long) n) % ((u32) base); \
n = ((unsigned long long) n) / ((u32) base); \
__res; })*/
	 s32 __res = (s32)(((unsigned long long) num) % ((u32) base));
	 num = (long long)(((unsigned long long) num) / ((u32) base));
	 tmp[i++] = digits[/*do_div(num, base)*/__res];
   }
   if (i > precision)
     precision = i;
   size -= precision;
   if (!(type&(ZEROPAD+LEFT)))
     while(size-->0)
       *str++ = L' ';
   if (sign)
     *str++ = sign;
   if (type & SPECIAL)
     {
	if (base==8)
	  {
	     *str++ = L'0';
	  }
	else if (base==16)
	  {
	     *str++ = L'0';
	     *str++ = digits[33];
	  }
     }
   if (!(type & LEFT))
     while (size-- > 0)
       *str++ = c;
   while (i < precision--)
     *str++ = '0';
   while (i-- > 0)
     *str++ = tmp[i];
   while (size-- > 0)
     *str++ = L' ';
   return str;
}
//---------------------------------------------------------------------------

s32 _vsnwprintf_c(wchar *buf, const wchar *fmt, va_list args)
{
	s32 len;
	unsigned long long num;
	s32 i, base;
	wchar * str;
	const char *s;
	const wchar *sw;
	wchar swnull[7];
	s32 flags;		/* flags to number() */
	s32 field_width;	/* width of output field */
	s32 precision;		/* min. # of digits for s32egers; max
				   number of chars for from string */
	s32 qualifier;		/* 'h', 'l', 'L', 'w' or 'I' for s32eger fields */

	for (str=buf ; *fmt ; ++fmt) {
		if (*fmt != L'%') {
			*str++ = *fmt;
			continue;
		}

		/* process flags */
		flags = 0;
		repeat:
			++fmt;		/* this also skips first '%' */
			switch (*fmt) {
				case L'-': flags |= LEFT; goto repeat;
				case L'+': flags |= PLUS; goto repeat;
				case L' ': flags |= SPACE; goto repeat;
				case L'#': flags |= SPECIAL; goto repeat;
				case L'0': flags |= ZEROPAD; goto repeat;
			}

		/* get field width */
		field_width = -1;
		if (_iswdigit(*fmt))
			field_width = _skip_atoi(&fmt);
		else if (*fmt == L'*') {
			++fmt;
			/* it's the next argument */
			field_width = va_arg(args, s32);
			if (field_width < 0) {
				field_width = -field_width;
				flags |= LEFT;
			}
		}

		/* get the precision */
		precision = -1;
		if (*fmt == L'.') {
			++fmt;
			if (_iswdigit(*fmt))
				precision = _skip_atoi(&fmt);
			else if (*fmt == L'*') {
				++fmt;
				/* it's the next argument */
				precision = va_arg(args, s32);
			}
			if (precision < 0)
				precision = 0;
		}

		/* get the conversion qualifier */
		qualifier = -1;
		if (*fmt == 'h' || *fmt == 'l' || *fmt == 'L' || *fmt == 'w') {
			qualifier = *fmt;
			++fmt;
		} else if (*fmt == 'I' && *(fmt+1) == '6' && *(fmt+2) == '4') {
			qualifier = *fmt;
			fmt += 3;
		}

		/* default base */
		base = 10;

		switch (*fmt) {
		case L'c':
			if (!(flags & LEFT))
				while (--field_width > 0)
					*str++ = L' ';
			if (qualifier == 'h')
				*str++ = (wchar) va_arg(args, s32);
			else
				*str++ = (wchar) va_arg(args, s32);
			while (--field_width > 0)
				*str++ = L' ';
			continue;

		case L'C':
			if (!(flags & LEFT))
				while (--field_width > 0)
					*str++ = L' ';
			if (qualifier == 'l' || qualifier == 'w')
				*str++ = (wchar) va_arg(args, s32);
			else
				*str++ = (wchar) va_arg(args, s32);
			while (--field_width > 0)
				*str++ = L' ';
			continue;

		case L's':
			if (qualifier == 'h') {
				/* prs32 ascii string */
				s = va_arg(args, char *);
				if (s == NULL)
					s = "<NULL>";

				len = STD_StrLen (s);
				if ((u32)len > (u32)precision)
					len = precision;

				if (!(flags & LEFT))
					while (len < field_width--)
						*str++ = L' ';
				for (i = 0; i < len; ++i)
					*str++ = (wchar)(*s++);
				while (len < field_width--)
					*str++ = L' ';
			} else {
				/* prs32 unicode string */
				sw = va_arg(args, wchar*);
				if (sw == NULL)
				{
					swnull[0] = L'<';
					swnull[1] = L'N';
					swnull[2] = L'U';
					swnull[3] = L'L';
					swnull[4] = L'L';
					swnull[5] = L'>';
					swnull[6] = L'\0';
					sw = swnull;
				}

				len = STD_WStrLen (sw);
				if ((u32)len > (u32)precision)
					len = precision;

				if (!(flags & LEFT))
					while (len < field_width--)
						*str++ = L' ';
				for (i = 0; i < len; ++i)
					*str++ = *sw++;
				while (len < field_width--)
					*str++ = L' ';
			}
			continue;

		case L'S':
			if (qualifier == 'l' || qualifier == 'w') {
				/* prs32 unicode string */
				sw = va_arg(args, wchar *);
				if (sw == NULL)
				{
					swnull[0] = L'<';
					swnull[1] = L'N';
					swnull[2] = L'U';
					swnull[3] = L'L';
					swnull[4] = L'L';
					swnull[5] = L'>';
					swnull[6] = L'\0';
					sw = swnull;
				}

				len = STD_WStrLen (sw);
				if ((u32)len > (u32)precision)
					len = precision;

				if (!(flags & LEFT))
					while (len < field_width--)
						*str++ = L' ';
				for (i = 0; i < len; ++i)
					*str++ = *sw++;
				while (len < field_width--)
					*str++ = L' ';
			} else {
				/* prs32 ascii string */
				s = va_arg(args, char *);
				if (s == NULL)
					s = "<NULL>";

				len = STD_StrLen (s);
				if ((u32)len > (u32)precision)
					len = precision;

				if (!(flags & LEFT))
					while (len < field_width--)
						*str++ = L' ';
				for (i = 0; i < len; ++i)
					*str++ = (wchar)(*s++);
				while (len < field_width--)
					*str++ = L' ';
			}
			continue;
/*
		case L'Z':
			if (qualifier == 'h') {
				// prs32 counted ascii string
				PANSI_STRING pus = va_arg(args, PANSI_STRING);
				if ((pus == NULL) || (pus->Buffer == NULL)) {
					sw = L"<NULL>";
					while ((*sw) != 0)
						*str++ = *sw++;
				} else {
					for (i = 0; pus->Buffer[i] && i < pus->Length; i++)
						*str++ = (wchar)(pus->Buffer[i]);
				}
			} else {
				// prs32 counted unicode string
				PUNICODE_STRING pus = va_arg(args, PUNICODE_STRING);
				if ((pus == NULL) || (pus->Buffer == NULL)) {
					sw = L"<NULL>";
					while ((*sw) != 0)
						*str++ = *sw++;
				} else {
					for (i = 0; pus->Buffer[i] && i < pus->Length / sizeof(WCHAR); i++)
						*str++ = pus->Buffer[i];
				}
			}
			continue;
*/
		case L'p':
			if (field_width == -1) {
				field_width = 2*sizeof(void *);
				flags |= ZEROPAD;
			}
			str = _number(str, (unsigned long) va_arg(args, void *), 16,
							field_width, precision, flags);
			continue;

		case L'n':
			if (qualifier == 'l')
            {
				long *ip = va_arg(args, long *);
				*ip = (str - buf);
			}
            else
            {
				s32 *ip = va_arg(args, s32 *);
				*ip = (s32)(str - buf);
			}
			continue;

		/* s32eger number formats - set up the flags and "break" */
		case L'o':
			base = 8;
			break;

		case L'b':
			base = 2;
			break;

		case L'X':
			flags |= LARGE;
		case L'x':
			base = 16;
			break;

		case L'd':
		case L'i':
			flags |= SIGN;
		case L'u':
			break;

		default:
			if (*fmt != L'%')
				*str++ = L'%';
			if (*fmt)
				*str++ = *fmt;
			else
				--fmt;
			continue;
		}

		if (qualifier == 'I')
			num = va_arg(args, unsigned long long);
		else if (qualifier == 'l')
			num = va_arg(args, unsigned long);
		else if (qualifier == 'h') {
			if (flags & SIGN)
				num = va_arg(args, s32);
			else
				num = va_arg(args, u32);
		}
		else {
			if (flags & SIGN)
				num = va_arg(args, s32);
			else
				num = va_arg(args, u32);
		}
		str = _number(str, (long long)num, base, field_width, precision, flags);
	}
	*str = L'\0';
	return (s32)(str - buf);
}
//---------------------------------------------------------------------------

s32 STD_WSprintf(wchar *buf, const wchar *fmt, ...)
{
	va_list args;
	s32 i;
	va_start(args, fmt);
	i=_vsnwprintf_c(buf, fmt, args);
	va_end(args);
	return i;
}
//---------------------------------------------------------------------------

s32 STD_WStrCmp(const wchar *cs, const wchar *ct)
{
	while (*cs != '\0' && *ct != '\0' && *cs == *ct)
	{
		++cs;
		++ct;
	}
	return *cs - *ct;
}
//---------------------------------------------------------------------------

s32 STD_WStrNCmp(const wchar *s1, const wchar *s2, s32 n)
{
	wchar c1 = L'\0';
	wchar c2 = L'\0';
	if(n >= 4)
	{
		size_t n4 = n >> 2;
		do
		{
			c1 = *s1++;
			c2 = *s2++;
			if(c1 == L'\0' || c1 != c2)
			{
				return c1 - c2;
			}
			c1 = *s1++;
			c2 = *s2++;
			if(c1 == L'\0' || c1 != c2)
			{
				return c1 - c2;
			}
			c1 = *s1++;
			c2 = *s2++;
			if(c1 == L'\0' || c1 != c2)
			{
				return c1 - c2;
			}
			c1 = *s1++;
			c2 = *s2++;
			if(c1 == L'\0' || c1 != c2)
			{
				return c1 - c2;
			}
		}
		while(--n4 > 0);
		n &= 3;
	}
	while (n > 0)
	{
		c1 = *s1++;
		c2 = *s2++;
		if (c1 == L'\0' || c1 != c2)
		{
			return c1 - c2;
		}
		n--;
	}
	return c1 - c2;
}
//---------------------------------------------------------------------------

wchar* STD_WStrCpy(wchar* str1, const wchar* str2)
{
	wchar* s = str1;
	while ((*str2)!=0)
	{
		*s = *str2;
		++s;
		++str2;
	}
	*s = 0;
	return(str1);
}
//---------------------------------------------------------------------------

wchar *STD_WStrStr(wchar *s, const wchar *find)
{
	wchar c, sc;
	s32 len;
	if((c = *find++) != 0)
	{
		len = STD_WStrLen(find);
		do
		{
			do
			{
				if((sc = *s++) == 0)
				{
					return NULL;
				}
			}
			while (sc != c);
		}
		while(STD_WStrNCmp(s, find, len) != 0);
		s--;
	}
	return s;
}
//---------------------------------------------------------------------------

u32 OS_GetTime()
{
#ifdef WINDOWS_APP
	return (DWORD)GetTickCount();
#elif ANDROID_NDK
	struct timespec res;
	clock_gettime(CLOCK_REALTIME, &res);
	return res.tv_nsec;
#else
	return 0;
#endif
}