/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include <jni.h>
#include <sys/time.h>
#include <time.h>
#include <android/log.h>
#include <stdint.h>

#include "gamefield.h"
#include "filesystem.h"
#include "lib/tengine_low.h"
#include "lib/render2dgl.h"
#include "lib/a_touchpad.h"
#include "lib/jobs_low.h"
#ifdef JOBS_IN_SEPARATE_THREAD
#include "pthread.h"
#endif

#ifdef NDK_NATIVE_ACTIVITY
#include <errno.h>

#include "EGL/egl.h"
#include "GLES/gl.h"

#include "android/sensor.h"
#include "android/log.h"
#include "android_native_app_glue.h"
#else
#include "android/asset_manager_jni.h"
#endif

#include "profiler/profile.h"
#ifdef PROFILER_ENABLE
#include "static_allocator.h"
#endif

#ifdef JOBS_IN_SEPARATE_THREAD
static void* ThreadTasks(void *t);
static pthread_t sgThread;
#endif

static long sPrevTime = 0;

struct ApplicationData
{
#ifdef NDK_NATIVE_ACTIVITY
	ASensorManager* sensorManager;
    const ASensor* accelerometerSensor;
    ASensorEventQueue* sensorEventQueue;

    EGLDisplay display;
    EGLSurface surface;
    EGLContext context;
    BOOL focus;
#endif
	BOOL tengineInit;
    BOOL appActive;
	BOOL renderDevice;
};

#ifdef PROFILER_ENABLE
#define PROFILER_STATIC_ALLOCATOR_HEAP_SIZE (1024 * 1024 * 4)
static struct StaticAllocator sProfilerStaticAllocator;
static u8 sStaticAllocatorHeap[PROFILER_STATIC_ALLOCATOR_HEAP_SIZE];
#endif

static struct ApplicationData gsAppData;

static BOOL sApplication = TRUE;

void _release_tengine();

static long _getTime(void)
{
    struct timeval now;
    gettimeofday(&now, NULL);
    return (long)(now.tv_sec * 1000 + now.tv_usec / 1000);
}

//-----------------------------------------------------------------------

#ifdef NDK_NATIVE_ACTIVITY

//-----------------------------------------------------------------------

static void _init_tengine(struct android_app* app);
static s32 _init_display(struct android_app* app);
static void _term_display();
static void _handle_cmd(struct android_app* app, int32_t cmd);
static int32_t _handle_input(struct android_app* app, AInputEvent* event);

static void _handle_onDestroy(ANativeActivity* activity);
static void _handle_onWindowFocusChanged(ANativeActivity* activity, s32 hasFocus);
static void _handle_onNativeWindowDestroyed(ANativeActivity* activity, ANativeWindow* window);
static void _handle_onNativeWindowResized(ANativeActivity* activity, ANativeWindow* window);

//-----------------------------------------------------------------------

s32 _init_display(struct android_app* app)
{
    EGLint dummy, format;
    EGLContext context;
    EGLint contextAttribs[] =
    {
        EGL_CONTEXT_CLIENT_VERSION, 1, EGL_NONE
    };
    EGLDisplay display = eglGetDisplay(EGL_DEFAULT_DISPLAY);
    if(EGL_NO_DISPLAY == display)
    {
    	return -1;
    }
    eglInitialize(display, 0, 0);
    {
		EGLint numConfigs;
		EGLConfig config;
	    EGLSurface surface;
		const EGLint configAttribs[] =
	    {
	        EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
	        EGL_BLUE_SIZE, 8,
	        EGL_GREEN_SIZE, 8,
	        EGL_RED_SIZE, 8,
	        EGL_ALPHA_SIZE, 8,
	        //EGL_BUFFER_SIZE, 32,
	        //EGL_DEPTH_SIZE, 24,
	        EGL_NONE
	    };
		eglChooseConfig(display, configAttribs, &config, 1, &numConfigs);
		eglGetConfigAttrib(display, config, EGL_NATIVE_VISUAL_ID, &format);
		ANativeWindow_setBuffersGeometry(app->window, 0, 0, format);
		surface = eglCreateWindowSurface(display, config, app->window, NULL);
		context = eglCreateContext(display, config, EGL_NO_CONTEXT, contextAttribs);

	    gsAppData.display = display;
	    gsAppData.context = context;
	    gsAppData.surface = surface;
    }

    if (eglMakeCurrent(display, gsAppData.surface, gsAppData.surface, context) == EGL_FALSE)
    {
        OS_Warning("Unable to eglMakeCurrent");
        return -1;
    }

    return 0;
}
//-----------------------------------------------------------------------

void _term_display()
{
	if (gsAppData.display != EGL_NO_DISPLAY)
    {
		eglMakeCurrent(gsAppData.display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
        if(gsAppData.context != EGL_NO_CONTEXT)
        {
            eglDestroyContext(gsAppData.display, gsAppData.context);
        }
        if(gsAppData.surface != EGL_NO_SURFACE)
        {
            eglDestroySurface(gsAppData.display, gsAppData.surface);
        }
        eglTerminate(gsAppData.display);
    }
    gsAppData.display = EGL_NO_DISPLAY;
    gsAppData.context = EGL_NO_CONTEXT;
    gsAppData.surface = EGL_NO_SURFACE;
}
//-----------------------------------------------------------------------

int32_t _handle_input(struct android_app* app, AInputEvent* event)
{
    if (AInputEvent_getType(event) == AINPUT_EVENT_TYPE_MOTION)
    {
    	gsAppData.state.x = AMotionEvent_getX(event, 0);
    	gsAppData.state.y = AMotionEvent_getY(event, 0);
        return 1;
    }
    return 0;
}
//-----------------------------------------------------------------------

void _handle_cmd(struct android_app* app, int32_t cmd)
{
    switch (cmd)
    {
		case APP_CMD_INIT_WINDOW:
        	OS_Printf("app APP_CMD_INIT_WINDOW");
			if(!sApplication)
			{
				ANativeActivity_finish(app->activity);
			}
			else
			if(app->window != NULL && !gsAppData.renderDevice)
		    {
				if(_init_display(app) != 0)
		    	{
					ANativeActivity_finish(app->activity);
		    	}
		    	else
		    	{
					if(gsAppData.tengineInit)
					{
						OS_Printf("restore res data");
						restoreRenderDevice();
						tfgRestoreRenderDevice();
					}
					else
					{
						_init_tengine(app);
					}
					gsAppData.renderDevice = TRUE;
		    	}
		    }
			break;
		//case APP_CMD_TERM_WINDOW:
		//    break;
        //case APP_CMD_DESTROY:
		//    break;
		//case APP_CMD_WINDOW_RESIZED:
		//	break;
		//case APP_CMD_GAINED_FOCUS:
		//    break;
		//case APP_CMD_LOST_FOCUS:
		//    break;

    	case APP_CMD_CONFIG_CHANGED:
    		break;

    	case APP_CMD_SAVE_STATE:
            // The system has asked us to save our current state.  Do so.
            //engine->app->savedState = malloc(sizeof(struct saved_state));
            //*((struct saved_state*)engine->app->savedState) = engine->state;
            //engine->app->savedStateSize = sizeof(struct saved_state);
            break;

        case APP_CMD_LOW_MEMORY:
        	break;

        case APP_CMD_PAUSE:
        	OS_Printf("app APP_CMD_PAUSE");
			if(gsAppData.tengineInit)
			{
				tfgLostRenderDevice();
				lostRenderDevice();
			}
			_handle_onNativeWindowDestroyed(NULL, NULL);
		break;

        case APP_CMD_RESUME:
        	OS_Printf("app APP_CMD_RESUME");
			break;

        case APP_CMD_START:
        	OS_Printf("app APP_CMD_START");
        	break;

        case APP_CMD_STOP:
        	OS_Printf("app APP_CMD_STOP");
        	break;
    }
}
//-----------------------------------------------------------------------

void _handle_onDestroy(ANativeActivity* activity)
{
	gsAppData.appActive = FALSE;
	sApplication = FALSE;
	_release_tengine();
	ANativeActivity_finish(activity);
}
//-----------------------------------------------------------------------

void _handle_onWindowFocusChanged(ANativeActivity* activity, s32 hasFocus)
{
	if(!sApplication)
	{
		return;
	}

	if(hasFocus)
	{
        // When our app gains focus, we start monitoring the accelerometer.
		gsAppData.focus = TRUE;
    	if (gsAppData.accelerometerSensor != NULL)
        {
            ASensorEventQueue_enableSensor(gsAppData.sensorEventQueue,
            		gsAppData.accelerometerSensor);
            // We'd like to get 60 events per second (in us).
            ASensorEventQueue_setEventRate(gsAppData.sensorEventQueue,
            		gsAppData.accelerometerSensor, (1000L/60)*1000);
        }
	}
	else
	{
        // When our app loses focus, we stop monitoring the accelerometer.
        // This is to avoid consuming battery while not being used.
		gsAppData.focus = FALSE;
    	if (gsAppData.accelerometerSensor != NULL)
        {
            ASensorEventQueue_disableSensor(gsAppData.sensorEventQueue,
            		gsAppData.accelerometerSensor);
        }
	}
}
//-----------------------------------------------------------------------

void _handle_onNativeWindowDestroyed(ANativeActivity* activity, ANativeWindow* window)
{
	if(gsAppData.renderDevice)
	{
		gsAppData.renderDevice = FALSE;
		glRender_DeleteTextures(eglGetCurrentContext() != NULL);
		_term_display();
	}
}
//-----------------------------------------------------------------------

void _handle_onNativeWindowResized(ANativeActivity* activity, ANativeWindow* window)
{
    if(window && gsAppData.renderDevice)
    {
    	s32 w, h;
    	h = ANativeWindow_getHeight(window);
		w = ANativeWindow_getWidth(window);
    	tfgResize(w, h);
		glRender_Resize(w, h);
		OS_Printf("window resize: %d %d", w, h);
    }
}
//-----------------------------------------------------------------------

void android_main(struct android_app* app)
{
    app_dummy();

#ifdef PROFILER_ENABLE
	StaticAllocator_Init(&sProfilerStaticAllocator, sStaticAllocatorHeap, PROFILER_STATIC_ALLOCATOR_HEAP_SIZE);
	PROFILE_INIT(sProfilerStaticAllocator);
#endif
	
    MI_CpuFill8(&gsAppData, 0, sizeof(gsAppData));
    app->userData = &gsAppData;
    app->onAppCmd = _handle_cmd;
    app->onInputEvent = _handle_input;

    gsAppData.appActive = TRUE;
    gsAppData.tengineInit = FALSE;
    gsAppData.renderDevice = FALSE;
    gsAppData.focus = FALSE;

    app->activity->callbacks->onDestroy = _handle_onDestroy;
    app->activity->callbacks->onWindowFocusChanged = _handle_onWindowFocusChanged;
    app->activity->callbacks->onNativeWindowDestroyed = _handle_onNativeWindowDestroyed;
    app->activity->callbacks->onNativeWindowResized = _handle_onNativeWindowResized;

    gsAppData.display = EGL_NO_DISPLAY;
    gsAppData.context = EGL_NO_CONTEXT;
    gsAppData.surface = EGL_NO_SURFACE;
    //gsAppData.app = app;

    gsAppData.sensorManager = ASensorManager_getInstance();
    gsAppData.accelerometerSensor = ASensorManager_getDefaultSensor(gsAppData.sensorManager, ASENSOR_TYPE_ACCELEROMETER);
    gsAppData.sensorEventQueue = ASensorManager_createEventQueue(gsAppData.sensorManager, app->looper, LOOPER_ID_USER, NULL, NULL);

    while(gsAppData.appActive)
    {
        long ms, curTime;
        s32 ident;
        s32 events;
        struct android_poll_source* source;
        
		PROFILE_START;
			
        while ((ident = ALooper_pollAll(0, NULL, &events, (void**)&source)) >= 0)
        {
			if (source != NULL)
            {
                source->process(app, source);
            }
        	// If a sensor has data, process it now.
            if (ident == LOOPER_ID_USER)
            {
                if (gsAppData.accelerometerSensor != NULL)
                {
                    ASensorEvent event;
                    while (ASensorEventQueue_getEvents(gsAppData.sensorEventQueue, &event, 1) > 0)
                    {
                    	//LOGI("accelerometer: x=%f y=%f z=%f",
                        //        event.acceleration.x, event.acceleration.y,
                        //        event.acceleration.z);
                    }
                }
            }

            if(app->destroyRequested != 0)
            {
            	OS_Printf("destroyRequested");
            	//if(!sApplication) // quit Y/N? -->Y
            	{
            		ANativeActivity_finish(app->activity);
            	}
            	return;
            }
        }
        curTime = _getTime();
        ms = curTime - sPrevTime;
    	sPrevTime = curTime;
		if (gsAppData.renderDevice && gsAppData.focus && gsAppData.tengineInit)
		{
			tfgTick(ms);
			transferToVRAM();
			glRender_DrawFrame();
			eglSwapBuffers(gsAppData.display, gsAppData.surface);
		}
		PROFILE_FINISH;
    }
    //OS_Printf("android_main exit");
}
//-----------------------------------------------------------------------

#else

//-----------------------------------------------------------------------

#ifdef __cplusplus
extern "C" {
#endif
 JNIEXPORT void JNICALL Java_tengine_common_AppGLSurfaceView_nativeInit(JNIEnv* env, jobject o, jobject assetManager);
 JNIEXPORT void JNICALL Java_tengine_common_AppGLSurfaceView_nativeRelease(JNIEnv* env);
 JNIEXPORT void JNICALL Java_tengine_common_AppGLSurfaceView_nativePause(JNIEnv* env);
 JNIEXPORT void JNICALL Java_tengine_common_AppGLSurfaceView_nativeResume(JNIEnv* env);
 JNIEXPORT void JNICALL Java_tengine_common_AppGLSurfaceView_nativeStop(JNIEnv* env);

 JNIEXPORT void JNICALL Java_tengine_common_AppTouchpad_nativeTouchPadDown(JNIEnv* env, jobject o, jint ptid, jfloat x, jfloat y);
 JNIEXPORT void JNICALL Java_tengine_common_AppTouchpad_nativeTouchPadUp(JNIEnv* env, jobject o, jint ptid, jfloat x, jfloat y);
 JNIEXPORT void JNICALL Java_tengine_common_AppTouchpad_nativeTouchPadMove(JNIEnv* env, jobject o, jint ptid, jfloat x, jfloat y);
 JNIEXPORT void JNICALL Java_tengine_common_AppGLSurfaceView_nativeKeyDown(JNIEnv* env, jobject o, jint keyCode);
 JNIEXPORT void JNICALL Java_tengine_common_AppGLSurfaceView_nativeKeyUp(JNIEnv* env, jobject o, jint keyCode);
 
 JNIEXPORT void JNICALL Java_tengine_common_AppGLSurfaceView_nativeCreateRenderDevice(JNIEnv* env);
 JNIEXPORT void JNICALL Java_tengine_common_AppGLSurfaceView_nativeResize(JNIEnv* env, jobject o, jint w, jint h);
 JNIEXPORT void JNICALL Java_tengine_common_AppGLSurfaceView_nativeTick(JNIEnv* env);
#ifdef __cplusplus
} /* extern "C" */
#endif

static void _init_tengine(struct AAssetManager *am, s32 w, s32 h);

static AAssetManager *gsAssetManager = NULL;

//-----------------------------------------------------------------------

JNIEXPORT void JNICALL Java_tengine_common_AppGLSurfaceView_nativeInit(JNIEnv* env, jobject o, jobject assetManager)
{
	(void)o;
#ifdef PROFILER_ENABLE
	StaticAllocator_Init(&sProfilerStaticAllocator, sStaticAllocatorHeap, PROFILER_STATIC_ALLOCATOR_HEAP_SIZE);
	PROFILE_INIT(sProfilerStaticAllocator);
#endif
	gsAssetManager = AAssetManager_fromJava(env, assetManager);
}
//-----------------------------------------------------------------------

JNIEXPORT void JNICALL Java_tengine_common_AppGLSurfaceView_nativeCreateRenderDevice(JNIEnv* env)
{
	(void)env;
	if(gsAppData.tengineInit)
	{
		OS_Printf("app restore res data");
		restoreRenderDevice();
		tfgRestoreRenderDevice();
		gsAppData.renderDevice = TRUE;
	}
}
//-----------------------------------------------------------------------

JNIEXPORT void JNICALL Java_tengine_common_AppGLSurfaceView_nativeStop(JNIEnv* env)
{
	(void)env;
	gsAppData.renderDevice = FALSE;
	OS_Printf("app lost render device");
	if(gsAppData.tengineInit)
	{
		tfgLostRenderDevice();
		lostRenderDevice();
	}
}
//-----------------------------------------------------------------------

JNIEXPORT void JNICALL Java_tengine_common_AppGLSurfaceView_nativeRelease(JNIEnv* env)
{
	(void)env;
	sApplication = FALSE;
	gsAppData.appActive = FALSE;
	_release_tengine();
}
//-----------------------------------------------------------------------

JNIEXPORT void JNICALL Java_tengine_common_AppGLSurfaceView_nativePause(JNIEnv* env)
{
	Java_tengine_common_AppGLSurfaceView_nativeStop(env);
}
//-----------------------------------------------------------------------

JNIEXPORT void JNICALL Java_tengine_common_AppGLSurfaceView_nativeResume(JNIEnv* env)
{
	(void)env;
}
//-----------------------------------------------------------------------

JNIEXPORT void JNICALL Java_tengine_common_AppGLSurfaceView_nativeResize(JNIEnv* env, jobject o, jint w, jint h)
{
	(void)env;
	(void)o;
	if(gsAppData.tengineInit)
	{
		tfgResize(w, h);
		glRender_Resize(w, h);
	}
	else
	{
		if(sApplication)
		{
			gsAppData.appActive = TRUE;
			_init_tengine(gsAssetManager, w, h);
			gsAppData.renderDevice = TRUE;
		}
	}
}
//-----------------------------------------------------------------------

JNIEXPORT void JNICALL Java_tengine_common_AppTouchpad_nativeTouchPadDown(JNIEnv* env, jobject o, jint ptid, jfloat x, jfloat y)
{
	(void)env;
	(void)o;
	onTouchPadDown((u8)ptid, (u16)x, (u16)y);
}
//-----------------------------------------------------------------------

JNIEXPORT void JNICALL Java_tengine_common_AppTouchpad_nativeTouchPadUp(JNIEnv* env, jobject o, jint ptid, jfloat x, jfloat y)
{
	(void)env;
	(void)o;
	onTouchPadUp((u8)ptid, (u16)x, (u16)y);
}
//-----------------------------------------------------------------------

JNIEXPORT void JNICALL Java_tengine_common_AppTouchpad_nativeTouchPadMove(JNIEnv* env, jobject o, jint ptid, jfloat x, jfloat y)
{
	(void)env;
	(void)o;
	onTouchPadMove((u8)ptid, (u16)x, (u16)y);
}
//-----------------------------------------------------------------------

JNIEXPORT void JNICALL Java_tengine_common_AppGLSurfaceView_nativeKeyDown(JNIEnv* env, jobject o, jint keyCode)
{
	(void)env;
	(void)o;
	(void)keyCode;
}
//-----------------------------------------------------------------------

JNIEXPORT void JNICALL Java_tengine_common_AppGLSurfaceView_nativeKeyUp(JNIEnv* env, jobject o, jint keyCode)
{
	(void)env;
	(void)o;
	(void)keyCode;
}
//-----------------------------------------------------------------------

JNIEXPORT void JNICALL Java_tengine_common_AppGLSurfaceView_nativeTick(JNIEnv* env)
{
	(void)env;
	long curTime, ms;
	PROFILE_START;
	curTime = _getTime();
	ms = curTime - sPrevTime;
	sPrevTime = curTime;
	if (gsAppData.renderDevice && gsAppData.tengineInit)
	{
		tfgTick(ms);
		transferToVRAM();
		glRender_DrawFrame();
	}
	PROFILE_FINISH;
}
//-----------------------------------------------------------------------

#endif //NDK_NATIVE_ACTIVITY

//-----------------------------------------------------------------------

#ifdef JOBS_IN_SEPARATE_THREAD
void* ThreadTasks(void *t)
{
	(void)t;
	while(gsAppData.appActive)
	{
		if(gsAppData.renderDevice 
#ifdef NDK_NATIVE_ACTIVITY
			&& gsAppData.focus
#endif
			)
		{
			jobSeparateThreadUpdate();
		}
		sched_yield();
	}
	pthread_exit(NULL);
	return NULL;
}
#endif
//-------------------------------------------------------------------------------------------

#ifdef NDK_NATIVE_ACTIVITY
void _init_tengine(struct android_app* app)
#else
void _init_tengine(struct AAssetManager *am, s32 w, s32 h)
#endif
{
	if(!gsAppData.tengineInit && sApplication)
	{
#ifdef NDK_NATIVE_ACTIVITY
		s32 w, h;
#endif
		struct InitFileSystemData fileSystemData;
		
	    tfgInitMemory();

	    glRender_Init();

#ifdef NDK_NATIVE_ACTIVITY
		h = ANativeWindow_getHeight(app->window);
		w = ANativeWindow_getWidth(app->window);

		fileSystemData.mpAssetManager = app->activity->assetManager;
#else
		fileSystemData.mpAssetManager = am; 
#endif
		InitFileSystem(&fileSystemData);

		tfgInit();
		tfgResize(w, h);
		OS_Printf("init tengine display with size: %d %d", w, h);
		glRender_Resize(w, h);

#ifdef JOBS_IN_SEPARATE_THREAD
		{
			s32 rc;
			pthread_attr_t threadAttr;
			pthread_attr_init(&threadAttr);
			pthread_attr_setdetachstate(&threadAttr, PTHREAD_CREATE_JOINABLE);
			rc = pthread_create(&sgThread, &threadAttr, ThreadTasks, NULL);
			if(rc)
			{
				OS_Warning("error: return code from pthread_create() is %d\n", rc);
				SDK_ASSERT(0);
			}
			pthread_attr_destroy(&threadAttr);
		}
#endif
#ifdef NDK_NATIVE_ACTIVITY
		if (app->savedState != NULL)
		{
			// We are starting with a previous saved state; restore from it
		}
#endif
		gsAppData.tengineInit = TRUE;
	}
}
//-------------------------------------------------------------------------------------------

void _release_tengine()
{
	if(gsAppData.tengineInit)
	{
#ifdef JOBS_IN_SEPARATE_THREAD
		{
			s32 rc = pthread_join(sgThread, NULL);
			if(rc)
			{
				OS_Warning("error: return code from pthread_join() is %d\n", rc);
				SDK_ASSERT(0);
			}
		}
#endif
		tfgRelease();
		ReleaseFileSystem();
		glRender_Release();
	}
	gsAppData.tengineInit = FALSE;
}
//-----------------------------------------------------------------------
