
LOCAL_PATH := $(call my-dir)

# -----------------------------
# ------ tengine --------------
# -----------------------------

include $(CLEAR_VARS)

LOCAL_MODULE := tengine

LOCAL_C_INCLUDES := \
	$(LOCAL_PATH)/../../game \
	$(LOCAL_PATH)/../../../../src/tengine \
	$(LOCAL_PATH)/../../../../src/tengine/lib \
	$(LOCAL_PATH)/../../../../src/gui \
	$(LOCAL_PATH)/../../../../src/gamefield \
	$(LOCAL_PATH)/../../../../src/_android

#LOCAL_CFLAGS := -DANDROID_NDK -DUSE_OPENGL_RENDER -DUSE_SLES_SOUND
LOCAL_CFLAGS := -DANDROID_NDK -DFRAME_ALLOCATOR -DSDK_DEBUG

LOCAL_SRC_FILES := \
	../../game/gamefield.cpp \
	../../game/gamelevel.cpp \
	../../game/gamelevelHud.cpp \
	../../game/gamelevelLoading.cpp \
	../../game/gamelevelScene1.cpp \
	../../game/gamelevelScene2.cpp \
	../../../../src/tengine/lib/a_filesystem.c \
	../../../../src/tengine/lib/crossgl.c \
	../../../../src/tengine/lib/fxmath.c \
	../../../../src/tengine/lib/gamepad.c \
	../../../../src/tengine/lib/gx_helpers.c \
	../../../../src/tengine/lib/hash.c \
	../../../../src/tengine/lib/jobs.c \
	../../../../src/tengine/lib/loadhelpers.c \
	../../../../src/tengine/lib/loadtdata.c \
	../../../../src/tengine/lib/memory.c \
	../../../../src/tengine/lib/network.c \
	../../../../src/tengine/lib/platform.c \
	../../../../src/tengine/lib/render2dgl.c \
	../../../../src/tengine/lib/serialization.c \
	../../../../src/tengine/lib/sound.c \
	../../../../src/tengine/lib/tengine.c \
	../../../../src/tengine/lib/tengine_pr.c \
	../../../../src/tengine/lib/touchpad.c \
	../../../../src/tengine/lib/static_allocator.c \
	../../../../src/tengine/containers/allocator_array.c \
	../../../../src/gui/guibasebutton.c \
	../../../../src/gui/guibutton.c \
	../../../../src/gui/guifactory.c \
	../../../../src/gui/guicontainer.c \
	../../../../src/gui/guiobject2D.c \
	../../../../src/gui/guispeedbutton.c \
	../../../../src/gui/guislider.c \
	../../../../src/_android/app-android.c

#debug
LOCAL_LDLIBS :=-lGLESv1_CM -lOpenSLES -landroid -ldl -llog
#release
#LOCAL_LDLIBS :=-lGLESv1_CM -lOpenSLES -landroid
LOCAL_STATIC_LIBRARIES := android_native_app_glue

include $(BUILD_SHARED_LIBRARY)

$(call import-module,android/native_app_glue)
