
#include "memory.h"
#include "verlet.h"
#include "verlet_low.h"

//---------------------------------------------------------------------------

#define VRLT_MIN_FLOAT FX32(0.0001)
#define VRLT_MIN_STEP_VALUE 15

static void _vrltInitObject(struct Verlet *v, s32 objectIndex, const struct VerletObjectInitParams* params);
static void _vrltReleaseObject(struct Verlet *v, s32 objectIndex);

static void _vrltParticlePush(struct VTObject *c, struct VTParticle* p);

static void _vrltBounds(struct Verlet *v, struct VTParticle *p);

//---------------------------------------------------------------------------

void vrltInit(struct Verlet *v, struct VerletInitParams *params)
{
	SDK_NULL_ASSERT(v);
	SDK_NULL_ASSERT(params);
	MI_CpuClear8(v, sizeof(struct Verlet));
	v->mWidth = params->mWidth;
	v->mHeight = params->mHeight;
	v->mObjectsPoolMax = (s32)params->mObjectsCount;
	if(v->mObjectsPoolMax > 0)
	{
		s32 i;
		v->mConstraintsPoolMax = (s32)params->mConstraintsPoolMax;
		if(v->mConstraintsPoolMax > 0)
		{
			v->mppConstraintsPool = (struct VTConstraint**)MALLOC(params->mConstraintsPoolMax * sizeof(struct VTConstraint*), "vrltInit:mppConstraintsPool");
			for(i = 0; i < v->mConstraintsPoolMax; i++)
			{
				v->mppConstraintsPool[i] = (struct VTConstraint*)MALLOC(sizeof(struct VTConstraint), "vrltInit:mppConstraintsPool[i]");
			}
		}
		v->mParticlesPoolMax = (s32)params->mParticlesPoolMax;
		if(v->mParticlesPoolMax > 0)
		{
			v->mppParticlesPool = (struct VTParticle**)MALLOC(params->mParticlesPoolMax * sizeof(struct VTParticle*), "vrltInit:mppParticlesPool");
			for(i = 0; i < v->mParticlesPoolMax; i++)
			{
				v->mppParticlesPool[i] = (struct VTParticle*)MALLOC(sizeof(struct VTParticle), "vrltInit:mppParticlesPool[i]");
			}
		}
		v->mppObjectsPool = (struct VTObject**)MALLOC(params->mObjectsCount * sizeof(struct VTObject*), "vrltInit:mppObjects");
		for(i = 0; i < v->mObjectsPoolMax; i++)
		{
			v->mppObjectsPool[i] = (struct VTObject*)MALLOC(sizeof(struct VTObject), "vrltInit:mppObjects[i]");
			_vrltInitObject(v, i, &params->mppObjectInitArr[i]);
		}
	}
}
//---------------------------------------------------------------------------

void vrltRelease(struct Verlet *v)
{
	SDK_NULL_ASSERT(v);
	if(v->mObjectsPoolMax > 0)
	{
		while(v->mObjectsPoolMax > 0)
		{
			v->mObjectsPoolMax--;
			if(v->mppObjectsPool[v->mObjectsPoolMax] != NULL)
			{
				_vrltReleaseObject(v, v->mObjectsPoolMax);
				FREE(v->mppObjectsPool[v->mObjectsPoolMax]);
			}
		}
		FREE(v->mppObjectsPool);
		if(v->mParticlesPoolMax > 0)
		{
			while(v->mParticlesPoolMax > 0)
			{
				v->mParticlesPoolMax--;
				FREE(v->mppParticlesPool[v->mParticlesPoolMax]);
			}
			FREE(v->mppParticlesPool);
		}
		if(v->mConstraintsPoolMax > 0)
		{
			while(v->mConstraintsPoolMax > 0)
			{
				v->mConstraintsPoolMax--;
				FREE(v->mppConstraintsPool[v->mConstraintsPoolMax]);
			}
			FREE(v->mppConstraintsPool);
		}
	}
	MI_CpuClear8(v, sizeof(struct Verlet));
}
//---------------------------------------------------------------------------

void vrltClear(struct Verlet *v)
{
	s32 c;
	SDK_NULL_ASSERT(v);
	for (c = 0 ; c < v->mObjectsPoolCount; c++)
	{
		v->mppObjectsPool[c]->mParticlesCount = 0;
		v->mppObjectsPool[c]->mConstraintsCount = 0;
	}
	v->mConstraintsPoolCount = 0;
	v->mParticlesPoolCount = 0;
	v->mObjectsPoolCount = 0;
}
//---------------------------------------------------------------------------

s32 vrltGetObjectsCount(struct Verlet *v)
{
	SDK_NULL_ASSERT(v);
	return v->mObjectsPoolCount;
}
//---------------------------------------------------------------------------

s32 vrltGetParticlesCount(struct Verlet *v)
{
	SDK_NULL_ASSERT(v);
	return v->mParticlesPoolCount = 0;
}
//---------------------------------------------------------------------------

s32 vrltGetConstraintsCount(struct Verlet *v)
{
	SDK_NULL_ASSERT(v);
	return v->mConstraintsPoolCount = 0;
}
//---------------------------------------------------------------------------

s32 vrltGetObjectParticlesCount(struct Verlet *v, s32 objIndex)
{
	SDK_NULL_ASSERT(v);
	if(objIndex < v->mObjectsPoolCount && objIndex >= 0)
	{
		return v->mppObjectsPool[objIndex]->mParticlesCount;
	}
	return 0;
}
//---------------------------------------------------------------------------

s32 vrltGetObjectConstraintsCount(struct Verlet *v, s32 objIndex)
{
	SDK_NULL_ASSERT(v);
	if(objIndex < v->mObjectsPoolCount && objIndex >= 0)
	{
		return v->mppObjectsPool[objIndex]->mConstraintsCount;
	}
	return 0;
}
//---------------------------------------------------------------------------

void _vrltInitObject(struct Verlet *v, s32 objectIndex, const struct VerletObjectInitParams* params)
{
	SDK_NULL_ASSERT(v);
	SDK_ASSERT(v->mObjectsPoolMax > (s32)objectIndex); // init Verlet before
	SDK_ASSERT(v->mppObjectsPool[objectIndex] != NULL);
	MI_CpuClear8(v->mppObjectsPool[objectIndex], sizeof(struct VTObject));
	v->mppObjectsPool[objectIndex]->mParticlesMax = (s32)params->mParticlesMax;
	v->mppObjectsPool[objectIndex]->mConstraintsMax = (s32)params->mConstraintsMax;
	v->mppObjectsPool[objectIndex]->mGravity = params->mGravity;
	v->mppObjectsPool[objectIndex]->mFriction = params->mFriction;
	v->mppObjectsPool[objectIndex]->mGroundFriction = params->mGroundFriction;
	if(v->mppObjectsPool[objectIndex]->mConstraintsMax > 0)
	{
		v->mppObjectsPool[objectIndex]->mppConstraints = (struct VTConstraint**)MALLOC(v->mppObjectsPool[objectIndex]->mConstraintsMax * sizeof(struct VTConstraint*), "vrltInitObject:mppConstraints");
	}
	if(v->mppObjectsPool[objectIndex]->mParticlesMax > 0)
	{
		v->mppObjectsPool[objectIndex]->mppParticles = (struct VTParticle**)MALLOC(v->mppObjectsPool[objectIndex]->mParticlesMax * sizeof(struct VTParticle*), "vrltInitObject:mppParticles");
	}
}
//---------------------------------------------------------------------------

void _vrltReleaseObject(struct Verlet *v, s32 objectIndex)
{
	SDK_NULL_ASSERT(v->mppObjectsPool[objectIndex]);
	if(v->mppObjectsPool[objectIndex]->mParticlesMax > 0)
	{
		FREE(v->mppObjectsPool[objectIndex]->mppParticles);
	}
	if(v->mppObjectsPool[objectIndex]->mConstraintsMax > 0)
	{
		FREE(v->mppObjectsPool[objectIndex]->mppConstraints);
	}
	MI_CpuClear8(v->mppObjectsPool[objectIndex], sizeof(struct VTObject));
}
//---------------------------------------------------------------------------

void _vrltInitParticle(struct VTParticle *v, const struct fxVec2 *pos)
{
	SDK_NULL_ASSERT(v);
	SDK_NULL_ASSERT(pos);
	v->pos = *pos;
	v->lastPos = *pos;
}
//---------------------------------------------------------------------------

struct VTObject *_vrltObjectPop(struct Verlet *v)
{
	SDK_ASSERT(v->mObjectsPoolMax > 0);
	if(v->mObjectsPoolCount < v->mObjectsPoolMax)
	{
		return v->mppObjectsPool[v->mObjectsPoolCount++];
	}
	SDK_ASSERT(v->mObjectsPoolCount < v->mObjectsPoolMax);
	return NULL;
}
//---------------------------------------------------------------------------

struct VTParticle* _vrltParticlePushToObj(struct Verlet *v, struct VTObject *c)
{
	SDK_ASSERT(v->mParticlesPoolMax > 0);
	if(v->mParticlesPoolCount < v->mParticlesPoolMax)
	{
		struct VTParticle* p;
		p = v->mppParticlesPool[v->mParticlesPoolCount];
		v->mParticlesPoolCount++;
		_vrltParticlePush(c, p);
		return p;
	}
	SDK_ASSERT(v->mParticlesPoolCount < v->mParticlesPoolMax);
	return NULL;
}
//---------------------------------------------------------------------------

void _vrltParticlePush(struct VTObject *c, struct VTParticle* p)
{
	SDK_ASSERT(c->mParticlesMax > 0);
	if(c->mParticlesCount < c->mParticlesMax)
	{
		c->mppParticles[c->mParticlesCount] = p;
		c->mParticlesCount++;
		return;
	}
	SDK_ASSERT(c->mParticlesCount < c->mParticlesMax);
}
//---------------------------------------------------------------------------

void _vrltBounds(struct Verlet *v, struct VTParticle *p)
{
	s32 x, y;
	SDK_NULL_ASSERT(v);
	SDK_NULL_ASSERT(p);
	x = p->pos.x >> FX32_SHIFT;
	y = p->pos.y >> FX32_SHIFT;
	/*if(y < 0)
	{
		p->pos.y = 0;
	}*/
	if(y > v->mHeight - 1)
	{
		p->pos.y = (v->mHeight - 1) << FX32_SHIFT;
	}
	if(x < 0)
	{
		p->pos.x = 0;
	}
	if(x > v->mWidth - 1)
	{
		p->pos.x = (v->mWidth - 1) << FX32_SHIFT;
	}
}
//---------------------------------------------------------------------------

void vrltUpdate(struct Verlet *v, s32 ms)
{
	s32 i, c;
	
	SDK_NULL_ASSERT(v);

	for(c = 0 ; c < v->mObjectsPoolCount; c++)
	{
		if(v->mppObjectsPool[c]->mState == VOS_READY)
		{
			struct VTParticle **particles = v->mppObjectsPool[c]->mppParticles;
			for (i = 0; i < v->mppObjectsPool[c]->mParticlesCount; i++)
			{
				struct fxVec2 p, velocity;
			
				// calculate velocity
				p = fxVec2Sub(particles[i]->pos, particles[i]->lastPos);
				velocity = fxVec2MulFx(p, v->mppObjectsPool[c]->mFriction);

				// ground friction
				if(v->mHeight > 0)
				{
					if ((particles[i]->pos.y >> FX32_SHIFT) >= v->mHeight - 1 &&
							(fxMul(velocity.x, velocity.x) + fxMul(velocity.y, velocity.y)) > VRLT_MIN_FLOAT)
					{
						fx32 m = fxVec2Length(velocity);
						velocity.x = fxDiv(velocity.x, m);
						velocity.y = fxDiv(velocity.y, m);
						velocity = fxVec2MulFx(velocity, fxMul(m, v->mppObjectsPool[c]->mGroundFriction));
					}
				}

				// save last good state
				particles[i]->lastPos = particles[i]->pos;

				// gravity
				particles[i]->pos = fxVec2Add(particles[i]->pos, v->mppObjectsPool[c]->mGravity);

				// inertia	
				particles[i]->pos = fxVec2Add(particles[i]->pos, velocity);
			}
		}
	}
	
	// handle dragging of entities
	//if (this.draggedEntity)
	//	this.draggedEntity.pos.mutableSet(this.mouse);

	// relax
	{
		fx32 stepCoef;
		if(ms < VRLT_MIN_STEP_VALUE)
		{
			ms = VRLT_MIN_STEP_VALUE;
		}
		stepCoef = fxDiv(FX32_ONE, (ms << FX32_SHIFT));
		for (c = 0 ; c < v->mObjectsPoolCount; c++)
		{
			if(v->mppObjectsPool[c]->mState == VOS_READY)
			{
				struct VTConstraint **constraints = v->mppObjectsPool[c]->mppConstraints;
				for(i = 0; i < ms; i++)
				{
					s32 j;
					for(j = 0; j < v->mppObjectsPool[c]->mConstraintsCount; j++)
					{
						_vrltConstraintRelax(constraints[j], stepCoef);
					}
				}
			}
		}
	}

	// bounds checking
	if(v->mHeight > 0 && v->mWidth > 0)
	{
		for (c = 0 ; c < v->mObjectsPoolCount; c++)
		{
			if(v->mppObjectsPool[c]->mState == VOS_READY)
			{
				for (i = 0; i < v->mppObjectsPool[c]->mParticlesCount; i++)
				{
					_vrltBounds(v, v->mppObjectsPool[c]->mppParticles[i]); 
				}
			}
		}
	}
}
//---------------------------------------------------------------------------

void vrltRender(struct Verlet *v, s32 objIndex, OnVrltRenderCallback pCallbackFn)
{
	SDK_NULL_ASSERT(pCallbackFn);
	if(objIndex < v->mObjectsPoolCount && v->mppObjectsPool[objIndex]->mState == VOS_READY)
	{
		s32 i;
		struct VTCallbackData cbd;
		struct VTParticle **p = v->mppObjectsPool[objIndex]->mppParticles;
		struct VTConstraint **c = v->mppObjectsPool[objIndex]->mppConstraints;
		cbd.mType = VTRT_CONSTRAINT;
		for (i = 0; i < v->mppObjectsPool[objIndex]->mConstraintsCount; i++)
		{
			cbd.mPointCount = 1;
			cbd.mpPoint[0] = &c[i]->mpParticle1->pos;
			if(c[i]->mpParticle2 != NULL)
			{
				cbd.mpPoint[1] = &c[i]->mpParticle2->pos;
				cbd.mPointCount = 2;
				if(c[i]->mpParticle3 != NULL)
				{
					cbd.mpPoint[1] = &c[i]->mpParticle3->pos;
					cbd.mPointCount = 3;
				}
			}
			pCallbackFn((const struct VTCallbackData*)&cbd);
		}
		cbd.mType = VTRT_PARTICLE;
		cbd.mPointCount = 1;
		for (i = 0; i < v->mppObjectsPool[objIndex]->mParticlesCount; i++)
		{
			cbd.mpPoint[0] = &p[i]->pos;
			pCallbackFn((const struct VTCallbackData*)&cbd);
		}
	}
}
//---------------------------------------------------------------------------

/*
VerletJS.prototype.nearestEntity = function() {
	var c, i;
	var d2Nearest = 0;
	var entity = null;
	var constraintsNearest = null;

	// find nearest point
	for (c in this.Objects) {
		var particles = this.Objects[c].particles;
		for (i in particles) {
			var d2 = particles[i].pos.dist2(this.mouse);
			if (d2 <= this.selectionRadius*this.selectionRadius && (entity == null || d2 < d2Nearest)) {
				entity = particles[i];
				constraintsNearest = this.Objects[c].constraints;
				d2Nearest = d2;
			}
		}
	}

	// search for pinned constraints for this entity
	for (i in constraintsNearest)
		if (constraintsNearest[i] instanceof PinConstraint && constraintsNearest[i].a == entity)
			entity = constraintsNearest[i];

	return entity;
}
*/
//---------------------------------------------------------------------------
