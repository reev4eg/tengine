/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#ifndef POOL_LIST_H
#define POOL_LIST_H

#include "platform.h"

#ifdef __cplusplus
extern "C" {
#endif

struct PoolListItem;

struct PoolList
{
	struct PoolListItem **mppItemsPool;
	struct PoolListItem *mpItemFirst;
	struct PoolListItem *mpItemLast;
	s32 mItemsCount;
	s32 mMaxPool;
	s32 mSizeOfData;
};

void PoolList_Init(struct PoolList *list, u32 sizeOfData, u32 maxPool);
void PoolList_Release(struct PoolList *list);

void PoolList_Clear(struct PoolList *list);
const struct PoolListItem* PoolList_Add(struct PoolList *list, const void *data);
const struct PoolListItem* PoolList_DeleteByIndex(struct PoolList *list, s32 idx);
const struct PoolListItem* PoolList_Delete(struct PoolList *list, const struct PoolListItem* item);
s32 PoolList_GetItemsCount(struct PoolList *list);
const void *PoolList_GetItemData(const struct PoolListItem* item);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif