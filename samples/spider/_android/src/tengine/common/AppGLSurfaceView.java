/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

package tengine.common;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.opengles.GL10;

import android.content.res.AssetManager;
import android.content.Context;
import android.graphics.PixelFormat;
import android.opengl.GLSurfaceView;
import android.view.KeyEvent;
import android.view.MotionEvent;

public class AppGLSurfaceView extends GLSurfaceView 
{	
	private static native void nativeInit(AssetManager mgr);
    private static native void nativeStop();
	private static native void nativeRelease();
	private static native void nativePause();
	private static native void nativeResume();
	
    private static native void nativeKeyDown(int keyCode);
    private static native void nativeKeyUp(int keyCode);
    
	private static native void nativeCreateRenderDevice();
	private static native void nativeResize(int w, int h);
	private static native void nativeTick();
    
	private AppRenderer mRenderer = null;
	
//-----------------------------------------------------------------------
	
    public AppGLSurfaceView(Context context)
    {
    	super(context);
    	
	    // By default, GLSurfaceView() creates a RGB_565 opaque surface.
	    // If we want a translucent one, we should change the surface's
	    // format here, using PixelFormat.TRANSLUCENT for GL Surfaces
	    // is interpreted as any 32-bit surface with alpha by SurfaceFlinger.
	    boolean translucent = true;
	    int depth = 16;
	    
    	if (translucent)
	    {
	        this.getHolder().setFormat(PixelFormat.TRANSLUCENT);
	    }
	
	    // Setup the context factory for 1.0 rendering.
	    // See ContextFactory class definition below
	    setEGLContextFactory(new ContextFactory());
	
	    // We need to choose an EGLConfig that matches the format of
	    // our surface exactly. This is going to be done in our
	    // custom config chooser. See ConfigChooser class definition
	    // below. 
	    setEGLConfigChooser(translucent ?
	                         new ConfigChooser(8, 8, 8, 8, depth) :
	                         new ConfigChooser(5, 6, 5, 0, depth));
	    
    	AppTouchpad.init();
    	mRenderer = new AppRenderer();
    	nativeInit(context.getResources().getAssets());
	    //setDebugFlags(DEBUG_CHECK_GL_ERROR | DEBUG_LOG_GL_CALLS);
    	setRenderer(mRenderer);
    }
//-----------------------------------------------------------------------

	@Override
    public void onPause()
    {
        super.onPause();
    	nativePause();
    }
//-----------------------------------------------------------------------

    @Override
    public void onResume()
    {
        super.onResume();
        nativeResume();
    }
//-----------------------------------------------------------------------

    public void onStop()
    {
    	nativeStop();
    }
//-----------------------------------------------------------------------
    
    public void onDestroy()
    {
    	nativeRelease();
    	mRenderer = null;
    }
//-----------------------------------------------------------------------
    
    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
    	return AppTouchpad.onTouchEventV2(event);
    }
//-----------------------------------------------------------------------
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent msg)
    {
		nativeKeyDown(keyCode);
    	return super.onKeyDown(keyCode, msg);
    }
//-----------------------------------------------------------------------
    
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent msg)
    {
		nativeKeyUp(keyCode);
    	return super.onKeyUp(keyCode, msg);
    } 
//-----------------------------------------------------------------------
    
	private static void checkEglError(String prompt, EGL10 egl)
	{
	    int error;
	    while ((error = egl.eglGetError()) != EGL10.EGL_SUCCESS)
	    {
	    	System.out.println(String.format("%s: EGL error: 0x%x", prompt, error));
	    }
	}
    
//-----------------------------------------------------------------------
//-----------------------------------------------------------------------
    
	private static class ContextFactory implements GLSurfaceView.EGLContextFactory
	{
	    private static int EGL_CONTEXT_CLIENT_VERSION = 0x3098;
	    public EGLContext createContext(EGL10 egl, EGLDisplay display, EGLConfig eglConfig)
	    {
	        checkEglError("before eglCreateContext", egl);
	        int[] attrib_list = {EGL_CONTEXT_CLIENT_VERSION, 1, EGL10.EGL_NONE};
	        EGLContext context = egl.eglCreateContext(display, eglConfig, EGL10.EGL_NO_CONTEXT, attrib_list);
	        checkEglError("after eglCreateContext", egl);
	        return context;
	    }
	    //-----------------------------------------------------------------------
	    
	    public void destroyContext(EGL10 egl, EGLDisplay display, EGLContext context)
	    {
	        egl.eglDestroyContext(display, context);
	    }
	}
    
//-----------------------------------------------------------------------
//-----------------------------------------------------------------------
    
    private static class ConfigChooser implements GLSurfaceView.EGLConfigChooser
    {
        public ConfigChooser(int r, int g, int b, int a, int depth)
        {
            mRedSize = r;
            mGreenSize = g;
            mBlueSize = b;
            mAlphaSize = a;
            mDepthSize = depth;
        }
        //-----------------------------------------------------------------------
        
        // This EGL config specification is used to specify 1.x rendering.
        // We use a minimum size of 4 bits for red/green/blue, but will
        // perform actual matching in chooseConfig() below.
        private static int EGL_OPENGL_ES_BIT = 1;
        private static int[] s_configAttribs2 =
        {
            EGL10.EGL_RED_SIZE, 4,
            EGL10.EGL_GREEN_SIZE, 4,
            EGL10.EGL_BLUE_SIZE, 4,
            EGL10.EGL_RENDERABLE_TYPE, EGL_OPENGL_ES_BIT,
            EGL10.EGL_NONE
        };
        //-----------------------------------------------------------------------
        
        public EGLConfig chooseConfig(EGL10 egl, EGLDisplay display)
        {
            int[] num_config = new int[1];
            egl.eglChooseConfig(display, s_configAttribs2, null, 0, num_config);
            int numConfigs = num_config[0];
            if (numConfigs <= 0)
            {
                throw new IllegalArgumentException("No configs match configSpec");
            }
            EGLConfig[] configs = new EGLConfig[numConfigs];
            egl.eglChooseConfig(display, s_configAttribs2, configs, numConfigs, num_config);
            return chooseConfig(egl, display, configs);
        }
        //-----------------------------------------------------------------------
        
        public EGLConfig chooseConfig(EGL10 egl, EGLDisplay display, EGLConfig[] configs)
        {
            for(EGLConfig config : configs)
            {
                int d = findConfigAttrib(egl, display, config, EGL10.EGL_DEPTH_SIZE, 0);
                if (d < mDepthSize)
                {
                    continue;
                }
				int r = findConfigAttrib(egl, display, config, EGL10.EGL_RED_SIZE, 0);
				int g = findConfigAttrib(egl, display, config, EGL10.EGL_GREEN_SIZE, 0);
				int b = findConfigAttrib(egl, display, config, EGL10.EGL_BLUE_SIZE, 0);
				int a = findConfigAttrib(egl, display, config, EGL10.EGL_ALPHA_SIZE, 0);
                if (r == mRedSize && g == mGreenSize && b == mBlueSize && a == mAlphaSize)
                {
                    return config;
                }
            }
            return null;
        }
        //-----------------------------------------------------------------------
        
        private int findConfigAttrib(EGL10 egl, EGLDisplay display, EGLConfig config, int attribute, int defaultValue)
        {
            if (egl.eglGetConfigAttrib(display, config, attribute, mValue))
            {
                return mValue[0];
            }
            return defaultValue;
        }
        //-----------------------------------------------------------------------
        
        protected int mRedSize;
        protected int mGreenSize;
        protected int mBlueSize;
        protected int mAlphaSize;
        protected int mDepthSize;
        private int[] mValue = new int[1];
    }
    
  //-----------------------------------------------------------------------
  //-----------------------------------------------------------------------

    private static class AppRenderer implements GLSurfaceView.Renderer
    {    
		public void onSurfaceCreated(GL10 gl, EGLConfig config)
		{
			//System.out.println("SurfaceCreated, renderer is " + gl.glGetString(GL10.GL_RENDERER));
			nativeCreateRenderDevice();
	  	}
		//-----------------------------------------------------------------------
	
		public void onSurfaceChanged(GL10 gl, int w, int h)
		{
			nativeResize(w, h);
		}
		//-----------------------------------------------------------------------
	      
		public void onDrawFrame(GL10 gl)
		{
			nativeTick();
		}
		//-----------------------------------------------------------------------
	  }
}
