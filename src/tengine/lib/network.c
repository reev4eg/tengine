/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include "system.h"
#include "memory.h"
#include "network.h"

// cool samples == http://cs.baylor.edu/~donahoo/practical/CSockets/winsock.html == //

#ifdef WINDOWS_APP
    #include <WinSock.h>
    #define MSG_WAITALL    0x08
    #define CERBER_PRINTF(a) OS_Printf("%s : %ld \n", a, GetLastError () )
    typedef int socklen_t;
	typedef long ssize_t;
    #define SOCK_T SOCKET
    #define ADDR_T SOCKADDR
    #define ADDRIN_T SOCKADDR_IN
    #define CLOSESOCK(s) closesocket(s)
#endif

#if defined NIX_APP || defined ANDROID_NDK || defined IOS_APP
    #include <sys/socket.h>
    #include <sys/types.h>
    #include <arpa/inet.h>
    #include <netinet/in.h>
    #include <unistd.h>
    #include <errno.h>
    #define CERBER_PRINTF(a) OS_Printf("%s ; error -  %d \n", a, errno )
    #define SOCK_T s32
    #define ADDR_T struct sockaddr
    #define ADDRIN_T struct sockaddr_in
    #define CLOSESOCK(s) close(s)
#endif

struct NW_internal_sender
{
	BOOL in_use;
	SOCK_T mySocket;
    ADDRIN_T myAddress;
}
NWsenderUDP[MAX_NETWORK_CLIENTS];

#ifdef WINDOWS_APP
    WSADATA WsaData;
#endif

//----------------------------------------------------------------------------------------------------------------

BOOL NWInitNetwork()
{
	s32 i;
#ifdef WINDOWS_APP
	s32 err;
	err = WSAStartup(0x0101, &(WsaData));
    if(err == SOCKET_ERROR)
    {
        CERBER_PRINTF("WSAStartup() failed " );
        return FALSE;
    }
#endif
	for(i = 0; i < MAX_NETWORK_CLIENTS; i++)
	{
		NWsenderUDP[i].in_use = FALSE;
	}
    return TRUE;
}
//----------------------------------------------------------------------------------------------------------------

BOOL NWstartUDPreciever(u16 port, BOOL (*CB_)(u32 addr, struct broadcastCB_* datain, struct broadcastCB_* dataout))
{
    s32 err;
    ssize_t bytesRecv, bytesSent;
    socklen_t receiveAddrSize;
    s32 UDPsock;
    ADDRIN_T localSin;
    ADDRIN_T adrin;
    receiveAddrSize = sizeof(ADDRIN_T);

    MI_CpuClear8((void*)&(localSin), receiveAddrSize);
    localSin.sin_family = AF_INET;
    localSin.sin_port = htons(port);
    localSin.sin_addr.s_addr = htonl(INADDR_ANY);

    UDPsock = socket(AF_INET,SOCK_DGRAM,IPPROTO_UDP);
    if (UDPsock < 0)
    {
        CERBER_PRINTF("Socket create failed ");
        return FALSE;
    }
    err = bind( UDPsock, (ADDR_T*)&(localSin), receiveAddrSize );
    if (err < 0)
    {
        CERBER_PRINTF("Socket binding failed ");
        return FALSE;
    }
    for(;;)
    {
        char in_data[MAX_DATABUF_SIZE];
        char out_data[MAX_DATABUF_SIZE];
        struct broadcastCB_ inData;
        struct broadcastCB_ outData;
        inData.data = (u8*)in_data;
        outData.data = (u8*)out_data;
        inData.length = 0;
        outData.length = 0;

        bytesRecv = recvfrom(UDPsock, in_data, MAX_DATABUF_SIZE, 0, (ADDR_T*)&adrin, &receiveAddrSize);

        if(bytesRecv >= 0)
        {
			BOOL isReplyNeed = FALSE;
            u32* income_addr = (u32*)&(adrin.sin_addr);
            OS_Printf("UDP Listner recieved %d bytes from %X \n", (s32)bytesRecv, (*income_addr));
			inData.length = (s32)bytesRecv;
            isReplyNeed = CB_(*(u32*)(&adrin.sin_addr), &inData, &outData);
			if(isReplyNeed == TRUE)
			{
				bytesSent = sendto(UDPsock, out_data, outData.length, 0,(ADDR_T*) &adrin, sizeof(ADDRIN_T));
				if(bytesSent == outData.length)
				{
					OS_Printf("UDP Listner broadcast send %d bytes \n", (s32)bytesSent);
				}
				else
				{
					CERBER_PRINTF((const char *)"Broadcast answering failed ");
					return FALSE;
				}
			}
        }
		else
        {
            CERBER_PRINTF((const char *)"Broadcast recieving failed ");
            return FALSE;
        }
    }
    //return TRUE;
}
//----------------------------------------------------------------------------------------------------------------

BOOL NWinitUDPsender(u32 addr, u16 port, UDPconn* conn)
{
	s32 err,i;
	struct NW_internal_sender* instance = NULL;
	for(i=0; i < MAX_NETWORK_CLIENTS;i++ )
	{
		if(NWsenderUDP[i].in_use == FALSE)
		{
			instance = &(NWsenderUDP[i]);
			NWsenderUDP[i].in_use = TRUE;
			(*conn) = i;
			break;
		}
	}
	if(instance == NULL)
	{
		OS_Warning("UDP Max connections;");
		return FALSE;
	}
	
	MI_CpuClear8(&(instance->myAddress), sizeof(ADDRIN_T));
    instance->myAddress.sin_family = AF_INET;
    instance->myAddress.sin_addr.s_addr = addr;
    instance->myAddress.sin_port = htons(port);

	instance->mySocket = socket(AF_INET,SOCK_DGRAM,IPPROTO_UDP);
    if (instance->mySocket < 0)
    {
        CERBER_PRINTF("Socket create failed ");
        return FALSE;
    }
    if(addr == 0xFFFFFFFF) //must configure if sending broadcast
    {
        BOOL val = TRUE;
        instance->myAddress.sin_addr.s_addr = INADDR_BROADCAST;
        err = setsockopt(instance->mySocket, SOL_SOCKET, SO_BROADCAST,(char *)&val,sizeof(BOOL));
		if (err < 0)
		{
			CERBER_PRINTF("Socket opt set failed ");
			return FALSE;
		}
    }
    
	return TRUE;
}
//----------------------------------------------------------------------------------------------------------------

BOOL NWsendUDP(UDPconn conn, struct broadcastCB_* outData)
{
    ssize_t nBytesSent, nBytesRecv;
	struct NW_internal_sender* instance = NULL;
	if( (conn < MAX_NETWORK_CLIENTS) && (NWsenderUDP[conn].in_use == TRUE) )
	{
		instance = &(NWsenderUDP[conn]);
	}
	else
	{
		OS_Warning("Invalid connetion ID;");
		return FALSE;
	}
	nBytesSent = nBytesRecv = 0;
   
	nBytesSent = sendto(instance->mySocket, (char*)outData->data, outData->length, 0,
											(ADDR_T*) &(instance->myAddress),  sizeof(ADDR_T));

    if(nBytesSent != outData->length)
    {
        CERBER_PRINTF("Sending failed ");
        return FALSE;
    }
	   
    return TRUE;
}
//----------------------------------------------------------------------------------------------------------------

BOOL NWcloseUDPsender(UDPconn conn)
{
	if((conn < MAX_NETWORK_CLIENTS) && (NWsenderUDP[conn].in_use == TRUE))
	{
		CLOSESOCK(NWsenderUDP[conn].mySocket);
		NWsenderUDP[conn].in_use = FALSE;		
	}
	else
	{
		OS_Warning("Faked connetion ID;");
		return FALSE;
	}
	return TRUE;
}
//----------------------------------------------------------------------------------------------------------------


//void *NWServerClientThread(void* data)
//{
//	s32 myClientID;
//	SOCK_T myClientSocket;
//	ADDRIN_T myClientAddr;
//	struct broadcastCB_ income_data;
//	struct broadcastCB_ outcome_data;
//	s8 incomeDataBuf[MAX_DATABUF_SIZE];
//	s8 outcomeDataBuf[MAX_DATABUF_SIZE];
//
//	income_data.data = (u8*)incomeDataBuf;
//	outcome_data.data = (u8*)outcomeDataBuf;
//	income_data.maxSize = MAX_DATABUF_SIZE;
//	outcome_data.maxSize = MAX_DATABUF_SIZE;
//
//	myClientID = *(s32*)data;
//	myClientSocket = Atlantos.clientSockets[myClientID];
//	myClientAddr = Atlantos.clientAddr[myClientID];
//
//	OS_Printf("Connection accepted.");
//
//	for(;;)
//	{
//		s32 recievedDataSize;
//		recievedDataSize = recv(myClientSocket, incomeDataBuf, MAX_DATABUF_SIZE, MSG_WAITALL);
//		if(recievedDataSize<0)
//		{
//			CERBER_PRINTF("Recieving data from client error");
//		}else
//		{
//			s32 rc, sendDataSize;
//			income_data.length = recievedDataSize;
//			rc = pthread_mutex_lock(&AtlantosCallbackLock);
//			Atlantos.connCallback_.connServerCallback(&income_data, &outcome_data);
//			pthread_mutex_unlock(&AtlantosCallbackLock);
//			sendDataSize = send(myClientSocket, outcomeDataBuf, outcome_data.length, 0 ); // Last Argument Maybe need FIX
//			if(sendDataSize<0)
//			{
//				CERBER_PRINTF("Sending reply to client error");
//			}
//			OS_SpleepMs(1);
//		}
//
//
//	}
//
//}
//
//
//void *NWServerMainListnerThread(void* data)
//{
//	s32 client_len, currClientId;
//	ADDRIN_T client_tmp_addr;
//	SOCK_T client_tmp_socket;
//    (void)data;
//    listen(Atlantos.serverSock, 5);
//    for(;;)
//    {
//		currClientId = Atlantos.clientsCount;
//        client_len = sizeof(client_tmp_addr);
//		client_tmp_socket = accept(Atlantos.serverSock , (ADDR_T *)&client_tmp_addr, &client_len);
//		if (client_tmp_socket < 0)
//        {
//            CERBER_PRINTF("Accepting connection error ");
//            return NULL;
//        }
//		Atlantos.clientSockets[currClientId] = client_tmp_socket;
//		Atlantos.clientAddr[currClientId] = client_tmp_addr;
//		Atlantos.clientsCount++;
//		pthread_create(&(Atlantos.clientThreads[currClientId]), NULL, &NWServerClientThread, &currClientId);
//    }
//}
//
//BOOL NWStartConnectionServer(void (*CB_)(struct broadcastCB_* in_data, struct broadcastCB_* out_data))
//{
//    s32 err,addrLen;
//    pthread_t mainListnerThr;
//
//    Atlantos.serverSock = socket(AF_INET, SOCK_STREAM, 0);
//    if (Atlantos.serverSock < 0)
//        {
//            CERBER_PRINTF("Socket create failed ");
//            return FALSE;
//        }
//    memset(&Atlantos.serverSin, 0, sizeof(Atlantos.serverSin));
//    Atlantos.serverSin.sin_family = AF_INET;
//    Atlantos.serverSin.sin_addr.s_addr = htonl(INADDR_ANY);
//    Atlantos.serverSin.sin_port = htons(CONNECTION_LISTENER_PORT);
//    addrLen = sizeof(Atlantos.serverSin);
//
//    err = bind(Atlantos.serverSock, (struct sockaddr *)&Atlantos.serverSin, addrLen);
//    if (err < 0)
//        {
//            CERBER_PRINTF("Socket binding failed ");
//            return FALSE;
//        }
//	Atlantos.connCallback_.connServerCallback = CB_;
//    pthread_create(&(mainListnerThr), NULL, &NWServerMainListnerThread, NULL);
//    return TRUE;
//}
//
//
//
////----------------------------------------------------------------------------------------------
//
//BOOL NWConnectToServer(char ip[15] , void (*CB_)(struct broadcastCB_* out_data, struct broadcastCB_* in_data))
//{
//	struct hostent *server;
//
//	Atlantos.serverSock = socket(AF_INET, SOCK_STREAM, 0);
//     if (Atlantos.serverSock < 0)
//        {
//            CERBER_PRINTF("Socket create failed ");
//            return FALSE;
//        }
//
//    server = gethostbyname(ip);
//    if (server == NULL) {
//        CERBER_PRINTF("Error No suck server ");
//        return FALSE;
//    }/*
//    bzero((char *) &serv_addr, sizeof(serv_addr));
//    serv_addr.sin_family = AF_INET;
//    bcopy((char *)server->h_addr,
//         (char *)&serv_addr.sin_addr.s_addr,
//         server->h_length);
//    serv_addr.sin_port = htons(portno);
//    if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0)
//        error("ERROR connecting");
//    printf("Please enter the message: ");
//    bzero(buffer,256);
//    fgets(buffer,255,stdin);
//    n = write(sockfd,buffer,strlen(buffer));
//    if (n < 0)
//         error("ERROR writing to socket");
//    bzero(buffer,256);
//    n = read(sockfd,buffer,255);
//    if (n < 0)
//         error("ERROR reading from socket");
//    printf("%s\n",buffer);
//    close(sockfd);*/
//    return TRUE;
//}
//----------------------------------------------------------------------------------------------
