
#include "tengine.h"
#include "texts.h"
#include "touchpad.h"
#include "gamepad.h"
#include "gamefield.h"
#include "level_id.h"
#include "sound.h"

#include "constants.h"

//-------------------------------------------------------------------------------------------

enum GameStates
{
	gstNONE = 0,
	gstDISPLAYINIT,
	gstLOADING,
	gstGAME
};

//map editor sizes
#define GAME_SCREEN_W 800
#define GAME_SCREEN_H 608

#define REPETITIVE_BG_SIZE_Y FX32(32 * 5)
#define SCROLL_SPEED FX32(1)

#define ADDITIONAL_OBJ_OFFSET_Y FX32(32)
#define ANGRY_TIME 1500
#define INVADER_STEP_DELAY 200
#define INVADER_STEP_X FX32(15)
#define INVADER_STEP_Y FX32(30)

#define TURRET_STEP FX32(6)

#define MISSILES_MAX 3
#define MISSILE_STEP FX32(16)

#define FINAL_FX_SCALE_INIT FX32(3)
#define FINAL_FX_SCALE_MAX FX32(500)

 static s32 gameState = gstNONE;

 static s32 key_action;
 static u32 c_c;
 
 static s32 invaders_count = 0;
 static BOOL need_rearrange_all_obj_posions = FALSE;
 static fx32 angry_invaders_count = 0;
 static s32 invaders_direction_side = 1;
 static s32 invaders_movement_delay = 0;
 static BOOL need_change_direction = FALSE;
 static BOOL need_step_down = FALSE;
 static BOOL need_shoot = FALSE;
 static BOOL end_game = FALSE;
 static BOOL last_invader = -1;
 static struct fxVec2 final_scale;
 static fx32 final_scale_step = FINAL_FX_SCALE_INIT;

 static fx32 camera_start_y;
 static fx32 scroll_delta;
 static struct fxVec2 camera_pos;
 static struct fxVec2 turret_pos;

 static s32 missile[MISSILES_MAX];

 static s32 fps_timer = 0;
 static s32 fps_tick = 0;

 static s32 resizeW = 0;
 static s32 resizeH = 0;

 enum ControlDirections
 {
	CD_UP = 0,
	CD_DOWN,
	CD_LEFT,
	CD_RIGHT
 };

 enum LoadMarkers
 {
	LM_INIT = 0,
	LM_LOAD
 };

 enum GameLayers
 {
	GAME_LAYER = 0,
	HUD_LAYER = 1,
	LAYER_COUNT
 };
 
//-------------------------------------------------------------------------------------------

static void onEngineEvent(const struct EventCallbackData *pData);
static void callForEachGameObjectChangeCoortdinates(u32 layer, s32 iIdInitiator, s32 iId);

static void keyProcessing(void);

static void onUpdateObject(u32 layer, s32 iId, BOOL* const oDraw);
static void onUpdateBackgroundTiles(u32 layer);
static void onFinishUpdate(u32 layer);

static void releaseTengineData(void);

static void load(void);

static void game_step(s32 ms);

static void doResize(void);

static void onLevelMapLoad(void);
static void onLevelHudLoad(void);

//-------------------------------------------------------------------------------------------

void tfgInitMemory()
{
	InitMemoryAllocator();
}
//-------------------------------------------------------------------------------------------

void tfgInit()
{
    InitGamePad();
	InitTouchPad();
	sndInitSoundSystem();
	gameState = gstNONE;
	mthInitRandom();
}
//-------------------------------------------------------------------------------------------

void tfgRelease()
{
	releaseTengineData();
	sndReleaseSoundSystem();
}
//-------------------------------------------------------------------------------------------

void releaseTengineData()
{
	// to avoid memory fragmentation, releaseMapData in opposite side
	releaseResources();
	releaseMapData(HUD_LAYER);
	releaseMapData(GAME_LAYER);
	releaseTextData();
	releaseCommonData();
	releaseRenderPlane(BGSELECT_SUB3);
	releaseEngine();
}
//-------------------------------------------------------------------------------------------

void tfgResize(s32 w, s32 h)
{
	(void)w;
	(void)h;
	gameState = gstDISPLAYINIT;
	c_c = 0;
}
//-------------------------------------------------------------------------------------------

static void doResize()
{
	struct RenderPlaneInitParams mp1;
	mp1.mBGType = BGSELECT_SUB3;
	mp1.mSizes.mViewWidth = GAME_SCREEN_W; 
	mp1.mSizes.mViewHeight = GAME_SCREEN_H;
	mp1.mMaxRenderObjectsOnPlane = 512;
	mp1.mX = 0;
	mp1.mY = 0;

	if(!isRenderPlaneInit(GAME_LAYER)) // any active plane, whatever
	{
		struct InitEngineParameters initparams;
		releaseTengineData();
		
		initparams.layersCount = LAYER_COUNT;
		initparams.particlesPoolSize = 0;

		initEngine(&initparams);
		initRenderPlane(&mp1);
		
		assignLayerWithRenderPlane(GAME_LAYER, BGSELECT_SUB3, TRUE);
		assignLayerWithRenderPlane(HUD_LAYER, BGSELECT_SUB3, FALSE);

        //register callbacks
		setOnEvent(onEngineEvent);
		setOnUpdateGameObject(onUpdateObject);
		setOnUpdateBackgroundTiles(onUpdateBackgroundTiles);
		setOnFinishUpdate(onFinishUpdate);

		//begin load task
		//parameter will return with iData.initiatorId in onEngineEvent() function ( EVENT_TYPE_ON_END_ASYNH_LOAD_DATA)
		beginLoadListAsynh(LM_INIT);
		{
			//load common data
			addToLoadListCommonData();
			//load text data for certain language
			// to avoid memory fragmentation load it once after common data
			addToLoadListLanguageData(LANGUAGE_LNG_ENG);
		}
		//end load task
		endLoadListAsynh(FALSE);
	
		camera_pos.x = turret_pos.x = 0;
		camera_pos.y = turret_pos.y = 0;
		key_action = 0;
		c_c = 0;

		//wait for  EVENT_TYPE_ON_END_ASYNH_LOAD_DATA in onEngineEvent() function
		gameState = gstNONE;
	}
	else
	{
		resizeRenderPlane(GAME_LAYER, &mp1.mSizes);
		gameState = gstGAME;
	}
}
//-------------------------------------------------------------------------------------------

void load()
{
	// to avoid memory fragmentation, releaseMapData in opposite side
	releaseResources();	

	releaseMapData(HUD_LAYER);
	releaseMapData(GAME_LAYER);

	OS_Printf("mem on newGame = %u\n", GetFreeMemorySize());

	beginLoadListAsynh(LM_LOAD);
	{
		//layer LAYER_GAME, load current level
		addToLoadListMap(GAME_LAYER, 0);
		//layer LAYER_HUD, load or reload hud
		addToLoadListMap(HUD_LAYER, 1);
	}
	endLoadListAsynh(TRUE);
	
	//wait for  EVENT_TYPE_ON_END_ASYNH_LOAD_DATA and EVENT_TYPE_ON_END_ASYNH_LOAD_RESOURSES in onEngineEvent() function
	gameState = gstNONE;
}
//-------------------------------------------------------------------------------------------

// file index 1, layer GAME_LAYER
void onLevelMapLoad()
{
	c_c = 0;
	camera_start_y = FX32(getMapHeight() / 2);
	camera_pos.y = camera_start_y;
	need_rearrange_all_obj_posions = TRUE;
	missile[0] =  M0_MISSILE_34;
	missile[1] =  M0_MISSILE_35;
	missile[2] =  M0_MISSILE_36;
	end_game = FALSE;
	last_invader = -1;
	invaders_count = 0;
	final_scale_step = FINAL_FX_SCALE_INIT;
	resetParticles();
}
//-------------------------------------------------------------------------------------------

void callForEachGameObjectChangeCoortdinates(u32 layer, s32 iIdInitiator, s32 iId)
{
	(void)iIdInitiator;
	switch(layer)
	{
		case GAME_LAYER:
		{
			prAddXYToPosition(iId, 0, camera_start_y / 2 - ADDITIONAL_OBJ_OFFSET_Y);
			if(prGetGroupIdx(iId) == SPACE_INVADER)
			{
				invaders_count++;
			}
		}
	}
}
//-------------------------------------------------------------------------------------------

void onLevelHudLoad()
{
	// you can work with M1_* objects, because current active layer is HUD_LAYER
	struct fxVec2 pos;
    gameState = gstGAME;
	pos.x = 0;
	pos.y = FX32(-getMapHeight());
	prSetPosition(M1_TXTBOX_GAMEOVER, pos);
}
//------------------------------------------------------------------------------------

void tfgLostRenderDevice()
{
}
//-------------------------------------------------------------------------------------------

void tfgRestoreRenderDevice()
{
}
//------------------------------------------------------------------------------------

void tfgLowMemory()
{
}
//------------------------------------------------------------------------------------

void tfgTick(s32 ms)
{
	UpdateGamePad();
	keyProcessing();
	
	updateEngine(ms);

	switch(gameState)
	{
		case gstNONE:
		break;

		case gstDISPLAYINIT:
			doResize();
		break;

		case gstLOADING:
			load();
		break;

		case gstGAME:
			game_step(ms);
	}
}
//-------------------------------------------------------------------------------------------

void keyProcessing()
{
    key_action = 0;

    if(IsButtonPress(TYPE_L) && IsButtonPress(TYPE_R) && IsButtonPress(TYPE_SELECT) && IsButtonPress(TYPE_START))
    {
        OS_ResetSystem(0);
        return;
	}

    if(IsButtonDown(TYPE_START))
    {
        key_action = TYPE_START;
		gameState = gstLOADING;
    }
}
//----------------------------------------------------------------------------------

void game_step(s32 ms)
{	
	fps_timer += ms;
	if(IsButtonDown(TYPE_A))
	{
		need_shoot = TRUE;
	}
}
//----------------------------------------------------------------------------------

static void onUpdateBackgroundTiles(u32 layer)
{
	switch(layer)
	{
		case GAME_LAYER:
		{
			fx32 old_val;
			if(need_rearrange_all_obj_posions)
			{
				need_rearrange_all_obj_posions = FALSE;
				callForEachGameObject(0, callForEachGameObjectChangeCoortdinates);
			}
			old_val = camera_pos.y;
			//set camera onUpdateBackgroundTiles only
			camera_pos.y -= SCROLL_SPEED;
			if(camera_pos.y <= camera_start_y - REPETITIVE_BG_SIZE_Y)
			{
				fx32 delta = camera_pos.y - (camera_start_y - REPETITIVE_BG_SIZE_Y);
				camera_pos.y = camera_start_y + delta;
			}
			setCamera(camera_pos);
			scroll_delta = old_val - camera_pos.y;
			if(end_game == TRUE)
			{
				if(last_invader >= 0 && (prGetProperty(last_invader, PRP_ENABLE) == 0 || final_scale.x != FX32_ONE))
				{
					prSetProperty(last_invader, PRP_ENABLE, 1);
					final_scale.x += final_scale_step; 
					final_scale.y = final_scale.x;
					final_scale_step += FX32(0.2f);
					prSetCustomScale(last_invader, final_scale);
				}
			}
			invaders_movement_delay -= getTickDelay();
		}
		break;

		case HUD_LAYER:
		{
			if(final_scale.y > FINAL_FX_SCALE_MAX)
			{
				struct fxVec2 pos;
				final_scale.x = FINAL_FX_SCALE_MAX; 
				final_scale.y = final_scale.x;
				final_scale_step = 0;
				pos.x = (getViewWidth(HUD_LAYER) / 2) << FX32_SHIFT;
				pos.y = (getViewHeight(HUD_LAYER) / 2) << FX32_SHIFT;
				prSetPosition(M1_TXTBOX_GAMEOVER, pos);
			}
		}
	}
}
//----------------------------------------------------------------------------------

void onFinishUpdate(u32 layer)
{
	switch(layer)
	{
		case GAME_LAYER:
		{
			if(invaders_movement_delay < 0)
			{
				invaders_movement_delay = INVADER_STEP_DELAY;
				if(need_step_down)
				{
					need_step_down = FALSE;
				}
				if(need_change_direction)
				{
					need_change_direction = FALSE;
					invaders_direction_side = -invaders_direction_side;
					need_step_down = TRUE;
				}
			}
		}
	}
}
//----------------------------------------------------------------------------------

void onUpdateObject(u32 layer, s32 iId, BOOL* const oDraw)
{
	(void)oDraw;
	switch(layer)
	{
		case GAME_LAYER:
		{
			fx32 rect[RECT_SIZE];
			
			prAddXYToPosition(iId, 0, -scroll_delta);

			// invaders
			if(prGetGroupIdx(iId) == SPACE_INVADER && prGetState(iId) == STATE_ACTIVE && end_game == FALSE)
			{
				fx32 val, angry_invaders_max;
				s32 i;

				//invaders angry mode
				val = prGetProperty(iId, PRP_ANGRY);
				if(val > 0)
				{
					struct fxVec2 scale;
					scale.x = scale.y = FX32_ONE + FX_Div(FX32(val), FX32(1000)) / 3;
					prSetCustomScale(iId, scale);
					val -= getTickDelay();
					if(val < 0)
					{
						angry_invaders_count--;
						val = 0;
					}
					prSetProperty(iId, PRP_ANGRY, val);
				}
				angry_invaders_max = 1 + invaders_count / 10;
				if(angry_invaders_count < angry_invaders_max && mthGetRandom(1000) == 143)
				{
					prSetProperty(iId, PRP_ANGRY, ANGRY_TIME);
				}

				prGetCollideRect(iId, rect);

				//invaders movement
				if(invaders_movement_delay < 0)
				{
					prAddXYToPosition(iId, INVADER_STEP_X * invaders_direction_side, need_step_down ? INVADER_STEP_Y : 0);
					if((rect[RECT_RIGHT_TOP_X] >= FX32(getMapWidth()) && invaders_direction_side > 0) ||
						 (rect[RECT_LEFT_TOP_X] <= 0 && invaders_direction_side < 0))
					{
						need_change_direction = TRUE;
					}
				}

				//invaders collisions
				for(i = 0; i < MISSILES_MAX; i++)
				{
					if(prGetProperty(missile[i], PRP_ENABLE) == 1)
					{
						fx32 m_rect[RECT_SIZE];
						prGetCollideRect(missile[i], m_rect);
						if(m_rect[RECT_LEFT_TOP_X] <= rect[RECT_RIGHT_TOP_X] && m_rect[RECT_RIGHT_TOP_X] >= rect[RECT_LEFT_TOP_X] && 
							m_rect[RECT_LEFT_BOTTOM_Y] >= rect[RECT_RIGHT_TOP_Y] && m_rect[RECT_RIGHT_TOP_Y] <= rect[RECT_LEFT_BOTTOM_Y])
						{
							prSetProperty(missile[i], PRP_ENABLE, 0);
							prSetState(iId, STATE_DAMAGED, SBM_NONE);
							invaders_count--;
							if(invaders_count <= 0)
							{
								last_invader = iId;
								final_scale.x = final_scale.y = FX32_ONE;
								end_game = TRUE;
							}
							{
								struct SoundData sd;
								sd.Id = M0_SND_EXPLOSION;
								sd.Looped = FALSE;
								sd.Type = SOUND_TYPE_SFX;
								sd.Volume = FX32(0.5f);
								sndPlay(&sd);
							}
							break;
						}
					}
				}

				{
					fx32 b_rect[RECT_SIZE];
					prGetCollideRect(M0_BASE_37, b_rect);
					if(rect[RECT_LEFT_BOTTOM_Y] >= b_rect[RECT_LEFT_TOP_Y])
					{
						last_invader = iId;
						final_scale.x = final_scale.y = FX32_ONE;
						prSetProperty(last_invader, PRP_ENABLE, 0);
						end_game = TRUE;
					}
				}
			}

			// missiles
			if(prGetGroupIdx(iId) == MISSILE)
			{
				prAddXYToPosition(iId, 0, -MISSILE_STEP);
				prGetCollideRect(iId, rect);
				if(rect[RECT_LEFT_TOP_Y] <= camera_start_y / 4)
				{
					prSetProperty(iId, PRP_ENABLE, 0);
				}
			}

			// turret
			switch(iId)
			{
				case M0_TURRET_33:
					if(end_game == FALSE)
					{
						if(IsButtonPress(TYPE_RIGHT))
						{	
							prAddXYToPosition(iId, TURRET_STEP, 0);
							prGetCollideRect(iId, rect);
							if(rect[RECT_RIGHT_TOP_X] >= FX32(getMapWidth()))
							{
								prAddXYToPosition(iId, -TURRET_STEP, 0);
							}
						}
						if(IsButtonPress(TYPE_LEFT))
						{	
							prAddXYToPosition(iId, -TURRET_STEP, 0);
							prGetCollideRect(iId, rect);
							if(rect[RECT_LEFT_TOP_X] <= 0)
							{
								prAddXYToPosition(iId, TURRET_STEP, 0);
							}
						}
						if(need_shoot)
						{
							s32 i;
							need_shoot = FALSE;
							for(i = 0; i < MISSILES_MAX; i++)
							{
								if(prGetProperty(missile[i], PRP_ENABLE) == 0)
								{
									struct fxVec2 pos = prGetPosition(M0_TURRET_33);
									prSetPosition(missile[i], pos);
									prSetProperty(missile[i], PRP_ENABLE, 1);
									{
										struct SoundData sd;
										sd.Id = M0_SND_LASER_SHOT;
										sd.Looped = FALSE;
										sd.Type = SOUND_TYPE_SFX;
										sd.Volume = FX32(0.5f);
										sndPlay(&sd);
									}
									break;
								}
							}
						}
					}
				break;	
			}
		}
		break;
		
		case HUD_LAYER:
		{
			switch(iId)
			{
				case M1_TXTBOX_FPS:
				{
					fps_tick++;
					if(fps_timer >= 1000)
					{
						wchar aaa[16];
						wchar bbb[16];
						bbb[0] = L'f';
						bbb[1] = L'p';
						bbb[2] = L's';
						bbb[3] = L'=';
						bbb[4] = L'%';
						bbb[5] = L'd';
						bbb[6] = L'\0';
						STD_WSprintf(aaa, bbb, fps_tick);
						txbSetDynamicTextLine(M1_TXTBOX_FPS, 0, aaa);
						fps_tick = fps_timer = 0;
					}		
				}
			}
		}
	}
}
//----------------------------------------------------------------------------------

//CALLBACK FUNCTIONS

//----------------------------------------------------------------------------------

static void onEngineEvent(const struct EventCallbackData *pData)
{
	switch(pData->eventType)
	{
		case  EVENT_TYPE_ON_END_ASYNH_LOAD_DATA:
			switch(pData->eventId)
			{
				case LOAD_TYPE_ENDLOADDATATASK:
					if(pData->initiatorId == LM_INIT)
					{
						gameState = gstLOADING;
					}
				break;
				case LOAD_TYPE_MAPDATA:
					if(pData->initiatorId == LM_LOAD)
					{
						switch(pData->layer)
						{
							case HUD_LAYER:
								onLevelHudLoad();
							break;
							case GAME_LAYER:
								onLevelMapLoad();
						}
					}
			}
		break;
		
		case EVENT_TYPE_ON_END_ASYNH_LOAD_RESOURSES:
			setVisible(HUD_LAYER, TRUE);
			setVisible(GAME_LAYER, TRUE);
			gameState = gstGAME;
			{
				struct SoundData sd;
				sd.Id = M0_SND_LASER_SHOT;
				sd.Looped = TRUE;
				sd.Type = SOUND_TYPE_BGM1;
				sd.Volume = FX32(0.4f);
				sndPlay(&sd);
			}
		break;

		// this custom event (EVENT_END_ANIM_DAMAGE_INVADER) was created in me3 and assigned to last frame space invader animation ANIM_DAMAGED 
		case EVENT_TYPE_ON_ANIMATION:
			if(pData->eventId == EVENT_END_ANIM_DAMAGE_INVADER)
			{
				prSetProperty(pData->ownerId, PRP_ENABLE, 0);	
			}
	}
}
//----------------------------------------------------------------------------------
