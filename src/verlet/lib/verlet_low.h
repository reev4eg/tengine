#ifndef __VERLET_LOW_H__
#define __VERLET_LOW_H__

#include "fxmath.h"

#ifdef __cplusplus
extern "C" {
#endif

//---------------------------------------------------------------------------

struct Verlet;
enum VTObjectType;

enum VTConstraintType
{
	VTCT_PIN = 0,
	VTCT_DISTANCE,
	VTCT_ANGLE
};

enum VTObjectState
{
	VOS_UNDEF = 0,
	VOS_INIT,
	VOS_READY
};

struct VTParticle
{
	struct fxVec2 pos;
	struct fxVec2 lastPos;
};

struct VTConstraint
{
	enum VTConstraintType mType;
	struct VTParticle* mpParticle1;
	struct VTParticle* mpParticle2;
	struct VTParticle* mpParticle3;
	struct fxVec2 mPos;
};

struct VTObject
{
	enum VTObjectState mState;
	s32 mParticlesCount;
	s32 mConstraintsCount;
	s32 mParticlesMax;
	s32 mConstraintsMax;
	fx32 mFriction;
	fx32 mGroundFriction;
	struct VTParticle** mppParticles;
	struct VTConstraint** mppConstraints;
	struct fxVec2 mGravity;
};

//---------------------------------------------------------------------------

struct VTObject* _vrltObjectPop(struct Verlet *v);

struct VTParticle* _vrltParticlePushToObj(struct Verlet *v, struct VTObject *c);
void _vrltInitParticle(struct VTParticle *v, const struct fxVec2 *pos);

void _vrltPinConstraintPushToObj(struct Verlet *v, struct VTObject *c, s32 index, struct fxVec2 *pos);
void _vrltDistanceConstraintPushToObj(struct Verlet *v, struct VTObject *c, s32 index1, s32 index2, fx32 stiffness, fx32 *distance);
void _vrltAngleConstraintPushToObj(struct Verlet *v, struct VTObject *c, s32 index1, s32 index2, s32 index3, fx32 stiffness);

void _vrltConstraintRelax(struct VTConstraint *c, fx32 k);

//---------------------------------------------------------------------------

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif