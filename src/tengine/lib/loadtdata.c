/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include "lib/tengine_low.h"
#include "lib/loadtdata.h"
#include "lib/jobs_low.h"
#include "lib/sound_low.h"
#include "filesystem.h"
#include "loadhelpers.h"
#include "texts.h"
#include "jobs.h"

#ifdef USE_CUSTOM_RENDER
	#include "easygraphics.h"
#endif 

#ifdef USE_OPENGL_RENDER
	#include "render2dgl.h"
#endif 

// ----------------------------------------------------------------------------------

static const char FILENAME_O = 'o'; 
static const char FILENAME_L = 'l'; 
static const char FILENAME_T = 't'; 

extern struct TEngineCommonData cd;
static struct JOBFileStreamTask ld_fst;
static char _gsfileName[MAX_FILENAME];

static void _loadFrames(struct TEngineCommonData *cd, s32 anm_ct, s32 i, s32 j, const u8 **inputstream);
static void _releaseFont(struct TERFont *oFont);
static void _getFileName(s32 idx, s32 type, char *iofileName, struct TEngineCommonData *cd);
static void _loadResource(s32 idx, s32 resIdx, s32 type, u8 value, void **oData, struct TEngineCommonData *cd);
static void _initFont(const u8* iData, struct TERFont *oFont, struct TEngineCommonData *cd);
static void _createZoneArrays(struct TEngineInstance * i);
static void _releaseZoneArrays(struct TEngineInstance * i);

static void _addToLoadList(u8 type, u32 p1, u16 p2);
static void _readCommonData(struct TEngineCommonData *cd, u8 *filedata);
static void _releaseCommonData(struct TEngineCommonData *cd);
static void _readMap(u32 layer, u8 *level, struct TEngineCommonData *cd);
static void _readTextData(u8* language, struct TEngineCommonData *cd);

static void _asynhloadFileInit(const char* fname, enum JFSDMode mode, u16 resIdx, u8 value);
static BOOL _asynhloadFileCheck(u8** data);
static void _initFile_ALS_STATE_LOAD_DATA(s32 l);

static void _readTriggers(struct TEngineInstance *i, const u8 **inputstream, BOOL common_mode, struct TEngineCommonData *cd);
static void _readScripts(struct TEngineInstance *i, const u8 **inputstream, BOOL common_mode, struct TEngineCommonData *cd);

static BOOL _assignRes_begin(struct TEngineCommonData *cd);
static BOOL _assignRes_load(struct TEngineCommonData *cd);
static void _assignRes_end(void);
static void _assignResUpdate(struct TEngineCommonData *cd);

// ----------------------------------------------------------------------------------

enum
{
	ALD_CD = 1,
	ALD_MD,
	ALD_LD
};
// ----------------------------------------------------------------------------------

void _loadDataInit()
{
	MI_CpuFill8(&ld_fst, 0, sizeof(struct JOBFileStreamTask));
	_gsfileName[0] = 0;
}
// ----------------------------------------------------------------------------------

void beginLoadListAsynh(s32 cb_parameter)
{
	if(cd.ld_st != ALS_STATE_READY || cd.agrl_s != 0)
	{
		// call this function only after  EVENT_TYPE_ON_END_ASYNH_LOAD_DATA event
		OS_Warning("beginLoadListAsynh: Trying to call a function during async loading process");
		SDK_ASSERT(0);
		return;
	}
	{
		u32 j;
		for (j = 0; j < cd.initParams.layersCount; j++)
		{
			struct TEngineInstance *ei = _getInstance(j);
			if(ei->bgType == BGSELECT_NUM)
			{
				OS_Warning("ASSERT: Please use assignLayerWithRenderPlane function before readMap (assign layer to render plane)");
				SDK_ASSERT(0);
				return;
			}
		}
	}
	cd.ld_param = cb_parameter; 
	cd.ld_st = ALS_STATE_LIST;
	cd.ld_ct_t = cd.ld_ct = 0;
}
// ----------------------------------------------------------------------------------

static void _addToLoadList(u8 type, u32 p1, u16 p2)
{
	if(cd.ld_st == ALS_STATE_LIST && (s16)(cd.initParams.layersCount + 2) > cd.ld_ct)
	{
		s32 i;
		for(i = 0; i < cd.ld_ct; i++)
		{
			if(cd.ld_task[i] == type && cd.ld_p1[i] == p1 && cd.ld_p2[i] == p2)
			{
				return;
			}
		}
		cd.ld_task[cd.ld_ct] = type;
		cd.ld_p1[cd.ld_ct] = p1;
		cd.ld_p2[cd.ld_ct] = p2;
		cd.ld_ct++;
		cd.ld_ct_t = cd.ld_ct; 
	}
}
// ----------------------------------------------------------------------------------

void addToLoadListCommonData(void)
{
	_addToLoadList(ALD_CD, 0, 0);
}
// ----------------------------------------------------------------------------------

void addToLoadListMap(u32 layer, u16 level)
{
	_addToLoadList(ALD_MD, layer, level);
}
// ----------------------------------------------------------------------------------

void addToLoadListLanguageData(u32 language_id)
{
	_addToLoadList(ALD_LD, language_id, 0);
}
// ----------------------------------------------------------------------------------

void endLoadListAsynh(BOOL loadGraphicResources)
{
	if(cd.ld_ct > 0 && cd.ld_st == ALS_STATE_LIST)
	{
		s32 i;
		for(i = 0; i < cd.ld_ct / 2; i++)
		{
			s16 t8;
			u16 t16;
			u32 t32;
			t8 = cd.ld_task[i]; 
			cd.ld_task[i] = cd.ld_task[cd.ld_ct - i - 1];
			cd.ld_task[cd.ld_ct - i - 1] = t8;
			t32 = cd.ld_p1[i];
			cd.ld_p1[i] = cd.ld_p1[cd.ld_ct - i - 1];
			cd.ld_p1[cd.ld_ct - i - 1] = t32;
			t16 = cd.ld_p2[i];
			cd.ld_p2[i] = cd.ld_p2[cd.ld_ct - i - 1];
			cd.ld_p2[cd.ld_ct - i - 1] = t16;
		}
		cd.ld_st = ALS_STATE_LOAD_DATA;
		jobSetActive(TRUE);
	}
	else
	{
		cd.ld_st = ALS_STATE_READY;
	}
	cd.ag_st = loadGraphicResources;
}
// ----------------------------------------------------------------------------------

void _asynhloadFileInit(const char* fname, enum JFSDMode mode, u16 resIdx, u8 value)
{
#ifdef JOBS_IN_SEPARATE_THREAD
	jobCriticalSectionBegin();
#endif
	ld_fst.mData.mpFilename = fname;
	ld_fst.mData.mpFile = NULL;
	ld_fst.mData.mpImage = NULL;	
	ld_fst.mData.mStatus = FALSE;
	ld_fst.mData.mMode = (u8)mode;	
	ld_fst.mData.mValue = value; 
	ld_fst.mData.mIdx = resIdx;
	jobAddStreamFileTask(&ld_fst);
#ifdef JOBS_IN_SEPARATE_THREAD
	jobCriticalSectionEnd();
#endif
}
// ----------------------------------------------------------------------------------

BOOL _asynhloadFileCheck(u8** data)
{
	if(ld_fst.mData.mStatus == TRUE)
	{
#ifdef JOBS_IN_SEPARATE_THREAD
		jobCriticalSectionBegin();
#endif
		jobRemoveStreamFileTask(&ld_fst);
		ld_fst.mData.mMode = JFSDM_MODE_NONE;
		if(data)
		{
			*data = ld_fst.mData.mpFile != NULL ? ld_fst.mData.mpFile : (u8*)ld_fst.mData.mpImage;
		}
		ld_fst.mData.mpFile = NULL;
		ld_fst.mData.mpImage = NULL;
		ld_fst.mData.mStatus = FALSE;
#ifdef JOBS_IN_SEPARATE_THREAD
		jobCriticalSectionEnd();
#endif
		return TRUE;
	}
	return FALSE;
}
// ----------------------------------------------------------------------------------

void _initFile_ALS_STATE_LOAD_DATA(s32 l)
{
	switch(cd.ld_task[l])
	{
		case ALD_CD:
			_gsfileName[0] = FILENAME_O;
			_gsfileName[1] = 0;
			_asynhloadFileInit(_gsfileName, JFSDM_MODE_FILE, 0, 0);
		break;
		case ALD_MD:
		{
			_gsfileName[0] = FILENAME_L;
			_gsfileName[1] = 0;
			STD_NumToString(cd.ld_p2[l], &_gsfileName[1], 6);
			_asynhloadFileInit(_gsfileName, JFSDM_MODE_FILE, 0, 0);
		}
		break;
		case ALD_LD:
		{
			_gsfileName[0] = FILENAME_T;
			_gsfileName[1] = 0;
			STD_NumToString((u16)cd.ld_p1[l], &_gsfileName[1], 6);
			_asynhloadFileInit(_gsfileName, JFSDM_MODE_FILE, 0, 0);
		}
	}
}
// ----------------------------------------------------------------------------------

void _updateLoadingProcess()
{
	cd.draw = TRUE;
	if(cd.ld_st == ALS_STATE_LOAD_DATA)
	{
		u8* buf;
		if(ld_fst.mData.mMode == JFSDM_MODE_NONE)
		{
			_initFile_ALS_STATE_LOAD_DATA(cd.ld_ct - 1);
		}
		if(_asynhloadFileCheck(&buf))
		{
			switch(cd.ld_task[cd.ld_ct - 1])
			{
				case ALD_CD:
					_readCommonData(&cd, buf);
				break;
				case ALD_MD:
					_readMap(cd.ld_p1[cd.ld_ct - 1], buf, &cd);
				break;
				case ALD_LD:
					_readTextData(buf, &cd);
				break;
				default:
					SDK_ASSERT(0);
			}
			cd.ld_ct--;
		}
		if(cd.ld_ct == 0)
		{
			cd.ld_st = ALS_STATE_READY;
			if(cd._onEventCB != NULL)
			{
				struct EventCallbackData ed;
				ed.eventType =  EVENT_TYPE_ON_END_ASYNH_LOAD_DATA;
				ed.initiatorId = cd.ld_param;
				while(--cd.ld_ct_t > -1)
				{
					switch(cd.ld_task[cd.ld_ct_t])
					{
						case ALD_CD:
							ed.layer = -1; 
							ed.eventId = ed.ownerId = LOAD_TYPE_COMMONDATA;
							cd._onEventCB(&ed);
							break;
						case ALD_MD:
							SDK_NULL_ASSERT(cd.instances[cd.ld_p1[cd.ld_ct_t]]->map);
							_setActiveLayer(cd.ld_p1[cd.ld_ct_t]); 
							ed.layer = cd.ld_p1[cd.ld_ct_t];
							ed.eventId = ed.ownerId = LOAD_TYPE_MAPDATA;
							cd.rl_available = TRUE;
							cd._onEventCB(&ed);
							cd.rl_available = FALSE;
							break;
						case ALD_LD:
							ed.layer = -1; 
							ed.eventId = ed.ownerId = LOAD_TYPE_TEXTDATA;
							cd._onEventCB(&ed);
							break;
						default:
							SDK_ASSERT(0);
					}
				}
				ed.layer = -1; 
				ed.eventId = ed.ownerId = LOAD_TYPE_ENDLOADDATATASK;
				cd._onEventCB(&ed);
			}
			cd.ld_param = 0;
		}
	}
	else
	{
		if(cd.ag_st)
		{
			_assignResUpdate(&cd);	
		}
	}
}
// ----------------------------------------------------------------------------------

static void _readCommonData(struct TEngineCommonData *cd, u8* filedata)
{
	const u8 *inputstream;
	s32 i, j;
	u32 k;

	SDK_ASSERT(cd->common_data_file == NULL); // only one common data can be loaded

	inputstream = cd->common_data_file = filedata;
	SDK_NULL_ASSERT(inputstream);
	if (inputstream != NULL)
	{
		cd->delay = (u16)_NBytesToInt(inputstream, 2, 0);
		inputstream += 2;
		cd->enable_prp_id = (u16)_NBytesToInt(inputstream, 2, 0);
		inputstream += 2;
		cd->spd_prp_id = (u16)_NBytesToInt(inputstream, 2, 0);
		inputstream += 2;
		cd->p_w = (u16)_NBytesToInt(inputstream, 2, 0);
		inputstream += 2;
		cd->p_h = (u16)_NBytesToInt(inputstream, 2, 0);
		inputstream += 2;
		cd->tl_layers_ct = *inputstream;
		inputstream++;
		cd->res_img_ct = (u16)_NBytesToInt(inputstream, 2, 0);
		inputstream += 2;
		cd->res_img_fnames = NULL;
		cd->res_img = NULL;
		if(cd->res_img_ct > 0)
		{
			inputstream += *inputstream + 1;
			cd->res_img_fnames = (const char**)inputstream;
			inputstream += cd->res_img_ct * sizeof(void*);
			inputstream += *inputstream + 1;			
			cd->res_img = (struct BMPImage**)inputstream;
			inputstream += cd->res_img_ct * sizeof(void*);
			for (i = 0; i < cd->res_img_ct; i++)
			{
				k = *inputstream;
				inputstream++;
				inputstream += *inputstream + 1;
				cd->res_img_fnames[i] = (char*)inputstream;
				inputstream += k;
				cd->res_img[i] = NULL;
			}
		}
		cd->res_fnt_ct = (u16)_NBytesToInt(inputstream, 2, 0);
		inputstream += 2;
		cd->res_fnt_fnames = NULL;
		cd->res_fnt = NULL;
		if(cd->res_fnt_ct > 0)
		{
			inputstream += *inputstream + 1;
			cd->res_fnt_fnames = (const char**)inputstream;
			inputstream += cd->res_fnt_ct * sizeof(void*);
			inputstream += *inputstream + 1;
			cd->res_fnt = (struct TERFont**)inputstream;
			inputstream += cd->res_fnt_ct * sizeof(void*);
			for (i = 0; i < cd->res_fnt_ct; i++)
			{
				k = *inputstream;
				inputstream++;
				inputstream += *inputstream + 1;
				cd->res_fnt_fnames[i] = (char*)inputstream;
				inputstream += k;
				cd->res_fnt[i] = NULL;
			}
		}
		cd->res_snd_ct = (u16)_NBytesToInt(inputstream, 2, 0);
		inputstream += 2;
		cd->res_snd_fnames = NULL;
		cd->res_snd = NULL;
		if(cd->res_snd_ct > 0)
		{
			inputstream += *inputstream + 1;
			cd->res_snd_fnames = (const char**)inputstream;
			inputstream += cd->res_snd_ct * sizeof(void*);
			inputstream += *inputstream + 1;
			cd->res_snd = (struct TERSound**)inputstream;
			inputstream += cd->res_snd_ct * sizeof(void*);
			for (i = 0; i < cd->res_snd_ct; i++)
			{
				k = *inputstream;
				inputstream++;
				inputstream += *inputstream + 1;
				cd->res_snd_fnames[i] = (char*)inputstream;
				inputstream += k;
				cd->res_snd[i] = NULL;
			}
		}
		cd->anim_ct = *inputstream;
		inputstream++;
		cd->states_ct = *inputstream;
		inputstream++;
		inputstream += *inputstream + 1;
		cd->states = (const u8**)inputstream;
		inputstream += cd->states_ct * sizeof(void*);
		for (i = 0; i < cd->states_ct; i++)
		{
			inputstream += *inputstream + 1;
			cd->states[i] = (u8*)inputstream;
			inputstream += stCOUNT;
		}
		inputstream += *inputstream + 1;
		cd->states_map = (u8*)inputstream; 
		inputstream += cd->states_ct;
		j = _NBytesToInt(inputstream, 2, 0);
		inputstream += 2;
		cd->fr_data = NULL;
		if(j > 0)
		{
			inputstream += *inputstream + 1;
			cd->fr_data = (const u16**)inputstream;
			inputstream += j * sizeof(void*);
			for (i = 0; i < j; i++)
			{
				inputstream += *inputstream + 1;
				cd->fr_data[i] = (u16*)inputstream;
				inputstream += resobjprpCount * sizeof(u16);
			}
		}
		cd->ppr_count = _NBytesToInt(inputstream, 2, 0);
		inputstream += 2;
		cd->ppr_decortype = NULL;
		cd->ppr_anim_data = NULL;
		cd->ppr_fr_data = NULL;
		cd->ppr_frfr_data = NULL;
		if(cd->ppr_count > 0)
		{
			inputstream += *inputstream + 1;
			cd->ppr_decortype = (s16*)inputstream;
			inputstream += cd->ppr_count * sizeof(s16);

			inputstream += *inputstream + 1;
			cd->ppr_anim_data = (s16***)inputstream;
			inputstream += cd->ppr_count * sizeof(s16**);

			inputstream += *inputstream + 1;
			cd->ppr_fr_data = (const s32****)inputstream;
			inputstream += cd->ppr_count * sizeof(const s32***);

			inputstream += *inputstream + 1;
			cd->ppr_frfr_data = (const s16*****)inputstream;
			inputstream += cd->ppr_count * sizeof(s16****);

			for (i = 0; i < cd->ppr_count; i++)
			{
				cd->ppr_decortype[i] = (s16)_NBytesToInt(inputstream, 2, 0);
				inputstream += 2;

				inputstream += *inputstream + 1;
				cd->ppr_anim_data[i] = (s16**)inputstream;
				inputstream += cd->anim_ct * sizeof(s16*);

				inputstream += *inputstream + 1;
				cd->ppr_fr_data[i] = (const s32***)inputstream;
				inputstream += cd->anim_ct * sizeof(s32**);

				inputstream += *inputstream + 1;
				cd->ppr_frfr_data[i] = (const s16****)inputstream;
				inputstream += cd->anim_ct * sizeof(s16***);

				for (j = 0; j < cd->anim_ct; j++)
				{
					s32 a = _NBytesToInt(inputstream, 2, 0);
					inputstream += 2;
					if(a > 0)
					{
						inputstream += *inputstream + 1;
						cd->ppr_anim_data[i][j] = (s16*)inputstream;
						inputstream += ppranidataCOUNT * sizeof(s16);
						if(cd->ppr_anim_data[i][j][ANIFRCOUNT] > 0)
						{
							_loadFrames(cd, cd->ppr_anim_data[i][j][ANIFRCOUNT], i, j, &inputstream);
						}
					}
                }
			}
		}
		for (j = 0; j < (s32)cd->initParams.layersCount; j++)
		{
			const u8 *tinputstream = inputstream;
			_readTriggers(cd->instances[j], &tinputstream, TRUE, cd);
			cd->common_data_tr_pt = tinputstream; 
			_readScripts(cd->instances[j], &tinputstream, TRUE, cd);
			cd->common_data_sp_pt = cd->common_data_tr_pt;
			cd->common_data_tr_pt = inputstream;
			
			for (i = 0; i < BGSELECT_NUM; i++)
			{
				cd->instances[j]->tsdy8_fx[i] = -FX32_ONE;
				cd->instances[j]->tsdx8_fx[i] = -FX32_ONE;
				if(cd->instances[j]->ds_plane_fr_array_beg[i] < 0)
				{
					cd->instances[j]->ds_plane_fr_array_beg[i] = 0;
				}
				else
				{
					SDK_ASSERT(cd->instances[j]->ds_plane_fr_array_beg[i] >= 0 && cd->instances[j]->ds_plane_fr_array_beg[i] <= cd->tl_layers_ct);
				}

				if(cd->instances[j]->ds_plane_fr_array_end[i] < 0)
				{
					cd->instances[j]->ds_plane_fr_array_end[i] = (s32)cd->tl_layers_ct;
				}
				else
				{
					SDK_ASSERT(cd->instances[j]->ds_plane_fr_array_end[i] >= 0 && cd->instances[j]->ds_plane_fr_array_end[i] <= cd->tl_layers_ct);
				}
			}
			assignLayerWithRenderPlane(j, cd->instances[j]->bgType, cd->instances[j]->primary);
		}

	}
}
// ----------------------------------------------------------------------------------

void _loadFrames(struct TEngineCommonData *cd, s32 anm_ct, s32 i, s32 j, const u8 **inputstream)
{
	s32 a;
	cd->ppr_anim_data[i][j][ANIFRCOUNT] = (s16)anm_ct; 
	*inputstream += **inputstream + 1;
	cd->ppr_fr_data[i][j] = (const s32**)*inputstream;
	*inputstream += cd->ppr_anim_data[i][j][ANIFRCOUNT] * sizeof(const s32*);
	*inputstream += **inputstream + 1;
	cd->ppr_frfr_data[i][j] = (const s16***)*inputstream;
	*inputstream += cd->ppr_anim_data[i][j][ANIFRCOUNT] * sizeof(s16**);

	for (a = 0; a < cd->ppr_anim_data[i][j][ANIFRCOUNT]; a++)
	{
		*inputstream += **inputstream + 1;
		cd->ppr_fr_data[i][j][a] = (const s32*)*inputstream;
		*inputstream += ppranifrdataCOUNT * sizeof(const s32);
		if(cd->ppr_fr_data[i][j][a][ANIFRFRCOUNT] > 0)
		{
			s32 b = 0;
			*inputstream += **inputstream + 1;
			cd->ppr_frfr_data[i][j][a] = (const s16**)*inputstream;
			*inputstream += cd->ppr_fr_data[i][j][a][ANIFRFRCOUNT] * sizeof(s16*);

			for(; b < cd->ppr_fr_data[i][j][a][ANIFRFRCOUNT]; b++)
			{
				*inputstream += **inputstream + 1;
				cd->ppr_frfr_data[i][j][a][b] = (const s16*)*inputstream;
				*inputstream += ppranifrfrdataCOUNT * sizeof(s16);
			}
		}
	}
}
// ----------------------------------------------------------------------------------

void releaseCommonData()
{
	_releaseCommonData(&cd);
}
// ----------------------------------------------------------------------------------

static void _releaseCommonData(struct TEngineCommonData *cd)
{
	s32 i;
	cd->res_img_fnames = NULL;
	cd->res_fnt_fnames = NULL;
	cd->res_snd_fnames = NULL;
	cd->fr_data = NULL;
	cd->ppr_decortype = NULL;
	cd->ppr_anim_data = NULL;
	cd->ppr_fr_data = NULL;
	cd->ppr_frfr_data = NULL;
	i = cd->res_img_ct;
	while(i > 0)
	{
		i--;
		if (cd->res_img[i] != NULL)
		{
			SDK_ASSERT(0); //please call releaseMapData before
		}
	}
	i = cd->res_fnt_ct;
	while(i > 0)
	{
		i--;
		if (cd->res_fnt[i] != NULL)
		{
			SDK_ASSERT(0); //please call releaseMapData before
		}
	}
	i = cd->res_snd_ct;
	while(i > 0)
	{
		i--;
		if (cd->res_snd[i] != NULL)
		{
			SDK_ASSERT(0); //please call releaseMapData before
		}
	}
	cd->states_map = NULL;
	cd->states = NULL;
	cd->res_img = NULL;
    cd->res_fnt = NULL;
    cd->res_snd = NULL;
	if(cd->common_data_file != NULL)
	{
		FREE(cd->common_data_file);
		cd->common_data_file = NULL;
		cd->common_data_tr_pt = NULL;
		cd->common_data_sp_pt = NULL;
	}
}
// ----------------------------------------------------------------------------------

void releaseTextData(void)
{
	if(cd.text_data_file != NULL)
	{
		FREE(cd.text_data_file);
		cd.text_data_file = NULL;
		cd.textdatatext = NULL;
		cd.textdatatext_sct = NULL;
	}
}
// ----------------------------------------------------------------------------------

static void _readTextData(u8* language, struct TEngineCommonData *cd)
{
	const u8 *inputstream;
	s32 i, j, c, k;

	SDK_ASSERT(cd->text_data_file == NULL); // only one language can be loaaded
	
	inputstream = cd->text_data_file = language;
	SDK_NULL_ASSERT(inputstream);
	if (inputstream != NULL)
	{
		i = _NBytesToInt(inputstream, 2, 0);
		inputstream += 2;
		if(i > 0)
		{
			inputstream += *inputstream + 1;
			cd->textdatatext = (const wchar***)inputstream;
			inputstream += i * sizeof(wchar***);
			inputstream += *inputstream + 1;
			cd->textdatatext_sct = (u16*)inputstream;
			inputstream += i * sizeof(u16);
			for(j = 0; j < i; j++)
			{
				c = _NBytesToInt(inputstream, 2, 0);
				inputstream += 2;
				inputstream += *inputstream + 1;
				cd->textdatatext[j] = (const wchar**)inputstream;
				inputstream += c * sizeof(wchar**);
				cd->textdatatext_sct[j] = (u16)c;
				for(k = 0; k < c; k++)
				{
					inputstream += *inputstream + 1;
					cd->textdatatext[j][k] = (wchar*)inputstream;
					inputstream += (*inputstream + 1 + 1) * sizeof(wchar);
				}
			}
		}
	}
}
// ----------------------------------------------------------------------------------

void _readMap(u32 layer, u8 *level, struct TEngineCommonData *cd)
{
	s32 strm_af_ct, j;
	s32 all_db_count, all_df_count, uz_db, uz_df, uz_pr, i;
	s32 start_script, camera_pos;
	const u8 *inputstream = NULL;
	const u16 *strm_ppraniarr = NULL;
	
	struct TEngineInstance *ei = _getInstance(layer);
	SDK_NULL_ASSERT(ei);

	start_script = -1;
	camera_pos = -1;
	cd->global_delay_ct = 0;

	inputstream = ei->data_file = level;
	SDK_NULL_ASSERT(inputstream);
	SDK_ASSERT(ei->ld_img_res_ct == 0); // free memory from resources before!
	SDK_ASSERT(ei->ld_snd_sfx_res_ct == 0);
	SDK_ASSERT(ei->ld_snd_bgm_res_ct == 0);
	SDK_ASSERT(ei->ld_fnt_res_ct == 0);

	if (inputstream != NULL)
	{
		i = _NBytesToInt(inputstream, 2, 0);
		inputstream += 2;
		j = _NBytesToInt(inputstream, 2, 0);
		inputstream += 2;
		ei->zones_ct = *inputstream;
		inputstream++;
		ei->maxbgres = (u16)_NBytesToInt(inputstream, 2, 0); 
		inputstream += 2;
		ei->pnt_count = _NBytesToInt(inputstream, 2, 0);
		inputstream += 2;
		if(ei->pnt_count > 0)
		{
			inputstream += *inputstream + 1;
			ei->tile_data = (const s16**)inputstream;
			inputstream += ei->pnt_count * sizeof(s16**);
			inputstream += *inputstream + 1;
			ei->fr_img = (const u16***)inputstream;
			inputstream += ei->pnt_count * sizeof(u16***);
			for (i = 0; i < ei->pnt_count; i++)
			{
				inputstream += *inputstream + 1;
				ei->tile_data[i] = (s16*)inputstream;
				inputstream += tileprpCount * sizeof(s16);
				if(ei->tile_data[i][TILE_LAYERS] > 0)
				{
					inputstream += *inputstream + 1;
					ei->fr_img[i] = (const u16**)inputstream;
					inputstream += ei->tile_data[i][TILE_LAYERS] * sizeof(u16*);
				}
				for (j = 0; j < ei->tile_data[i][TILE_LAYERS]; j++)
				{
					inputstream += *inputstream + 1;
					ei->fr_img[i][j] = (u16*)inputstream;
					inputstream += resfrprpCount * sizeof(u16);
				}
			}
		}
		ei->map_w = _NBytesToInt(inputstream, 2, 0);
		inputstream += 2;
		ei->map_h = _NBytesToInt(inputstream, 2, 0);
		inputstream += 2;

		_calculateLogicSizes(ei, cd);

		// OS_Printf("ei->map_w=%d map_h=%d\n",ei->map_w,map_h);
		ei->sz_map = ei->map_w * ei->map_h;
		ei->sz_mapH = ei->map_h * cd->p_h;
		ei->sz_mapW = ei->map_w * cd->p_w;
		inputstream += *inputstream + 1;
		ei->map = (u16*)inputstream;
		ei->t_map = ei->map;
		inputstream += ei->sz_map * sizeof(u16);
		ei->map_img_ct = (u16)_NBytesToInt(inputstream, 2, 0);
		inputstream += 2;
		if(ei->map_img_ct > 0)
		{
			inputstream += *inputstream + 1;
			ei->map_img = (u16*)inputstream;
			inputstream += ei->map_img_ct * sizeof(u16);
		}
		ei->map_fnt_ct = (u16)_NBytesToInt(inputstream, 2, 0);
		inputstream += 2;
		if(ei->map_fnt_ct > 0)
		{
			inputstream += *inputstream + 1;
			ei->map_fnt = (u16*)inputstream;
			inputstream += ei->map_fnt_ct * sizeof(u16);
			for (i = 0; i < ei->map_fnt_ct; i++)
			{
				if(cd->res_fnt[ei->map_fnt[i]] == NULL)
				{
					cd->res_fnt[ei->map_fnt[i]] = (struct TERFont*)MALLOC(sizeof(struct TERFont), "_readMap:TERFont");
					MI_CpuFill8(cd->res_fnt[ei->map_fnt[i]], 0, sizeof(struct TERFont));
				}
				cd->res_fnt[ei->map_fnt[i]]->mThisRef++;
			}
		}
		ei->map_snd_sfx_ct = (u16)_NBytesToInt(inputstream, 2, 0);
		inputstream += 2;
		if(ei->map_snd_sfx_ct > 0)
		{
			inputstream += *inputstream + 1;
			ei->map_snd_sfx = (u16*)inputstream;
			inputstream += ei->map_snd_sfx_ct * sizeof(u16);
			for (i = 0; i < ei->map_snd_sfx_ct; i++)
			{
				if(cd->res_snd[ei->map_snd_sfx[i]] == NULL)
				{
					cd->res_snd[ei->map_snd_sfx[i]] = (struct TERSound*)MALLOC(sizeof(struct TERSound), "_readMap: sfx TERSound");
					MI_CpuFill8(cd->res_snd[ei->map_snd_sfx[i]], 0, sizeof(struct TERSound));
				}
				cd->res_snd[ei->map_snd_sfx[i]]->mThisRef++;
			}
		}
		ei->map_snd_bgm_ct = (u16)_NBytesToInt(inputstream, 2, 0);
		inputstream += 2;
		if(ei->map_snd_bgm_ct > 0)
		{
			inputstream += *inputstream + 1;
			ei->map_snd_bgm = (u16*)inputstream;
			inputstream += ei->map_snd_bgm_ct * sizeof(u16);
			for (i = 0; i < ei->map_snd_bgm_ct; i++)
			{
				if(cd->res_snd[ei->map_snd_bgm[i]] == NULL)
				{
					cd->res_snd[ei->map_snd_bgm[i]] = (struct TERSound*)MALLOC(sizeof(struct TERSound), "_readMap: bgm TERSound");
					MI_CpuFill8(cd->res_snd[ei->map_snd_bgm[i]], 0, sizeof(struct TERSound));
				}
				cd->res_snd[ei->map_snd_bgm[i]]->mThisRef++;
			}
		}
		strm_af_ct = _NBytesToInt(inputstream, 2, 0);
		inputstream += 2;
		if(strm_af_ct > 0)
		{
			SDK_ASSERT(cd->res_strm_names == 0); //only a single layer with a stream objects is allowed
			
			inputstream += *inputstream + 1;
			cd->res_strm_names = (const char **)inputstream;
			inputstream += strm_af_ct * sizeof(const char *);
			for(j = 0; j < strm_af_ct; j++)
			{
				i = *inputstream;
				inputstream++;
				inputstream += *inputstream + 1;
				cd->res_strm_names[j] = (const char *)inputstream;
				inputstream += i;
			}
			cd->strm_obj_ct = _NBytesToInt(inputstream, 2, 0);
			inputstream += 2;
			cd->strm_type_ct = _NBytesToInt(inputstream, 2, 0); 
			inputstream += 2;
			SDK_ASSERT(cd->strm_type_ct > 0);
			inputstream += *inputstream + 1;
			strm_ppraniarr = (const u16*)inputstream;
			inputstream += cd->strm_type_ct * sizeof(u16) * 2;
			
			for(uz_pr = 0; uz_pr < cd->strm_type_ct * 2; uz_pr += 2)
			{
				i = _NBytesToInt(inputstream, 2, 0);
				inputstream += 2;
				_loadFrames(cd, i, strm_ppraniarr[uz_pr], strm_ppraniarr[uz_pr + 1], &inputstream);
			}
		}
		ei->db_count = (s16*)MALLOC(ei->zones_ct * sizeof(s16), "readMap:ei->db_count");
		ei->pr_count = (s32*)MALLOC(ei->zones_ct * sizeof(s32), "readMap:ei->pr_count");
		ei->df_count = (s16*)MALLOC(ei->zones_ct * sizeof(s16), "readMap:ei->df_count");
		all_db_count = uz_db = _NBytesToInt(inputstream, 2, 0);
		inputstream += 2;
		for (i = 0; i < ei->zones_ct; i++)
		{
			ei->db_count[i] = (s16)_NBytesToInt(inputstream, 2, 0);
			inputstream += 2;
			all_db_count += ei->db_count[i];
			ei->db_count[i] += (s16)uz_db;
		}
		ei->all_pr_count = uz_pr = _NBytesToInt(inputstream, 2, 0);
		inputstream += 2;
		for (i = 0; i < ei->zones_ct; i++)
		{
			ei->pr_count[i] = _NBytesToInt(inputstream, 2, 0);
			inputstream += 2;
			ei->all_pr_count += ei->pr_count[i];
			ei->pr_count[i] += (s32)uz_pr;
		}
		all_df_count = uz_df = _NBytesToInt(inputstream, 2, 0);
		inputstream += 2;
		for (i = 0; i < ei->zones_ct; i++)
		{
			ei->df_count[i] = (s16)_NBytesToInt(inputstream, 2, 0);
			inputstream += 2;
			all_df_count += ei->df_count[i];
			ei->df_count[i] += (s16)uz_df;
		}

		ei->pr_idata_ct = all_db_count + ei->all_pr_count + all_df_count;
		if(strm_af_ct > 0)
		{
			cd->strm_instance = ei;
		}
		SDK_ASSERT(ei->pr_idata_ct); // Attempt to load map with no objects. Check current map with MapEditor.
		ei->pr_idata = (s32**)MALLOC(ei->pr_idata_ct * sizeof(s32**), "readMap:ei->pr_idata");
		ei->prg_idata = (s32**)MALLOC(ei->all_pr_count * sizeof(s32**), "readMap:ei->prg_idata");
		ei->pr_prp = (fx32**)MALLOC(ei->all_pr_count * sizeof(fx32**), "readMap:ei->pr_prp");
		ei->pr_spt = (u16**)MALLOC(ei->all_pr_count * sizeof(u16**), "readMap:ei->pr_spt");
		ei->pr_textdata = (u16**)MALLOC(ei->all_pr_count * sizeof(u16**), "readMap:ei->pr_textdata");
		ei->pr_textdataalign = (u32*)MALLOC(ei->all_pr_count * sizeof(u32), "readMap:ei->pr_textdataalign");
		ei->pr_textdatatext = (wchar***)MALLOC(ei->all_pr_count * sizeof(wchar***), "readMap:ei->pr_textdatatext");
		ei->pr_arr_off[OBJ_GAME] = 0;
		ei->pr_arr_off[OBJ_BACK] = (u32)ei->all_pr_count;
		for (i = (s32)ei->pr_arr_off[OBJ_BACK]; i < (s32)(all_db_count + ei->pr_arr_off[OBJ_BACK]); i++)
		{
			inputstream += *inputstream + 1;
			ei->pr_idata[i] = (s32*)inputstream;
			inputstream += prpCOUNT * sizeof(s32);
			ei->pr_idata[i][IANIENDFLAG] = 0;
			ei->pr_idata[i][ITEMPSTATE] = -1;
			_setState(ei, (s32)i, ei->pr_idata[i][ISTATE], FALSE);
		}
		for (i = 0; i < ei->all_pr_count; i++)
		{
			inputstream += *inputstream + 1;

			ei->prg_idata[i] = (s32*)MALLOC(prpgCOUNT * sizeof(s32), "readMap:ei->prg_idata[i]");
			MI_CpuClear32(ei->prg_idata[i], prpgCOUNT * sizeof(s32));

			ei->pr_idata[i] = (s32*)inputstream;
			inputstream += prpCOUNT * sizeof(s32);

			ei->prg_idata[i][ILINKTOOBJ] = _NBytesToInt(inputstream, 2, 0);
			inputstream += 2;
			ei->prg_idata[i][IJOINNODE1] = _NBytesToInt(inputstream, 2, 0);
			inputstream += 2;
			ei->prg_idata[i][IJOINNODE2] = _NBytesToInt(inputstream, 2, 0);
			inputstream += 2;
			ei->prg_idata[i][IJOINNODE3] = _NBytesToInt(inputstream, 2, 0);
			inputstream += 2;
			ei->prg_idata[i][INODE] = _NBytesToInt(inputstream, 2, 0);
			inputstream += 2;
			ei->prg_idata[i][ICUSTOMFLAGS] = 0;
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
			ei->prg_idata[i][IROTSIN] = 0;
			ei->prg_idata[i][IROTCOS] = FX32_ONE;
			ei->prg_idata[i][ICUSTOMSCALEXVALUE] = 
			ei->prg_idata[i][ICUSTOMSCALEYVALUE] = FX32_ONE;
			ei->prg_idata[i][ICUSTOMFLAGS] |= IRECALCULATELOGICRECTSFLAG;
#endif
			ei->prg_idata[i][ICUSTOMALPHA] = ALPHA_OPAQ;

			inputstream += *inputstream + 1;
			ei->pr_spt[i] = (u16*)inputstream;
			inputstream += SPT_COUNT * sizeof(u16);

			j = _NBytesToInt(inputstream, 2, 0);
			inputstream += 2;
			inputstream += *inputstream + 1;
			ei->pr_prp[i] = (s32*)inputstream;
			inputstream += j;

			ei->pr_idata[i][IANIFRIDX] = -1;
			ei->pr_idata[i][IANIENDFLAG] = 0;
			ei->pr_idata[i][ITEMPSTATE] = -1;
			ei->prg_idata[i][IDISTLENGHT]= -1;
			ei->prg_idata[i][IX0_NODE] = ei->pr_idata[i][IX];
			ei->prg_idata[i][IY0_NODE] = ei->pr_idata[i][IY];
			ei->prg_idata[i][IPREVNODE] = NONE_MAP_IDX;
			ei->prg_idata[i][ICLIPRECTOBJ] = NONE_MAP_IDX;

			if(ei->pr_idata[i][IPARENT_IDX] != PARENT_TEXT_OBJ)
			{
				ei->pr_textdata[i] = NULL;
				_setState(ei, (s32)i, ei->pr_idata[i][ISTATE], FALSE); // ÂÒÎË ÌÂÚ state -- Ò‚‡ÎËÚÒˇ!
			}
			else
			{
				if(cd->enable_prp_id != NONE_MAP_IDX)
				{
					ei->pr_prp[i][cd->enable_prp_id] = FX32_ONE;
				}
				inputstream += *inputstream + 1;
				ei->pr_textdata[i] = (u16*)inputstream;
				inputstream += pprtxtdataCOUNT * sizeof(s16);
				ei->pr_textdataalign[i] = _NBytesToInt(inputstream, 4, 0);
				inputstream += 4;
				inputstream += *inputstream + 1;
				if(ei->pr_textdata[i][TXTCHRCOUNT] == MAX_RES_IDX)
				{
                    s32 intval = _NBytesToInt(inputstream, 4, 0);
                    MI_CpuCopy8(&intval, &ei->pr_textdatatext[i], 4);
					inputstream += 4;
				}
				else
				{
					if(ei->pr_textdata[i][TXTSTRCOUNT] > 0)
					{
						ei->pr_textdatatext[i] = (wchar**)inputstream;
						inputstream += ei->pr_textdata[i][TXTSTRCOUNT] * sizeof(wchar**);
						for(j = 0; j < ei->pr_textdata[i][TXTSTRCOUNT]; j++)
						{
							inputstream += *inputstream + 1;
							ei->pr_textdatatext[i][j] = (wchar*)inputstream;
							inputstream += (ei->pr_textdata[i][TXTCHRCOUNT] + 1 + 1) * sizeof(wchar);
						}
					}
					else
					{
						ei->pr_textdatatext[i] = NULL;
					}
				}
			}
		}
		ei->pr_arr_off[OBJ_FORE] = ei->pr_arr_off[OBJ_BACK] + all_db_count;
		for (i = (s32)ei->pr_arr_off[OBJ_FORE]; i < (s32)(all_df_count + ei->pr_arr_off[OBJ_FORE]); i++)
		{
			inputstream += *inputstream + 1;
			ei->pr_idata[i] = (s32*)inputstream;
			inputstream += prpCOUNT * sizeof(s32);
			ei->pr_idata[i][IANIENDFLAG] = 0;
			ei->pr_idata[i][ITEMPSTATE] = -1;
			_setState(ei, (s32)i, ei->pr_idata[i][ISTATE], FALSE);
		}
		if(strm_af_ct > 0 && cd->strm_type_ct)
		{
			cd->strm_obj_data = (struct JOBAnimStreamTask ***)MALLOC(ei->pr_idata_ct * sizeof(struct JOBAnimStreamTask **), "readMap:ei->strm_obj_data");
			for (i = 0; i < ei->pr_idata_ct; i++)
			{
				cd->strm_obj_data[i] = (struct JOBAnimStreamTask **)MALLOC(cd->anim_ct * sizeof(struct JOBAnimStreamTask *), "readMap:ei->strm_obj_data[i]");
				for(j = 0; j < cd->anim_ct; j++)
				{
					cd->strm_obj_data[i][j] = NULL;
					for(uz_pr = 0; uz_pr < cd->strm_type_ct * 2; uz_pr += 2)
					{
						s32 ppr_id, ani_id;
						ppr_id = strm_ppraniarr[uz_pr];
						ani_id = strm_ppraniarr[uz_pr + 1];
						if(ppr_id == ei->pr_idata[i][IPARENT_IDX] && ani_id == j)
						{
							cd->strm_obj_data[i][j] = (struct JOBAnimStreamTask *)MALLOC(sizeof(struct JOBAnimStreamTask), "readMap::ei->strm_obj_data[i][j]");
							MI_CpuFill8(cd->strm_obj_data[i][j], 0, sizeof(struct JOBAnimStreamTask));
							ei->pr_idata[i][ITEMPSTATE] = -1;
							_setState(ei, i, ei->pr_idata[i][ISTATE], FALSE);
							break;
						}
					}
				}
			}
		}
		_readTriggers(ei, &inputstream, FALSE, cd);
		{
			const u8* pt = cd->common_data_tr_pt; 
			SDK_NULL_ASSERT(cd->common_data_tr_pt);
			_readTriggers(ei, &pt, TRUE, cd); //read common triggers
		}
		ei->nodes_ct = _NBytesToInt(inputstream, 2, 0);
		inputstream += 2;
		if(ei->nodes_ct > 0)
		{
			inputstream += *inputstream + 1;
			ei->nodes = (u16**)inputstream;
			inputstream += ei->nodes_ct * sizeof(u16*);
			inputstream += *inputstream + 1;
			ei->nodes_idata = (s32**)inputstream;
			inputstream += ei->nodes_ct * sizeof(s32*);
			for (i = 0; i < ei->nodes_ct; i++)
			{
				inputstream += *inputstream + 1;
				ei->nodes_idata[i] = (s32*)inputstream;
				inputstream += 2 * sizeof(s32);
				inputstream += *inputstream + 1;
				ei->nodes[i] = (u16*)inputstream;
				inputstream += PP_COUNT * sizeof(u16);
				ei->nodes[i][PP_CHANGEPOSFLAF_EVENTID] = (ei->nodes[i][PP_CHANGEPOSFLAF_EVENTID] << 8) | 1;
			}
		}
		_readScripts(ei, &inputstream, FALSE, cd);
		{
			const u8* pt = cd->common_data_sp_pt;
			SDK_NULL_ASSERT(cd->common_data_sp_pt);
			_readScripts(ei, &pt, TRUE, cd); //read common scripts
		}
		ei->scl_ct = *inputstream;
		inputstream++;
		if(ei->scl_ct > 0)
		{
			inputstream += *inputstream + 1;
			ei->sclmap = (const u8*)inputstream;
			inputstream += ei->sz_map;
			inputstream += *inputstream + 1;
			ei->scllines = (const s32*)inputstream;
			inputstream += ei->scl_ct * sizeof(s32) * 4;
		}
		ei->t_current_zone = ei->current_zone = *inputstream;
		inputstream++;
		start_script = (s16)_NBytesToInt(inputstream, 2, 0);
		inputstream += 2;
		camera_pos = (u16)_NBytesToInt(inputstream, 2, 0);
		//inputstream += 2;

		if (camera_pos != NONE_MAP_IDX)
		{
			s32 k, ct;
			k = camera_pos / (s32)ei->map_w;
			ct = camera_pos % (s32)ei->map_w;
			ei->tcx_fx[ei->bgType] = FX32(ct * cd->p_w + ei->logicWidth - ei->logicWidth / 2);
			ei->tcy_fx[ei->bgType] = FX32(k * cd->p_h + ei->logicHeight - ei->logicHeight / 2);
			_calculateCXY8(ei, &ei->tcx_fx[ei->bgType], &ei->tcy_fx[ei->bgType], &ei->tc_dx8_fx[ei->bgType], &ei->tc_dy8_fx[ei->bgType]);
			ei->camerax = ei->tcx_fx[ei->bgType];
			ei->cameray = ei->tcy_fx[ei->bgType];
		}
		
		_setActiveLayer(layer);
		
		if (start_script >= 0)
		{
			_setActiveLayer(layer);
#ifdef TENGINE_LEGACY_CODE
			startScript(start_script, FALSE, -1, -1);
#endif
			cd->ainst = NULL;
			cd->a_layer = NONE_MAP_IDX; 
		}
		_createZoneArrays(ei);
		/*
		{
			BOOL t[BGSELECT_NUM + 1];
			BOOL p[BGSELECT_NUM + 1];
			MI_CpuFill8(&t, 0, sizeof(s32) * (BGSELECT_NUM + 1));
			MI_CpuFill8(&p, 0, sizeof(s32) * (BGSELECT_NUM + 1));
			for (j = 0; j < (s32)cd->instances_count; j++)
			{
				ei = _getInstance(j);
				t[ei->bgType] = TRUE;
				p[ei->bgType] |= ei->primary;
			}
			for (j = 0; j < BGSELECT_NUM; j++)
			{
				if(!t[j] && !p[j])
				{
					OS_Warning("ASSERT: Please use assignLayerWithRenderPlane function before readMap (assign layer to render plane)");
					SDK_ASSERT(0);
				}
			}
		}
		*/
	}
}
// ----------------------------------------------------------------------------------

void _calculateLogicSizes(struct TEngineInstance *ei, struct TEngineCommonData *cd)
{
	SDK_ASSERT(cd->p_w > 0 && cd->p_h > 0); // Load CommonData file before!
	{
#ifdef USE_CUSTOM_RENDER
		s32 val;
#endif
		ei->logicWidth =	 RENDERFN(GetViewWidth)(ei->bgType); 
		ei->logicHeight = RENDERFN(GetViewHeight)(ei->bgType);
		ei->d_w = ei->logicWidth / cd->p_w;
		ei->d_h = ei->logicHeight / cd->p_h;
		if(ei->d_w % 2 != 0)
		{
			ei->d_w++;
		}
		if(ei->d_h % 2 != 0)
		{
			ei->d_h++;
		}
		if(ei->d_h > ei->map_h)
		{
			ei->d_h = ei->map_h - 1; 	
		}
		if(ei->d_w > ei->map_w)
		{
			ei->d_w = ei->map_w - 1; 	
		}
		ei->logicWidth = ei->d_w * cd->p_w;
		ei->logicHeight = ei->d_h * cd->p_h;
		ei->shiftTilesMax = -1;
#ifdef USE_CUSTOM_RENDER
		val = RENDERFN(GetFrameBufferHeight)(iBGType); 
		if(val < ei->logicHeight)
		{
			OS_Printf("Please increase mFrameBufferHeight8 parameter in RenderPlaneInitParams struct and reinit engine");
			SDK_ASSERT(0);
		}
		val = RENDERFN(GetFrameBufferWidth)(iBGType); 
		if(val < ei->logicWidth)
		{
			OS_Printf("Please increase mFrameBufferWidth8 parameter in RenderPlaneInitParams struct and reinit engine");
			SDK_ASSERT(0);
		}
		ei->shiftTilesMax = ei->d_w > ei->d_h ? ei->d_w / 2 : ei->d_h / 2;
#endif
	}
#ifdef USE_CUSTOM_RENDER
	ei->shiftTilesMax = ei->d_w > ei->d_h ? ei->d_w / 2 : ei->d_h / 2;
#endif
}
// ----------------------------------------------------------------------------------

void releaseResources(void)
{
	if(cd.instances != NULL)
	{
		_freeRes(&cd);
	}
}
// ----------------------------------------------------------------------------------

void releaseMapData(u32 layer)
{
	struct TEngineInstance *ei;
	
	jobSetActive(FALSE);

	if(cd.instances != NULL)
	{
		ei = _getInstance(layer);
		if(ei == NULL)
		{
			return;
		}

		_releaseZoneArrays(ei);

		ei->tile_data = NULL;
		ei->fr_img = NULL;
		ei->map_img = NULL;
		ei->map_img_ct = 0;
		ei->map = NULL;
		ei->t_map = NULL;
		ei->nodes = NULL;
		ei->nodes_idata = NULL;
		_releaseScriptData(ei);
		_releaseTrigData(ei);
		if(ei->prg_idata != NULL)
		{
			s32 i;
			if(cd.strm_obj_data != NULL && cd.strm_instance == ei)
			{
				i = ei->pr_idata_ct;
				while(i > 0)
				{
					s32 j;
					i--;
					j = cd.anim_ct;
					while(j > 0)
					{
						j--;
						if(cd.strm_obj_data[i][j])
						{
							jobRemoveStreamVideoTask(cd.strm_obj_data[i][j]);
							FREE(cd.strm_obj_data[i][j]);
						}
					}
					FREE(cd.strm_obj_data[i]);
				}
				FREE(cd.strm_obj_data);
				cd.strm_instance = NULL;
				cd.strm_obj_data = NULL;
				cd.strm_obj_ct = 0;
				cd.res_strm_names = NULL;
			}
			i = ei->all_pr_count;
			while(i > 0)
			{
				i--;
				FREE(ei->prg_idata[i]);
				ei->prg_idata[i] = NULL;
			}	
			FREE(ei->pr_textdatatext);
			FREE(ei->pr_textdataalign);
			FREE(ei->pr_textdata);
			FREE(ei->pr_spt);
			FREE(ei->pr_prp);
			FREE(ei->prg_idata);
			ei->prg_idata = NULL;
			ei->pr_prp = NULL;
			ei->pr_spt = NULL;
			ei->pr_textdatatext = NULL;
			ei->pr_textdataalign = NULL;
			ei->pr_textdata = NULL;
		}
		if (ei->pr_idata != NULL)
		{
			FREE(ei->pr_idata);
			ei->pr_idata = NULL;
		}
		if (ei->db_count != NULL)
		{
			FREE(ei->df_count);
			FREE(ei->pr_count);
			FREE(ei->db_count);
			ei->db_count = NULL;
			ei->pr_count = NULL;
			ei->df_count = NULL;
		}
		while(ei->map_snd_bgm_ct > 0)
		{
			ei->map_snd_bgm_ct--;
			SDK_NULL_ASSERT(cd.res_snd[ei->map_snd_bgm[ei->map_snd_bgm_ct]]);
			if(--cd.res_snd[ei->map_snd_bgm[ei->map_snd_bgm_ct]]->mThisRef == 0)
			{
				FREE(cd.res_snd[ei->map_snd_bgm[ei->map_snd_bgm_ct]]);
				cd.res_snd[ei->map_snd_bgm[ei->map_snd_bgm_ct]] = NULL;
			}
		}
		while(ei->map_snd_sfx_ct > 0)
		{
			ei->map_snd_sfx_ct--;
			SDK_NULL_ASSERT(cd.res_snd[ei->map_snd_sfx[ei->map_snd_sfx_ct]]);
			if(--cd.res_snd[ei->map_snd_sfx[ei->map_snd_sfx_ct]]->mThisRef == 0)
			{
				FREE(cd.res_snd[ei->map_snd_sfx[ei->map_snd_sfx_ct]]);
				cd.res_snd[ei->map_snd_sfx[ei->map_snd_sfx_ct]] = NULL;
			}
		}
		while(ei->map_fnt_ct > 0)
		{
			ei->map_fnt_ct--;
			SDK_NULL_ASSERT(cd.res_fnt[ei->map_fnt[ei->map_fnt_ct]]);
			if(--cd.res_fnt[ei->map_fnt[ei->map_fnt_ct]]->mThisRef == 0)
			{
				FREE(cd.res_fnt[ei->map_fnt[ei->map_fnt_ct]]);
				cd.res_fnt[ei->map_fnt[ei->map_fnt_ct]] = NULL;
			}
		}
		ei->map_img_ct = 0;
		if (ei->data_file != NULL)
		{
			FREE(ei->data_file);
			ei->data_file = NULL;
		}
	}
}
// ----------------------------------------------------------------------------------

void _createZoneArrays(struct TEngineInstance *ei)
{
	s32 i, z, j;
	s32 p[LOGIC_ZONES_MAX];
	s32 b[LOGIC_ZONES_MAX];
	s32 f[LOGIC_ZONES_MAX];

	SDK_ASSERT(ei->db_znlist == NULL);
	SDK_ASSERT(ei->df_znlist == NULL);
	SDK_ASSERT(ei->pr_znlist == NULL);

	ei->db_znlist = (s16**)MALLOC(ei->zones_ct * sizeof(s16**), "_createZoneArrays:ei->db_znlist");
	ei->df_znlist = (s16**)MALLOC(ei->zones_ct * sizeof(s16**), "_createZoneArrays:ei->df_znlist");
	ei->pr_znlist = (s32**)MALLOC(ei->zones_ct * sizeof(s32**), "_createZoneArrays:ei->pr_znlist");
	ei->rndr_znlist = (s32**)MALLOC(ei->zones_ct * sizeof(s32**), "_createZoneArrays:ei->rndr_znlist");
	for (i = 0; i < ei->zones_ct; i++)
	{
		ei->db_znlist[i] = (s16*)MALLOC(ei->db_count[i] * sizeof(s16), "_crZoneArrays:ei->db_znlist[i]");
		ei->df_znlist[i] = (s16*)MALLOC(ei->df_count[i] * sizeof(s16), "_crZoneArrays:ei->df_znlist[i]");
		ei->pr_znlist[i] = (s32*)MALLOC(ei->pr_count[i] * sizeof(s32), "_crZoneArrays:ei->pr_znlist[i]");
		ei->rndr_znlist[i] = (s32*)MALLOC(ei->all_pr_count * sizeof(s32), "_crZoneArrays:ei->rndr_znlist[i]");
		MI_CpuFill8(ei->rndr_znlist[i], 0xff, ei->all_pr_count * sizeof(s32));
		if(ei->pr_count[i] > 0)
		{
			ei->rndr_znlist[i][0] = 0xffff0000;
			for (j = 0; j < ei->pr_count[i] - 1; j++)
			{
				ei->pr_znlist[i][j] = (j + 1) << 16;
			}
			ei->pr_znlist[i][ei->pr_count[i] - 1] = 0xffff0000;
		}
	}
	for (i = 0; i < ei->zones_ct; i++)
	{
		f[i] = b[i] = p[i] = 0;
	}
	for (i = 0; i < ei->pr_idata_ct; i++)
	{
		z = ei->pr_idata[i][IZONE];
		if (i >= (s32)ei->pr_arr_off[OBJ_FORE])
		{
			if (z < 0)
				for (j = 0; j < ei->zones_ct; j++)
				{
					ei->df_znlist[j][f[j]] = (s16)i;
					f[j]++;
				}
			else
			{
				ei->df_znlist[z][f[z]] = (s16)i;
				f[z]++;
			}
		}
		else if (i >= (s32)ei->pr_arr_off[OBJ_BACK])
		{
			if (z < 0)
				for (j = 0; j < ei->zones_ct; j++)
				{
					ei->db_znlist[j][b[j]] = (s16)i;
					b[j]++;
				}
			else
			{
				ei->db_znlist[z][b[z]] = (s16)i;
				b[z]++;
			}
		}
		else
		{			
			if (z < 0)
				for (j = 0; j < ei->zones_ct; j++)
				{
					ei->pr_znlist[j][p[j]] |= i;
					p[j]++;
				}
			else
			{
				ei->pr_znlist[z][p[z]] |= i;
				p[z]++;
			}
		}
	}
}
// ----------------------------------------------------------------------------------

void _releaseZoneArrays(struct TEngineInstance *ei)
{
	if (ei->rndr_znlist != NULL)
	{
		s32 i = ei->zones_ct; 
		while(i > 0)
		{
			i--;
			if(ei->rndr_znlist[i] != NULL)
			{
				FREE(ei->rndr_znlist[i]);
			}
			if(ei->pr_znlist[i] != NULL)
			{
				FREE(ei->pr_znlist[i]);
			}
			if(ei->df_znlist[i] != NULL)
			{
				FREE(ei->df_znlist[i]);
			}
			if(ei->db_znlist[i] != NULL)
			{
				FREE(ei->db_znlist[i]);	
			}
		}
	}
	if (ei->rndr_znlist != NULL)
	{
		FREE(ei->rndr_znlist);
		ei->rndr_znlist = NULL;
	}
	if (ei->pr_znlist != NULL)
	{
		FREE(ei->pr_znlist);
		ei->pr_znlist = NULL;
	}
	if (ei->df_znlist != NULL)
	{
		FREE(ei->df_znlist);
		ei->df_znlist = NULL;
	}
	if (ei->db_znlist != NULL)
	{
		FREE(ei->db_znlist);
		ei->db_znlist = NULL;
	}
}
// ----------------------------------------------------------------------------------

void _releaseTrigData(struct TEngineInstance *ei)
{
	if(ei->trig_idata != NULL)
	{
		FREE(ei->trig_st);
		FREE(ei->trig_spt);
		FREE(ei->trig_prp);
		FREE(ei->trig_idata);
		FREE(ei->trig_st_ct);
		FREE(ei->trig_prp_ct);
		ei->trig_st_ct = NULL;
		ei->trig_prp_ct = NULL;
		ei->trig_st = NULL;
		ei->trig_spt = NULL;
		ei->trig_prp = NULL;
		ei->trig_idata = NULL;
	}
}
// ----------------------------------------------------------------------------------

void _readTriggers(struct TEngineInstance *ei, const u8 **inputstream, BOOL common_mode, struct TEngineCommonData *cd)
{
	s32 i, j;
	BOOL can_read;
	s32 count, offset, trig_total_count;
	can_read = (common_mode && cd->common_data_tr_pt != NULL) || !common_mode;
	count = _NBytesToInt(*inputstream, 2, 0);
	*inputstream += 2;
	if (common_mode)
	{
		offset = 0;
		ei->trig_common_count = count;
	}
	else
	{
		offset = ei->trig_common_count;
		trig_total_count = offset + count;
		SDK_ASSERT(ei->trig_idata == NULL);
		ei->trig_prp_ct = (u32*)MALLOC(trig_total_count * sizeof(u32), "_readTriggers:t_trig_prp_ct");
		ei->trig_st_ct = (u32*)MALLOC(trig_total_count * sizeof(u32), "_readTriggers:t_trig_st_ct");
		ei->trig_idata = (s32**)MALLOC(trig_total_count * sizeof(s32**), "_readTriggers:t_trig_idata");
		ei->trig_prp = (fx32**)MALLOC(trig_total_count * sizeof(fx32**), "_readTriggers:t_trig_prp");
		ei->trig_spt = (u16**)MALLOC(trig_total_count * sizeof(u16**), "_readTriggers:t_trig_spt");
		ei->trig_st = (u8**)MALLOC(trig_total_count * sizeof(u8**), "_readTriggers:t_trig_st");
	}
	for (i = 0; i < count; i++)
	{
		s32 ii = i + offset;
		*inputstream += **inputstream + 1;
		if (can_read)
		{
			ei->trig_idata[ii] = (s32*)*inputstream;
		}
		*inputstream += trprpCOUNT * sizeof(s32);
		j = **inputstream;
		(*inputstream)++;
		if (can_read)
		{
			ei->trig_st_ct[ii] = (u32)j;
		}
		if (j > 0)
		{
			*inputstream += **inputstream + 1;
			if (can_read)
			{
				ei->trig_st[ii] = (u8*)*inputstream;
			}
		}
		else
		{
			if (can_read)
			{
				ei->trig_st[ii] = NULL;
			}
		}
		*inputstream += j;
		j = **inputstream;
		(*inputstream)++;
		if (j > 0)
		{
			*inputstream += **inputstream + 1;
			if (can_read)
			{
				ei->trig_spt[ii] = (u16*)*inputstream;
			}
			*inputstream += SPT_COUNT * sizeof(u16);
		}
		else
		{
			if (can_read)
			{
				ei->trig_spt[ii] = NULL;
			}
		}
		j = (u16)(_NBytesToInt(*inputstream, 2, 0) * 2);
		*inputstream += 2;
		if (can_read)
		{
			ei->trig_prp_ct[ii] = (u32)(j / sizeof(fx32));
		}
		if (j > 0)
		{
			*inputstream += **inputstream + 1;
			if (can_read)
			{
				ei->trig_prp[ii] = (fx32*)*inputstream;
			}
		}
		else
		{
			if (can_read)
			{
				ei->trig_prp[ii] = NULL;
			}
		}
		*inputstream += j;
	}
}
// ----------------------------------------------------------------------------------

void _releaseScriptData(struct TEngineInstance *ei)
{
	if(ei->spt_bdata != NULL)
	{
		FREE(ei->spt_triglist_ct);
		FREE(ei->spt_triglist);
		FREE(ei->spt_cldobjlist_ct);
		FREE(ei->spt_cldobjlist);
		FREE(ei->spt_cldgrouplist_ct);
		FREE(ei->spt_cldgrouplist);
		FREE(ei->spt_bdata);
		ei->spt_triglist_ct = NULL;
		ei->spt_triglist = NULL;
		ei->spt_cldobjlist_ct = NULL;
		ei->spt_cldobjlist = NULL;
		ei->spt_cldgrouplist_ct = NULL;
		ei->spt_cldgrouplist = NULL;
		ei->spt_bdata = NULL;
	}
}
// ----------------------------------------------------------------------------------

static void _readScripts(struct TEngineInstance *ei, const u8 **inputstream, BOOL common_mode, struct TEngineCommonData *cd)
{
	s32 i, j;
	s32 count, offset, script_total_count;
	BOOL can_read = (common_mode && cd->common_data_sp_pt!= NULL) || !common_mode;
	count = _NBytesToInt(*inputstream, 2, 0);
	*inputstream += 2;
	if (common_mode)
	{
		offset = 0;
		ei->script_common_count = count;
	}
	else
	{
		offset = ei->script_common_count;
		script_total_count = offset + count;
		SDK_ASSERT(ei->spt_bdata == NULL);
		ei->spt_bdata = (u16**)MALLOC(script_total_count * sizeof(u16**), "_readScripts:t_spt_bdata");
		ei->spt_cldgrouplist = (char**)MALLOC(script_total_count * sizeof(char**), "_readScripts:t_spt_cldgrouplist");
		ei->spt_cldgrouplist_ct = (u32*)MALLOC(script_total_count * sizeof(u32), "_readScripts:t_spt_cldgrouplist_ct");
		ei->spt_cldobjlist = (u16**)MALLOC(script_total_count * sizeof(u16**), "_readScripts:t_spt_cldobjlist");
		ei->spt_cldobjlist_ct = (u32*)MALLOC(script_total_count * sizeof(u32), "_readScripts:t_spt_cldobjlist_ct");
		ei->spt_triglist = (u16**)MALLOC(script_total_count * sizeof(u16**), "_readScripts:t_spt_triglist");
		ei->spt_triglist_ct = (u32*)MALLOC(script_total_count * sizeof(u32), "_readScripts:t_spt_triglist_ct");
	}
	for (i = 0; i < count; i++)
	{
		s32 ii = i + offset;
		j = **inputstream;
		(*inputstream)++;
		if (can_read)
		{
			ei->spt_cldgrouplist_ct[ii] = (u32)j;
		}
		*inputstream += **inputstream + 1;
		if (can_read)
		{
			ei->spt_cldgrouplist[ii] = (char*)*inputstream;
		}
		*inputstream += j;
		j = **inputstream;
		(*inputstream)++;
		if (can_read)
		{
			ei->spt_cldobjlist_ct[ii] = (u32)j;
		}
		*inputstream += **inputstream + 1;
		if (can_read)
		{
			ei->spt_cldobjlist[ii] = (u16*)*inputstream;
		}
		*inputstream += j * sizeof(u16);
		*inputstream += **inputstream + 1;
		if (can_read)
		{
			ei->spt_bdata[ii] = (u16*) * inputstream;
		}
		*inputstream += sptprpCOUNT * sizeof(u16);
		j = **inputstream;
		(*inputstream)++;
		if (can_read)
		{
			ei->spt_triglist_ct[ii] = (u32)j;
		}
		*inputstream += **inputstream + 1;
		if (can_read)
		{
			ei->spt_triglist[ii] = (u16*)*inputstream;
		}
		*inputstream += j * sizeof(u16);
	}
}
// ----------------------------------------------------------------------------------

void _freeRes(struct TEngineCommonData *cd)
{
	u32 i;

	cd->agrl_s = 0;
	cd->agrl_i = 0;
	cd->rl_available = FALSE;

#ifdef JOBS_IN_SEPARATE_THREAD
	jobCriticalSectionBegin();	
#endif
	
	jobSetActive(FALSE);
	jobResetTasks();

	jobRemoveStreamFileTask(&ld_fst);
	ld_fst.mData.mMode = JFSDM_MODE_NONE;
	if(ld_fst.mData.mpFile)
	{
		FREE(ld_fst.mData.mpFile);
	}
	if(ld_fst.mData.mpImage != NULL)
	{
		DeleteBMPImage(ld_fst.mData.mpImage);
	}
	ld_fst.mData.mStatus = FALSE;	
	ld_fst.mData.mpImage = NULL;
	ld_fst.mData.mpFile = NULL;

	i = cd->initParams.layersCount; 
	while(i > 0)
	{
		i--;
		RENDERFN(ClearFrameBuffer)(cd->instances[i]->bgType);
		cd->instances[i]->ueof_state = UEOF_EMPTY;
	}

	if(cd->strm_obj_data != NULL && cd->strm_obj_ct > 0)
	{
		s32 prid = cd->strm_instance->pr_idata_ct; 
		while(prid > 0)
		{
			s32 aid; 
			prid--;
			aid = cd->anim_ct; 
			while(aid > 0)
			{				
				aid--;
				if(cd->strm_obj_data[prid][aid])
				{
					s32 bfidx = JOB_ANIM_TASK_BUFFER_MAX; 
					while(bfidx > 0)
					{
						bfidx--;
						if(cd->strm_obj_data[prid][aid]->mpFileData[bfidx] != NULL)
						{
							DeleteBMPImage(cd->strm_obj_data[prid][aid]->mpFileData[bfidx]);
						}
						cd->strm_obj_data[prid][aid]->mpFileData[bfidx] = NULL;
						cd->strm_obj_data[prid][aid]->mData[bfidx].mpImage = NULL;
						cd->strm_obj_data[prid][aid]->mData[bfidx].mBufferStatus = FALSE;
						cd->strm_obj_data[prid][aid]->mData[bfidx].mBindStatus = FALSE;
					}
				}
			}
		}
	}

	for (i = 0; i < cd->initParams.layersCount; i++)
	{
		struct TEngineInstance *ei = cd->instances[i];
		if(ei->data_file != NULL)
		{
			s32 idx;
#ifdef USE_OPENGL_RENDER
			if(cd->instances[i]->primary)
			{
				glRender_ReleaseBGLayersData(cd->instances[i]->bgType);
			}
#endif
			while(ei->ld_snd_bgm_res_ct > 0)
			{
				ei->ld_snd_bgm_res_ct--;
				idx = ei->map_snd_bgm[ei->ld_snd_bgm_res_ct];
				if(idx < NULL_IMG_IDX)
				{
					if(--cd->res_snd[idx]->mResRef == 0)
					{
						sndReleaseResource(cd->res_snd[idx]);
					}
				}
			}
			while(ei->ld_snd_sfx_res_ct > 0)
			{
				ei->ld_snd_sfx_res_ct--;
				idx = ei->map_snd_sfx[ei->ld_snd_sfx_res_ct];
				if(idx < NULL_IMG_IDX)
				{
					if(--cd->res_snd[idx]->mResRef == 0)
					{
						sndReleaseResource(cd->res_snd[idx]);
					}
				}
			}
			while(ei->ld_fnt_res_ct > 0)
			{
				ei->ld_fnt_res_ct--;
				idx = ei->map_fnt[ei->ld_fnt_res_ct];
				if(idx < NULL_IMG_IDX)
				{
					if(--cd->res_fnt[idx]->mResRef == 0)
					{
						SDK_ASSERT((cd->res_fnt[idx]->mAlphabet.mpAlphabet != NULL));
						_releaseFont(cd->res_fnt[idx]);
					}
				}
			}
			while(ei->ld_img_res_ct > 0)
			{
				ei->ld_img_res_ct--;
				idx = ei->map_img[ei->ld_img_res_ct];
				if(idx < NULL_IMG_IDX)
				{
					SDK_NULL_ASSERT(cd->res_img[idx]);
					if(--cd->res_img[idx]->mRef == 0)
					{
						DeleteBMPImage(cd->res_img[idx]);
						cd->res_img[idx] = NULL;
					}
				}
			}
		}
	}
	cd->snd_instance = NULL;
	sndDeleteDataBuffers();
#ifdef USE_OPENGL_RENDER
	glRender_DeleteTextures();
#endif
#ifdef USE_CUSTOM_RENDER
	egRender_LostVRAMDevice();
#endif
#ifdef JOBS_IN_SEPARATE_THREAD
	jobCriticalSectionEnd();	
#endif
#if defined SDK_DEBUG && defined FRAME_ALLOCATOR
	OS_Printf("after free resources dmem = %d\n", GetTotalMALLOCSizeDbg());
#endif
}
// ----------------------------------------------------------------------------------

void _releaseFont(struct TERFont *oFont)
{
	SDK_ASSERT(oFont->mpRawData != NULL);
	SDK_ASSERT(oFont->mAlphabet.mpAlphabet != NULL); //font is not inited
	SDK_ASSERT(oFont->mAlphabet.mpAlphabetTextureOffsetMap != NULL); //font is not inited
	FREE(oFont->mAlphabet.mpAlphabetTextureOffsetMap);
	FREE(oFont->mAlphabet.mpAlphabet);
	FREE((u8*)oFont->mpRawData);
	oFont->mAlphabet.mAlphabetSize = 0;
	oFont->mAlphabet.mpAlphabet = NULL;
	oFont->mAlphabet.mpAlphabetTextureOffsetMap = NULL;
	oFont->mpAbcImage = NULL;
	oFont->mpRawData = NULL;
}
// ----------------------------------------------------------------------------------

static void _assignResUpdate(struct TEngineCommonData *cd)
{
	switch(cd->agrl_s)
	{
		case 0:
			if(_assignRes_begin(cd))
			{
				cd->agrl_s++;	
			}
		break;
		case 1:
			if(_assignRes_load(cd))
			{
				cd->agrl_s++;	
			}
		break;
		case 2:
			_assignRes_end();
			cd->agrl_s++;
		break;
		case 3:
			{
				u32 i;
				for (i = 0; i < cd->initParams.layersCount; i++)
				{
					struct TEngineInstance *ei = cd->instances[i]; 
					if(ei->data_file != NULL && ei->ueof_state != UEOF_READY)
					{
						--ei->ueof_state;
						return;
					}
				}
				cd->agrl_s = 0;
				cd->agrl_i = 0;
				cd->ag_st = FALSE;
				{
					if(cd->_onEventCB != NULL)
					{
						struct EventCallbackData ed;
						ed.eventType = EVENT_TYPE_ON_END_ASYNH_LOAD_RESOURSES;
						cd->_onEventCB(&ed);
					}
				}
			}
	}
}
// ----------------------------------------------------------------------------------

static BOOL _assignRes_begin(struct TEngineCommonData *cd)
{
	u32 i, sfx_ct, bgm_ct;
#ifdef USE_OPENGL_RENDER
	s32 gltstrm_ct;
#endif
	if(cd->ag_st == FALSE || cd->ld_st != ALS_STATE_READY || cd->instances == NULL)
	{
		return FALSE;
	}
	sfx_ct = 0;
	bgm_ct = 0;
	cd->snd_instance = NULL;
	for (i = 0; i < cd->initParams.layersCount; i++)
	{
		struct TEngineInstance *ei = cd->instances[i]; 
		RENDERFN(ClearFrameBuffer)(ei->bgType);
		if(cd->instances[i]->ueof_state != UEOF_EMPTY)
		{
			return FALSE;	
		}
		SDK_ASSERT(ei->ld_img_res_ct == 0);
		SDK_ASSERT(ei->ld_fnt_res_ct == 0);
		SDK_ASSERT(ei->ld_snd_sfx_res_ct == 0);
		SDK_ASSERT(ei->ld_snd_bgm_res_ct == 0);
		if(ei->primary && cd->snd_instance == NULL && (ei->map_snd_sfx_ct != 0 || ei->map_snd_bgm_ct != 0))
		{
			cd->snd_instance = ei; 
			sfx_ct = ei->map_snd_sfx_ct;
			bgm_ct = ei->map_snd_bgm_ct;
		}
	}
#if defined SDK_DEBUG && defined FRAME_ALLOCATOR
	OS_Printf("assign resources dmem = %d\n", GetTotalMALLOCSizeDbg());
#else
	OS_Printf("assign resources\n");
#endif
#ifdef USE_OPENGL_RENDER
	gltstrm_ct = cd->strm_obj_ct;	
	gltstrm_ct *= JOB_ANIM_TASK_BUFFER_MAX;
	cd->glstrmidx = cd->res_img_ct;
	glRender_CreateTextures(cd->glstrmidx + gltstrm_ct);
#endif
#ifdef USE_CUSTOM_RENDER
	egRender_RestoreVRAMDevice();
#endif
	if(sfx_ct > 0 || bgm_ct > 0)
	{
		sndCreateDataBuffers(sfx_ct, bgm_ct);
	}
	cd->agrl_i = (s32)cd->initParams.layersCount - 1;
	cd->agrl_strmprid = 0;
	cd->agrl_strmaid = 0;
	cd->agrl_strmbfi = 0;
	return TRUE;
}
// ----------------------------------------------------------------------------------

static BOOL _assignRes_load(struct TEngineCommonData *cd)
{
	if(cd->agrl_i >= 0)
	{
		for (; cd->agrl_i >= 0; cd->agrl_i--)
		{
			struct TEngineInstance *ei = cd->instances[cd->agrl_i];
			if(ei->data_file == NULL)
			{
				continue;
			}
			if(ei->ld_img_res_ct < ei->map_img_ct)
			{
				SDK_ASSERT(ei->map_img != NULL);
				for (; ei->ld_img_res_ct < ei->map_img_ct; ei->ld_img_res_ct++)
				{
					s32 idx = ei->map_img[ei->ld_img_res_ct];
					if(idx < NULL_IMG_IDX)
					{
						if (cd->res_img[idx] == NULL)
						{
							_loadResource(idx, idx, RES_TYPE_IMAGE, 0, (void**)&cd->res_img[idx], cd);
							if(cd->res_img[idx] != NULL)
							{
								cd->res_img[idx]->mRef++;
								ei->ld_img_res_ct++;
							}
							return FALSE;
						}
						else
						{
							cd->res_img[idx]	->mRef++;
						}
					}
				}
			}
			if(ei->ld_fnt_res_ct < ei->map_fnt_ct)
			{
				SDK_ASSERT(ei->map_fnt != NULL);
				for (; ei->ld_fnt_res_ct < ei->map_fnt_ct; ei->ld_fnt_res_ct++)
				{
					s32 idx = ei->map_fnt[ei->ld_fnt_res_ct];
					if(idx < NULL_IMG_IDX)
					{
						if (cd->res_fnt[idx]->mpAbcImage == NULL)
						{
							_loadResource(idx, idx, RES_TYPE_FONT, 0, (void**)&cd->res_fnt[idx], cd);
							if(cd->res_fnt[idx]->mpAbcImage != NULL)
							{
								cd->res_fnt[idx]->mResRef++;
								ei->ld_fnt_res_ct++;
							}
							return FALSE;
						}
						else
						{
							cd->res_fnt[idx]	->mResRef++;
						}
					}
				}
			}
#ifndef USE_NO_SOUND
			if(cd->snd_instance == ei && ei->ld_snd_sfx_res_ct < ei->map_snd_sfx_ct)
			{
				SDK_ASSERT(ei->map_snd_sfx != NULL);
				for (; ei->ld_snd_sfx_res_ct < ei->map_snd_sfx_ct; ei->ld_snd_sfx_res_ct++)
				{
					s32 idx = ei->map_snd_sfx[ei->ld_snd_sfx_res_ct];
					if(idx < NULL_IMG_IDX)
					{
						if (sndIsInitedResource(cd->res_snd[idx]	) == FALSE)
						{
							_loadResource(ei->ld_snd_sfx_res_ct, idx, RES_TYPE_SOUND, 1, (void**)&cd->res_snd[idx], cd); // 1 == sfx
							if(sndIsInitedResource(cd->res_snd[idx]) == TRUE)
							{
								cd->res_snd[idx]	->mResRef++;
								ei->ld_snd_sfx_res_ct++;
							}
							return FALSE;
						}
						else
						{
							cd->res_snd[idx]	->mResRef++;
						}
					}
				}
			}
			if(cd->snd_instance == ei && ei->ld_snd_bgm_res_ct < ei->map_snd_bgm_ct)
			{
				SDK_ASSERT(ei->map_snd_bgm != NULL);
				for (; ei->ld_snd_bgm_res_ct < ei->map_snd_bgm_ct; ei->ld_snd_bgm_res_ct++)
				{
					s32 idx = ei->map_snd_bgm[ei->ld_snd_bgm_res_ct];
					if(idx < NULL_IMG_IDX)
					{
						if (sndIsInitedResource(cd->res_snd[idx]	) == FALSE)
						{
							_loadResource(ei->ld_snd_bgm_res_ct, idx, RES_TYPE_SOUND, 0, (void**)&cd->res_snd[idx], cd); // 0 == bgm
							if(sndIsInitedResource(cd->res_snd[idx]) == TRUE)
							{
								cd->res_snd[idx]	->mResRef++;
								ei->ld_snd_bgm_res_ct++;
							}
							return FALSE;
						}
						else
						{
							cd->res_snd[idx]	->mResRef++;
						}
					}
				}
			}
#endif
			//mark res_state as soon as possible (loading screens etc)
			if(ei->data_file)
			{
				if(ei->primary)
				{
					_calculateLogicSizes(ei, cd);
#ifdef USE_OPENGL_RENDER
					glRender_SetupBGLayersData(ei->bgType, cd->tl_layers_ct, ei->maxbgres, (ei->d_h + 1) * (ei->d_w + 1));
#endif
				}
				if(cd->strm_instance != ei)
				{
					ei->ueof_state = UEOF_SKIP_FRAME_2;
				}
			}
		}
	}
#ifdef JOBS_IN_SEPARATE_THREAD
	jobCriticalSectionBegin();	
#endif
	if(cd->strm_obj_data != NULL && cd->strm_obj_ct > 0)
	{
		s32 j;		
		for (; cd->agrl_strmprid < cd->strm_instance->pr_idata_ct; cd->agrl_strmprid++)
		{
			for (; cd->agrl_strmaid < cd->anim_ct; cd->agrl_strmaid++)
			{				
				if(cd->strm_obj_data[cd->agrl_strmprid][cd->agrl_strmaid])
				{
					s32 ppr_idx, fr_idx;
					const char *filename = NULL;
					ppr_idx = cd->strm_instance->pr_idata[cd->agrl_strmprid][IPARENT_IDX];
					fr_idx = 0;
					while(filename == NULL)
					{
						s32 pc_count = cd->ppr_fr_data[ppr_idx][cd->agrl_strmaid][fr_idx][ANIFRFRCOUNT];
						for(j = 0; j < pc_count; j++)
						{
							const s16* frdata = _getFramePieceData(ppr_idx, cd->agrl_strmaid, fr_idx, j);
							if(_isStreamFrame(frdata[ANIFRFRDATA]))
							{
								filename = cd->res_strm_names[frdata[ANIFRFRID]];
								break;
							}
						}
						fr_idx++;
					}
					fr_idx = 0;
					for(; cd->agrl_strmbfi < JOB_ANIM_TASK_BUFFER_MAX;)
					{
						char fullPath[MAX_FILEPATH];
						struct BMPImage *pImg;
						STD_StrCpy(fullPath, filename);
						STD_StrCat(fullPath, FILERES_EXTENTION);
						LoadBMPImage(fullPath, &pImg, FALSE);
						cd->strm_obj_data[cd->agrl_strmprid][cd->agrl_strmaid]->mpFileData[cd->agrl_strmbfi] = pImg;
						glRender_CreateEmptyTexture(pImg, cd->glstrmidx);
						pImg->mOpaqType = cd->glstrmidx;
						cd->strm_obj_data[cd->agrl_strmprid][cd->agrl_strmaid]->mData[cd->agrl_strmbfi].mBufferStatus = FALSE;
						cd->strm_obj_data[cd->agrl_strmprid][cd->agrl_strmaid]->mData[cd->agrl_strmbfi].mBindStatus = FALSE;
						cd->strm_obj_data[cd->agrl_strmprid][cd->agrl_strmaid]->mData[cd->agrl_strmbfi].mFrpcdata[RES_X] = 0;
						cd->strm_obj_data[cd->agrl_strmprid][cd->agrl_strmaid]->mData[cd->agrl_strmbfi].mFrpcdata[RES_Y] = 0;
						cd->strm_obj_data[cd->agrl_strmprid][cd->agrl_strmaid]->mData[cd->agrl_strmbfi].mFrpcdata[RES_W] = pImg->mWidth;
						cd->strm_obj_data[cd->agrl_strmprid][cd->agrl_strmaid]->mData[cd->agrl_strmbfi].mFrpcdata[RES_H] = pImg->mHeight;
						cd->strm_obj_data[cd->agrl_strmprid][cd->agrl_strmaid]->mData[cd->agrl_strmbfi].mpImage = pImg;
						if(cd->strm_obj_data[cd->agrl_strmprid][cd->agrl_strmaid]->mData[cd->agrl_strmbfi].mpFilename != NULL)
						{
							fr_idx = 1;	
						}
#ifdef USE_OPENGL_RENDER
						cd->glstrmidx++;
#endif
						cd->agrl_strmbfi++;
#ifdef JOBS_IN_SEPARATE_THREAD
						jobCriticalSectionEnd();	
#endif
						return FALSE;
					}
					if(fr_idx)
					{
						jobAddStreamVideoTask(cd->strm_obj_data[cd->agrl_strmprid][cd->agrl_strmaid]);
					}
				}
			}
			cd->agrl_strmaid = 0;
			cd->agrl_strmbfi = 0;
		}
		cd->strm_instance->ueof_state = UEOF_SKIP_FRAME_2;
	}
#ifdef JOBS_IN_SEPARATE_THREAD
	jobCriticalSectionEnd();	
#endif
	return TRUE;
}
// ----------------------------------------------------------------------------------

static void _assignRes_end()
{
#if defined SDK_DEBUG && defined FRAME_ALLOCATOR
	OS_Printf("end load resources dmem = %d\n", GetTotalMALLOCSizeDbg());
#else
	OS_Printf("end load resources\n");
#endif
}
// ----------------------------------------------------------------------------------

void _getFileName(s32 idx, s32 type, char *iofileName, struct TEngineCommonData *cd)
{
	switch(type)
	{
		case RES_TYPE_IMAGE:
			STD_StrCpy(iofileName, cd->res_img_fnames[idx]);
			STD_StrCat(iofileName, FILERES_EXTENTION); 
		break;
		case RES_TYPE_FONT:
			STD_StrCpy(iofileName, cd->res_fnt_fnames[idx]);
			STD_StrCat(iofileName, FONT_EXTENTION);
		break;
		case RES_TYPE_SOUND:
			STD_StrCpy(iofileName, cd->res_snd_fnames[idx]);
			STD_StrCat(iofileName, SOUND_EXTENTION);
		break;
		default:
			SDK_ASSERT(0); // wrong type
	}
}
// ----------------------------------------------------------------------------------

void _loadResource(s32 idx, s32 resIdx, s32 type, u8 value, void **oData, struct TEngineCommonData *cd)
{
	u8 *buf;	
	if(ld_fst.mData.mMode == JFSDM_MODE_NONE)
	{
		enum JFSDMode mode;
		_getFileName(resIdx, type, _gsfileName, cd);
		switch(type)
		{
			case RES_TYPE_IMAGE:
				mode = JFSDM_MODE_IMG;
			break;
			case RES_TYPE_SOUND:
				mode = JFSDM_MODE_SND;
			break;
			default:
				mode = JFSDM_MODE_FILE;
		}
		_asynhloadFileInit(_gsfileName, mode, (u16)idx, value);
	}
	if(_asynhloadFileCheck(&buf))
	{
		switch(type)
		{
			case RES_TYPE_IMAGE:
			{
				struct BMPImage* img = (struct BMPImage*)buf;
				SDK_ASSERT(img->mType < BMP_TYPE_NUM);
				*oData = buf; 
#ifdef USE_OPENGL_RENDER
				glRender_CreateEmptyTexture(img, idx);
				glRender_LoadTextureToVRAM(img, idx);
				if(img->mpA5Data != NULL)
				{
					FREE(img->mpA5Data);
					img->mpA5Data = NULL;
				}
				if(img->data.mpData256 != NULL)
				{
					FREE(img->data.mpData256);
					img->data.mpData256 = NULL;
				}
				img->mOpaqType = (u16)idx;
#endif
			}
			break;
			case RES_TYPE_FONT:
				_initFont(buf, (struct TERFont*)*oData, cd);
			break;
			case RES_TYPE_SOUND:
				sndInitResource(buf, (struct TERSound*)*oData);
			break;
			default:
				SDK_ASSERT(0); // wrong type
		}
	}
}
// ----------------------------------------------------------------------------------

void _initFont(const u8* ipData, struct TERFont *oFont, struct TEngineCommonData *cd)
{
	s32 intVal, i;
	char resName[MAX_FILENAME];
	SDK_NULL_ASSERT(oFont);
	SDK_NULL_ASSERT(ipData);
	SDK_ASSERT(ipData[0] == 'B' &&
		        ipData[1] == 'M' &&
		        ipData[2] == 'F' &&
				ipData[3] == 3); //Unsupportet font data format
	SDK_ASSERT(oFont->mpRawData == NULL); //font is already assigned
	oFont->mpRawData = ipData;
	ipData += 4;
	//block1
	ipData++;
	intVal = _NBytesToInt(ipData, 4, 0);
	ipData += 4 + intVal;
	//block2
	ipData++;
	intVal = _NBytesToInt(ipData, 4, 0);
	ipData += 4 + intVal;
	//block3
	ipData += 5;
	intVal = 0;
	while(*ipData != '.')
	{
		resName[intVal] = (char)(*ipData);
		intVal++;
		ipData++;
	}
	resName[intVal] = 0;
	ipData += 5;
	//block4
	ipData++;
	intVal = _NBytesToInt(ipData, 4, 0);
	ipData += 4;
	SDK_ASSERT(oFont->mAlphabet.mpAlphabet == NULL); //font is already assigned
	SDK_ASSERT(oFont->mAlphabet.mpAlphabetTextureOffsetMap == NULL); //font is already assigned
	oFont->mAlphabet.mAlphabetSize = (u16)(intVal / 20);
	oFont->mAlphabet.mpAlphabet = (wchar*)MALLOC(oFont->mAlphabet.mAlphabetSize * sizeof(wchar), "_initFont:oFont->mAlphabet.mpAlphabet");
	oFont->mAlphabet.mpAlphabetTextureOffsetMap = (struct AlphabetTextureOffsetMap*)MALLOC(oFont->mAlphabet.mAlphabetSize * sizeof(struct AlphabetTextureOffsetMap), "oFont->mAlphabet.mpAlphabetTextureOffsetMap");
	oFont->mpAbcImage = NULL;
	for(i = 0; i < cd->res_img_ct; i++)
	{
		if(STD_StrCmp(cd->res_img_fnames[i], resName) == 0)
		{
			oFont->mpAbcImage = cd->res_img[i];
			break;
		}
	}
	SDK_NULL_ASSERT(oFont->mpAbcImage);
	for(intVal = 0; intVal < oFont->mAlphabet.mAlphabetSize; intVal++)
	{
		oFont->mAlphabet.mpAlphabet[intVal] = (wchar)_NBytesToInt(ipData, 4, 0);
		ipData += 4;
		oFont->mAlphabet.mpAlphabetTextureOffsetMap[intVal].mX = (s16)_NBytesToInt(ipData, 2, 0);
		ipData += 2;
		oFont->mAlphabet.mpAlphabetTextureOffsetMap[intVal].mY = (s16)_NBytesToInt(ipData, 2, 0);
		ipData += 2;
		oFont->mAlphabet.mpAlphabetTextureOffsetMap[intVal].mW = (s16)_NBytesToInt(ipData, 2, 0);
		ipData += 2;
		oFont->mAlphabet.mpAlphabetTextureOffsetMap[intVal].mH = (s16)_NBytesToInt(ipData, 2, 0);
		ipData += 2;
		oFont->mAlphabet.mpAlphabetTextureOffsetMap[intVal].mX_off = (s16)_NBytesToInt(ipData, 2, 0);
		ipData += 2;
		oFont->mAlphabet.mpAlphabetTextureOffsetMap[intVal].mY_off = (s16)_NBytesToInt(ipData, 2, 0);
		ipData += 6;
	}
	if(oFont->mAlphabet.mAlphabetSize > 0)
	{
		oFont->mAlphabet.mFixedHeight = (u16)(oFont->mAlphabet.mpAlphabetTextureOffsetMap[0].mH + oFont->mAlphabet.mpAlphabetTextureOffsetMap[0].mY_off);
		oFont->mAlphabet.mFixedWidth = 0;
	}
	else
	{
		oFont->mAlphabet.mFixedHeight = 0;
		oFont->mAlphabet.mFixedWidth = 0;
	}
}
// ----------------------------------------------------------------------------------
