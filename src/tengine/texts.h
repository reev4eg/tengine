/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#ifndef TEXTS_H
#define TEXTS_H

#include "platform.h"

#ifdef __cplusplus
extern "C" {
#endif

enum TextAlignment
{
	TEXT_ALIGNMENT_LEFT		= 0x00011000,
	TEXT_ALIGNMENT_RIGHT	= 0x00110000,
	TEXT_ALIGNMENT_TOP		= 0x00000011,
	TEXT_ALIGNMENT_BOTTOM	= 0x00000110,
	TEXT_ALIGNMENT_VCENTER	= 0x00000111,
	TEXT_ALIGNMENT_HCENTER	= 0x00111000
};

//----------------------------------------------------------------------------------------------------------------

struct AlphabetTextureOffsetMap
{
	s16 mX;
	s16 mY;
	s16 mW;
	s16 mH;
	s16 mX_off;
	s16 mY_off;
};

//----------------------------------------------------------------------------------------------------------------

struct TextAlphabet
{
	u16	mFixedWidth;	///< Value of fixed width, this parameter has no effect when mpAlphabetTextureOffsetMap != NULL 
	u16	mFixedHeight;	///< Value of fixed height, this parameter has no effect when mpAlphabetTextureOffsetMap != NULL
	u16	mAlphabetSize;	///< Alphabet size (in characters)
	wchar *mpAlphabet;	///< pointer to alphabet string. Can be NULL, in this case Text object will be use global alphabet values
	struct AlphabetTextureOffsetMap *mpAlphabetTextureOffsetMap;	///< pointer to sequence of Rect that contain X, Y offset on 2d texture and width and height of each char. Can be NULL, in this case it is expected, that 2d texture contains chars with fixed width and height
};

//----------------------------------------------------------------------------------------------------------------

struct TERFont
{
	s32 mResRef;
	s32 mThisRef;
	struct TextAlphabet mAlphabet;
	const struct BMPImage *mpAbcImage;
	const u8 *mpRawData;
};

//----------------------------------------------------------------------------------------------------------------

#ifndef NITRO_SDK

s32 STD_StrLen(const char *str);

char* STD_StrCpy(char *dest, const char *src);

s32 STD_StrCmp(const char *a, const char *b);

s32 STD_StrCmp(const char *a, const char *b);

char *STD_StrCat(char *str1, const char *str2);

#endif

void STD_NumToString(u16 num, char *buf, u16 bufSize);

s32 STD_WStrLen(const wchar *str);

s32 STD_WSprintf(wchar *buf, const wchar *fmt, ...);

s32 STD_WStrCmp(const wchar *cs, const wchar *ct);

s32 STD_WStrNCmp(const wchar *s1, const wchar *s2, s32 n);

wchar *STD_WStrStr(wchar *s, const wchar *find);

wchar* STD_WStrCpy(wchar* str1, const wchar* str2);

//----------------------------------------------------------------------------------------------------------------

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif //TEXTS_H
