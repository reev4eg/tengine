/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include "_ios/app_ios.h"
#import <QuartzCore/QuartzCore.h>
#import "EAGLView.h"

@implementation EAGLView

@synthesize
        surfaceSize=mSize,
        framebuffer=mFramebuffer,
        pixelFormat=mFormat,
        depthFormat=mDepthFormat,
        context=mContext;

+ (Class) layerClass
{
	return [CAEAGLLayer class];
}

- (BOOL) CreateSurface
{
	CAEAGLLayer* eaglLayer = (CAEAGLLayer*)[self layer];
	
	if(![EAGLContext setCurrentContext:mContext])
    {
		return NO;
	}
	
	if([self respondsToSelector:@selector(contentScaleFactor)]) 
    {
		self.contentScaleFactor = mScale;
	}
	else 
    {
        // @TODO?
	}
	
	GLint width, height;
	
	glGenRenderbuffers(1, &mRenderbuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, mRenderbuffer);

	if(![mContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer])
	{
		glDeleteRenderbuffers(1, &mRenderbuffer);
		return NO;
	}
    
	glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
	glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
	glGenFramebuffers(1, &mFramebuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, mFramebuffer);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, mRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);

    if (mDepthFormat)
	{
		glGenRenderbuffers(1, &mDepthBuffer);
		glBindRenderbuffer(GL_RENDERBUFFER, mDepthBuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, mDepthFormat, width, height);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, mDepthBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, 0);
	}
	
	if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
		//iLog("failed to make complete framebuffer object %x", glCheckFramebufferStatus(GL_FRAMEBUFFER));
		return NO;
	}

    mSize.width = width;
    mSize.height = height;
    
    mInitSurface = 1;
    
	return YES;
}

- (void) DestroySurface
{
	if(mInitSurface == 1)
    {
        if(mDepthFormat)
        {
            glDeleteRenderbuffers(1, &mDepthBuffer);
            mDepthBuffer = 0;
        }
        glDeleteRenderbuffers(1, &mRenderbuffer);
        mRenderbuffer = 0;
        glDeleteFramebuffers(1, &mFramebuffer);
        mFramebuffer = 0;
    }
    mInitSurface = 0;
}

- (id) InitWithFrame:(CGRect)frame
{
	if((self = [super initWithFrame:frame]))
    {
#if TARGET_IPHONE_SIMULATOR
        mFormat = kEAGLColorFormatRGBA8;
#else
        mFormat = kEAGLColorFormatRGB565;
#endif
        mDepthFormat = 0; //GL_DEPTH_COMPONENT16;
        UIScreen* mainScreen = [UIScreen mainScreen];
        mScale = mainScreen.scale;
        self.bounds = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height);
		CAEAGLLayer* eaglLayer = (CAEAGLLayer*)[self layer];
        eaglLayer.opaque = TRUE;
		[eaglLayer setDrawableProperties: [NSDictionary dictionaryWithObjectsAndKeys: [NSNumber numberWithBool:NO], kEAGLDrawablePropertyRetainedBacking, mFormat, kEAGLDrawablePropertyColorFormat, nil]];
		mContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES1];
		if(mContext == nil)
        {
			[self release];
			return nil;
		}
        
        IOSApp_nativeCreateRenderDevice();
        
        [self setMultipleTouchEnabled:NO];
	}

	return self;
}

- (void) dealloc
{
	[self DestroySurface];
	[mContext release];
	mContext = nil;
	[super dealloc];
}

- (void) layoutSubviews
{
	GLint width = self.bounds.size.width*mScale;
    GLint height = self.bounds.size.height*mScale;
    if(mSize.width != width || mSize.height != height)
    {
        [self DestroySurface];
        [self CreateSurface];
    }
    IOSApp_nativeResize(width, height);
}

- (BOOL) BeginRender
{
    if(mFramebuffer != 0)
    {
        glBindFramebuffer(GL_FRAMEBUFFER, mFramebuffer);
        return true;
    }
    else
    {
        return false;
    }
}

- (void) EndRender
{
    if(mDepthFormat) 
    {
        GLenum attachments[] = { GL_DEPTH_ATTACHMENT };
        glDiscardFramebufferEXT(GL_FRAMEBUFFER, 1, attachments);
    }
    glBindRenderbuffer(GL_RENDERBUFFER, mRenderbuffer);
    if(![mContext presentRenderbuffer:GL_RENDERBUFFER])
    {
        //printf("Failed to swap renderbuffer in %s\n", __FUNCTION__);
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    int i = 0;
	for(UITouch* touch in touches)
	{
        CGPoint touchLocation = [touch locationInView:self];
        IOSApp_nativeTouchPadDown(i, touchLocation.x, touchLocation.y);
        ++i;
	}
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    int i = 0;
	for(UITouch* touch in touches)
	{
		CGPoint touchLocation = [touch locationInView:self];
        IOSApp_nativeTouchPadMove(i, touchLocation.x, touchLocation.y);
        ++i;
	}
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    int i = 0;
	for(UITouch* touch in touches)
	{
		CGPoint touchLocation = [touch locationInView:self];
        IOSApp_nativeTouchPadUp(i, touchLocation.x, touchLocation.y);
        ++i;
	}
}

@end
