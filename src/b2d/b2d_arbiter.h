#ifndef B2D_ARBITER_H
#define B2D_ARBITER_H

#include "tengine.h"

#include "b2d_body.h"

#ifdef __cplusplus
extern "C" {
#endif

	union b2d_feature_pair
	{
		struct edges
		{
			u8 inEdge1;
			u8 outEdge1;
			u8 inEdge2;
			u8 outEdge2;
		} e;
		s32 value;
	};

	struct b2d_contact
	{
		struct fxVec2 position;
		struct fxVec2 normal;
		struct fxVec2 r1;
		struct fxVec2 r2;
		fx32 separation;
		fx32 Pn;	// accumulated normal impulse
		fx32 Pt;	// accumulated tangent impulse
		fx32 Pnb;	// accumulated normal impulse for position bias
		fx32 massNormal;
		fx32 massTangent;
		fx32 bias;
		union b2d_feature_pair feature;

		BOOL inited_;
	};

	void b2d_contact_init(struct b2d_contact *);

	struct b2d_arbiter_key
	{
		struct b2d_body* body1;
		struct b2d_body* body2;

		BOOL inited_;
	};

	void b2d_arbiter_key_init(struct b2d_arbiter_key *,
								struct b2d_body*,
								struct b2d_body*);

#define B2D_ARBITER_MAX_POINTS 2
	struct b2d_arbiter
	{
		struct b2d_contact contacts[B2D_ARBITER_MAX_POINTS];
		s32 numContacts;

		struct b2d_body* body1;
		struct b2d_body* body2;

		// Combined friction
		fx32 friction;

		BOOL inited_;
	};

	void b2d_arbiter_init(struct b2d_arbiter* ,struct b2d_body* b1, struct b2d_body* b2);
	void b2d_arbiter_update(struct b2d_arbiter* ,struct b2d_contact* contacts, s32 numContacts);
	void b2d_arbiter_prestep(struct b2d_arbiter* ,fx32 inv_dt);
	void b2d_arbiter_apply_impulse(struct b2d_arbiter*);

	// #operator <
	inline BOOL b2d_arbiter_is_less (const struct b2d_arbiter_key* a1, const struct b2d_arbiter_key* a2)
	{
		if (a1->body1 < a2->body1) //Magic pointer compare
			return TRUE;
		if (a1->body1 == a2->body1 && a1->body2 < a2->body2)
			return TRUE;
		return FALSE;
	}


	s32 b2d_collide(struct b2d_contact* contacts, struct b2d_body* body1, struct b2d_body* body2);


#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
