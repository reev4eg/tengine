
#include "b2d_arbiter.h"
#include "b2d_body.h"

#include "tengine.h"

enum Axis
{
	FACE_A_X,
	FACE_A_Y,
	FACE_B_X,
	FACE_B_Y
};

enum EdgeNumbers
{
	NO_EDGE = 0,
	EDGE1,
	EDGE2,
	EDGE3,
	EDGE4
};

struct clip_vertex
{
	struct fxVec2 v;
	union b2d_feature_pair fp;
};


void clip_vertex_init(struct clip_vertex* cv) 
{ 
	cv->fp.value = 0; 
}

void Flip(union b2d_feature_pair* fp)
{
	u8 ctmp = fp->e.inEdge1;
	fp->e.inEdge1 = fp->e.inEdge2;
	fp->e.inEdge2 = ctmp;

	ctmp = fp->e.outEdge1;
	fp->e.outEdge1 = fp->e.outEdge2;
	fp->e.outEdge2 = ctmp;
}

s32 clip_segment_to_line(struct clip_vertex vOut[2], struct clip_vertex vIn[2],
						const struct fxVec2* normal, fx32 offset, u8 clipEdge)
{
	s32 numOut = 0;
	fx32 distance0 = fxVec2Dot(*normal, vIn[0].v) - offset;
	fx32 distance1 = fxVec2Dot(*normal, vIn[1].v) - offset;

	if(distance0 <= FX32(0.0f)) vOut[numOut++] = vIn[0];
	if(distance1 <= FX32(0.0f)) vOut[numOut++] = vIn[1];

	if ( fxMul(distance0, distance1) < FX32(0.0f) )
	{
		fx32 interp = fxDiv(distance0, (distance0- distance1) );
		vOut[numOut].v = fxVec2Add(vIn[0].v, fxVec2MulFx( fxVec2Sub(vIn[1].v, vIn[0].v) , interp));
		if (distance0 > FX32(0.0f))
		{
			vOut[numOut].fp = vIn[0].fp;
			vOut[numOut].fp.e.inEdge1 = clipEdge;
			vOut[numOut].fp.e.inEdge2 = NO_EDGE;
		}else
		{
			vOut[numOut].fp = vIn[1].fp;
			vOut[numOut].fp.e.outEdge1 = clipEdge;
			vOut[numOut].fp.e.outEdge2 = NO_EDGE;
		}
		++numOut;
	}
	return numOut;
}

static void compute_incident_edge(struct clip_vertex c[2], const struct fxVec2* h, const struct fxVec2* pos,
								fxMat22* Rot, struct fxVec2* normal)
{
	fxMat22 RotT = fxMat22Transpose(Rot);
	struct fxVec2 n = fxVec2Sub(fxVec2Create(0,0), fxMat22MulVec(&RotT, normal) );
	struct fxVec2 nAbs = fxVec2Abs(n);

	if(nAbs.x > nAbs.y)
	{
		if(fxSign(n.x) > FX32(0.0f))
		{
			c[0].v = fxVec2Create(h->x, -h->y);
			c[0].fp.e.inEdge2 = EDGE3;
			c[0].fp.e.outEdge2 = EDGE4;

			c[1].v = fxVec2Create(h->x, h->y);
			c[1].fp.e.inEdge2 = EDGE4;
			c[1].fp.e.outEdge2 = EDGE1;
		}else
		{
			c[0].v = fxVec2Create(-h->x, h->y);
			c[0].fp.e.inEdge2 = EDGE1;
			c[0].fp.e.outEdge2 = EDGE2;

			c[1].v = fxVec2Create(-h->x, -h->y);
			c[1].fp.e.inEdge2 = EDGE2;
			c[1].fp.e.outEdge2 = EDGE3;
		}

	}else
	{
		if (fxSign(n.y) > FX32(0.0f))
		{
			c[0].v = fxVec2Create(h->x, h->y);
			c[0].fp.e.inEdge2 = EDGE4;
			c[0].fp.e.outEdge2 = EDGE1;

			c[1].v = fxVec2Create(-h->x, h->y);
			c[1].fp.e.inEdge2 = EDGE1;
			c[1].fp.e.outEdge2 = EDGE2;
		}
		else
		{
			c[0].v = fxVec2Create(-h->x, -h->y);
			c[0].fp.e.inEdge2 = EDGE2;
			c[0].fp.e.outEdge2 = EDGE3;

			c[1].v = fxVec2Create(h->x, -h->y);
			c[1].fp.e.inEdge2 = EDGE3;
			c[1].fp.e.outEdge2 = EDGE4;
		}
	}

	c[0].v = fxVec2Add(*pos, fxMat22MulVec(Rot, &c[0].v));
	c[1].v = fxVec2Add(*pos, fxMat22MulVec(Rot, &c[1].v));
}

s32 b2d_collide(struct b2d_contact* contacts, struct b2d_body* bodyA, struct b2d_body* bodyB)
{
	struct fxVec2 hA = fxVec2MulFx( bodyA->width, FX32(0.5f));
	struct fxVec2 hB = fxVec2MulFx( bodyB->width, FX32(0.5f));
	struct fxVec2 posA = bodyA->position;
	struct fxVec2 posB = bodyB->position;
	fxMat22 RotA = fxMat22FromAngle(bodyA->rotation);
	fxMat22 RotB = fxMat22FromAngle(bodyB->rotation);
	fxMat22 RotAT = fxMat22Transpose(&RotA);
	fxMat22 RotBT = fxMat22Transpose(&RotB);
	struct fxVec2 a1 = RotA.c.col1;
	struct fxVec2 a2 = RotA.c.col2;
	struct fxVec2 b1 = RotB.c.col1;
	struct fxVec2 b2 = RotB.c.col2;

	struct fxVec2 dp = fxVec2Sub(posB, posA);
	struct fxVec2 dA = fxMat22MulVec(&RotAT, &dp);
	struct fxVec2 dB = fxMat22MulVec(&RotBT, &dp);

	fxMat22 C = fxMat22Mul(&RotAT, &RotB);
	fxMat22 absC = fxMat22Abs(&C);
	fxMat22 absCT = fxMat22Transpose( &absC);

	struct fxVec2 faceA = fxVec2Sub(fxVec2Sub(fxVec2Abs(dA), hA), fxMat22MulVec(&absC, &hB));
	struct fxVec2 faceB = fxVec2Sub(fxVec2Sub(fxVec2Abs(dB), fxMat22MulVec(&absCT ,&hA) ), hB);

	const fx32 relativeTol = FX32(0.95f);
	const fx32 absoluteTol = FX32(0.01f);

	enum Axis axis;
	fx32 separation;
	struct fxVec2 normal;

	// Setup clipping plane data based on the separating axis
	struct fxVec2 frontNormal;
	struct fxVec2 sideNormal;
	struct clip_vertex incidentEdge[2];
	fx32 front;
	fx32 negSide;
	fx32 posSide;
	u8 negEdge;
	u8 posEdge;

	if (faceA.x > FX32(0.0f) || faceA.y > FX32(0.0f) )
		return 0;

	if (faceB.x > FX32(0.0f) || faceB.y > FX32(0.0f) )
		return 0;

	clip_vertex_init(&incidentEdge[0]);
	clip_vertex_init(&incidentEdge[1]);

	axis = FACE_A_X;
	separation = faceA.x;
	normal = (dA.x > FX32(0.0f)) ? RotA.c.col1 : fxVec2Inv(RotA.c.col1);

	if (faceA.y > (fxMul(relativeTol, separation) + fxMul(absoluteTol, hA.y)))
	{
		axis = FACE_A_Y;
		separation = faceA.y;
		normal = (dA.y > FX32(0.0f)) ? RotA.c.col2 : fxVec2Inv(RotA.c.col2);
	}

	// Box B faces
	if (faceB.x > (fxMul(relativeTol, separation) + fxMul(absoluteTol, hB.x)))
	{
		axis = FACE_B_X;
		separation = faceB.x;
		normal = (dB.x > FX32(0.0f) ) ? RotB.c.col1 : fxVec2Inv(RotB.c.col1);
	}

	if (faceB.y > (fxMul(relativeTol, separation) + fxMul(absoluteTol, hB.y)))
	{
		axis = FACE_B_Y;
		separation = faceB.y;
		normal = dB.y > 0.0f ? RotB.c.col2 : fxVec2Inv(RotB.c.col2);
	}



	switch (axis)
	{
	case FACE_A_X:
		{
			fx32 side;
			frontNormal = normal;
			front = fxVec2Dot(posA, frontNormal) + hA.x;
			sideNormal = RotA.c.col2;
			side = fxVec2Dot(posA, sideNormal);
			negSide = -side + hA.y;
			posSide =  side + hA.y;
			negEdge = EDGE3;
			posEdge = EDGE1;
			compute_incident_edge(incidentEdge, &hB, &posB, &RotB, &frontNormal);
		}
		break;

	case FACE_A_Y:
		{
			fx32 side;
			frontNormal = normal;
			front = fxVec2Dot(posA, frontNormal) + hA.y;
			sideNormal = RotA.c.col1;
			;side = fxVec2Dot(posA, sideNormal);
			negSide = -side + hA.x;
			posSide =  side + hA.x;
			negEdge = EDGE2;
			posEdge = EDGE4;
			compute_incident_edge(incidentEdge, &hB, &posB, &RotB, &frontNormal);
		}
		break;

	case FACE_B_X:
		{
			fx32 side;
			frontNormal = fxVec2Inv(normal);
			front = fxVec2Dot(posB, frontNormal) + hB.x;
			sideNormal = RotB.c.col2;
			side = fxVec2Dot(posB, sideNormal);
			negSide = -side + hB.y;
			posSide =  side + hB.y;
			negEdge = EDGE3;
			posEdge = EDGE1;
			compute_incident_edge(incidentEdge, &hA, &posA, &RotA, &frontNormal);
		}
		break;

	case FACE_B_Y:
		{
			fx32 side;
			frontNormal = fxVec2Inv(normal);
			front = fxVec2Dot(posB, frontNormal) + hB.y;
			sideNormal = RotB.c.col1;
			side = fxVec2Dot(posB, sideNormal);
			negSide = -side + hB.x;
			posSide =  side + hB.x;
			negEdge = EDGE2;
			posEdge = EDGE4;
			compute_incident_edge(incidentEdge, &hA, &posA, &RotA, &frontNormal);
		}
		break;
	}

	{
		struct clip_vertex clipPoints1[2];
		struct clip_vertex clipPoints2[2];
		struct fxVec2 tvec;
		s32 np;
		s32 numContacts = 0;
		s32 i;

		// Clip to box side 1
		tvec = fxVec2Inv(sideNormal);
		np = clip_segment_to_line(clipPoints1, incidentEdge, &tvec, negSide, negEdge);

		if (np < 2)
			return 0;

		// Clip to negative box side 1
		np = clip_segment_to_line(clipPoints2, clipPoints1,  &sideNormal, posSide, posEdge);

		if (np < 2)
			return 0;


		for (i = 0; i < 2; ++i)
		{
			fx32 separation = fxVec2Dot(frontNormal, clipPoints2[i].v) - front;

			if (separation <= 0)
			{
				contacts[numContacts].separation = separation;
				contacts[numContacts].normal = normal;
				// slide contact point onto reference face (easy to cull)
				contacts[numContacts].position = fxVec2Sub(clipPoints2[i].v, fxVec2MulFx(frontNormal, separation ));
				contacts[numContacts].feature = clipPoints2[i].fp;
				if (axis == FACE_B_X || axis == FACE_B_Y)
					Flip(&contacts[numContacts].feature);
				++numContacts;
			}
		}

		return numContacts;


	}

}
