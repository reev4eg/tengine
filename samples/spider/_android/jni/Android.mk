
LOCAL_PATH := $(call my-dir)

# -----------------------------
# ------ tengine --------------
# -----------------------------

include $(CLEAR_VARS)

LOCAL_MODULE := tengine

LOCAL_C_INCLUDES := \
	$(LOCAL_PATH)/../../game \
	$(LOCAL_PATH)/../../../../src/tengine \
	$(LOCAL_PATH)/../../../../src/gui \
	$(LOCAL_PATH)/../../../../src/verlet \
	$(LOCAL_PATH)/../../../../src/gamefield \
	$(LOCAL_PATH)/../../../../src/_android

LOCAL_CFLAGS := -DANDROID_NDK -DFRAME_ALLOCATOR -DSDK_DEBUG -DUSE_OPENGL_RENDER -DUSE_SLES_SOUND
# -DJOBS_IN_SINGLE_THREAD
#LOCAL_CFLAGS := -DANDROID_NDK -DFRAME_ALLOCATOR -DSDK_DEBUG -DUSE_CUSTOM_RENDER

LOCAL_SRC_FILES := \
	../../game/gamefield.c \
	../../../../src/verlet/lib/verlet.c \
	../../../../src/verlet/lib/verlet_constraints.c \
	../../../../src/verlet/lib/verlet_objects.c \
	../../../../src/tengine/lib/a_filesystem.c \
	../../../../src/tengine/lib/crossgl.c \
	../../../../src/tengine/lib/fxmath.c \
	../../../../src/tengine/lib/gamepad.c \
	../../../../src/tengine/lib/gx_helpers.c \
	../../../../src/tengine/lib/hash.c \
	../../../../src/tengine/lib/jobs.c \
	../../../../src/tengine/lib/loadhelpers.c \
	../../../../src/tengine/lib/loadtdata.c \
	../../../../src/tengine/lib/memory.c \
	../../../../src/tengine/lib/network.c \
	../../../../src/tengine/lib/platform.c \
	../../../../src/tengine/lib/render2dgl.c \
	../../../../src/tengine/lib/serialization.c \
	../../../../src/tengine/lib/sound.c \
	../../../../src/tengine/lib/tengine.c \
	../../../../src/tengine/lib/tengine_pr.c \
	../../../../src/tengine/lib/touchpad.c \
	../../../../src/_android/app-android.c

LOCAL_LDLIBS :=-lGLESv1_CM -lOpenSLES -landroid -ldl -llog
LOCAL_STATIC_LIBRARIES := android_native_app_glue

include $(BUILD_SHARED_LIBRARY)

$(call import-module,android/native_app_glue)
