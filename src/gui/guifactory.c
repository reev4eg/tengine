/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include "lib/tengine_low.h"
#include "guifactory.h"
#include "guiobject2D.h"
#include "guibutton.h"
#include "guispeedbutton.h"
#include "guicontainer.h"
#include "gui_low.h"

#define MAX_GUI_TYPES 8

static struct GUIObject2D* _ParseButton(s32 mapObjId, s32 gpId);
static BOOL _ParseButtonFocus(s32 mapObjId, s32 gpId);
static struct GUIObject2D* _ParseSpeedButton(s32 mapObjId, s32 gpId);
static BOOL _ParseButtonText(s32 mapObjId, s32 gpId, enum GUIControlType type);
static struct GUIObject2D* _ParseSlider(s32 mapObjId, s32 gpId);
static BOOL _ParseSliderContainer(s32 mapObjId, s32 gpId);

static struct GUIButtonInitParameters _mButtonTypes[MAX_GUI_TYPES];
static struct GUISpeedButtonInitParameters _mSpeedButtonTypes[MAX_GUI_TYPES];
static struct GUISliderInitParameters _mSliderTypes[MAX_GUI_TYPES];
static s32 _mButtonTypesSize = 0;
static s32 _mSpeedButtonTypesSize = 0;
static s32 _mSliderTypesSize = 0;
static s32 _mImageTypesSize = 0;
static struct GUIContainer* _mpTargetGUIContainer = NULL;

//---------------------------------------------------------------------------

void GUIFactory_Reset(void)
{
    _mButtonTypesSize = 0;
    _mSliderTypesSize = 0;
    _mSpeedButtonTypesSize = 0;
	_mImageTypesSize = 0;
	_mpTargetGUIContainer = NULL;
}
//---------------------------------------------------------------------------

void GUIFactory_Init(struct GUIContainer *pTargetGUIManager)
{
	_mpTargetGUIContainer = pTargetGUIManager;
}
//---------------------------------------------------------------------------

void GUIFactory_AddGUIButtonType(const struct GUIButtonInitParameters *params)
{
    SDK_ASSERT(_mButtonTypesSize < MAX_GUI_TYPES);
    _mButtonTypes[_mButtonTypesSize] = *params;
    _mButtonTypesSize++;
}
//---------------------------------------------------------------------------

void GUIFactory_AddGUISpeedButtonType(const struct GUISpeedButtonInitParameters *params)
{
    SDK_ASSERT(_mSpeedButtonTypesSize < MAX_GUI_TYPES);
    _mSpeedButtonTypes[_mSpeedButtonTypesSize] = *params;
    _mSpeedButtonTypesSize++;
}
//---------------------------------------------------------------------------

void GUIFactory_AddGUISliderType(const struct GUISliderInitParameters *params)
{
    SDK_ASSERT(_mSliderTypesSize < MAX_GUI_TYPES);
    _mSliderTypes[_mSliderTypesSize] = *params;
    _mSliderTypesSize++;
}
//---------------------------------------------------------------------------

struct GUIButtonInitParameters GUIFactory_GetButtonInitParameters(s32 idx)
{
    SDK_ASSERT(idx >= 0 && idx < _mButtonTypesSize);
    return _mButtonTypes[idx];
}
//---------------------------------------------------------------------------

struct GUISpeedButtonInitParameters GUIFactory_GetSpeedButtonInitParameters(s32 idx)
{
    SDK_ASSERT(idx >= 0 && idx < _mSpeedButtonTypesSize);
    return _mSpeedButtonTypes[idx];
}
//---------------------------------------------------------------------------

void GUIFactory_ParseMapObjects(void)
{
	s32 layer, z, i;
	SDK_NULL_ASSERT(_mpTargetGUIContainer);
	layer = (s32)GUIContainer_GetLayer(_mpTargetGUIContainer);
	for(z = 0; z < getZonesCount(layer); z++)
	{
		s32 map_array_count = low_getZoneGameObjCount(GUIContainer_GetLayer(_mpTargetGUIContainer), z);
		s32 *map_array = low_getZoneGameObjDataArray(GUIContainer_GetLayer(_mpTargetGUIContainer), z);
		for(i = 0; i < map_array_count; i++)
		{
			struct GUIObject2D *obj;
			s32 mapObjId = map_array[i] & 0xffff; 
			s32 gpId = prGetGroupIdxLayer(mapObjId, GUIContainer_GetLayer(_mpTargetGUIContainer));

			obj = _ParseButton(mapObjId, gpId);
			if(obj != NULL)
			{
				GUIContainer_protected_Add(_mpTargetGUIContainer, obj);
				continue;
			}
			if(_ParseButtonFocus(mapObjId, gpId))
			{
				continue;
			}
			if(_ParseButtonText(mapObjId, gpId, GUI_TYPE_BUTTON))
			{
				continue;
			}
    
			obj = _ParseSpeedButton(mapObjId, gpId);
			if(obj != NULL)
			{
				GUIContainer_protected_Add(_mpTargetGUIContainer, obj);
				continue;
			}
			if(_ParseButtonText(mapObjId, gpId, GUI_TYPE_SPEEDBUTTON))
			{
				continue;
			}

			obj = _ParseSlider(mapObjId, gpId);
			if(obj != NULL)
			{
				GUIContainer_protected_Add(_mpTargetGUIContainer, obj);
				continue;
			}
			if(_ParseSliderContainer(mapObjId, gpId))
			{
				continue;
			}
		}
	}
}
//---------------------------------------------------------------------------

struct GUIObject2D* _ParseButton(s32 mapObjId, s32 gpId)
{
    s32 i;
    struct GUIObject2D *obj;
    for(i = 0; i < _mButtonTypesSize; ++i)
    {
		if(_mButtonTypes[i].baseInitParameters.mapObjTypeID[BUTTON_MAPOBJ_ID_IDLE] == gpId)
        {
            obj = (struct GUIObject2D *)MALLOC(sizeof(struct GUIButton), "guifactory::ParseButton::obj");
			GUIButton_protected_Init((struct GUIButton*)obj, &_mButtonTypes[i], mapObjId, _mpTargetGUIContainer);
            return obj;
        }
	}
    return NULL;
}
//---------------------------------------------------------------------------

struct GUIObject2D* _ParseSpeedButton(s32 mapObjId, s32 gpId)
{
    s32 i;
    struct GUIObject2D *obj;
    for(i = 0; i < _mSpeedButtonTypesSize; ++i)
    {
		if(_mSpeedButtonTypes[i].baseInitParameters.mapObjTypeID[BUTTON_MAPOBJ_ID_IDLE] == gpId)
        {
            obj = (struct GUIObject2D *)MALLOC(sizeof(struct GUISpeedButton), "guifactory::ParseSpeedButton::obj");
			GUISpeedButton_protected_Init((struct GUISpeedButton*)obj, &_mSpeedButtonTypes[i], mapObjId, _mpTargetGUIContainer);
			return obj;
        }
    }
    return NULL;
}
//---------------------------------------------------------------------------

BOOL _ParseButtonFocus(s32 mapObjId, s32 gpId)
{
    s32 i, j, p;
    struct GUIObject2D* obj;
	p = prGetJoinedToLayer(mapObjId, GUIContainer_GetLayer(_mpTargetGUIContainer));
    
    if(p == NONE_MAP_IDX)
	{
        return FALSE;
    }
    
    for(i = 0; i < _mButtonTypesSize; ++i)
    {
		if(_mButtonTypes[i].baseInitParameters.mapObjTypeID[BUTTON_MAPOBJ_ID_FOCUSED] == gpId)
        {
			for(j = 0; j < GUIContainer_GetObjectsCount(_mpTargetGUIContainer); ++j)
            {
				obj = GUIContainer_GetObject(_mpTargetGUIContainer, j);
				if(GUIObject2D_GetMapObjId(obj) == p && GUIObject2D_GetType(obj) == GUI_TYPE_BUTTON)
                {
					GUIButton_protected_SetFocusMapObjId(((struct GUIButton*)obj), mapObjId); 
					GUIObject2D_protected_SetFocusFlag(obj, FALSE);
                    return TRUE;
                }

            }
        }
    }
    return FALSE;
}
//---------------------------------------------------------------------------

BOOL _ParseButtonText(s32 mapObjId, s32 gpId, enum GUIControlType type)
{
    s32 i, j, p, btt_sz;
    struct GUIObject2D* obj;
	p = prGetJoinedToLayer(mapObjId, GUIContainer_GetLayer(_mpTargetGUIContainer));
    
    if(p == NONE_MAP_IDX)
    {
        return FALSE;
    }
    
	btt_sz = 0;
	switch(type)
	{
		case GUI_TYPE_NONE:
		case GUI_TYPE_SLIDER:
			SDK_ASSERT(0);
		break;
		case GUI_TYPE_BUTTON:
			btt_sz = _mButtonTypesSize;
		break;
		case GUI_TYPE_SPEEDBUTTON:
			btt_sz = _mSpeedButtonTypesSize;
	}

    for(i = 0; i < btt_sz; ++i)
    {
		s32 id = -1;
		switch(type)
		{
			case GUI_TYPE_NONE:
			case GUI_TYPE_SLIDER:
				SDK_ASSERT(0);
			break;
			case GUI_TYPE_BUTTON:
				id = _mButtonTypes[i].baseInitParameters.mapObjTypeID[BUTTON_MAPOBJ_ID_TEXT];
			break;
			case GUI_TYPE_SPEEDBUTTON:
				id = _mSpeedButtonTypes[i].baseInitParameters.mapObjTypeID[BUTTON_MAPOBJ_ID_TEXT];
		}
		if(id == gpId)
        {
			for(j = 0; j < GUIContainer_GetObjectsCount(_mpTargetGUIContainer); ++j)
            {
				obj = GUIContainer_GetObject(_mpTargetGUIContainer, j);
				if(GUIObject2D_GetMapObjId(obj) == p && GUIObject2D_GetType(obj) == type)
				{
					switch(type)
					{
						case GUI_TYPE_NONE:
							SDK_ASSERT(0);
						break;	
						case GUI_TYPE_BUTTON:
							GUIButton_protected_SetTextMapObjId(((struct GUIButton*)obj), mapObjId);
						break;
						case GUI_TYPE_SPEEDBUTTON:
							GUISpeedButton_protected_SetTextMapObjId(((struct GUISpeedButton*)obj), mapObjId);
						break;
						default:
							SDK_ASSERT(0);
					}
					return TRUE;
                }

            }
        }
    }
    return FALSE;
}
//---------------------------------------------------------------------------

struct GUIObject2D* _ParseSlider(s32 mapObjId, s32 gpId)
{
    s32 i;
    struct GUIObject2D *obj;
    for(i = 0; i < _mSliderTypesSize; ++i)
    {
		if(_mSliderTypes[i].mapObjSlider[SLIDER_MAPOBJ_ID_PANEL_DEFAULT] == gpId)
        {
            obj = (struct GUIObject2D *)MALLOC(sizeof(struct GUISlider), "guifactory::ParseSlider::obj");
			GUISlider_protected_Init((struct GUISlider*)obj, &_mSliderTypes[i], mapObjId, _mpTargetGUIContainer);
			return obj;
        }
    }
    return NULL;
}
//---------------------------------------------------------------------------

BOOL _ParseSliderContainer(s32 mapObjId, s32 gpId)
{
    s32 i, j, p;
    struct GUIObject2D* obj;
	p = prGetJoinedToLayer(mapObjId, GUIContainer_GetLayer(_mpTargetGUIContainer));
    
    if(p == NONE_MAP_IDX)
	{
        return FALSE;
    }
    
    for(i = 0; i < _mSliderTypesSize; ++i)
    {
		if(_mSliderTypes[i].mapObjSlider[SLIDER_MAPOBJ_ID_CONTAINER] == gpId)
        {
			for(j = 0; j < GUIContainer_GetObjectsCount(_mpTargetGUIContainer); ++j)
            {
				obj = GUIContainer_GetObject(_mpTargetGUIContainer, j);
				if(GUIObject2D_GetMapObjId(obj) == p && GUIObject2D_GetType(obj) == GUI_TYPE_SLIDER)
                {
					GUISlider_protected_SetContainerId(((struct GUISlider*)obj), mapObjId);
                    return TRUE;
                }

            }
        }
    }
    return FALSE;
}
//---------------------------------------------------------------------------
