#ifndef B2D_JOINT_H
#define B2D_JOINT_H

#include "tengine.h"

#include "b2d_body.h"

#ifdef __cplusplus
extern "C" {
#endif

struct b2d_joint
{
	fxMat22 M;
	struct fxVec2 localAnchor1;
	struct fxVec2 localAnchor2;
	struct fxVec2 r1;
	struct fxVec2 r2;
	struct fxVec2 bias;
	struct fxVec2 P;		// accumulated impulse
	struct b2d_body* body1;
	struct b2d_body* body2;
	fx32 biasFactor;
	fx32 softness; 

	BOOL inited_;
};

void b2d_joint_init(struct b2d_joint*);
void b2d_joint_set(struct b2d_joint*
				   , struct b2d_body* body1, struct b2d_body* body2
				   , const struct fxVec2* anchor);

void b2d_joint_prestep(struct b2d_joint*, fx32 inv_dt);
void b2d_joint_apply_impulse(struct b2d_joint*);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif //ARRAY_H
