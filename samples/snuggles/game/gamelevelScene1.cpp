
#include "gamelevelScene1.h" 
#include "constants.h"
#include "level_id.h"
#include "touchpad.h"

//------------------------------------------------------------------------------------

extern TouchPadData gTPData;

//------------------------------------------------------------------------------------

void GameFieldLevelScene1::init(u32 layerIdx)
{
	GameFieldLevel::init(layerIdx);
	mLevelFileIdx = LEVEL_IDX_SCENE1;
}
//------------------------------------------------------------------------------------

void GameFieldLevelScene1::release()
{
	GameFieldLevel::release();
}
//------------------------------------------------------------------------------------

void GameFieldLevelScene1::onUpdateObject(s32 iId, BOOL* const oDraw)
{
	(void)oDraw;
	switch(iId)
	{
		case M2_ORANGE:
		{
			fxVec2 pos;
			pos = prGetPosition(M2_ORANGE);
			pos.x += FX32(0.01f); //<-- fx32 operations 
			pos.y += FX32(0.02f);
			pos.x = FX_Mul(pos.x, pos.y);// == x * y;
			pos.x = FX_Div(pos.x, pos.y);// == x / y;
			// also
			pos.x = pos.y * FX32(3.0f);
			pos.y = FX32(100.0f) / FX32(5.0f);

			prAddXYToPosition(M2_ORANGE, FX32(0.5f), FX32(-0.7f));
		}
		break;
		case M2_FURSNUGGLE_BALL:
		{
			if(gTPData.point[0].mTrg)
			{
				if(prGetState(M2_FURSNUGGLE_BALL) == ST_TRANS_TO_IDLE)
				{
					prSetState(M2_FURSNUGGLE_BALL, ST_TRANS_TO_ACTIVE, SBM_FROM_END_ANIMATION);
				}
				else
				{
					if(prGetState(M2_FURSNUGGLE_BALL) != ST_TRANS_TO_ACTIVE)
					{
						prSetState(M2_FURSNUGGLE_BALL, ST_TRANS_TO_ACTIVE, SBM_NONE);
					}
				}
			}
			else
			{
				if(prGetState(M2_FURSNUGGLE_BALL) == ST_TRANS_TO_ACTIVE)
				{
					prSetState(M2_FURSNUGGLE_BALL, ST_TRANS_TO_IDLE, SBM_FROM_END_ANIMATION);

				}
			}
		}
	}
}
//----------------------------------------------------------------------------------

void GameFieldLevelScene1::afterDataLoad(s32 level)
{
	if(level == getLevelFileIdx())
	{
		prSetState(M2_CACTUS_29, ST_ACTIVATED, SBM_NONE);
		prSetState(M2_CACTUS_30, ST_IDLE, SBM_NONE);
		prSetState(M2_CACTUS_31, ST_IDLE, SBM_NONE);
		prSetState(M2_BEDCOVER_3_61, ST_ACTIVATED, SBM_NONE);
	}
}
//----------------------------------------------------------------------------------

void GameFieldLevelScene1::onEngineEvent(const struct EventCallbackData *pData)
{
	switch(pData->eventType)
	{
		case EVENT_TYPE_ON_ANIMATION:
			if(pData->eventId == EVENT_ENDANIM)
			{
				switch(pData->ownerId)
				{
					case M2_CACTUS_29:
						if(prGetState(M2_CACTUS_29) == ST_ACTIVATED)
						{
							prSetState(M2_CACTUS_29, ST_IDLE, SBM_NONE);
							prSetState(M2_CACTUS_30, ST_ACTIVATED, SBM_NONE);
							prSetState(M2_CACTUS_31, ST_IDLE, SBM_NONE);
						}
					break;
					case M2_CACTUS_30:
						if(prGetState(M2_CACTUS_30) == ST_ACTIVATED)
						{
							prSetState(M2_CACTUS_29, ST_IDLE, SBM_NONE);
							prSetState(M2_CACTUS_30, ST_IDLE, SBM_NONE);
							prSetState(M2_CACTUS_31, ST_ACTIVATED, SBM_NONE);
						}
					break;
					case M2_CACTUS_31:
						if(prGetState(M2_CACTUS_31) == ST_ACTIVATED)
						{
							prSetState(M2_CACTUS_29, ST_ACTIVATED, SBM_NONE);
							prSetState(M2_CACTUS_30, ST_IDLE, SBM_NONE);
							prSetState(M2_CACTUS_31, ST_IDLE, SBM_NONE);
						}
				}
			}
			break;
	}
}
//----------------------------------------------------------------------------------
