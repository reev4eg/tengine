#ifndef _GAMELEVEL_SCENE1_H_
#define _GAMELEVEL_SCENE1_H_

#include "gamelevel.h"

class GameFieldLevelScene1 : public GameFieldLevel
{
 public:

	virtual ~GameFieldLevelScene1(){};

	virtual void init(u32 layerIdx);
	virtual void release();

	virtual void afterDataLoad(s32 level);
	virtual void onUpdateObject(s32 iId, BOOL* const oDraw);
	virtual void onEngineEvent(const struct EventCallbackData *pData);
};

#endif //_GAMELEVEL_SCENE1_H_
