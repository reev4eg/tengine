/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#ifndef __VERLET_H__
#define __VERLET_H__

#include "fxmath.h"

#ifdef __cplusplus
extern "C" {
#endif

//---------------------------------------------------------------------------

struct VTObject;
struct VTParticle;
struct VTConstraint;

//---------------------------------------------------------------------------

struct VerletObjectInitParams
{
	u32 mParticlesMax;
	u32 mConstraintsMax;
	fx32 mFriction;
	fx32 mGroundFriction;
	struct fxVec2 mGravity;
};

struct VerletInitParams
{
	s32 mWidth;
	s32 mHeight;
	u32 mParticlesPoolMax;
	u32 mConstraintsPoolMax;
	u32 mObjectsCount;
	const struct VerletObjectInitParams *mppObjectInitArr;
};

struct Verlet
{
	s32 mWidth;
	s32 mHeight;
	s32 mObjectsPoolMax;
	s32 mParticlesPoolMax;
	s32 mConstraintsPoolMax;
	s32 mObjectsPoolCount;
	s32 mParticlesPoolCount;
	s32 mConstraintsPoolCount;
	struct VTParticle **mppParticlesPool;
	struct VTConstraint **mppConstraintsPool;
	struct VTObject **mppObjectsPool;
};

enum VTRenderCallbackDataType
{
	VTRT_PARTICLE = 0,
	VTRT_CONSTRAINT = 1
};

struct VTCallbackData
{
	enum VTRenderCallbackDataType mType;
	s32 mPointCount;
	const struct fxVec2 *mpPoint[3];
};

//---------------------------------------------------------------------------

typedef void (*OnVrltRenderCallback)(const struct VTCallbackData *iData);

//---------------------------------------------------------------------------

void vrltInit(struct Verlet *v, struct VerletInitParams *params);
void vrltRelease(struct Verlet *v);

void vrltClear(struct Verlet *v);
s32 vrltGetObjectsCount(struct Verlet *v);
s32 vrltGetParticlesCount(struct Verlet *v);
s32 vrltGetConstraintsCount(struct Verlet *v);
s32 vrltGetObjectParticlesCount(struct Verlet *v, s32 objIndex);
s32 vrltGetObjectConstraintsCount(struct Verlet *v, s32 objIndex);

s32 vrltBeginInitObject(struct Verlet *v);
void vrltInitObject_AddParticle(struct Verlet *v, s32 obj_id, struct fxVec2 pos);
void vrltInitObject_AddPinConstraint(struct Verlet *v, s32 obj_id, s32 particle_idx, struct fxVec2 *pos);
void vrltInitObject_AddDistanceConstraint(struct Verlet *v, s32 obj_id, s32 particle_idx1, s32 particle_idx2, fx32 stiffness, fx32 *distance);
void vrltInitObject_AddAngleConstraint(struct Verlet *v, s32 obj_id, s32 particle_idx1, s32 particle_idx2, s32 particle_idx3, fx32 stiffness);
void vrltEndInitObject(struct Verlet *v, s32 obj_id);
const struct fxVec2* vrltGetObjectParticlePosition(struct Verlet *v, s32 obj_id, s32 particle_idx);

s32 vrltCreateTire(struct Verlet *v, struct fxVec2 origin, fx32 radius, s32 segments, fx32 spokeStiffness, fx32 treadStiffness);
s32 vrltCreateCloth(struct Verlet *v, struct fxVec2 origin, s32 width, s32 height, s32 segments, s32 pinMod, fx32 stiffness);
s32 vrltCreateLine(struct Verlet *v, struct fxVec2 *vertices, s32 verticesCount, fx32 stiffness);
s32 vrltCreatePoint(struct Verlet *v, struct fxVec2 pos);

void vrltUpdate(struct Verlet *v, s32 ms);
void vrltRender(struct Verlet *v, s32 objIndex, OnVrltRenderCallback pCallbackFn);

//---------------------------------------------------------------------------

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif