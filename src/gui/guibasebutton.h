#ifndef GUI_BASEBUTTON_H
#define GUI_BASEBUTTON_H

#ifdef __cplusplus
extern "C" {
#endif

#include "system.h"
#include "guiobject2D.h"
#include "gamepad.h"

struct GUIBaseButton;

void GUIBaseButton_Click(struct GUIBaseButton *self);

void GUIBaseButton_SetEnable(struct GUIBaseButton *self, BOOL value);
BOOL GUIBaseButton_IsEnable(const struct GUIBaseButton *self);

void GUIBaseButton_EnableInput(struct GUIBaseButton *self, BOOL val);
BOOL GUIBaseButton_IsEnableInput(const struct GUIBaseButton *self);

void GUIBaseButton_SetAction(struct GUIBaseButton *self, const struct GUIAction *action);

struct GUIAction GUIBaseButton_GetAction(const struct GUIBaseButton *self);

void GUIBaseButton_GetCollideRect(const struct GUIBaseButton *self, fx32 rect[RECT_SIZE]);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
