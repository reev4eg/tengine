#ifndef B2D_BODY_H
#define B2D_BODY_H

#include "tengine.h"

#ifdef __cplusplus
extern "C" {
#endif

	struct b2d_body
	{
		struct fxVec2 position;
		fx32 rotation;

		struct fxVec2 velocity;
		fx32 angularVelocity;

		struct fxVec2 force;
		fx32 torque;

		struct fxVec2 width;

		fx32 friction;
		fx32 mass;
		fx32 invMass;
		fx32 I;
		fx32 invI;

		BOOL inited_;
	};

	void b2d_body_init(struct b2d_body*);
	void b2d_body_set(struct b2d_body*, const struct fxVec2* w, fx32 m);
	void b2d_body_add_force(struct b2d_body*, const struct fxVec2* f);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif //ARRAY_H
