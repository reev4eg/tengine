
package com.app.snuggles;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import tengine.common.AppGLSurfaceView;

//-----------------------------------------------------------------------
//-----------------------------------------------------------------------
//-----------------------------------------------------------------------

public class snuggles extends Activity
{		
	private AppGLSurfaceView mGLView = null;
    
//-----------------------------------------------------------------------
	
	static
	{
		System.loadLibrary("tengine");
	}
//-----------------------------------------------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
    	requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
                                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        
    	super.onCreate(savedInstanceState);
    	mGLView = new AppGLSurfaceView(this);
        setContentView(mGLView);
    }
//-----------------------------------------------------------------------

	@Override
    protected void onPause()
    {
        super.onPause();
        if(mGLView != null)
		{
			mGLView.onPause();
		}
    }
//-----------------------------------------------------------------------

    @Override
    protected void onResume()
    {
        super.onResume();
        if(mGLView != null)
		{
			mGLView.onResume();
		}
    }
//-----------------------------------------------------------------------

	@Override
    protected void onStop()
    {
        super.onStop();
        if(mGLView != null)
		{
			mGLView.onStop();
		}
    }
//-----------------------------------------------------------------------
    
    @Override
    protected void onDestroy()
    {
    	super.onDestroy();
        if(mGLView != null)
		{
			mGLView.onDestroy();
			mGLView = null;
		}
        System.runFinalizersOnExit(true);
        System.exit(0);
    }
}

//-----------------------------------------------------------------------
//-----------------------------------------------------------------------
//-----------------------------------------------------------------------