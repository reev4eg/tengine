/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#ifdef NITRO_SDK

#include "easygraphics.h"
#include "system.h"
#include "tengine.h"
#include "gamefield.h"

#define RES_PATH "data/"

//---------------------------------------------------------------------------

void VBlankIntr(void);

//---------------------------------------------------------------------------

void NitroMain()
{
    OSTick tickTime, tickTime1, dtick;
    u64 curr_time;

    OS_Init();
    FX_Init();
    GX_Init();

    OS_InitTick();

    OS_SetIrqFunction(OS_IE_V_BLANK, VBlankIntr);
    OS_EnableIrqMask(OS_IE_V_BLANK);
    OS_EnableIrq();
    GX_VBlankIntr(TRUE);

    GX_SetBankForLCDC(GX_VRAM_LCDC_ALL);
    MI_CpuClearFast((void *)HW_LCDC_VRAM, HW_LCDC_VRAM_SIZE);
    GX_DisableBankForLCDC();
    MI_CpuFillFast((void *)HW_OAM, 0, HW_OAM_SIZE);
    MI_CpuFillFast((void *)HW_PLTT, 0, HW_PLTT_SIZE);
    MI_CpuFillFast((void *)HW_DB_OAM, 0, HW_DB_OAM_SIZE);
    MI_CpuFillFast((void *)HW_DB_PLTT, 0, HW_DB_PLTT_SIZE);

    InitMemoryAllocator();
    {
    	struct InitFileSystemData fsd;
    	fsd.mpPath = RES_PATH;
    	InitFileSystem(&fsd);
    }
    InitRandom();
    
    GX_SetVisiblePlane(GX_PLANEMASK_NONE);
    GXS_SetVisiblePlane(GX_PLANEMASK_NONE);
    OS_WaitVBlankIntr();

    dtick = 0;
    tickTime = tickTime = OS_GetTick();

    tfgInit();
    tfgResize(HW_LCD_WIDTH, HW_LCD_HEIGHT);
        
    while(1)
    {
        tfgTick((s32)curr_time);
        egRender_TransferDataToVRAM();		
        OS_WaitVBlankIntr(); // waiting the end of V-Blank interrupt
        
        tickTime = tickTime1;    
        {
            tickTime1 = OS_GetTick();
            dtick = tickTime1 - tickTime;
            curr_time = OS_TicksToMilliSeconds( dtick );    
        }
    }
    
    tfgRelease();
    ReleaseFileSystem();
}
//---------------------------------------------------------------------------

void VBlankIntr(void)
{
   OS_SetIrqCheckFlag(OS_IE_V_BLANK);
}
//---------------------------------------------------------------------------

#endif //NITRO_SDK
