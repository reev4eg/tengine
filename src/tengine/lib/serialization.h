#ifndef SERIALIZATION_H
#define SERIALIZATION_H

#include "platform.h"
#include "fxmath.h"

#ifdef __cplusplus
extern "C" {
#endif

//---------------------------------------------------------------------------

struct broadcastCB_;

void SerializeU32(u32 in_, struct broadcastCB_* out_);
void UnSerializeU32(struct broadcastCB_* in_, u32* out_);

void SerializeS32(s32 in_, struct broadcastCB_* out_);
void UnSerializeS32(struct broadcastCB_* in_, s32* out_);

void SerializeFx32(fx32 in_, struct broadcastCB_* out_);
void UnSerializeFx32(struct broadcastCB_* in_, fx32* out_);

void SerializeFxVec2(struct fxVec2 in_, struct broadcastCB_* out_);
void UnSerializeFxVec2(struct broadcastCB_* in_, struct fxVec2* out_);

//---------------------------------------------------------------------------

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
