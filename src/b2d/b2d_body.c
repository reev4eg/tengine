
#include "b2d_body.h"

void b2d_body_init(struct b2d_body* body)
{
	body->position = fxVec2Create( FX32(0.0f), FX32(0.0f) );
	body->rotation = FX32(0.0f);
	body->velocity= fxVec2Create( FX32(0.0f), FX32(0.0f) );
	body->angularVelocity = FX32(0.0f);
	body->force= fxVec2Create( FX32(0.0f), FX32(0.0f) );
	body->torque = FX32(0.0f);
	body->friction = FX32(0.2f);

	body->width= fxVec2Create( FX32(1.0f), FX32(1.0f) );
	body->mass = FXMAX;
	body->invMass = FX32(0.0f);
	body->I = FXMAX;
	body->invI = FX32(0.0f);

	body->inited_ = TRUE;
}

void b2d_body_set(struct b2d_body* body, const struct fxVec2* w, fx32 m)
{
	SDK_ASSERT(body->inited_==TRUE);
	body->position = fxVec2Create( FX32(0.0f), FX32(0.0f) );
	body->rotation = FX32(0.0f);
	body->velocity = fxVec2Create( FX32(0.0f), FX32(0.0f) );
	body->angularVelocity = FX32(0.0f);
	body->force = fxVec2Create( FX32(0.0f), FX32(0.0f) );
	body->torque = FX32(0.0f);
	body->friction = FX32(0.2f);

	body->width = *w;
	body->mass = m;

	if (body->mass < FXMAX)
	{
		body->invMass = fxDiv( FX32(1.0f) , body->mass);
		body->I = fxDiv( 
			fxMul(body->mass , 
				(	fxMul(body->width.x, body->width.x)
				+	fxMul(body->width.y, body->width.y) )) 
			, FX32(12.0f));
		body->invI = fxDiv(FX32(1.0f), body->I);
	}
	else
	{
		body->invMass = FX32(0.0f);
		body->I = FXMAX;
		body->invI = FX32(0.0f);
	}
}

void b2d_body_add_force(struct b2d_body* body, const struct fxVec2* f)
{
	SDK_ASSERT(body->inited_==TRUE);
	body->force = fxVec2Add( body->force, *f);
}


