/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#ifndef PLATFORM_H_INCLUDED
#define PLATFORM_H_INCLUDED

#ifndef NITRO_SDK
#ifdef ANDROID_NDK
 #include "stdio.h"
 #include "assert.h"
 #include "android/log.h"
#endif

#ifdef WINDOWS_APP
 #ifdef _MSC_VER
#define _ALLOW_KEYWORD_MACROS
	#define inline __inline
	#define _CRT_SECURE_NO_WARNINGS 
 #endif
 #include "tchar.h"
 #include "windows.h"
 #include "stdio.h"
 #include "assert.h"
#endif

#ifdef NIX_APP
 #include "stdio.h"
 #include "assert.h"
 #define Sleep(ms) usleep(ms*1000)
#endif

#ifdef IOS_APP
 #include "stdio.h"
 #include "assert.h"
 #include "unistd.h"
 #define Sleep(ms) usleep(ms*1000)
#endif
#else
 #include <nitro.h>
 #include <nnsys.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif

#ifndef NITRO_SDK

#ifdef ANDROID_NDK
 #define NDK_NATIVE_API10
 #define Sleep(ms) usleep(ms*1000)
#endif

typedef unsigned char u8;
/* 
	ANDROID_NDK by default char is unsigned
	to avoid compatibility issues s8 type is removed
	typedef char s8;
*/
typedef unsigned short u16;
typedef short s16;
typedef unsigned int u32;
typedef signed long long int s64;
typedef unsigned long long int u64;
typedef int s32;
typedef int fx32;
typedef int BOOL;
#define FALSE 0
#define TRUE 1

#define SDK_ASSERT assert
#define SDK_NULL_ASSERT assert

#define FX32_SHIFT 16
#define FX32_ONE (1 << FX32_SHIFT)

 #ifdef WINDOWS_APP
  #define OS_Printf _tprintf
  #define OS_Warning _tprintf
  #define OS_SpleepMs(timeInMs) Sleep(timeInMs)   
 #else
  #ifdef NIX_APP
   #define OS_Printf printf
   #define OS_Warning printf
   #define OS_SpleepMs(timeInMs) usleep(timeInMs*1000) 
  #else
   #ifdef IOS_APP
    #define OS_Printf printf
    #define OS_Warning printf
    #define OS_SpleepMs(timeInMs) usleep(timeInMs*1000)
   #else
   #define OS_Printf(...) __android_log_print(ANDROID_LOG_INFO, ">>", __VA_ARGS__)
   #define OS_Warning(...) __android_log_print(ANDROID_LOG_WARN, "!!!", __VA_ARGS__)
   #define OS_SpleepMs(timeInMs) usleep(timeInMs*1000)  
  #endif
 #endif
 #endif

 typedef u16 GXRgba;
 typedef u16 GXRgb;
 #define GX_RGBA_B_SHIFT           1
 #define GX_RGBA_B_MASK            0x003e
 #define GX_RGBA_G_SHIFT           6
 #define GX_RGBA_G_MASK            0x07c0
 #define GX_RGBA_R_SHIFT           11
 #define GX_RGBA_R_MASK            0xf800
 #define GX_RGBA_A_SHIFT           0
 #define GX_RGBA_A_MASK            0x0001
 
 #define GX_RGBA(r, g, b, a)       (GXRgba)(((r) << GX_RGBA_R_SHIFT) | \
											((g) << GX_RGBA_G_SHIFT) | \
											((b) << GX_RGBA_B_SHIFT) | \
											((a) << GX_RGBA_A_SHIFT))
 typedef u32 GXRgba32;

 /*stubs*/
 #define GX_VRAM_BG_256_AB 0
 #define GX_VRAM_SUB_BG_128_C 0
 #define GX_DISP_SELECT_SUB_MAIN 0
 #define GX_BG_BMPSCRBASE_0x00000 0
 #define GX_PLANEMASK_BG3 0
 #define GX_PLANEMASK_BG1 0
 #define GX_BG_SCRSIZE_DCBMP_256x256 0
 #define GX_BG_AREAOVER_XLU 0

 #define GX_SetBankForBG(v)
 #define GX_SetBankForSubBG(v)
 #define GX_SetDispSelect(v)
 #define GXS_SetVisiblePlane(v)
 #define GX_SetVisiblePlane(v)
 #define G2_SetBG2ControlDCBmp(v)
 #define G2_SetBG2Priority(v)

 #define OS_ResetSystem(v)

 typedef unsigned int GXBGBmpScrBase;
 typedef void NNSSndHandle;
 typedef void NNSSndStrmHandle;

#endif

#define COLOR888TO1555(r,g,b) (GX_RGBA((r * 31) >> 8, (g * 31) >> 8, (b * 31) >> 8, 1))
#define COLOR_TO_1555(c) (GX_RGBA(((((c >> 16) & 255) * 31) / 255), ((((c >> 8) & 255) * 31) / 255), (((c & 255) * 31) / 255), 1))

#define FX32(v) ((fx32)((v) * FX32_ONE))

typedef unsigned short wchar;

u32 OS_GetTime(void);

//----------------------------------------------------------------------------------------------

#ifdef __cplusplus
}
#endif

#endif /* PLATFORM_H_INCLUDED*/

