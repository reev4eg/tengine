
#include "gamelevelScene2.h" 
#include "constants.h"
#include "level_id.h"

void GameFieldLevelScene2::init(u32 layerIdx)
{
	GameFieldLevel::init(layerIdx);
	mLevelFileIdx = LEVEL_IDX_SCENE2;
}
//------------------------------------------------------------------------------------

void GameFieldLevelScene2::release()
{
	GameFieldLevel::release();
}
//------------------------------------------------------------------------------------

void GameFieldLevelScene2::onUpdateObject(s32 iId, BOOL* const oDraw)
{
	(void)iId;
	(void)oDraw;
}
//----------------------------------------------------------------------------------

void GameFieldLevelScene2::onEngineEvent(const struct EventCallbackData *pData)
{
	switch(pData->eventType)
	{
		case EVENT_TYPE_ON_ANIMATION:
			break;
	}
}
//----------------------------------------------------------------------------------
