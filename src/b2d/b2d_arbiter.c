
#include "b2d_arbiter.h"

#include "b2d_world.h"

void b2d_contact_init(struct b2d_contact * contact)
{
	contact->Pn = FX32(0.0f);
	contact->Pt = FX32(0.0f);
	contact->Pnb = FX32(0.0f);
};

void b2d_arbiter_key_init(	struct b2d_arbiter_key * arb_key, struct b2d_body* b1,	struct b2d_body* b2)
{
	if (b1 < b2)
	{
		arb_key->body1 = b1; 
		arb_key->body2 = b2;
	}
	else
	{
		arb_key->body1 = b2; 
		arb_key->body2 = b1;
	}
};

void b2d_arbiter_init(struct b2d_arbiter* arbiter, struct b2d_body* b1, struct b2d_body* b2)
{
	if (b1 < b2)
	{
		arbiter->body1 = b1;
		arbiter->body2 = b2;
	}
	else
	{
		arbiter->body1 = b2;
		arbiter->body2 = b1;
	}
	arbiter->numContacts = b2d_collide(arbiter->contacts, arbiter->body1, arbiter->body2);
	arbiter->friction = fxSqrt( 
		fxMul(arbiter->body1->friction, arbiter->body2->friction) );
};

void b2d_arbiter_update(struct b2d_arbiter* arbiter,struct b2d_contact* newContacts, s32 numNewContacts)
{
	struct b2d_contact mergedContacts[2];
	s32 i;
	s32 j;
	for(i=0; i<numNewContacts; ++i)
	{
		struct b2d_contact* cNew = newContacts + i;
		s32 k = -1;
		for (j = 0; j < arbiter->numContacts; ++j)
		{
			struct b2d_contact* cOld = arbiter->contacts + j;
			if (cNew->feature.value == cOld->feature.value)
			{
				k = j;
				break;
			}
		}

		if (k > -1)
		{
			struct b2d_contact* c = mergedContacts + i;
			struct b2d_contact* cOld = arbiter->contacts + k;
			*c = *cNew;
			if (b2d_world_warm_starting == TRUE)
			{
				c->Pn = cOld->Pn;
				c->Pt = cOld->Pt;
				c->Pnb = cOld->Pnb;
			}
			else
			{
				c->Pn = FX32(0.0f);
				c->Pt = FX32(0.0f);
				c->Pnb = FX32(0.0f);
			}
		}
		else
		{
			mergedContacts[i] = newContacts[i];
		}
	}
	for (i = 0; i < numNewContacts; ++i)
		arbiter->contacts[i] = mergedContacts[i];

	arbiter->numContacts = numNewContacts;
};

void b2d_arbiter_prestep(struct b2d_arbiter* arbiter,fx32 inv_dt)
{
	const fx32 k_allowedPenetration = FX32(0.01f);
	fx32 k_biasFactor = (b2d_world_position_correction==TRUE ? FX32(0.2f) : FX32(0.0f));
	s32 i;
	for (i = 0; i < arbiter->numContacts; ++i)
	{
		struct b2d_contact* c =	arbiter->contacts + i;
		struct fxVec2 r1 = fxVec2Sub(c->position, arbiter->body1->position);
		struct fxVec2 r2 = fxVec2Sub(c->position, arbiter->body2->position);
		// Precompute normal mass, tangent mass, and bias.
		fx32 rn1 = fxVec2Dot(r1, c->normal);
		fx32 rn2 = fxVec2Dot(r2, c->normal);
		fx32 kNormal = (arbiter->body1->invMass + arbiter->body2->invMass)
					+	fxMul(arbiter->body1->invI, (fxVec2Dot(r1, r1) - fxMul(rn1, rn1) ) ) 
					+	fxMul(arbiter->body2->invI, (fxVec2Dot(r2, r2) - fxMul(rn2, rn2) ) );
				
		struct fxVec2 tangent = fxVec2CrossFx(c->normal, FX32(1.0f));
		fx32 rt1 = fxVec2Dot(r1, tangent);
		fx32 rt2 = fxVec2Dot(r2, tangent);
		fx32 kTangent = (arbiter->body1->invMass + arbiter->body2->invMass)
					+ fxMul(arbiter->body1->invI, (fxVec2Dot(r1, r1) - fxMul(rt1, rt1) )) 
					+ fxMul(arbiter->body2->invI, (fxVec2Dot(r2, r2) - fxMul(rt2, rt2) ));
		
		c->massNormal = fxDiv(FX32(1.0f), kNormal);		
		c->massTangent = fxDiv(FX32(1.0f),  kTangent);

		c->bias = fxMul(-k_biasFactor, fxMul(inv_dt, fxMin(FX32(0.0f), c->separation + k_allowedPenetration)));
		if (b2d_world_accumulate_impulses==TRUE)
		{
			// Apply normal + friction impulse
			struct fxVec2 P = fxVec2Add(fxVec2MulFx(c->normal, c->Pn), fxVec2MulFx(tangent, c->Pt));

			arbiter->body1->velocity = fxVec2Sub( arbiter->body1->velocity
				, fxVec2MulFx( P, arbiter->body1->invMass));
			arbiter->body1->angularVelocity -= fxMul(fxVec2Cross(r1, P), arbiter->body1->invI);


			arbiter->body2->velocity = fxVec2Sub( arbiter->body2->velocity
				, fxVec2MulFx( P, arbiter->body2->invMass));
			arbiter->body2->angularVelocity -= fxMul(fxVec2Cross(r2, P), arbiter->body2->invI);
		}
	}
};


// Its HELL baby
void b2d_arbiter_apply_impulse(struct b2d_arbiter* arbiter)
{
	struct b2d_body* b1 = arbiter->body1;
	struct b2d_body* b2 = arbiter->body2;
	s32 i;

	for (i = 0; i < arbiter->numContacts; ++i)
	{
		struct b2d_contact* c = arbiter->contacts + i;
		c->r1 = fxVec2Sub(c->position, b1->position);
		c->r2 = fxVec2Sub(c->position, b2->position);
		{
			// Relative velocity at contact
			struct fxVec2 dv =	fxVec2Sub(
									fxVec2Sub(
										fxVec2Add( 
											b2->velocity, 
											fxVec2CrossFxInv(b2->angularVelocity, c->r2))
										, b1->velocity)
									, fxVec2CrossFxInv(b1->angularVelocity, c->r1));

			// Compute normal impulse
			fx32 vn = fxVec2Dot(dv, c->normal);

			fx32 dPn = fxMul(c->massNormal, (-vn + c->bias));

			if (b2d_world_accumulate_impulses==TRUE)
			{
				// Clamp the accumulated impulse
				fx32 Pn0 = c->Pn;
				c->Pn = fxMax(Pn0 + dPn, FX32(0.0f));
				dPn = c->Pn - Pn0;
			}
			else
			{
				dPn = fxMax(dPn, FX32(0.0f));
			}

			{
				// Apply contact impulse
				struct fxVec2 Pn = fxVec2MulFx(c->normal, dPn);

				b1->velocity = fxVec2Sub(b1->velocity, fxVec2MulFx(Pn, b1->invMass) );
				b1->angularVelocity -= fxMul(fxVec2Cross(c->r1, Pn), b1->invI);

				b1->velocity = fxVec2Add(b2->velocity, fxVec2MulFx(Pn, b2->invMass) );
				b2->angularVelocity += fxMul(fxVec2Cross(c->r2, Pn), b2->invI);

				// Relative velocity at contact
				dv = fxVec2Sub(
					fxVec2Sub(
						fxVec2Add(b2->velocity, fxVec2CrossFxInv(b2->angularVelocity, c->r2))
						, b1->velocity)
					, fxVec2CrossFxInv(b1->angularVelocity, c->r1));

				{
					struct fxVec2 tangent = fxVec2CrossFx(c->normal, FX32(1.0f));
					fx32 vt = fxVec2Dot(dv, tangent);
					fx32 dPt = fxMul(c->massTangent, (-vt));

					if (b2d_world_accumulate_impulses==TRUE)
					{
						// Compute friction impulse
						fx32 maxPt = fxMul(arbiter->friction, c->Pn);

						// Clamp friction
						fx32 oldTangentImpulse = c->Pt;
						c->Pt = fxClamp(oldTangentImpulse + dPt, -maxPt, maxPt);
						dPt = c->Pt - oldTangentImpulse;
					}
					else
					{
						fx32 maxPt = fxMul(arbiter->friction, dPn);
						dPt = fxClamp(dPt, -maxPt, maxPt);
					}

					{
						// Apply contact impulse
						struct fxVec2 Pt = fxVec2MulFx(tangent, dPt);

						b1->velocity = fxVec2Sub(b1->velocity, fxVec2MulFx(Pt, b1->invMass));
						b1->angularVelocity -= fxMul(fxVec2Cross(c->r1, Pt), b1->invI);

						b2->velocity = fxVec2Add(b2->velocity, fxVec2MulFx(Pt, b2->invMass));
						b2->angularVelocity += fxMul(b2->invI, fxVec2Cross(c->r2, Pt));
					}

				}
			}

		}
	}
};

