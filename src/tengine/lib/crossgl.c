/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#ifndef NITRO_SDK

#include "crossgl.h"

#if defined WINDOWS_APP || defined NIX_APP
	void APIENTRY glOrtho_x (GLfx32 left, GLfx32 right, GLfx32 bottom, GLfx32 top, GLfx32 zNear, GLfx32 zFar)
	{
		glOrtho(left, right, bottom, top, zNear, zFar);
	}
	void APIENTRY glClearColor_x(GLclampfx32 red, GLclampfx32 green, GLclampfx32 blue, GLclampfx32 alpha)
	{
		glClearColor(red, green, blue, alpha);
	}
	void APIENTRY glColor4_x(GLfx32 red, GLfx32 green, GLfx32 blue, GLfx32 alpha)
	{
		glColor4f(red, green, blue, alpha);
	}
	void APIENTRY glFrustum_x(GLfx32 left, GLfx32 right, GLfx32 bottom, GLfx32 top, GLfx32 zNear, GLfx32 zFar)
	{
		glFrustum(left, right, bottom, top, zNear, zFar);
	}
	void APIENTRY glLight_xv(GLenum light, GLenum pname, const GLfx32 *params)
	{
		glLightfv(light, pname, params);
	}
	void APIENTRY glMaterial_xv(GLenum face, GLenum pname, const GLfx32 *params)
	{
		glMaterialfv(face, pname, params);
	}
	void APIENTRY glMaterial_x(GLenum face, GLenum pname, GLfx32 param)
	{
		glMaterialf(face, pname, param);
	}
	void APIENTRY glScale_x(GLfx32 x, GLfx32 y, GLfx32 z)
	{
		glScalef(x, y, z);
	}
	void APIENTRY glTranslate_x(GLfx32 x, GLfx32 y, GLfx32 z)
	{
		glTranslatef(x, y, z);
	}
	void APIENTRY glRotate_x(GLfx32 angle, GLfx32 x, GLfx32 y, GLfx32 z)
	{
		glRotatef(angle, x, y, z);
	}
	void APIENTRY glMultMatrix_x(const GLfx32 *m)
	{
		glMultMatrixf(m);
	}
	void APIENTRY glPointSize_x(GLfx32 size)
	{
		glPointSize(size);
	}
	void APIENTRY glLineWidth_x (GLfx32 width)
	{
		glLineWidth(width);
	}
#endif
#endif

