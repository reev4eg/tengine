/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include "tengine.h"
#include "filesystem.h"
#include "loadhelpers.h"
#include "lib/tengine_low.h"
#include "lib/loadtdata.h"
#include "lib/jobs_low.h"
#include "lib/sound_low.h"
#include "texts.h"
#include "jobs.h"
#ifdef JOBS_IN_SEPARATE_THREAD
#include "pthread.h"
#endif

#ifdef USE_CUSTOM_RENDER
	#include "easygraphics.h"
#endif 

#ifdef USE_OPENGL_RENDER
	#include "render2dgl.h"
#endif 

//----------------------------------------------------------------------------------------------------------------

static const u16 DEFAULT_DELAY = 30;
static const s32 MINIMAL_TICK_TIME = 3;

static const s32 MAX_INT = 2147483647;

static void (*_updateEngineObjFn[UEOF_COUNT])(struct TEngineInstance* ei, u32 layer, s32 ms);

s32 fakeTextFrData[ppranifrdataCOUNT] = {0};

//-------------------------------------------------------------------------------------------

struct TEngineCommonData cd;

static BOOL lost_device = FALSE;
static BOOL lost_device_is_jobs_active = FALSE;

static void _initInstance(struct TEngineInstance *i);
static void _draw(const u16 *frpcdt, struct DrawImageFunctionData *fd);
static inline void _forceDrawBackgroundTiles(struct TEngineInstance * i, const fx32 c_dx8, const fx32 c_dy8, s32 active_gr_bg);
static s32 _findTextureOffset(const struct TERFont* ipFont, wchar iChar, u16 arr[SRC_DATA_SIZE]);
static void _drawText(struct TEngineInstance *ei, s32 pr_id);
static BOOL _txbSetDynamicText(struct TEngineInstance *a, s32 pr_id, const wchar *text);
static BOOL _txbSetDynamicTextLine(struct TEngineInstance *a, s32 pr_id, s32 line_idx, const wchar *text);
static void _drawBackgroundTiles(BOOL iRedrawBackgroundEvenIfNoScrolling);
static void _updateParticles(BOOL draw, s32 ms);
static void _drawGameObj(s32 pr_id);
static void _drawObjectsDummy(struct TEngineInstance* ei, u32 layer, s32 ms);
static void _drawObjectsWithoutUpdate(struct TEngineInstance* ei, u32 layer, s32 ms);
static void _drawObjectsWithUpdate(struct TEngineInstance* ei, u32 layer, s32 ms);

//-------------------------------------------------------------------------------------------

void initEngineParametersWithDefaultValues(struct InitEngineParameters *const params)
{
	SDK_NULL_ASSERT(params);
	params->layersCount = 1;
	params->particlesPoolSize = PCS_DEF_POOL_SIZE;
}
//-------------------------------------------------------------------------------------------

void initEngine(const struct InitEngineParameters *params)
{
	u32 i;
	u32 j;
	
	SDK_ASSERT(cd.instances == NULL);

	_loadDataInit();
	MI_CpuFill8(&cd, 0, sizeof(struct TEngineCommonData));

	if(cd.instances == NULL)
	{
		MI_CpuFill8(&cd, 0, sizeof(struct TEngineCommonData));
		cd.delay = DEFAULT_DELAY;
		cd.a_layer = NONE_MAP_IDX;
		cd.spd_prp_id = NONE_MAP_IDX;
		cd.enable_prp_id = NONE_MAP_IDX;
	}

	cd.initParams = *params;
	OS_Printf("initEngine with layers = %d\n", cd.initParams.layersCount);

	SDK_ASSERT(cd.initParams.layersCount > 0); // instanceCount == 0!

	mthInitRandom();

	MI_CpuFill8(fakeTextFrData, 0, ppranifrdataCOUNT * sizeof(s32));
	
	cd.ld_task = (s16*)MALLOC((cd.initParams.layersCount + 2) * sizeof(s16), "initEngine:ld_task");
	cd.ld_p1 = (u32*)MALLOC((cd.initParams.layersCount + 2) * sizeof(u32), "initEngine:ld_p1");
	cd.ld_p2 = (u16*)MALLOC((cd.initParams.layersCount + 2) * sizeof(u16), "initEngine:ld_p2");

	cd.instances = (struct TEngineInstance **)MALLOC(cd.initParams.layersCount * sizeof(struct TEngineInstance**), "initEngine:cd.instances");
	for (j = 0; j < cd.initParams.layersCount; j++)
	{
		cd.instances[j] = (struct TEngineInstance*)MALLOC(sizeof(struct TEngineInstance), "initEngine:cd.instances[j]");
		_initInstance(cd.instances[j]);
		setVisible(j, TRUE);
		if(cd.initParams.particlesPoolSize > 0)
		{
			if (cd.instances[j]->pcs_data == NULL)
			{
				cd.instances[j]->pcs_data = (s32**)MALLOC(cd.initParams.particlesPoolSize * sizeof(s32**), "initEngine:cd.instances[j]->pcs_data");
				for (i = 0; i < cd.initParams.particlesPoolSize; i++)
				{
					cd.instances[j]->pcs_data[i] = (s32*)MALLOC(PCS_COUNT * sizeof(s32), "cd.instances[j]->pcs_data[i]");
				}
			}
		}
	}

	_updateEngineObjFn[UEOF_EMPTY] = _drawObjectsDummy;
	_updateEngineObjFn[UEOF_READY] = _drawObjectsWithUpdate;
	_updateEngineObjFn[UEOF_SKIP_FRAME_1] = _drawObjectsWithoutUpdate;
	_updateEngineObjFn[UEOF_SKIP_FRAME_2] = _drawObjectsWithUpdate;

	cd.ainst = NULL;
	cd.a_layer = NONE_MAP_IDX;
	jobInit();
}
//-------------------------------------------------------------------------------------------

void releaseEngine()
{
	u32 i;
	lost_device_is_jobs_active = FALSE;
	jobRelease();
	if (cd.instances != NULL)
	{
		_freeRes(&cd);
		i = cd.initParams.layersCount; 
		while(i > 0)
		{
			i--;
			releaseMapData(i);
			_releaseTrigData(cd.instances[i]);
			_releaseScriptData(cd.instances[i]);
		}
	}
#ifdef USE_CUSTOM_RENDER
	RENDERFN(GraphicsRelease)();
#endif	
	if (cd.instances != NULL)
	{
		i = cd.initParams.layersCount;
		while(i > 0)
		{
			i--;
			if (cd.instances[i]->pcs_data != NULL)
			{
				s32 j = cd.initParams.particlesPoolSize; 
				while(j > 0)
				{
					j--;
					FREE(cd.instances[i]->pcs_data[j]);
					cd.instances[i]->pcs_data[j] = NULL;
				}
				FREE(cd.instances[i]->pcs_data);
				cd.instances[i]->pcs_data = NULL;
			}
			FREE(cd.instances[i]);
		}
		FREE(cd.instances);
		cd.instances = NULL;
		cd.initParams.layersCount = 0;
	}
	if(cd.ld_task != NULL)
	{
		FREE(cd.ld_p2);
		FREE(cd.ld_p1);
		FREE(cd.ld_task);
	}
	cd.ainst = NULL;
	MI_CpuFill8(&cd, 0, sizeof(struct TEngineCommonData));
	//OS_Printf("releaseEngine\n");
}
//-------------------------------------------------------------------------------------------

struct TEngineInstance *_getInstance(u32 idx)
{
	SDK_ASSERT(idx <= cd.initParams.layersCount);
	SDK_ASSERT(cd.instances);
	SDK_ASSERT(cd.instances[idx]);
	return cd.instances[idx];
}
//-------------------------------------------------------------------------------------------

void initRenderPlaneParametersWithDefaultValues(struct RenderPlaneInitParams *const params)
{
	SDK_NULL_ASSERT(params);
	params->mSizes.mViewHeight = 512;
	params->mSizes.mViewWidth = 512;
#if defined USE_CUSTOM_RENDER || defined NITRO_SDK
#ifdef NITRO_SDK
	params->mSizes.mFrameBufferWidth8 = 512;	 // buffer must be multiple of 8
	params->mSizes.mFrameBufferHeight8 = 512;
#else
	params->mFrameBufferWidth2n = 512; // buffer must be multiple 2^n
	params->mFrameBufferHeight2n = 512;
#endif
#endif
	params->mBGType = BGSELECT_SUB2;
#ifdef USE_OPENGL_RENDER
	params->mMaxRenderObjectsOnPlane = 4096;
#endif
#if defined USE_CUSTOM_RENDER || defined NITRO_SDK
	params->mColorType = BMP_TYPE_DC16;
#ifdef NITRO_SDK
	params->mBGPriority = 0;
	params->mScreenBase = GX_BG_BMPSCRBASE_0x00000;
#endif
#endif
	params->mX = 0;
	params->mY = 0;
}
//-------------------------------------------------------------------------------------------

void initRenderPlane(const struct RenderPlaneInitParams* p)
{
	SDK_ASSERT(cd.instances);
	SDK_ASSERT(p);
	SDK_ASSERT(p->mSizes.mViewWidth < DISPLAY_VIEW_W_MAX && p->mSizes.mViewHeight < DISPLAY_VIEW_H_MAX);
	RENDERFN(PlaneInit)(p);
}
//-------------------------------------------------------------------------------------------

void setRenderPlaneScale(fx32 val, u32 layer)
{
	SDK_ASSERT(cd.instances);
	SDK_ASSERT(RENDERFN(IsGraphicsInit)()); // please init graphics system before
	RENDERFN(SetRenderPlaneScale)(val, _getInstance(layer)->bgType);
}
//-------------------------------------------------------------------------------------------

fx32 getRenderPlaneScale(u32 layer)
{
	SDK_ASSERT(cd.instances);
	SDK_ASSERT(RENDERFN(IsGraphicsInit)()); // please init graphics system before
	return RENDERFN(GetRenderPlaneScale)(_getInstance(layer)->bgType);
}
//-------------------------------------------------------------------------------------------

BOOL isRenderPlaneInit(u32 layer)
{
	if(cd.instances)
	{
		return RENDERFN(IsRenderPlaneInit)(_getInstance(layer)->bgType);
	}
	return FALSE;
}
//-------------------------------------------------------------------------------------------

void resizeRenderPlane(u32 layer, const struct RenderPlaneSizeParams* p)
{
	u32 i;
	struct TEngineInstance *ei;
	SDK_ASSERT(isRenderPlaneInit(layer)); // please init graphics system before
	ei = _getInstance(layer);
	RENDERFN(PlaneResize)(ei->bgType, p);	
	ei->tsdy8_fx[ei->bgType] = ei->tsdx8_fx[ei->bgType] = -FX32_ONE;
	//reload all res because of FRAME_ALLOCATOR
	i = cd.initParams.layersCount; 
	while(i > 0)
	{
		i--;
		ei = cd.instances[i];
		if(ei->ueof_state != UEOF_EMPTY)
		{
			BOOL ja = jobIsActive();
			_freeRes(&cd);
			cd.ag_st = TRUE;
			jobSetActive(ja);
			return;
		}
	}
}
//-------------------------------------------------------------------------------------------

void releaseRenderPlane(enum BGSelect iType)
{
	SDK_ASSERT(RENDERFN(IsGraphicsInit)()); // please init graphics system before
	RENDERFN(PlaneRelease)(iType);
}
//-------------------------------------------------------------------------------------------

BOOL assignLayerWithRenderPlane(u32 layer, enum BGSelect iBGType, BOOL primaryLayer)
{
	struct TEngineInstance *ei;
	SDK_ASSERT(RENDERFN(IsGraphicsInit)()); // please init graphics system before
	ei = _getInstance(layer);
	SDK_ASSERT(ei->ueof_state == UEOF_EMPTY); // please call this function before resources loading
	if(ei->ueof_state != UEOF_EMPTY)
	{
		return FALSE;
	}
	ei->bgType = iBGType;
	ei->primary = FALSE;
	if(primaryLayer)
	{
		u32 j;
		for (j = 0; j < cd.initParams.layersCount; j++)
		{
			struct TEngineInstance *tei = _getInstance(j);
			if(tei->primary && tei->bgType == iBGType)
			{
				tei->primary = FALSE;	
			}
		}
		ei->primary = TRUE;
	}
	return TRUE;
}
//-------------------------------------------------------------------------------------------

static void _initInstance(struct TEngineInstance *i)
{
	s32 j;
	MI_CpuFill8(i, 0, sizeof(struct TEngineInstance));
	i->t_current_zone = i->current_zone = -1;
	i->bgType = BGSELECT_NUM;
	for (j = 0; j < BGSELECT_NUM; j++)
	{
		i->tsdy8_fx[j] = -FX32_ONE;
		i->tsdx8_fx[j] = -FX32_ONE;
		i->ds_plane_fr_array_beg[j] = -1;
		i->ds_plane_fr_array_end[j] = -1;
	}
}
//-------------------------------------------------------------------------------------------

void _setActiveLayer(u32 layer)
{
	cd.ainst = _getInstance(layer);
	cd.a_layer = layer; 
}
//-------------------------------------------------------------------------------------------

u32 getActiveLayer(void)
{
	return cd.a_layer;	
}
//-------------------------------------------------------------------------------------------

void setVisible(u32 layer, BOOL val)
{
	_getInstance(layer)->visible_mask = val ? 0xf : 0;
	RENDERFN(ClearFrameBuffer(_getInstance(layer)->bgType));
	cd.draw = TRUE;
}
//-------------------------------------------------------------------------------------------

void setBackgroundTileLayersForDS(u32 layer, enum BGSelect iBGType, s32 iStartIdx, s32 iEndIdx)
{
	SDK_ASSERT(iBGType < BGSELECT_NUM);
	_getInstance(layer)->ds_plane_fr_array_beg[iBGType] = iStartIdx;
	_getInstance(layer)->ds_plane_fr_array_end[iBGType] = iEndIdx;
}
//-------------------------------------------------------------------------------------------

u16 getTickDelay(void)
{
	return cd.delay;
}
//-------------------------------------------------------------------------------------------

s32 getHeightBGTile()
{
	return cd.p_h;
}
//-------------------------------------------------------------------------------------------

s32 getWidthBGTile()
{
	return  cd.p_w;
}
//-------------------------------------------------------------------------------------------

s32 getViewWidth(u32 layer)
{
	SDK_NULL_ASSERT(cd.ainst);
	return RENDERFN(GetViewWidth)(_getInstance(layer)->bgType);
}
//-------------------------------------------------------------------------------------------

s32 getViewHeight(u32 layer)
{
	SDK_NULL_ASSERT(cd.ainst);
	return RENDERFN(GetViewHeight)(_getInstance(layer)->bgType);
}
//-------------------------------------------------------------------------------------------

s32 getViewLeft(u32 layer)
{
	SDK_NULL_ASSERT(cd.ainst);
	return RENDERFN(GetViewLeft)(_getInstance(layer)->bgType);
}
//-------------------------------------------------------------------------------------------

s32 getViewTop(u32 layer)
{
	SDK_NULL_ASSERT(cd.ainst);
	return RENDERFN(GetViewTop)(_getInstance(layer)->bgType);
}
//-------------------------------------------------------------------------------------------

s32 getMapWidth()
{
	return cd.ainst->sz_mapW;
}
//-------------------------------------------------------------------------------------------

s32 getMapHeight()
{
	return cd.ainst->sz_mapH;
}
//-------------------------------------------------------------------------------------------

void setOnUpdateDecorationObject(OnUpdateObject pCallbackFn)
{
	cd._onUpdateDecorationObject = pCallbackFn;
}
//-------------------------------------------------------------------------------------------

void setOnUpdateGameObject(OnUpdateObject pCallbackFn)
{
	cd._onUpdateGameObject = pCallbackFn;
}
//-------------------------------------------------------------------------------------------

void setOnUpdateBackgroundTiles(OnUpdateBackgroundTiles pCallbackFn)
{
	cd._onDrawBackgroundTiles = pCallbackFn;
}
//-------------------------------------------------------------------------------------------

void setOnFinishUpdate(OnFinishUpdate pCallbackFn)
{
	cd._onFinishUpdate = pCallbackFn;
}
//-------------------------------------------------------------------------------------------

void setOnEvent(OnEventCallback pCallbackFn)
{
	cd._onEventCB = pCallbackFn;
}
//-------------------------------------------------------------------------------------------

void setOnScript(u32 layer, OnScriptCallback pCallbackFn)
{
	_getInstance(layer)->_onScriptCB = pCallbackFn;
}
//-------------------------------------------------------------------------------------------

void setOnChangeHero(u32 layer, OnChangeHeroCallback pCallbackFn)
{
	_getInstance(layer)->_onChangeHeroCB = pCallbackFn;
}
//-------------------------------------------------------------------------------------------

void _calculateCXY8(struct TEngineInstance *ei, fx32 *cx, fx32 *cy, fx32 *c_dx8, fx32 *c_dy8)
{
	*c_dx8 = (ei->logicWidth / 2) << FX32_SHIFT;
	*c_dy8 = (ei->logicHeight / 2) << FX32_SHIFT;
	if (*cx + *c_dx8 > (ei->sz_mapW << FX32_SHIFT))
	{
		*cx = (ei->sz_mapW << FX32_SHIFT) - *c_dx8;
	}
	else if (*cx - *c_dx8 < 0)
	{
		*cx = *c_dx8 = (((*c_dx8 >> FX32_SHIFT) / cd.p_w) * cd.p_w) << FX32_SHIFT;
	}
	if (*cy + *c_dy8 > (ei->sz_mapH << FX32_SHIFT))
	{
		*cy = (ei->sz_mapH << FX32_SHIFT) - *c_dy8;
	}
	else if (*cy - *c_dy8 < 0)
	{
		*cy = *c_dy8 = (((*c_dy8 >> FX32_SHIFT) / cd.p_h) * cd.p_h) << FX32_SHIFT;
	}
	*c_dx8 = (((*cx - *c_dx8) >> FX32_SHIFT) / cd.p_w) << FX32_SHIFT;
	*c_dy8 = (((*cy - *c_dy8) >> FX32_SHIFT) / cd.p_h) << FX32_SHIFT;
	ei->sdx_fx = *cx % (cd.p_w << FX32_SHIFT);
	ei->sdy_fx = *cy % (cd.p_h << FX32_SHIFT);
	ei->dpx_fx = *c_dx8 * cd.p_w;
	ei->dpy_fx = *c_dy8 * cd.p_h;
}
//-------------------------------------------------------------------------------------------

static inline void _forceDrawBackgroundTiles(struct TEngineInstance *ei, const fx32 c_dx_8, const fx32 c_dy_8, s32 abg)
{
	s32 a;
	u16 *m_c, *m_e, *ml_e;
	const u16 *o;
	struct DrawImageFunctionData fd;
	m_c = ei->map + (c_dy_8 >> FX32_SHIFT) * ei->map_w + (c_dx_8 >> FX32_SHIFT);
	m_e = m_c + ei->d_h * ei->map_w;
	MI_CpuClear8(&fd, sizeof(struct DrawImageFunctionData));
	fd.mY = -ei->sdy_fx;
	//OS_Printf("refresh all\n");
	//ClearFrameBuffer();
	fd.mSrcSizeData[SRC_DATA_W] = cd.p_w;
	fd.mSrcSizeData[SRC_DATA_H] = cd.p_h;
	fd.mAlpha = ALPHA_OPAQ;
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
	fd.mScaleX = fd.mScaleY = FX32_ONE;
#endif
	while (m_c <= m_e)
	{
		fd.mX = (ei->logicWidth << FX32_SHIFT) - ei->sdx_fx;
		ml_e = m_c + ei->d_w;
		while (m_c <= ml_e)
		{
			if (*ml_e < ei->pnt_count)
			{
				for (a = ei->ds_plane_fr_array_beg[abg];
						a < ei->ds_plane_fr_array_end[abg]; ++a)
				{
					SDK_NULL_ASSERT(ei->fr_img[*ml_e][a]);
					o = ei->fr_img[*ml_e][a];
					if(o[RES_IMG_IDX] < NULL_IMG_IDX)
					{
						fd.mType = (enum DrawImageType)a;
						fd.mpSrcData = cd.res_img[o[RES_IMG_IDX]];
						MI_CpuCopy16(&o[RES_X], &fd.mSrcSizeData[SRC_DATA_X], sizeof(u16) * 2);
						RENDERFN(DrawImage)(&fd);
					}
				}
			}
			--ml_e;
			fd.mX -=  cd.p_w << FX32_SHIFT;
		}
		m_c += ei->map_w;
		fd.mY += cd.p_h << FX32_SHIFT;
	}
}
//-------------------------------------------------------------------------------------------

void setCamera(struct fxVec2 pos)
{
	SDK_ASSERT(cd.ainst);
	SDK_ASSERT(cd.ainst->bgType != BGSELECT_NUM);
	cd.ainst->tcx_fx[cd.ainst->bgType] = cd.ainst->tsdx8_fx[cd.ainst->bgType];
	cd.ainst->tcy_fx[cd.ainst->bgType] = cd.ainst->tsdy8_fx[cd.ainst->bgType];
	_calculateCXY8(cd.ainst, &cd.ainst->tcx_fx[cd.ainst->bgType], &cd.ainst->tcy_fx[cd.ainst->bgType], &cd.ainst->tc_dx8_fx[cd.ainst->bgType], &cd.ainst->tc_dy8_fx[cd.ainst->bgType]);
	cd.ainst->tsdx0_fx[cd.ainst->bgType] = cd.ainst->sdx_fx;
	cd.ainst->tsdy0_fx[cd.ainst->bgType] = cd.ainst->sdy_fx;
	cd.ainst->camerax = pos.x;
	cd.ainst->tcx_fx[cd.ainst->bgType] = pos.x;
	cd.ainst->cameray = pos.y;
	cd.ainst->tcy_fx[cd.ainst->bgType] = pos.y;
	_calculateCXY8(cd.ainst, &cd.ainst->tcx_fx[cd.ainst->bgType], &cd.ainst->tcy_fx[cd.ainst->bgType], &cd.ainst->tc_dx8_fx[cd.ainst->bgType], &cd.ainst->tc_dy8_fx[cd.ainst->bgType]);
}
//-------------------------------------------------------------------------------------------

struct fxVec2 getCameraMinPosition()
{
	struct fxVec2 pos;
	SDK_ASSERT(cd.ainst);
	pos.x = (cd.ainst->logicWidth / 2) << FX32_SHIFT;
	pos.y = (cd.ainst->logicHeight / 2) << FX32_SHIFT;
	if((cd.ainst->sz_mapW << FX32_SHIFT) <= pos.x)
	{
		pos.x = 0;
	}
	if((cd.ainst->sz_mapH << FX32_SHIFT) <= pos.y)
	{
		pos.y = 0;
	}
	return pos;
}
//-------------------------------------------------------------------------------------------

struct fxVec2 getCameraMaxPosition()
{
	struct fxVec2 pos;
	SDK_ASSERT(cd.ainst);
	pos.x = (cd.ainst->sz_mapW - cd.ainst->logicWidth / 2) << FX32_SHIFT;
	pos.y = (cd.ainst->sz_mapH - cd.ainst->logicHeight / 2) << FX32_SHIFT;
	if(pos.x < 0)
	{
		pos.x = 0;
	}
	if(pos.y < 0)
	{
		pos.y = 0;
	}
	return pos;
}
//-------------------------------------------------------------------------------------------

struct fxVec2 getCamera()
{
	struct fxVec2 pos;
	SDK_ASSERT(cd.ainst);
	pos.x = cd.ainst->camerax;
	pos.y = cd.ainst->cameray;
	return pos;
}
//-------------------------------------------------------------------------------------------

void _drawBackgroundTiles(BOOL forceRedraw)
{
	fx32 c_dx8, c_dy8;
#ifdef USE_CUSTOM_RENDER
	s32 cx, cy, j, c_dx, c_dy, k, sdx0, sdy0; 
	u16 *m_c, *m_e, *ml_e;
	const u16 *o;
#endif

	struct TEngineInstance *ei = cd.ainst;

	SDK_ASSERT(ei);
	SDK_ASSERT(ei->bgType != BGSELECT_NUM);

	//if(!ei->visible || !ei->primary)
	//{
	//	return;
	//}

	RENDERFN(SetActiveBGForGraphics)(ei->bgType);
#ifdef USE_CUSTOM_RENDER
	sdx0 = ei->tsdx0[ei->bgType];
	sdy0 = ei->tsdy0[ei->bgType];
	cx = ei->tcx[ei->bgType];
	cy = ei->tcy[ei->bgType]; 
#endif
	c_dx8 = ei->tc_dx8_fx[ei->bgType]; 
	c_dy8 = ei->tc_dy8_fx[ei->bgType]; 

#ifdef USE_OPENGL_RENDER
	(void)forceRedraw;
	_forceDrawBackgroundTiles(ei, c_dx8, c_dy8, ei->bgType);
#endif

#ifdef USE_CUSTOM_RENDER
// ��� ���� ������� ������ ����� ���������� ���������, ��� ��� � ����� ������ easygraphics ����� ��������� �������� ��������,
// ��� ������ ��� ���� ��� ����������� �����, ��� ���������� ������ ������� ������, ���������� ������� �� �������,
// � ������� ������ ��������������, ��� ���������� ������ ����
	if (cx != ei->tsdx8[ei->bgType] || cy != ei->tsdy8[ei->bgType])
	{
		if (ei->tsdx8[ei->bgType] < 0 && ei->tsdy8[ei->bgType] < 0)
		{
			ei->tsdy8[ei->bgType] = cy;
			ei->tsdx8[ei->bgType] = cx;
			_forceDrawBackgroundTiles(ei, c_dx8, c_dy8, ei->bgType);
		}
		else
		{
			c_dx = ei->tsdx8[ei->bgType] - cx;
			c_dy = ei->tsdy8[ei->bgType] - cy;
			ei->tsdy8[ei->bgType] = cy;
			ei->tsdx8[ei->bgType] = cx;

			if (MATH_ABS(c_dx) > ei->shiftTilesMax || MATH_ABS(c_dy) > ei->shiftTilesMax)
			{
				refreshAllScreen();
				_forceDrawBackgroundTiles(ei, c_dx8, c_dy8, ei->bgType);
			}
			else
			{
				RENDERFN(RestoreGraphicsFromMemory)(-c_dx, -c_dy);
				if (c_dx < 0) // left <
				{
					m_c = ei->map + c_dy8 * ei->map_w + c_dx8 + c_dx / cd.p_w - 1 + ei->d_w;
					cy = - ei->sdy;
					m_e = m_c + (ei->d_h + (c_dy == 0 ? 1 : 0)) * ei->map_w;
					while (m_c < m_e)
					{
						k = cd.p_w;
						cx = ei->logicWidth - ei->sdx;
						ml_e = m_c - c_dx / cd.p_w + 1;
						while (m_c <= ml_e)
						{
							if (*ml_e < ei->pnt_count)
							{
								for (j = ei->ds_plane_fr_array_beg[ei->bgType];
										j < ei->ds_plane_fr_array_end[ei->bgType]; ++j)
								{
									SDK_NULL_ASSERT(ei->fr_img[*ml_e][j]);
									o = ei->fr_img[*ml_e][j];
									if(o[RES_IMG_IDX] < NULL_IMG_IDX)
									{
										RENDERFN(DrawImage)(
											(DrawImageType)j,
											cx, cy,
											cd.res_img[o[RES_IMG_IDX]],
											o[RES_X] + cd.p_w - k, o[RES_Y], k, cd.p_h,
											ALPHA_OPAQ, DT_NONE);
									}
								}
							}
							--ml_e;
							cx -= cd.p_w;
							if (m_c == ml_e)
							{
								k = cd.p_w - sdx0;
								cx += sdx0;
							}
						}
						m_c += ei->map_w;
						cy += cd.p_h;
					}
				}
				else if (c_dx > 0) // right >
				{
					m_c = ei->map + c_dy8 * ei->map_w + c_dx8;
					cy = /*ei->viewTop*/ - ei->sdy;
					m_e = m_c + (ei->d_h + (c_dy == 0 ? 1 : 0)) * ei->map_w;
					while (m_c < m_e)
					{
						k = sdx0;
						cx = c_dx /  cd.p_w + 1;
						ml_e = m_c + cx;
						cx = /*ei->viewLeft +*/ cx *  cd.p_w - ei->sdx;
						while (m_c <= ml_e)
						{
							if (*ml_e < ei->pnt_count)
							{
								for (j = ei->ds_plane_fr_array_beg[ei->bgType];
										j < ei->ds_plane_fr_array_end[ei->bgType]; ++j)
								{
									SDK_NULL_ASSERT(ei->fr_img[*ml_e][j]);
									o = ei->fr_img[*ml_e][j];
									if(o[RES_IMG_IDX] < NULL_IMG_IDX)
									{
										RENDERFN(DrawImage)(
											(DrawImageType)j,
											cx, cy,
											cd.res_img[o[RES_IMG_IDX]],
											o[RES_X], o[RES_Y], k, cd.p_h,
											ALPHA_OPAQ, DT_NONE);
									}
								}
							}
							--ml_e;
							cx -=  cd.p_w;
							if (k == sdx0)
							{
								k = cd.p_w;
							}
						}
						m_c += ei->map_w;
						cy += cd.p_h;
					}
				}

				if (c_dy > 0) // down
				{
					m_c = ei->map + c_dy8 * ei->map_w + c_dx8;
					cy = /*ei->viewTop*/ - ei->sdy;
					m_e = m_c + (c_dy / cd.p_h + 2) * ei->map_w;
					k = cd.p_h;
					while (m_c < m_e)
					{
						cx = /*ei->viewLeft +*/ ei->logicWidth - ei->sdx;
						ml_e = m_c + ei->d_w; // (c_dx == 0 ? 1 : 0);
						while (m_c <= ml_e)
						{
							if (*ml_e < ei->pnt_count)
							{
								for (j = ei->ds_plane_fr_array_beg[ei->bgType];
										j < ei->ds_plane_fr_array_end[ei->bgType]; ++j)
								{
									SDK_NULL_ASSERT(ei->fr_img[*ml_e][j]);
									o = ei->fr_img[*ml_e][j];
									if(o[RES_IMG_IDX] < NULL_IMG_IDX)
									{
										RENDERFN(DrawImage)(
											(DrawImageType)j,
											cx, cy,
											cd.res_img[o[RES_IMG_IDX]],
											o[RES_X], o[RES_Y],  cd.p_w, k,
											ALPHA_OPAQ, DT_NONE);
									}
								}
							}
							--ml_e;
							cx -=  cd.p_w;
						}
						m_c += ei->map_w;
						cy += cd.p_h;
						if (m_c + ei->map_w >= m_e)
						{
							k = sdy0;
						}
					}

				}
				else if (c_dy < 0) // up
				{
					j = c_dy / cd.p_h - 1;
					m_c = ei->map + (c_dy8 + ei->d_h + j) * ei->map_w + c_dx8;
					cy = /*ei->viewTop +*/ ei->d_h * cd.p_h + j * cd.p_h - ei->sdy;
					m_e = m_c - (c_dy / cd.p_h - 2) * ei->map_w;
					cy += sdy0;
					k = cd.p_h - sdy0;
					while (m_c < m_e)
					{
						cx = /*ei->viewLeft +*/ ei->logicWidth - ei->sdx;
						ml_e = m_c + ei->d_w; // (c_dx == 0 ? 1 : 0);
						while (m_c <= ml_e)
						{
							if (*ml_e < ei->pnt_count)
							{
								for (j = ei->ds_plane_fr_array_beg[ei->bgType];
										j < ei->ds_plane_fr_array_end[ei->bgType]; ++j)
								{
									SDK_NULL_ASSERT(ei->fr_img[*ml_e][j]);
									o = ei->fr_img[*ml_e][j];
									if(o[RES_IMG_IDX] < NULL_IMG_IDX)
									{
										RENDERFN(DrawImage)(
											(DrawImageType)j,
											cx, cy,
											cd.res_img[o[RES_IMG_IDX]],
											o[RES_X], o[RES_Y] + cd.p_h - k,  cd.p_w, k,
											ALPHA_OPAQ, DT_NONE);
									}
								}
							}
							--ml_e;
							cx -=  cd.p_w;
						}
						m_c += ei->map_w;
						cy += k;
						k = cd.p_h;
					}
				}
			}
		}
		RENDERFN(SaveGraphicsToMemory)();
	}
	else
	{
		if (forceRedraw)
		{
			RENDERFN(RestoreGraphicsFromMemory)(0, 0);
		}
	}
	// SetFrameBufferOffset(ei->sdx - + ei->viewLeft, ei->sdy - + ei->viewTop);
	/* while(ch_buff > 0)
						{
						--ch_buff;
						m_off = ch_buff_pos[ch_buff];
						m_block = ch_buff_idx[ch_buff];
						yf = m_off / ei->map_w;
						c_dy = (c_dy8 << BITS_TILE_SHIFT) / cd.p_h;
						c_dx = (c_dx8 << BITS_TILE_SHIFT) / cd.p_w;
						c_dy8 = (yf - c_dy) * cd.p_h + ei->viewTop;
						c_dx8 = (m_off - yf * ei->map_w - c_dx) * cd.p_w + ei->viewLeft;
						//bg.fillRect(ch_buff_x, ch_buff_y, cd.p_w, cd.p_h);
						m_block = 36;
						if(m_block < pnt_count)
						for(j = 0; j < tl_layers_ct[m_block]; ++j)
						if(fr_img[m_block][j] != NULL)
						{
						o = fr_img[m_block][j];
						DrawImage(c_dx8, c_dy8,
						res_img[o[RES_IMG_IDX]],
						o[RES_X], o[RES_Y], cd.p_w, cd.p_h);
						}
						SaveGraphicsToMemory();
						} */
#endif
}
//-------------------------------------------------------------------------------------------

fx32 getViewOffsetX(u32 layer)
{
	struct TEngineInstance *l = _getInstance(layer);
	return l->dpx_fx + l->sdx_fx;
}
//-------------------------------------------------------------------------------------------

fx32 getViewOffsetY(u32 layer)
{
	struct TEngineInstance *l = _getInstance(layer);
	return l->dpy_fx + l->sdy_fx;
}
//-------------------------------------------------------------------------------------------

void refreshAllScreen()
{
	enum BGSelect active_gr_bg = RENDERFN(GetActiveBGForGraphics)();
	if (active_gr_bg != BGSELECT_NUM)
	{
		cd.ainst->tsdy8_fx[active_gr_bg] = cd.ainst->tsdx8_fx[active_gr_bg] = -1;
	}
}
//-------------------------------------------------------------------------------------------

void transferToVRAM()
{
#ifdef USE_CUSTOM_RENDER
	RENDERFN(TransferDataToVRAM)();
#endif
}
//-------------------------------------------------------------------------------------------
/*
void changeMapIndex(s32 *tileInfo, u16 idxChange, BOOL redrawTile)
{
	s32 pos;
	// SDK_NULL_ASSERT(tileInfo);
	SDK_NULL_ASSERT(cd.ainst->map);
	pos = tileInfo[POSITION];
	if (pos < cd.ainst->sz_map && pos >= 0 &&
		((idxChange < cd.ainst->pnt_count && idxChange >= 0)
			|| idxChange == NONE_MAP_IDX))
	{
		cd.ainst->map[pos] = idxChange;
		// ��������, ���� ��� fillrect
		refreshAllScreen();

		// if(redrawTile)
		//		{
		//		ch_buff_idx[ch_buff] = idxChange;
		//		ch_buff_pos[ch_buff] = pos;
		//		ch_buff++;
		//		if(ch_buff > CHANGE_MAP_BUFFER_MAX)
		//		refreshAllScreen();
		//		}
	}
}
*/
//-------------------------------------------------------------------------------------------

void lostRenderDevice(void)
{
	if(lost_device == FALSE)
	{
		RENDERFN(LostDevice)();
		lost_device_is_jobs_active = jobIsActive();
		jobSetActive(FALSE);
		lost_device = TRUE;
		_freeRes(&cd);
		if(sndIsSoundSystemInit())
		{
			sndLostDevice();
		}
	}
}
//-------------------------------------------------------------------------------------------

void restoreRenderDevice()
{
	if(lost_device)
	{
		if(sndIsSoundSystemInit())
		{
			sndRestoreDevice();
		}
		cd.ag_st = TRUE;
		lost_device = FALSE;
		jobSetActive(lost_device_is_jobs_active);
		RENDERFN(RestoreDevice)();
	}
}
//-------------------------------------------------------------------------------------------

u16 *createCopyTileMap(u32 layer)
{
	u16 *newMap;
	struct TEngineInstance *ei = _getInstance(layer);
	SDK_NULL_ASSERT(ei->map);
	newMap = (u16*)MALLOC(ei->map_w * ei->map_h * sizeof(u16), "createCopyTileMap:newMap");
	MI_CpuCopy16(ei->map, newMap, ei->map_w * ei->map_h * sizeof(u16));
	return newMap;
}
//-------------------------------------------------------------------------------------------

void setTileMap(u32 layer, u16 *ipTileMap)
{
	struct TEngineInstance *ei = _getInstance(layer);
	SDK_NULL_ASSERT(ipTileMap);
	ei->map = ipTileMap;
}
//-------------------------------------------------------------------------------------------

void setDefaultTileMap(u32 layer)
{
	struct TEngineInstance *ei = _getInstance(layer);
	ei->map = ei->t_map;
}
//-------------------------------------------------------------------------------------------
/*
s16 _getTransform(s16 data)
{
	return data & 0x7FFF; 	
}
//-------------------------------------------------------------------------------------------
*/
BOOL _isStreamFrame(s16 data)
{
	return data & 0x8000;
}
//-------------------------------------------------------------------------------------------

s32 getCurrentLanguageId(void)
{
	return cd.lang_id;
}
//-------------------------------------------------------------------------------------------

s32 getStaticTextLinesCount(s32 text_id)
{
	return cd.textdatatext_sct[text_id];
}
//-------------------------------------------------------------------------------------------

const wchar* getStaticText(s32 text_id, s32 line)
{
	return cd.textdatatext[text_id][line] + 1;
}
//-------------------------------------------------------------------------------------------

TextBoxType txbGetType(s32 pr_id)
{
	if(cd.ainst->all_pr_count > pr_id)
	{
		SDK_NULL_ASSERT(cd.ainst->pr_textdata[pr_id]); // wrong pr_id
		return (cd.ainst->pr_textdata[pr_id][TXTCHRCOUNT] == MAX_RES_IDX) ? TEXTBOX_TYPE_STATIC : TEXTBOX_TYPE_DYNAMIC;
	}
	return TEXTBOX_TYPE_ERROR;
}
//-------------------------------------------------------------------------------------------

TextBoxType txbGetTypeLayer(s32 pr_id, u32 layer)
{
	struct TEngineInstance *l = _getInstance(layer);
	if(l->all_pr_count > pr_id)
	{
		SDK_NULL_ASSERT(l->pr_textdata);
		return (l->pr_textdata[pr_id][TXTCHRCOUNT] == MAX_RES_IDX) ? TEXTBOX_TYPE_STATIC : TEXTBOX_TYPE_DYNAMIC;
	}
	return TEXTBOX_TYPE_ERROR;
}
//-------------------------------------------------------------------------------------------

s32 txbGetDynamicTextLinesCount(s32 pr_id)
{
	if(txbGetType(pr_id) == TEXTBOX_TYPE_DYNAMIC)
	{
		return cd.ainst->pr_textdata[pr_id][TXTSTRCOUNT];
	}
	return 0;
}
//-------------------------------------------------------------------------------------------

s32 txbGetDynamicTextLinesCountLayer(s32 pr_id, u32 layer)
{
	if(txbGetTypeLayer(pr_id, layer) == TEXTBOX_TYPE_DYNAMIC)
	{
		struct TEngineInstance *l = _getInstance(layer);
		return l->pr_textdata[pr_id][TXTSTRCOUNT];
	}
	return 0;
}
//-------------------------------------------------------------------------------------------

s32 txbGetDynamicTextMaximumLineCharsCount(s32 pr_id)
{
	if(txbGetType(pr_id) == TEXTBOX_TYPE_DYNAMIC)
	{
		return cd.ainst->pr_textdata[pr_id][TXTCHRCOUNT];
	}
	return 0;
}
//-------------------------------------------------------------------------------------------

s32 txbGetDynamicTextMaximumLineCharsCountLayer(s32 pr_id, u32 layer)
{
	if(txbGetTypeLayer(pr_id, layer) == TEXTBOX_TYPE_DYNAMIC)
	{
		struct TEngineInstance *l = _getInstance(layer);
		return l->pr_textdata[pr_id][TXTCHRCOUNT];
	}
	return 0;
}
//-------------------------------------------------------------------------------------------

BOOL _txbSetDynamicTextLine(struct TEngineInstance *a, s32 pr_id, s32 line_idx, const wchar *text)
{
	if(line_idx < a->pr_textdata[pr_id][TXTSTRCOUNT])
	{
		s32 cct, str_ct;
		SDK_NULL_ASSERT(text);
		str_ct = STD_WStrLen(text);
		cct = (str_ct > a->pr_textdata[pr_id][TXTCHRCOUNT]) ? a->pr_textdata[pr_id][TXTCHRCOUNT] : str_ct;
		MI_CpuCopy8(text, a->pr_textdatatext[pr_id][line_idx] + 1, cct * sizeof(wchar));
		a->pr_textdatatext[pr_id][line_idx][cct + 1] = L'\0';
		a->pr_textdatatext[pr_id][line_idx][0] = (wchar)cct;
		return TRUE;
	}
	return FALSE;
}
//-------------------------------------------------------------------------------------------

BOOL txbSetDynamicTextLine(s32 pr_id, s32 line_idx, const wchar *text)
{
	if(txbGetType(pr_id) == TEXTBOX_TYPE_DYNAMIC)
	{
		return _txbSetDynamicTextLine(cd.ainst, pr_id, line_idx, text);
	}
	return FALSE;
}
//-------------------------------------------------------------------------------------------

BOOL txbSetDynamicTextLineLayer(s32 pr_id, s32 line_idx, const wchar *text, u32 layer)
{
	if(txbGetTypeLayer(pr_id, layer) == TEXTBOX_TYPE_DYNAMIC)
	{
		struct TEngineInstance *l = _getInstance(layer);
		return _txbSetDynamicTextLine(l, pr_id, line_idx, text);
	}
	return FALSE;
}
//-------------------------------------------------------------------------------------------

BOOL txbSetDynamicText(s32 pr_id, const wchar *text)
{
	if(txbGetType(pr_id) == TEXTBOX_TYPE_DYNAMIC)
	{
		return _txbSetDynamicText(cd.ainst, pr_id, text);
	}
	return FALSE;
}
//-------------------------------------------------------------------------------------------

BOOL txbSetDynamicTextLayer(s32 pr_id, const wchar *text, u32 layer)
{
	if(txbGetTypeLayer(pr_id, layer) == TEXTBOX_TYPE_DYNAMIC)
	{
		struct TEngineInstance *l = _getInstance(layer);
		return _txbSetDynamicText(l, pr_id, text);
	}
	return FALSE;
}
//-------------------------------------------------------------------------------------------

const wchar* txbGetDynamicText(s32 pr_id, s32 line)
{
	if(txbGetType(pr_id) == TEXTBOX_TYPE_DYNAMIC)
	{
		return cd.ainst->pr_textdatatext[pr_id][line] + 1;
	}
	return NULL;
}
//-------------------------------------------------------------------------------------------

const wchar* txbGetDynamicTextLayer(s32 pr_id, s32 line, u32 layer)
{
	if(txbGetTypeLayer(pr_id, layer) == TEXTBOX_TYPE_DYNAMIC)
	{
		struct TEngineInstance *l = _getInstance(layer);
		return l->pr_textdatatext[pr_id][line] + 1;
	}
	return NULL;
}
//-------------------------------------------------------------------------------------------

BOOL _txbSetDynamicText(struct TEngineInstance *a, s32 pr_id, const wchar *text)
{
	s32 i, str_ct, c, beg, cct;
	SDK_NULL_ASSERT(text);
	beg = c = i = str_ct = 0;
	i = -1;
	do
	{
		i++;
		if(text[i] == L'\n' || text[i] == L'\0')
		{
			cct = (str_ct > a->pr_textdata[pr_id][TXTCHRCOUNT]) ? a->pr_textdata[pr_id][TXTCHRCOUNT] : str_ct;
			if(a->pr_textdatatext[pr_id] == NULL)
			{
				SDK_ASSERT(0); //please increase the number of lines for this textbox in MapEditor
				return FALSE;
			}
			MI_CpuCopy8(text + beg, a->pr_textdatatext[pr_id][c] + 1, cct * sizeof(wchar));
			a->pr_textdatatext[pr_id][c][cct + 1] = L'\0';
			a->pr_textdatatext[pr_id][c][0] = (wchar)cct;
			c++;
			beg += str_ct + 1;
			str_ct = 0;
			if(c == a->pr_textdata[pr_id][TXTSTRCOUNT])
			{
				break;
			}
		}
		else
		{
			if(text[i] == L'\r')
			{
				beg += 1;
			}
			else
			{
				str_ct++;
			}
		}
	}
	while(text[i] != 0);
	return TRUE;
}
//-------------------------------------------------------------------------------------------

s32 txbGetWidth(s32 pr_id)
{
	if(cd.ainst->all_pr_count > pr_id)
	{
		return cd.ainst->pr_textdata[pr_id][TXTWIDTH];
	}
	return 0;
}
//-------------------------------------------------------------------------------------------

s32 txbGetWidthLayer(s32 pr_id, u32 layer)
{
	struct TEngineInstance *l = _getInstance(layer);
	if(l->all_pr_count > pr_id)
	{
		return l->pr_textdata[pr_id][TXTWIDTH];
	}
	return 0;
}
//-------------------------------------------------------------------------------------------

s32 txbGetHeight(s32 pr_id)
{
	if(cd.ainst->all_pr_count > pr_id)
	{
		return cd.ainst->pr_textdata[pr_id][TXTHEIGHT];
	}
	return 0;
}
//-------------------------------------------------------------------------------------------

s32 txbGetHeightLayer(s32 pr_id, u32 layer)
{
	struct TEngineInstance *l = _getInstance(layer);
	if(l->all_pr_count > pr_id)
	{
		return l->pr_textdata[pr_id][TXTHEIGHT];
	}
	return 0;
}
//-------------------------------------------------------------------------------------------

s32 _findTextureOffset(const struct TERFont* ipFont, wchar iChar, u16 arr[SRC_DATA_SIZE])
{
	s16 i;
	SDK_NULL_ASSERT(ipFont);
	SDK_NULL_ASSERT(ipFont->mAlphabet.mpAlphabet); // Current font is not assigned by data
	for(i = 0; i < ipFont->mAlphabet.mAlphabetSize; i++)
	{
		if(ipFont->mAlphabet.mpAlphabet[i] == iChar)
		{
			if(ipFont->mAlphabet.mpAlphabetTextureOffsetMap != NULL)
			{
				arr[SRC_DATA_X] = ipFont->mAlphabet.mpAlphabetTextureOffsetMap[i].mX;
				arr[SRC_DATA_Y] = ipFont->mAlphabet.mpAlphabetTextureOffsetMap[i].mY;
				arr[SRC_DATA_W] = ipFont->mAlphabet.mpAlphabetTextureOffsetMap[i].mW;
				arr[SRC_DATA_H] = ipFont->mAlphabet.mpAlphabetTextureOffsetMap[i].mH;
			}
			else
			{
				arr[SRC_DATA_Y] = 0;
				arr[SRC_DATA_X] = i * ipFont->mAlphabet.mFixedWidth;
				if (arr[SRC_DATA_X] >= ipFont->mpAbcImage->mWidth)
				{
					arr[SRC_DATA_Y]	= (arr[SRC_DATA_X] / ipFont->mpAbcImage->mWidth) * ipFont->mAlphabet.mFixedHeight;
					arr[SRC_DATA_X] = arr[SRC_DATA_X] % ipFont->mpAbcImage->mWidth;
				}
				SDK_ASSERT(arr[SRC_DATA_Y] <= ipFont->mpAbcImage->mHeight); // Offset for  char is out of font-image Y-bound
				SDK_ASSERT(arr[SRC_DATA_X] <= ipFont->mpAbcImage->mWidth); // Offset for  char is out of font-image X-bound
				arr[SRC_DATA_W] = ipFont->mpAbcImage->mWidth;
				arr[SRC_DATA_H] = ipFont->mpAbcImage->mHeight;
			}
			return i;
		}
	}
	if(ipFont->mAlphabet.mpAlphabetTextureOffsetMap != NULL)
	{
		arr[SRC_DATA_X] = 0;
		arr[SRC_DATA_Y] = 0;
		arr[SRC_DATA_W] = ipFont->mAlphabet.mpAlphabetTextureOffsetMap[0].mW;
		arr[SRC_DATA_H] = ipFont->mAlphabet.mpAlphabetTextureOffsetMap[0].mH;
		return 0;
	}
	else
	{
		arr[SRC_DATA_Y] = 0;
		arr[SRC_DATA_X] = 0;
		arr[SRC_DATA_W] = ipFont->mAlphabet.mFixedWidth;
		arr[SRC_DATA_H] = ipFont->mAlphabet.mFixedHeight;
		SDK_ASSERT(arr[SRC_DATA_Y] <= ipFont->mpAbcImage->mHeight); // Offset for  char is out of font-image Y-bound
		SDK_ASSERT(arr[SRC_DATA_X] <= ipFont->mpAbcImage->mWidth); // Offset for  char is out of font-image X-bound
		return 0;
	}
}
//----------------------------------------------------------------------------------------------------------------

void _drawText(struct TEngineInstance *ei, s32 pr_id)
{
	s32 sct;
	const wchar **ptxt;
	struct TERFont *pfont = cd.res_fnt[ei->pr_textdata[pr_id][TXTFONTID]];
	SDK_NULL_ASSERT(pfont); // Text is not assigned by font
	SDK_NULL_ASSERT(pfont->mAlphabet.mpAlphabet); // Current font is not assigned by data
	SDK_NULL_ASSERT(pfont->mpAbcImage);
	if(ei->pr_textdata[pr_id][TXTCHRCOUNT] == MAX_RES_IDX)
	{
		sct = (s32)ei->pr_textdatatext[pr_id];
		ptxt = cd.textdatatext[sct];
		sct = cd.textdatatext_sct[sct];
	}
	else
	{
		ptxt = (const wchar **)ei->pr_textdatatext[pr_id];
		sct = ei->pr_textdata[pr_id][TXTSTRCOUNT];
	}
	if(ptxt != NULL && ei->pr_textdata[pr_id][TXTWIDTH] > 0 && ei->pr_textdata[pr_id][TXTHEIGHT] > 0)
	{
		struct DrawImageFunctionData fd;
		s32 i, j, pos, len, xb, xe, xs; 
		fx32 ty, tx0, ty0;
		RENDERFN(SetActiveBGForGraphics)(cd.ainst->bgType);
		RENDERFN(SetColor)(ei->pr_textdata[pr_id][TXTCOLOR]);
		fd.mType = DIT_Obj;
		fd.text = TRUE;
		fd.mpClipRect = NULL;
		fd.mpSrcData = pfont->mpAbcImage;
		fd.mFillWithColor = (GXRgba)cd.ainst->prg_idata[pr_id][ICUSTOMBLENDCOLOR];
		tx0 = fd.mX = ei->pr_idata[pr_id][IX] - ei->dpx_fx - ei->sdx_fx - ((ei->pr_textdata[pr_id][TXTWIDTH] / 2) << FX32_SHIFT);
		ty0 = ty = ei->pr_idata[pr_id][IY] - ei->dpy_fx - ei->sdy_fx - ((ei->pr_textdata[pr_id][TXTHEIGHT] / 2) << FX32_SHIFT);
		RENDERFN(ColorRect)(fd.mX, ty,
							ei->pr_textdata[pr_id][TXTWIDTH] << FX32_SHIFT,
							ei->pr_textdata[pr_id][TXTHEIGHT] << FX32_SHIFT);
		if ((ei->pr_textdataalign[pr_id] & TEXT_ALIGNMENT_VCENTER) == TEXT_ALIGNMENT_VCENTER)
		{
			ty += ((ei->pr_textdata[pr_id][TXTHEIGHT] - pfont->mAlphabet.mFixedHeight * sct) / 2) << FX32_SHIFT;
		}
		else if ((ei->pr_textdataalign[pr_id] & TEXT_ALIGNMENT_BOTTOM) == TEXT_ALIGNMENT_BOTTOM)
		{
			ty += (ei->pr_textdata[pr_id][TXTHEIGHT] - pfont->mAlphabet.mFixedHeight * sct) << FX32_SHIFT;
		}
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
		fd.mSin = ei->prg_idata[pr_id][IROTSIN];
		fd.mCos = ei->prg_idata[pr_id][IROTCOS];
		fd.mScaleX = ei->prg_idata[pr_id][ICUSTOMSCALEXVALUE];
		fd.mScaleY = ei->prg_idata[pr_id][ICUSTOMSCALEYVALUE];
#endif
		fd.mAlpha = (u8)ei->prg_idata[pr_id][ICUSTOMALPHA];	
		for(j = 0; j < sct; j++)
		{
			fd.mSrcSizeData[SRC_DATA_H] = 0;
			len = (s32)ptxt[j][0];
			if ((ei->pr_textdataalign[pr_id] & TEXT_ALIGNMENT_HCENTER) == TEXT_ALIGNMENT_HCENTER)
			{
                fd.mX = 0;
				for (i = 0; i < len; i ++)
				{
					_findTextureOffset(pfont, ptxt[j][i + 1], fd.mSrcSizeData);
					if ((ei->pr_textdataalign[pr_id] & TEXT_ALIGNMENT_HCENTER) == TEXT_ALIGNMENT_HCENTER)
					{
						fd.mX += (fd.mSrcSizeData[SRC_DATA_W] + ei->pr_textdata[pr_id][TXTMARGIN]) << FX32_SHIFT;
					}
				}
				fd.mX = tx0 + ((ei->pr_textdata[pr_id][TXTWIDTH] << FX32_SHIFT) - fd.mX) / 2;
				xb = 0;
				xe = len;
				xs = 1;
			}
			else if ((ei->pr_textdataalign[pr_id] & TEXT_ALIGNMENT_LEFT) == TEXT_ALIGNMENT_LEFT)
			{
                fd.mX = tx0;
				xb = 0;
				xe = len;
				xs = 1;
			}
			else if ((ei->pr_textdataalign[pr_id] & TEXT_ALIGNMENT_RIGHT) == TEXT_ALIGNMENT_RIGHT)
			{
				fd.mX = tx0 + (ei->pr_textdata[pr_id][TXTWIDTH] << FX32_SHIFT);
				xb = len - 1;
				xe = -1;
				xs = -1;
			}
			else
			{
				xb = xe = xs = -1;
			}
			for (i = xb; i != xe; i += xs)
			{
				if ((pos = _findTextureOffset(pfont, ptxt[j][i + 1], fd.mSrcSizeData)) >= 0)
				{
					if (xs < 0)
					{
						fd.mX -= (fd.mSrcSizeData[SRC_DATA_W] + ei->pr_textdata[pr_id][TXTMARGIN]) << FX32_SHIFT;
					}
					if ((((fd.mX - tx0) >> FX32_SHIFT) + fd.mSrcSizeData[SRC_DATA_W] < 0 || ((fd.mX - tx0) >> FX32_SHIFT) > ei->pr_textdata[pr_id][TXTWIDTH])
							&& (ei->pr_textdataalign[pr_id] & TEXT_ALIGNMENT_HCENTER) != TEXT_ALIGNMENT_HCENTER)
				    {
						break;
				    }
					if(((fd.mX - tx0) >> FX32_SHIFT) + fd.mSrcSizeData[SRC_DATA_W] > ei->pr_textdata[pr_id][TXTWIDTH])
					{
						fd.mSrcSizeData[SRC_DATA_W] = (u16)(ei->pr_textdata[pr_id][TXTWIDTH] + ((tx0 - fd.mX) >> FX32_SHIFT)); 
					}
					if(((ty - ty0) >> FX32_SHIFT) + pfont->mAlphabet.mpAlphabetTextureOffsetMap[pos].mY_off + fd.mSrcSizeData[SRC_DATA_H] > ei->pr_textdata[pr_id][TXTHEIGHT])
					{
						fd.mSrcSizeData[SRC_DATA_H] = (u16)(ei->pr_textdata[pr_id][TXTHEIGHT] - pfont->mAlphabet.mpAlphabetTextureOffsetMap[pos].mY_off + ((ty0 - ty) >> FX32_SHIFT)); 
					}
					fd.mY = ty + (pfont->mAlphabet.mpAlphabetTextureOffsetMap[pos].mY_off << FX32_SHIFT);
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
					fd.mAffineOriginX = fd.mX - tx0 - ((ei->pr_textdata[pr_id][TXTWIDTH] / 2) << FX32_SHIFT);
					fd.mAffineOriginY = fd.mY - ty0 - ((ei->pr_textdata[pr_id][TXTHEIGHT] / 2) << FX32_SHIFT);
#endif				
					fd.mX = fxFloor(fd.mX);
					fd.mY = fxFloor(fd.mY);
					RENDERFN(DrawImage)(&fd);
				}
				else if (xs < 0)
				{
					fd.mX -= (fd.mSrcSizeData[SRC_DATA_W] + ei->pr_textdata[pr_id][TXTMARGIN]) << FX32_SHIFT;
				}
				if (xs > 0)
				{
					fd.mX += (fd.mSrcSizeData[SRC_DATA_W] + ei->pr_textdata[pr_id][TXTMARGIN]) << FX32_SHIFT;
				}
			}
			ty += (pfont->mAlphabet.mFixedHeight + ei->pr_textdata[pr_id][TXTMARGIN]) << FX32_SHIFT;
			if ((((ty - ty0) >> FX32_SHIFT) + fd.mSrcSizeData[SRC_DATA_H] < 0 || ((ty - ty0) >> FX32_SHIFT) > ei->pr_textdata[pr_id][TXTHEIGHT]) 
				&& (ei->pr_textdataalign[pr_id] & TEXT_ALIGNMENT_VCENTER) != TEXT_ALIGNMENT_VCENTER)
			{
				break;
			}
		}
	}
}
//-------------------------------------------------------------------------------------------

static void _draw(const u16 *frpcdt, struct DrawImageFunctionData *fd)
{
	SDK_NULL_ASSERT(frpcdt);
	if (cd.ainst->visible_mask != 0)
	{
		RENDERFN(SetActiveBGForGraphics)(cd.ainst->bgType);
		MI_CpuCopy16(&frpcdt[RES_X], fd->mSrcSizeData, sizeof(u16) * 4);
		RENDERFN(DrawImage)(fd);
	}
}
//-------------------------------------------------------------------------------------------

void setColor(GXRgba color)
{
	if (cd.ainst->visible_mask == 0)
	{
		return;
	}
	RENDERFN(SetActiveBGForGraphics)(cd.ainst->bgType);
	RENDERFN(SetColor)(color);
}
//-------------------------------------------------------------------------------------------

void fillRect(fx32 x, fx32 y, fx32 w, fx32 h)
{
	if (cd.ainst->visible_mask == 0)
	{
		return;
	}
	RENDERFN(SetActiveBGForGraphics)(cd.ainst->bgType);
	RENDERFN(ColorRect)(x - cd.ainst->dpx_fx - cd.ainst->sdx_fx, y - cd.ainst->dpy_fx - cd.ainst->sdy_fx, w, h);
}
//-------------------------------------------------------------------------------------------
#ifdef USE_CUSTOM_RENDER
void clearRect(s32 x, s32 y, s32 w, s32 h)
{
	if (!cd.ainst->visible)
	{
		return;
	}
	RENDERFN(SetActiveBGForGraphics)(cd.ainst->bgType);
	RENDERFN(ClearRect)(x - cd.ainst->dpx - cd.ainst->sdx, y - cd.ainst->dpy - cd.ainst->sdy, w, h);
}
//-------------------------------------------------------------------------------------------
#endif
void drawLine(fx32 x0, fx32 y0, fx32 x1, fx32 y1)
{
	if (cd.ainst->visible_mask == 0)
	{
		return;
	}
	RENDERFN(SetActiveBGForGraphics)(cd.ainst->bgType);
	RENDERFN(DrawLine)(x0 - cd.ainst->dpx_fx - cd.ainst->sdx_fx,
				y0 - cd.ainst->dpy_fx - cd.ainst->sdy_fx,
				x1 - cd.ainst->dpx_fx - cd.ainst->sdx_fx,
				y1 - cd.ainst->dpy_fx - cd.ainst->sdy_fx);
}
//-------------------------------------------------------------------------------------------

void drawSingleFrame(s32 group_id, s32 anim_id, s32 anim_frame, fx32 left, fx32 top, u8* alpha, 
						fx32* rotationAngle, fx32* scaleX, fx32* scaleY, fx32 affineOriginX, fx32 affineOriginY)
{
	if (cd.ainst->visible_mask != 0)
	{
		s32 i, pc_count;
		fx32 fl, ft;
		const s16 *frdata;
		struct DrawImageFunctionData fd;
		u16 grfr_id;
		SDK_NULL_ASSERT(cd.ainst->pr_idata);
		SDK_NULL_ASSERT(cd.ppr_fr_data);
		pc_count = cd.ppr_fr_data[group_id][anim_id][anim_frame][ANIFRFRCOUNT];
		fl = cd.ppr_fr_data[group_id][anim_id][anim_frame][ANIFROFFL];
		ft = cd.ppr_fr_data[group_id][anim_id][anim_frame][ANIFROFFT];
		fd.mAlpha = (alpha != NULL) ? *alpha : (u8)ALPHA_OPAQ;
		fd.mType = DIT_Obj;
		fd.text = FALSE;
		fd.mpClipRect = NULL;
		fd.mFillWithColor = FALSE;
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
		fd.mAffineOriginX = affineOriginX;
		fd.mAffineOriginY = affineOriginY;
		fd.mScaleX = (scaleX != NULL) ? *scaleX : FX32_ONE;
		fd.mScaleY = (scaleY != NULL) ? *scaleY : FX32_ONE;
#else
		(void)rotationAngle;
		(void)affineOriginX;
		(void)affineOriginY;
		(void)scaleX;
		(void)scaleY;
#endif
		for(i = 0; i < pc_count; i++)
		{
			frdata = _getFramePieceData(group_id, anim_id, anim_frame, i);
			SDK_NULL_ASSERT(cd.fr_data);
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
			if(rotationAngle == NULL)
			{
				//_getTransform(frdata[ANIFRFRDATA])
				fd.mSin = 0;
				fd.mCos = FX32_ONE;
			}
			else
			{
				fxCordic(FX_Mul((*rotationAngle / 180), FX_PI), &fd.mSin, &fd.mCos, FX_CORDIC_MIDDLE_PRECISION);
			}
#endif
			if(_isStreamFrame(frdata[ANIFRFRDATA]))
			{
				SDK_ASSERT(0); // do not use this function for streamed frames
				return;	
			}
			else
			{
				grfr_id = (u16)frdata[ANIFRFRID];
			}
			fd.mX = left - fl - cd.ainst->dpx_fx - cd.ainst->sdx_fx + (frdata[ANIFRFROFFX] << FX32_SHIFT);
			fd.mY = top - ft - cd.ainst->dpy_fx - cd.ainst->sdy_fx + (frdata[ANIFRFROFFY] << FX32_SHIFT);
			if (cd.fr_data[grfr_id][RES_IMG_IDX] < NULL_IMG_IDX)
			{
				fd.mpSrcData = cd.res_img[cd.fr_data[grfr_id][RES_IMG_IDX]];
				_draw(cd.fr_data[grfr_id], &fd);
			}
		}
	}
}
//-------------------------------------------------------------------------------------------

void _drawGameObj(s32 pr_id)
{
	SDK_NULL_ASSERT(cd.ainst->pr_idata);
	if(prIsTextBox(pr_id))
	{
		_drawText(cd.ainst, pr_id);
	}
	else
	{
		s32 pc_count, anim_id, i;
		const s16 *frdata;
		struct DrawImageFunctionData fd;
		fd.mType = DIT_Obj;
		_prGetFramePiecesInfo(cd.ainst, pr_id, &pc_count, &anim_id);
		fd.mFillWithColor = FALSE;
		fd.mpClipRect = NULL;
		fd.text = FALSE;
		if(cd.ainst->all_pr_count > pr_id)
		{
			fd.mFillWithColor = (GXRgba)cd.ainst->prg_idata[pr_id][ICUSTOMBLENDCOLOR];
			if(cd.ainst->prg_idata[pr_id][ICLIPRECTOBJ] != NONE_MAP_IDX)
			{
				cd.ainst->prg_idata[cd.ainst->prg_idata[pr_id][ICLIPRECTOBJ]][ICLIPRECTTEMPXY] = 
					(((cd.ainst->pr_idata[cd.ainst->prg_idata[pr_id][ICLIPRECTOBJ]][ILEFT] - cd.ainst->dpx_fx - cd.ainst->sdx_fx) >> FX32_SHIFT) << 16) |
					((cd.ainst->pr_idata[cd.ainst->prg_idata[pr_id][ICLIPRECTOBJ]][ITOP] - cd.ainst->dpy_fx - cd.ainst->sdy_fx) >> FX32_SHIFT);
				cd.ainst->prg_idata[cd.ainst->prg_idata[pr_id][ICLIPRECTOBJ]][ICLIPRECTTEMPWH] =
					(((cd.ainst->pr_idata[cd.ainst->prg_idata[pr_id][ICLIPRECTOBJ]][ICURRWIDTH] >> FX32_SHIFT) << 16) |
					(cd.ainst->pr_idata[cd.ainst->prg_idata[pr_id][ICLIPRECTOBJ]][ICURRHEIGHT]) >> FX32_SHIFT);
				fd.mpClipRect = &cd.ainst->prg_idata[cd.ainst->prg_idata[pr_id][ICLIPRECTOBJ]][ICLIPRECTTEMPXY];
			}
		}
		for(i = 0; i < pc_count; i++)
		{
			frdata = _getFramePieceData(cd.ainst->pr_idata[pr_id][IPARENT_IDX], anim_id, cd.ainst->pr_idata[pr_id][IANIFRIDX], i);
			SDK_NULL_ASSERT(cd.fr_data);
			fd.mX = cd.ainst->pr_idata[pr_id][IX] - cd.ainst->dpx_fx - cd.ainst->sdx_fx + (frdata[ANIFRFROFFX] << FX32_SHIFT);
			fd.mY = cd.ainst->pr_idata[pr_id][IY] - cd.ainst->dpy_fx - cd.ainst->sdy_fx + (frdata[ANIFRFROFFY] << FX32_SHIFT);
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
			fd.mAffineOriginX = frdata[ANIFRFROFFX] << FX32_SHIFT;
			fd.mAffineOriginY = frdata[ANIFRFROFFY] << FX32_SHIFT;
#endif
			if(cd.ainst->all_pr_count > pr_id)
			{
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
				if((cd.ainst->prg_idata[pr_id][ICUSTOMFLAGS] & ICUSTOMROTATIONFLAG) == ICUSTOMROTATIONFLAG)
				{
					fd.mSin = cd.ainst->prg_idata[pr_id][IROTSIN];
					fd.mCos = cd.ainst->prg_idata[pr_id][IROTCOS];
				}
				else
				{
					//_getTransform(frdata[ANIFRFRDATA]);
					fd.mSin = 0;
					fd.mCos = FX32_ONE;
				}
				if((cd.ainst->prg_idata[pr_id][ICUSTOMFLAGS] & ICUSTOMSCALEFLAG) == ICUSTOMSCALEFLAG)
				{
					fd.mScaleX = cd.ainst->prg_idata[pr_id][ICUSTOMSCALEXVALUE];
					fd.mScaleY = cd.ainst->prg_idata[pr_id][ICUSTOMSCALEYVALUE];
				}
				else
				{
					fd.mScaleX = fd.mScaleY = FX32_ONE;
				}
#endif
				if((cd.ainst->prg_idata[pr_id][ICUSTOMFLAGS] & ICUSTOMALPHAFLAG) == ICUSTOMALPHAFLAG)
				{
					fd.mAlpha = (u8)cd.ainst->prg_idata[pr_id][ICUSTOMALPHA];
				}
				else
				{
					fd.mAlpha = (u8)frdata[ANIFRFRALPHA];
				}
			}
			else
			{
				fd.mAlpha = (u8)frdata[ANIFRFRALPHA];
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
				//_getTransform(frdata[ANIFRFRDATA]);
				fd.mSin = 0;
				fd.mCos = FX32_ONE;
				fd.mScaleX = fd.mScaleY = FX32_ONE;
#endif
			}
			if(_isStreamFrame(frdata[ANIFRFRDATA]))
			{
				SDK_NULL_ASSERT(cd.strm_obj_data);
				SDK_NULL_ASSERT(cd.strm_obj_data[pr_id][anim_id]);
#if defined JOBS_IN_SEPARATE_THREAD && defined USE_OPENGL_RENDER
				if(!cd.strm_obj_data[pr_id][anim_id]->mData[0].mBindStatus)
#else
				if(!cd.strm_obj_data[pr_id][anim_id]->mData[0].mBufferStatus)
#endif
				{
					OS_Warning("warning: waiting for stream frame loading...\n");
					//SDK_ASSERT(0);
					continue;
				}
#ifdef JOBS_IN_SEPARATE_THREAD
				jobCriticalSectionBegin();
#endif
				fd.mpSrcData = cd.strm_obj_data[pr_id][anim_id]->mData[0].mpImage;
				_draw(cd.strm_obj_data[pr_id][anim_id]->mData[0].mFrpcdata, &fd);
#ifdef JOBS_IN_SEPARATE_THREAD
				jobCriticalSectionEnd();
#endif
			}
			else
			{
				SDK_ASSERT(frdata[ANIFRFRID] >= 0);
				if (cd.fr_data[frdata[ANIFRFRID]][RES_IMG_IDX] < NULL_IMG_IDX)
				{
					fd.mpSrcData = cd.res_img[cd.fr_data[frdata[ANIFRFRID]][RES_IMG_IDX]];
					_draw(cd.fr_data[frdata[ANIFRFRID]], &fd);
				}
			}
		}
	}
#ifdef DRAW_DEBUG_COLLIDERECTS
		if(cd.ainst->all_pr_count > pr_id)
		{
			if(cd.ainst->prg_idata[pr_id][ICBLEFT0] != cd.ainst->prg_idata[pr_id][ICBRIGHT0] || 
				cd.ainst->prg_idata[pr_id][ICBTOP0] != cd.ainst->prg_idata[pr_id][ICBBOTTOM0])
			{
				fx32 xrt, yrt, xlb, ylb, xlt, ylt, xrb, yrb;
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT

				xlt = cd.ainst->prg_idata[pr_id][ICBLTX];
				ylt = cd.ainst->prg_idata[pr_id][ICBLTY];
				xrt = cd.ainst->prg_idata[pr_id][ICBRTX];
				yrt = cd.ainst->prg_idata[pr_id][ICBRTY];
				xrb = cd.ainst->prg_idata[pr_id][ICBRBX];
				yrb = cd.ainst->prg_idata[pr_id][ICBRBY];
				xlb = cd.ainst->prg_idata[pr_id][ICBLBX];
				ylb = cd.ainst->prg_idata[pr_id][ICBLBY];
#else
				xrt = cd.ainst->prg_idata[pr_id][ICBRIGHT0];
				yrt = cd.ainst->prg_idata[pr_id][ICBTOP0];
				xlb = cd.ainst->prg_idata[pr_id][ICBLEFT0];
				ylb = cd.ainst->prg_idata[pr_id][ICBBOTTOM0];
				xlt = xlb;
				ylt = yrt;
				xrb = xrt;
				yrb = ylb;
#endif
				setColor(GX_RGBA(31, 0, 0, 1));
				drawLine(xlt, ylt, xrt, yrt);
				drawLine(xrt, yrt, xrb, yrb);
				drawLine(xrb, yrb, xlb, ylb);
				drawLine(xlb, ylb, xlt, ylt);
			}
		}
#endif

#ifdef DRAW_DEBUG_VIEWRECTS
		if(cd.ainst->all_pr_count > pr_id)
		{
			if(cd.ainst->prg_idata[pr_id][IVBLEFT0] != cd.ainst->prg_idata[pr_id][IVBRIGHT0] || 
				cd.ainst->prg_idata[pr_id][IVBTOP0] != cd.ainst->prg_idata[pr_id][IVBBOTTOM0])
			{
				fx32 xrt, yrt, xlb, ylb, xlt, ylt, xrb, yrb; 
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT				
				xlt = cd.ainst->prg_idata[pr_id][IVBLTX];
				ylt = cd.ainst->prg_idata[pr_id][IVBLTY];
				xrt = cd.ainst->prg_idata[pr_id][IVBRTX];
				yrt = cd.ainst->prg_idata[pr_id][IVBRTY];
				xrb = cd.ainst->prg_idata[pr_id][IVBRBX];
				yrb = cd.ainst->prg_idata[pr_id][IVBRBY];
				xlb = cd.ainst->prg_idata[pr_id][IVBLBX];
				ylb = cd.ainst->prg_idata[pr_id][IVBLBY];
#else
				xrt = cd.ainst->prg_idata[pr_id][IVBRIGHT0];
				yrt = cd.ainst->prg_idata[pr_id][IVBTOP0];
				xlb = cd.ainst->prg_idata[pr_id][IVBLEFT0];
				ylb = cd.ainst->prg_idata[pr_id][IVBBOTTOM0];
				xlt = xlb;
				ylt = yrt;
				xrb = xrt;
				yrb = ylb;
#endif
				setColor(GX_RGBA(0, 0, 31, 1));
				drawLine(xlt, ylt, xrt, yrt);
				drawLine(xrt, yrt, xrb, yrb);
				drawLine(xrb, yrb, xlb, ylb);
				drawLine(xlb, ylb, xlt, ylt);
			}
		}
#endif
}
//-------------------------------------------------------------------------------------------

BOOL prIsTextBox(s32 pr_id)
{
	return cd.ainst->pr_idata[pr_id][IPARENT_IDX] == PARENT_TEXT_OBJ;
}
//-------------------------------------------------------------------------------------------

BOOL prIsTextBoxLayer(s32 pr_id, u32 layer)
{
	struct TEngineInstance *l = _getInstance(layer);
	return l->pr_idata[pr_id][IPARENT_IDX] == PARENT_TEXT_OBJ;
}
//-------------------------------------------------------------------------------------------

void callForEachGameObject(s32 iId, CallForEachGameObjectFn pCallbackFn)
{
	SDK_NULL_ASSERT(pCallbackFn);
	if(cd.ainst->ueof_state != UEOF_EMPTY && cd.ainst->visible_mask != 0)
	{			
		s32 *goa;
		s32 goa_ct, i;
		SDK_NULL_ASSERT(cd.ainst->current_zone >= 0);
		SDK_NULL_ASSERT(cd.ainst->pr_znlist);
		SDK_NULL_ASSERT(cd.ainst->pr_count);
		goa = cd.ainst->pr_znlist[cd.ainst->current_zone];
		goa_ct = cd.ainst->pr_count[cd.ainst->current_zone];		
		for(i = 0; i < goa_ct; i++)
		{
			pCallbackFn(cd.a_layer, iId, (u16)goa[i]);
		}
	}
}
//-------------------------------------------------------------------------------------------

void updateEngine(s32 ms)
{
	u32 layer;

	cd.global_delay_ct -= ms;
	if(cd.global_delay_ct <= MINIMAL_TICK_TIME)
	{
		cd.draw = TRUE;
	}

	if(cd.ld_st == ALS_STATE_LOAD_DATA || cd.ag_st)
	{
		_updateLoadingProcess();
#ifndef JOBS_IN_SEPARATE_THREAD 
		jobUpdateFileStreamTasks();
#endif
	}

#ifndef JOBS_IN_SEPARATE_THREAD 	
	jobUpdateAudioStreamTasks();
#endif

	for(layer = 0; layer < cd.initParams.layersCount; layer++)
	{
		struct TEngineInstance* ei = cd.instances[layer];
		_updateEngineObjFn[ei->ueof_state & ei->visible_mask](ei, layer, ms);
	}

	if(cd.draw)
	{
		cd.draw = FALSE;
		cd.global_delay_ct = getTickDelay();
	}
}
//-------------------------------------------------------------------------------------------

void _drawObjectsWithUpdate(struct TEngineInstance* ei, u32 layer, s32 ms)
{
	s32 *goad;
	s16 *gfo, *gbo;
	s32 gfo_ct, gbo_ct, goa_ct, i;
	SDK_NULL_ASSERT(ei->current_zone >= 0);
	SDK_NULL_ASSERT(ei->pr_znlist);
	SDK_NULL_ASSERT(ei->pr_count);
	gbo = ei->db_znlist[ei->current_zone];
	gfo	= ei->df_znlist[ei->current_zone];
	goad = ei->pr_znlist[ei->current_zone];
	goa_ct = ei->pr_count[ei->current_zone];
	gfo_ct = ei->df_count[ei->current_zone];
	gbo_ct = ei->db_count[ei->current_zone];

	_setActiveLayer(layer);

	for(i = 0; i < gbo_ct; i++)
	{
		_updateAnimation(gbo[i], ms);
	}
	for(i = 0; i < goa_ct; i++)
	{
		_updateAnimation(goad[i] & 0xffff, ms);
	}
	for(i = 0; i < gfo_ct; i++)
	{
		_updateAnimation(gfo[i], ms);
	}
	if(cd.strm_instance == ei)
	{
#if defined JOBS_IN_SEPARATE_THREAD && defined USE_OPENGL_RENDER
		while(!IsFileSystemError() && !jobBindTexturesToVRAM())
		{
			OS_Warning("warning: reload stream frames...\n");
			jobUpdateAnimStreamTasksAndLoadTextures();
            Sleep(0);
		}
#else
		jobUpdateAnimStreamTasksAndLoadTextures();
#endif
	}
	if(cd.draw)
	{			
		BOOL draw = TRUE;
		if(cd._onDrawBackgroundTiles)
		{
			cd.rl_available = TRUE;
			cd._onDrawBackgroundTiles(layer);
			cd.rl_available = FALSE;
		}
		else
		{
			cd.rl_available = FALSE;
		}
		if(ei->primary)
		{
			_drawBackgroundTiles(draw);
		}
		for(i = 0; i < gbo_ct; i++)
		{
			draw = TRUE;
			if(cd._onUpdateDecorationObject)
			{
				cd._onUpdateDecorationObject(layer, gbo[i], &draw);
			}
			if(draw)
			{
				_drawGameObj(gbo[i]);
			}
			if(ei->pr_idata[gbo[i]][ITEMPSTATE] != -1)
			{
				_setState(ei, gbo[i], ei->pr_idata[gbo[i]][ITEMPSTATE], FALSE);
			}
		}
		if(cd.enable_prp_id != NONE_MAP_IDX)
		{
			u16 rid = 0;
			while(rid != 0xffff)
			{
				u16 oidx = (u16)ei->rndr_znlist[ei->current_zone][rid];
				while(oidx != 0xffff)
				{
					u16 oid = (u16)ei->pr_znlist[ei->current_zone][oidx];
					if(prGetProperty(oid, cd.enable_prp_id) != 0)
					{
						draw = TRUE;
						_processMovement(oid);
						if(cd._onUpdateGameObject)
						{
							cd._onUpdateGameObject(layer, oid, &draw);
						}
						if(draw)
						{
							_drawGameObj(oid);
						}
					}
					oidx = ei->pr_znlist[ei->current_zone][oidx] >> 16;
				}
				rid = ei->rndr_znlist[ei->current_zone][rid] >> 16;
			}
		}
		else
		{
			u16 rid = 0;
			while(rid != 0xffff)
			{
				u16 oidx = (u16)ei->rndr_znlist[ei->current_zone][rid];
				while(oidx != 0xffff)
				{
					u16 oid = (u16)ei->pr_znlist[ei->current_zone][oidx];
					draw = TRUE;
					_processMovement(oid);
					if(cd._onUpdateGameObject)
					{
						cd._onUpdateGameObject(layer, oid, &draw);
					}
					if(draw)
					{
						_drawGameObj(oid);
					}
					oidx = ei->pr_znlist[ei->current_zone][oidx] >> 16;
				}
				rid = ei->rndr_znlist[ei->current_zone][rid] >> 16;
			}
		}
		for(i = 0; i < gfo_ct; i++)
		{
			draw = TRUE;
			if(cd._onUpdateDecorationObject)
			{
				cd._onUpdateDecorationObject(layer, gfo[i], &draw);
			}
			if(draw)
			{
				_drawGameObj(gfo[i]);
			}
			if(ei->pr_idata[gfo[i]][ITEMPSTATE] != -1)
			{
				_setState(ei, gfo[i], ei->pr_idata[gfo[i]][ITEMPSTATE], FALSE);
			}
		}
		for(i = 0; i < goa_ct; i++)
		{
			u16 oid = (u16)goad[i];
			if(ei->pr_idata[oid][ITEMPSTATE] != -1)
			{
				_setState(ei, oid, ei->pr_idata[oid][ITEMPSTATE], TRUE);
			}
		}
		_updateParticles(TRUE, ms);
		cd.rl_available = TRUE;
		if(cd._onFinishUpdate)
		{
			cd._onFinishUpdate(layer);
		}
	}
	else
	{
		_updateParticles(FALSE, ms);
	}
	cd.rl_available = TRUE;
	if(ei->t_current_zone != ei->current_zone)
	{
		ei->current_zone = ei->t_current_zone;
	}
}
//-------------------------------------------------------------------------------------------

void _drawObjectsDummy(struct TEngineInstance* ei, u32 layer, s32 ms)
{
	(void)ei;
	(void)layer;
	(void)ms;
}
//-------------------------------------------------------------------------------------------

void _drawObjectsWithoutUpdate(struct TEngineInstance* ei, u32 layer, s32 ms)
{
	s16 *gfo, *gbo;
	s32 gfo_ct, gbo_ct, i;
	(void)ms;
	SDK_NULL_ASSERT(ei->current_zone >= 0);
	SDK_NULL_ASSERT(ei->pr_znlist);
	SDK_NULL_ASSERT(ei->pr_count);
	gbo = ei->db_znlist[ei->current_zone];
	gfo	= ei->df_znlist[ei->current_zone];
	gfo_ct = ei->df_count[ei->current_zone];
	gbo_ct = ei->db_count[ei->current_zone];
			
	_setActiveLayer(layer);

	cd.rl_available = FALSE;

	if(ei->primary)
	{
		_drawBackgroundTiles(TRUE);
	}
	for(i = 0; i < gbo_ct; i++)
	{
		_drawGameObj(gbo[i]);
	}
	if(cd.enable_prp_id != NONE_MAP_IDX)
	{
		u16 rid = 0;
		while(rid != 0xffff)
		{
			u16 oidx = (u16)ei->rndr_znlist[ei->current_zone][rid];
			while(oidx != 0xffff)
			{
				u16 oid = (u16)ei->pr_znlist[ei->current_zone][oidx];
				if(prGetProperty(oid, cd.enable_prp_id) != 0)
				{
					_drawGameObj(oid);
				}
				oidx = ei->pr_znlist[ei->current_zone][oidx] >> 16;
			}
			rid = ei->rndr_znlist[ei->current_zone][rid] >> 16;
		}
	}
	else
	{
		u16 rid = 0;
		while(rid != 0xffff)
		{
			u16 oidx = (u16)ei->rndr_znlist[ei->current_zone][rid];
			while(oidx != 0xffff)
			{
				_drawGameObj((u16)ei->pr_znlist[ei->current_zone][oidx]);
				oidx = ei->pr_znlist[ei->current_zone][oidx] >> 16;
			}
			rid = ei->rndr_znlist[ei->current_zone][rid] >> 16;
		}
	}
	for(i = 0; i < gfo_ct; i++)
	{
		_drawGameObj(gfo[i]);
	}

	cd.rl_available = TRUE;
}
//-------------------------------------------------------------------------------------------

// Particles

//-------------------------------------------------------------------------------------------
void createParticles(s32 pr_idx,
					  u32 colCount,
					  u32 rowCount,
					  fx32 minSpeed,
					  fx32 varSpeed,
					  u32 minLifeTime,
					  u32 varLifeTime,
					  u32 exploudTime,
					  fx32 *targetX,
					  fx32 *targetY)
{
	const s16 *frdata;
	u32 x, y;
	s32 i, ox, oy, ct, w, h, t, a, pc_count, anim_id, fpcl, fpct, *pcs;
	u16 grfr_id;
	SDK_NULL_ASSERT(cd.ainst->pr_idata);
	SDK_NULL_ASSERT(cd.fr_data);
	if(cd.initParams.particlesPoolSize == 0)
	{
		return;
	}
	_prGetFramePiecesInfo(cd.ainst, pr_idx, &pc_count, &anim_id);
	for(i = 0; i < pc_count; i++)
	{
		frdata = _getFramePieceData(cd.ainst->pr_idata[pr_idx][IPARENT_IDX], anim_id, cd.ainst->pr_idata[pr_idx][IANIFRIDX], i);
		SDK_NULL_ASSERT(cd.fr_data);
		if(_isStreamFrame(frdata[ANIFRFRDATA]))
		{
			SDK_ASSERT(0); // do not use this function for streamed frames
			return;	
		}
		grfr_id = (u16)frdata[ANIFRFRID];
		SDK_ASSERT(grfr_id < NULL_IMG_IDX);
		fpcl = (cd.ainst->pr_idata[pr_idx][IX] >> FX32_SHIFT) + frdata[ANIFRFROFFX];
		fpct = (cd.ainst->pr_idata[pr_idx][IY] >> FX32_SHIFT) + frdata[ANIFRFROFFY];
		w = cd.fr_data[grfr_id][RES_W] / (s32)colCount;
		h = cd.fr_data[grfr_id][RES_H] / (s32)rowCount;
		ox = cd.fr_data[grfr_id][RES_X];
		oy = cd.fr_data[grfr_id][RES_Y];
		ct = cd.ainst->particlesCount;
		for (x = 0; x < colCount; x++)
			for (y = 0; y < rowCount; y++)
			{
				pcs = cd.ainst->pcs_data[ct];
				pcs[PCS_W] = w;
				pcs[PCS_H] = h;
				pcs[PCS_RESID] = cd.fr_data[grfr_id][RES_IMG_IDX];
				pcs[IX] = (s32)((fpcl + x * w) << FX32_SHIFT);
				pcs[IY] = (s32)((fpct + y * h) << FX32_SHIFT);
				pcs[PCS_OFFX] = (s32)(ox + x * w);
				pcs[PCS_OFFY] = (s32)(oy + y * h);
				pcs[PCS_SPEED] = (s32)(minSpeed + (mthGetRandom(varSpeed >> FX32_SHIFT) << FX32_SHIFT));
				pcs[PCS_ADDITIONAL] = pcs[PCS_LIFE] = (s32)(minLifeTime + mthGetRandom(varLifeTime));
				pcs[PCS_EXPLODE] = (s32)(exploudTime / 2 + mthGetRandom(exploudTime / 2));
				pcs[PCS_TGTX0] = MAX_INT;
				pcs[PCS_TGTY0] = MAX_INT;
				if(targetX != NULL)
				{
					pcs[PCS_TGTX0] = *targetX;
				}
				if(targetY != NULL)
				{
					pcs[PCS_TGTY0] = *targetY;
                }
				pcs[PCS_SINCOS] = 0; //(0 << 16) | (FX32_ONE - 1); //_getTransform(frdata[ANIFRFRDATA]);
				pcs[PCS_ROTATIONORIGX] = frdata[ANIFRFROFFX];
				pcs[PCS_ROTATIONORIGY] = frdata[ANIFRFROFFY];
				t = FX32_ONE / 10; //cd.ainst->sz_mapH > cd.ainst->sz_mapW ? cd.ainst->sz_mapH : cd.ainst->sz_mapW;
				{
					fx32 fsin, fcos;
					a = (s32)mthGetRandom(360);
					fxCordic((a * FX_PI) / 180, &fsin, &fcos, FX_CORDIC_LOW_PRECISION);
					pcs[PCS_TGTX] = pcs[IX] + t * fcos;
					pcs[PCS_TGTY] = pcs[IX] + t * fsin;
				}
				pcs[PCS_DISTLENGHT] = _calcLenght(pcs[IX], pcs[IY], pcs[PCS_TGTX], pcs[PCS_TGTY]);
				pcs[PCS_ALPHA] = frdata[ANIFRFRALPHA];
				ct++;
				if (++cd.ainst->particlesCount >= (s32)cd.initParams.particlesPoolSize)
				{
					cd.ainst->particlesCount = cd.initParams.particlesPoolSize - 1;
					x = colCount;
					i = pc_count;
					break;
				}
			}
	}
}
//-------------------------------------------------------------------------------------------

void createExplode(fx32 x,
					fx32 y,
					u32 radius,
					u32 count,
					u32 time,
					s32 pr_idx,
                    s32 state)
{
	u32 i;
	s32 ct, r, a, *pcs;
	if(cd.initParams.particlesPoolSize == 0)
	{
		return;
	}
	ct = cd.ainst->particlesCount;
	for (i = 0; i < count; i++)
	{
		pcs = cd.ainst->pcs_data[ct];
		pcs[PCS_OFFX] = (s32)x;
		pcs[PCS_OFFY] = (s32)y;
		pcs[PCS_W] = (s32)radius;
		pcs[PCS_H] = -1;
		{
			fx32 fsin, fcos;
			a = (s32)mthGetRandom(360);
			r = (s32)mthGetRandom(radius);
			fxCordic((a * FX_PI) / 180, &fsin, &fcos, FX_CORDIC_LOW_PRECISION);
			pcs[IX] = x + r * fsin;
			pcs[IY] = y + r * fcos;
		}
		pcs[PCS_RESID] = -2; // id Explode
		pcs[PCS_SPEED] = state;
		pcs[PCS_TGTX0] = pr_idx;
		pcs[PCS_TGTY0] = cd.states[state][stIDANIMATION];
		pcs[PCS_SINCOS] = (s32)(i * 2);
		pcs[PCS_ROTATIONORIGX] = 0;
		pcs[PCS_ROTATIONORIGY] = 0;
		pcs[PCS_LIFE] = (s32)time;
		pcs[PCS_EXPLODE] = 0;
		pcs[PCS_ALPHA] = 15 + (s32)mthGetRandom(16);
		ct++;
		if (++cd.ainst->particlesCount >= (s32)cd.initParams.particlesPoolSize)
		{
			cd.ainst->particlesCount = cd.initParams.particlesPoolSize - 1;
			break;
		}
	}
}
//-------------------------------------------------------------------------------------------

void createSimpleParticles(u32 size,
							GXRgba color,
							fx32 xpos,
							fx32 ypos,
							u32 radius,
							u32 count,
							fx32 minSpeed,
							fx32 varSpeed,
							u32 minLifeTime,
							u32 varLifeTime,
							u32 exploudTime,
							fx32 *targetX,
							fx32 *targetY)
{
	u32 y;
	s32 ct, a, r, *pcs;
	fx32 fsin, fcos;
	if(cd.initParams.particlesPoolSize == 0)
	{
		return;
	}
	ct = cd.ainst->particlesCount;
	for (y = 0; y < count; y++)
	{
		pcs = cd.ainst->pcs_data[ct];
		pcs[PCS_W] = (s32)size;
		pcs[PCS_RESID] = -1;
		pcs[PCS_OFFX] = (s32)color;
		a = (s32)mthGetRandom(360);
		r = (s32)mthGetRandom(radius);
		fxCordic((a * FX_PI) / 180, &fsin, &fcos, FX_CORDIC_LOW_PRECISION);	
		pcs[IX] = xpos + r * fcos - FX32(size / 2);
		pcs[IY] = ypos + r * fsin - FX32(size / 2);
		pcs[PCS_SPEED] = (s32)(minSpeed + (mthGetRandom(varSpeed >> FX32_SHIFT) << FX32_SHIFT));
		pcs[PCS_LIFE] = (s32)(minLifeTime + mthGetRandom(varLifeTime));
		pcs[PCS_EXPLODE] = (s32)(exploudTime / 2 + mthGetRandom(exploudTime / 2));
		pcs[PCS_TGTX0] = MAX_INT;
		pcs[PCS_TGTY0] = MAX_INT;
		if(targetX != NULL)
		{
			pcs[PCS_TGTX0] = *targetX;
		}
		if(targetY != NULL)
		{
			pcs[PCS_TGTY0] = *targetY;
		}
		pcs[PCS_SINCOS] = 1;
		r = FX32_ONE / 10; //cd.ainst->sz_mapH > cd.ainst->sz_mapW ? cd.ainst->sz_mapH : cd.ainst->sz_mapW;
		pcs[PCS_TGTX] = xpos + r * fcos;
		pcs[PCS_TGTY] = ypos + r * fsin;
		pcs[PCS_DISTLENGHT] = _calcLenght(pcs[IX], pcs[IY], pcs[PCS_TGTX], pcs[PCS_TGTY]);
		ct++;
		if (++cd.ainst->particlesCount >= (s32)cd.initParams.particlesPoolSize)
		{
			cd.ainst->particlesCount = cd.initParams.particlesPoolSize - 1;
			break;
		}
	}
}
//-------------------------------------------------------------------------------------------

void resetParticles()
{
	cd.ainst->particlesCount = 0;
}
//-------------------------------------------------------------------------------------------

s32 getParticlesCount()
{
	return cd.ainst->particlesCount;
}
//-------------------------------------------------------------------------------------------

void _updateParticles(BOOL draw, s32 ms)
{
	s32 *pcs;
	s32 i, res_id;
	for (i = 0; i < cd.ainst->particlesCount; i++)
	{
		pcs = cd.ainst->pcs_data[i];
		if(draw)
		{
			res_id = pcs[PCS_RESID];
			switch(res_id)
			{
				case -2:
				{
					pcs[PCS_SINCOS]--;
					if (pcs[PCS_SINCOS] <= 0)
					{
						const s16 *frdata;
						s32 j, pc_count, fr_count;
						u16 grfr_id;
						pcs[PCS_SINCOS] = 0;
						pcs[PCS_H]++;
						fr_count = cd.ppr_anim_data[cd.ainst->pr_idata[pcs[PCS_TGTX0]][IPARENT_IDX]]
												[pcs[PCS_TGTY0]]
												[ANIFRCOUNT];
						if(fr_count <= pcs[PCS_H])
						{
							s32 a, r;
							fx32 fsin, fcos;  
							pcs[PCS_H] = 0;
							a = (s32)mthGetRandom(360);
							r = (s32)mthGetRandom(pcs[PCS_W]);
							fxCordic((a * FX_PI) / 180, &fsin, &fcos, FX_CORDIC_LOW_PRECISION);	
							pcs[IX] = (s32)(pcs[PCS_OFFX] + r * fcos);
							pcs[IY] = (s32)(pcs[PCS_OFFY] + r * fsin);
							pcs[PCS_ALPHA] = 15 + (s32)mthGetRandom(16);
						}
						if(fr_count > 0)
						{
							pc_count = cd.ppr_fr_data[cd.ainst->pr_idata[pcs[PCS_TGTX0]][IPARENT_IDX]]
												[pcs[PCS_TGTY0]]
												[pcs[PCS_H]]
												[ANIFRFRCOUNT];
							for(j = 0; j < pc_count; j++)
							{
								frdata = _getFramePieceData(cd.ainst->pr_idata[pcs[PCS_TGTX0]][IPARENT_IDX],
																pcs[PCS_TGTY0],
																pcs[PCS_H], j);
								SDK_NULL_ASSERT(cd.fr_data);
								grfr_id = (u16)frdata[ANIFRFRID];
								SDK_ASSERT(grfr_id < NULL_IMG_IDX);
								if (cd.fr_data[grfr_id][RES_IMG_IDX] < NULL_IMG_IDX)
								{
									struct DrawImageFunctionData fd;
									MI_CpuClear8(&fd, sizeof(struct DrawImageFunctionData));
									fd.mType = DIT_Obj;
									fd.mX = pcs[IX] - cd.ainst->dpx_fx - cd.ainst->sdx_fx + ((frdata[ANIFRFROFFX] - (cd.fr_data[grfr_id][RES_W] >> 1)) << FX32_SHIFT);
									fd.mY = pcs[IY] - cd.ainst->dpy_fx - cd.ainst->sdy_fx + ((frdata[ANIFRFROFFY] - (cd.fr_data[grfr_id][RES_H] >> 1)) << FX32_SHIFT);
									fd.mAlpha = (u8)pcs[PCS_ALPHA];
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
									fd.mScaleX = fd.mScaleY = FX32_ONE;
#endif
									fd.mpSrcData = cd.res_img[cd.fr_data[grfr_id][RES_IMG_IDX]];
									_draw(cd.fr_data[grfr_id], &fd);
								}
							}
						}
					}
				}
				break;

				case -1:
				if (pcs[PCS_SINCOS] == 0)
				{
					RENDERFN(SetColor)((GXRgba)pcs[PCS_OFFX]);
					RENDERFN(ColorRect)(pcs[IX] - cd.ainst->dpx_fx - cd.ainst->sdx_fx,
										pcs[IY] - cd.ainst->dpy_fx - cd.ainst->sdy_fx,
										pcs[PCS_W] << FX32_SHIFT,
										pcs[PCS_W] << FX32_SHIFT);
				}
				pcs[PCS_SINCOS] = 0;
				break;

				default:
				{
					struct DrawImageFunctionData fd;
					u8 a = (u8)((pcs[PCS_ALPHA] * (pcs[PCS_LIFE] + ((pcs[PCS_ADDITIONAL] - pcs[PCS_LIFE]) >> 4))) / pcs[PCS_ADDITIONAL]);
					if(a > 31)
					{
						a = 0;
					}
					RENDERFN(SetActiveBGForGraphics)(cd.ainst->bgType);									
					fd.mType = DIT_Obj;
					fd.text = FALSE;
					fd.mpClipRect = NULL;
					fd.mX = pcs[IX] - cd.ainst->dpx_fx - cd.ainst->sdx_fx;
					fd.mY = pcs[IY] - cd.ainst->dpy_fx - cd.ainst->sdy_fx;
					fd.mpSrcData = cd.res_img[res_id];
					fd.mFillWithColor = FALSE;
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
					fd.mScaleX = fd.mScaleY = FX32_ONE;
					fd.mSin = 0; //(pcs[PCS_SINCOS] >> 16) & 0xffff;
					fd.mCos = FX32_ONE; //pcs[PCS_SINCOS] & 0xffff;
					fd.mAffineOriginX = pcs[PCS_ROTATIONORIGX];
					fd.mAffineOriginY = pcs[PCS_ROTATIONORIGY];
#endif
					fd.mAlpha = a;
					fd.mSrcSizeData[SRC_DATA_X] = (u16)pcs[PCS_OFFX];
					fd.mSrcSizeData[SRC_DATA_Y] = (u16)pcs[PCS_OFFY];
					fd.mSrcSizeData[SRC_DATA_W] = (u16)pcs[PCS_W];
					fd.mSrcSizeData[SRC_DATA_H] = (u16)pcs[PCS_H];
					RENDERFN(DrawImage)(&fd);
				}
			}
		}
		if (ms > 0)
		{
			pcs[PCS_LIFE] -= ms;
			if (pcs[PCS_LIFE] <= 0)
			{
				s32 tpcs[PCS_COUNT];
				pcs[PCS_LIFE] = -1;
				pcs[PCS_EXPLODE] = -1;
				cd.ainst->particlesCount--;
				MI_CpuCopy8(cd.ainst->pcs_data[i], tpcs, PCS_COUNT * sizeof(s32));
				MI_CpuCopy8(cd.ainst->pcs_data[cd.ainst->particlesCount], cd.ainst->pcs_data[i], PCS_COUNT * sizeof(s32));
				MI_CpuCopy8(tpcs, cd.ainst->pcs_data[cd.ainst->particlesCount], PCS_COUNT * sizeof(s32));
				i--;
			}
			else
			{
				pcs[PCS_EXPLODE] -= ms;
				if(pcs[PCS_RESID] != -2)
				{
					if (pcs[PCS_EXPLODE] <= 0)
					{
						pcs[PCS_EXPLODE] = -1;
						if (pcs[PCS_TGTX0] != MAX_INT)
						{
							pcs[PCS_TGTX] = pcs[PCS_TGTX0];
						}
						if (pcs[PCS_TGTY0] != MAX_INT)
						{
							pcs[PCS_TGTY] = pcs[PCS_TGTY0];
						}
						pcs[PCS_DISTLENGHT] = _calcLenght(pcs[IX], pcs[IY], pcs[PCS_TGTX], pcs[PCS_TGTY]);
					}
					_nextPointAtLine(pcs[IX], pcs[IY], pcs[PCS_TGTX], pcs[PCS_TGTY], &pcs[PCS_DISTLENGHT], pcs[PCS_SPEED], pcs);
				}
			}
		}
	}
}
//-------------------------------------------------------------------------------------------

u16 rlGetCount(u32 layer)
{
	(void)layer;
	return 1;//(u16)_getInstance(layer)->all_pr_count;
}
//-------------------------------------------------------------------------------------------

BOOL rlIsAvailable(void)
{
	return cd.rl_available;
}
//-------------------------------------------------------------------------------------------

BOOL rlMove(u32 layer, u32 zone, u16 index_from, u16 index_to)
{
	struct TEngineInstance *ei = _getInstance(layer);
	if(cd.rl_available == TRUE && 
		zone < (u32)ei->zones_ct && index_from < (u16)ei->all_pr_count && index_to < (u16)ei->all_pr_count)
	{
		if(index_from == index_to)
		{
			return TRUE;
		}
		/*
		u16 idx_f = (u16)ei->pr_znlist[ei->zones_ct][index_from];
		if(0xffff != idx_f)
		{
			u16 idx_t = (u16)ei->pr_znlist[ei->zones_ct][index_to];
			if(0xffff != idx_t)
			{
				u16 next_f, next_t;
				next_f = (u16)ei->pr_znlist[ei->zones_ct][index_from] >> 16;
				next_t = (u16)ei->pr_znlist[ei->zones_ct][index_to] >> 16;
			}
		}
		*/
		return FALSE;
	}
	return FALSE;
}
//-------------------------------------------------------------------------------------------

BOOL rlGetGameObjectIndex(s32 pr_id, u32 layer, u32 zone, u16 *const orenderlist_index, u16 *const oindex)
{
	struct TEngineInstance *ei = _getInstance(layer);
	if(zone < (u32)ei->zones_ct)
	{
		u16 rid = 0;
		u16 idx = 0;
		while(rid != 0xffff)
		{
			u16 oidx = (u16)ei->rndr_znlist[zone][rid];
			while(oidx != 0xffff)
			{
				if((u16)pr_id == (u16)ei->pr_znlist[zone][oidx])
				{
					if(orenderlist_index != NULL)
					{
						*orenderlist_index = rid;
					}
					if(oindex != NULL)
					{
						*oindex = idx; 
					}
					return TRUE;
				}
				oidx = ei->pr_znlist[zone][oidx] >> 16;
				++idx;
			}
			rid = ei->rndr_znlist[zone][rid] >> 16;
		}
	}
	return FALSE;
}
//-------------------------------------------------------------------------------------------

BOOL rlMoveGameObject(u32 layer, u32 zone, u16 renderlist_index, u16 index_from, u16 index_to)
{
	struct TEngineInstance *ei = _getInstance(layer);
	if(cd.rl_available == TRUE && zone < (u32)ei->zones_ct && renderlist_index < rlGetCount(layer))
	{
		u16 oidx, c_idx; 
		s32 *prev_obj, *prev_obj_f, *obj_f, *obj_t, *obj;
		if(index_from == index_to)
		{
			return FALSE;
		}
		oidx = (u16)ei->rndr_znlist[zone][renderlist_index];
		prev_obj = prev_obj_f = obj_f = obj_t = NULL;
		c_idx = 0;
		while(oidx != 0xffff)
		{
			obj = &ei->pr_znlist[zone][oidx];
			if(index_from == c_idx)
			{
				obj_f = obj; 
				prev_obj_f = prev_obj;
				if(obj_t != NULL)
				{
					break;
				}
			}
			else if(index_to == c_idx)
			{
				obj_t = index_to > index_from ? obj : prev_obj;
				if(obj_f != NULL)
				{
					break;
				}
			}
			prev_obj = obj;
			oidx = ei->pr_znlist[zone][oidx] >> 16;
			++c_idx;
		}
		if(obj_f != NULL)
		{
			if(obj_t == NULL && obj_f == prev_obj)
			{
				return TRUE;
			}
			if(prev_obj_f != NULL)
			{
				c_idx = (*prev_obj_f) >> 16;
				*prev_obj_f = ((*obj_f) & 0xffff0000) | ((*prev_obj_f) & 0xffff);
			}
			else
			{
				SDK_ASSERT(index_from == 0);
				c_idx = (u16)ei->rndr_znlist[zone][renderlist_index];
				ei->rndr_znlist[zone][renderlist_index] = (ei->rndr_znlist[zone][renderlist_index] & 0xffff0000) | ((u16)((*obj_f) >> 16));
			}
			if(obj_t == NULL)
			{
				if(index_to > index_from)
				{
					obj_t = prev_obj;
				}
				else
				{
					SDK_ASSERT(index_to == 0);
					oidx = (u16)ei->rndr_znlist[zone][renderlist_index];
					ei->rndr_znlist[zone][renderlist_index] = (ei->rndr_znlist[zone][renderlist_index] & 0xffff0000) | c_idx;
					*obj_f = (oidx << 16) | ((*obj_f) & 0xffff);
					return TRUE;
				}
			}
			oidx = (*obj_t) >> 16;
			*obj_t = (c_idx << 16) | ((*obj_t) & 0xffff);
			*obj_f = (oidx << 16) | ((*obj_f) & 0xffff);
			return TRUE;
		}
	}
	return FALSE;
}
//-------------------------------------------------------------------------------------------

u16 rlGetGameObjectsCount(u32 layer, u32 zone, u16 renderlist_index)
{
	struct TEngineInstance *ei = _getInstance(layer);
	u16 ct = 0;
	if(zone < (u32)ei->zones_ct && renderlist_index < rlGetCount(layer))
	{
		u16 oidx = (u16)ei->rndr_znlist[zone][renderlist_index];
		while(oidx != 0xffff)
		{
			++ct;
			oidx = ei->pr_znlist[zone][oidx] >> 16;
		}
	}
	return ct;
}
//-------------------------------------------------------------------------------------------
