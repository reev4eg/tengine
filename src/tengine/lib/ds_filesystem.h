#ifndef NITRO_FILESYSTEM_H
#define NITRO_FILESYSTEM_H

#ifdef NITRO_SDK

#include "platform.h"

#ifdef __cplusplus
extern "C" {
#endif

struct InitFileSystemData;

#define FILESYSTEMFN(name) nitroFS_##name

void nitroFS_InitFileSystem(struct InitFileSystemData* data);
void nitroFS_ReleaseFileSystem(void);
BOOL nitroFS_IsFileSystemInit(void);

s8 nitroFS_fopen(const char* filename);
s32 nitroFS_fsize(u8 id);
BOOL nitroFS_fseek(u32 off, u8 id);
s32 nitroFS_fread(unsigned char* buf, u32 size, u8 id);
void nitroFS_fclose(s8 id);

void nitroFS_lostDevice(void);

#ifdef __cplusplus
}
#endif
#endif
#endif // NITRO_FILESYSTEM_H
