#ifndef HASH_H
#define HASH_H

#include "platform.h"
#include "fxmath.h"

#ifdef __cplusplus
extern "C" {
#endif

//---------------------------------------------------------------------------

u16 crc16_init(void);

u16 crc16_update(u16 crc, const void *data, size_t size);

u16 crc16_finalize(u16 crc);

//---------------------------------------------------------------------------

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
